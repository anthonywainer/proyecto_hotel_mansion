<h1>View Tipos Concepto Pago</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('tipos-concepto-pagos/edit/' . $tipos_concepto_pago->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Tipos_concepto_pago">
		Edit	</a>
	<a href="<?php echo site_url('tipos-concepto-pagos/delete/' . $tipos_concepto_pago->getId()) ?>"
		class="button" data-icon="trash" title="Delete Tipos_concepto_pago"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('conceptos-de-pagos?tipo_de_pago_id=' . $tipos_concepto_pago->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Conceptos De Pagos Tipos_concepto_pago">
		Conceptos De Pagos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($tipos_concepto_pago->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($tipos_concepto_pago->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($tipos_concepto_pago->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($tipos_concepto_pago->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>