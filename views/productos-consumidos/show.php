<h1>View Productos Consumidos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('productos-consumidos/edit/' . $productos_consumidos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Productos_consumidos">
		Edit	</a>
	<a href="<?php echo site_url('productos-consumidos/delete/' . $productos_consumidos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Productos_consumidos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Producto Id</span>
		<?php echo h($productos_consumidos->getProductoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Habitacion Id</span>
		<?php echo h($productos_consumidos->getHabitacionId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cantidad</span>
		<?php echo h($productos_consumidos->getCantidad()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Precio</span>
		<?php echo h($productos_consumidos->getPrecio()) ?>
	</div>
</div>