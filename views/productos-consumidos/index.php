<h1>
	<a href="<?php echo site_url('productos-consumidos/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Productos consumidos">
		New Productos consumidos
	</a>
	Productos Consumidos
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('productos-consumidos/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>