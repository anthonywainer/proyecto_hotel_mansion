<h1><?php echo $productos_consumidos->isNew() ? "New" : "Edit" ?> Productos Consumidos</h1>
<form method="post" action="<?php echo site_url('productos-consumidos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($productos_consumidos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_consumidos_producto_id">Producto Id</label>
			<input id="productos_consumidos_producto_id" type="text" name="producto_id" value="<?php echo h($productos_consumidos->getProductoId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_consumidos_habitacion_id">Habitacion Id</label>
			<input id="productos_consumidos_habitacion_id" type="text" name="habitacion_id" value="<?php echo h($productos_consumidos->getHabitacionId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_consumidos_cantidad">Cantidad</label>
			<input id="productos_consumidos_cantidad" type="text" name="cantidad" value="<?php echo h($productos_consumidos->getCantidad()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_consumidos_precio">Precio</label>
			<input id="productos_consumidos_precio" type="text" name="precio" value="<?php echo h($productos_consumidos->getPrecio()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $productos_consumidos->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>