<div id="usa">
<h6>
    <div align="center" >
    <?php echo $permisos->isNew() ? "NUEVO" : "EDITAR" ?> PERMISO</h6>
<form method="post" action="<?php echo site_url('permisos/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($permisos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_idaccion">Acciones</label>
			<select required class="form-control" id="permisos_idaccion" name="idaccion">

			<?php foreach (Acciones::getAll() as $acciones): ?>
				<option <?php if ($permisos->getIdaccion() === $acciones->id) echo 'selected="selected"' ?> value="<?php echo $acciones->id ?>"><?php echo $acciones->accion?></option>
			<?php endforeach ?>
			</select>
		</div>
        <div class="form-field-wrapper">
            <label class="form-field-label" >Módulo</label>
            <select required class="form-control"  name="idmodulo">
                <?php foreach (Modulos::getAll() as $modulo): ?>
                    <option <?php $b = true; if ($permisos->getIdmodulo() == $modulo->id){ echo 'selected="selected"'; $b= false;}
                    ?> value="<?php echo $modulo->id ?>"><?php echo $modulo->nombre?></option>
                <?php endforeach ?>
                <option value=""  <?php if($b) echo 'selected'?> >nulo</option>

            </select>

        </div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_idsubmodulo">Submódulo</label>
			<select required class="form-control" id="permisos_idsubmodulo" name="idsubmodulo">
			<?php $sb = true; foreach (Submodulo::getAll() as $submodulo): ?>
				<option <?php  if ($permisos->getIdsubmodulo() == $submodulo->id){ echo 'selected="selected"'; $sb= false; }
				?> value="<?php echo $submodulo->id ?>"><?php echo $submodulo->nombre?></option>
			<?php endforeach ?>
                <option value=""  <?php if($sb){echo 'selected';}?> >nulo</option>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_nombre">Nombre</label>
			<input required class="form-control" id="permisos_nombre" type="text" name="nombre" value="<?php echo h($permisos->getNombre()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white"  class="btn btn-primary" type="submit"  value="<?php echo $permisos->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>


	</div>
</form>