<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid modulos-grid table table-condensed table-bordered" cellspacing="0" id="table-responsive">
	<thead>
		<tr>
            <th class="ui-widget-header ">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					URL
			</th>
			<th class="ui-widget-header ">
					ICONO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($modulos as $key => $modulos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($modulos->getId()) ?>&nbsp;</td>
			<td><?php echo h($modulos->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($modulos->getUrl()) ?>&nbsp;</td>
			<td><?php echo h($modulos->getIcon()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('modulos/editar/' . $modulos->getId()) ?>">
                    <i  class="fa fa-edit fa-lg"></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar módulos','¿Está seguro de Eliminar?','<?php echo site_url('modulos/eliminar/' . $modulos->getId()) ?>')">
                    <i  class="fa fa-trash-o fa-lg" ></i>
                </a>
				<a class="btn btn-outline-success" title="ver modulos"href="<?php echo site_url('submodulos?idmodulo=' . $modulos->getId()) ?>">
                    <i class="fa fa-eye fa-lg" ></i>
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>