
<div class="container-fluid">
<h3 align="center">
    LISTA DE TELEVISORES
</h3>
<br>
<div class="row">
    <div class="form-group col-md-12">
        <div class="input-group">
            <i href="<?php echo site_url('televisores/editar') ?>"onclick="openmodal(this)"
               class="button"
               data-icon="plusthick"
               title="Nuevo Televisor">
                <button class="btn btn-primary" style="background: #3C8DBC; color: white">
                    Registrar
                </button>
            </i>
            <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                   href="<?= site_url('televisores/index?search=') ?>"
                   value="<?= $_GET['search'] ?>"
                   class="form-control" placeholder="Buscar televisor" style="margin-left: 40%"/>
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
    </div>
</div>
<div id="grilla">
<div class="ui-widget-content ui-corner-all">
	<?php View::load('televisores/grid', $params) ?>
</div>
</div>
<?php View::load('pager', compact('pager')) ?>
    <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <!--  <div class="modal-footer">

                 </div>-->
             </div>
             <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>