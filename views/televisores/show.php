<h1>View Televisores</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('televisores/edit/' . $televisores->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Televisores">
		Edit	</a>
	<a href="<?php echo site_url('televisores/delete/' . $televisores->getId()) ?>"
		class="button" data-icon="trash" title="Delete Televisores"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($televisores->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado</span>
		<?php echo h($televisores->getEstado()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($televisores->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($televisores->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($televisores->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>