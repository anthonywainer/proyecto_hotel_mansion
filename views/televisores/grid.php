<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid televisores-grid table table-condensed table-bordered table-striped" cellspacing="0" id="table-responsive">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Televisores::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Televisores::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Televisores::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Televisores::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descripcion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Televisores::ESTADO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Televisores::ESTADO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Estado
				</a>
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($televisores as $key => $televisores): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo  $key+1;//echo h($televisores->getId()) ?>&nbsp;</td>
			<td><?php echo h($televisores->getDescripcion()) ?>&nbsp;</td>
			<td><?php echo h($televisores->getEstado()) ?>&nbsp;</td>

			<td>

                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('televisores/editar/' . $televisores->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Televisores','¿Está seguro de Eliminar?', '<?php echo site_url('televisores/eliminar/' . $televisores->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>