<div id="usa">
<h3 align="center"><?php echo $televisores->isNew() ? "Registrar" : "Editar" ?> Televisores</h3>
    <br>
<form method="post" action="<?php echo site_url('televisores/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($televisores->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="televisores_descripcion">Descripcion</label>
			<input class="form-control" required id="televisores_descripcion" type="text" name="descripcion" value="<?php echo h($televisores->getDescripcion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="televisores_estado">Estado</label>
			<input class="form-control" required id="televisores_estado" type="text" name="estado" value="<?php echo h($televisores->getEstado()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $televisores->isNew() ? "Guargar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
    </div>