<h1>View Producto Consumido Hotel</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('producto-consumido-hotels/edit/' . $producto_consumido_hotel->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Producto_consumido_hotel">
		Edit	</a>
	<a href="<?php echo site_url('producto-consumido-hotels/delete/' . $producto_consumido_hotel->getId()) ?>"
		class="button" data-icon="trash" title="Delete Producto_consumido_hotel"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Producto</span>
		<?php echo h($producto_consumido_hotel->getProductosRelatedByProductoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Hotel</span>
		<?php echo h($producto_consumido_hotel->getVentasHotelRelatedByHotel()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cantidad</span>
		<?php echo h($producto_consumido_hotel->getCantidad()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Monto</span>
		<?php echo h($producto_consumido_hotel->getMonto()) ?>
	</div>
</div>