<h1><?php echo $producto_consumido_hotel->isNew() ? "New" : "Edit" ?> Producto Consumido Hotel</h1>
<form method="post" action="<?php echo site_url('producto-consumido-hotels/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($producto_consumido_hotel->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_consumido_hotel_producto_id">Productos</label>
			<select id="producto_consumido_hotel_producto_id" name="producto_id">
			<?php foreach (Productos::doSelect() as $productos): ?>
				<option <?php if ($producto_consumido_hotel->getProductoId() === $productos->getId()) echo 'selected="selected"' ?> value="<?php echo $productos->getId() ?>"><?php echo $productos?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_consumido_hotel_hotel">Ventas Hotel</label>
			<select id="producto_consumido_hotel_hotel" name="hotel">
			<?php foreach (VentasHotel::doSelect() as $ventas_hotel): ?>
				<option <?php if ($producto_consumido_hotel->getHotel() === $ventas_hotel->getId()) echo 'selected="selected"' ?> value="<?php echo $ventas_hotel->getId() ?>"><?php echo $ventas_hotel?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_consumido_hotel_cantidad">Cantidad</label>
			<input id="producto_consumido_hotel_cantidad" type="text" name="cantidad" value="<?php echo h($producto_consumido_hotel->getCantidad()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="producto_consumido_hotel_monto">Monto</label>
			<input id="producto_consumido_hotel_monto" type="text" name="monto" value="<?php echo h($producto_consumido_hotel->getMonto()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $producto_consumido_hotel->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>