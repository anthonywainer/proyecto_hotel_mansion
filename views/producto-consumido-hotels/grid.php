<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid producto_consumido_hotel-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoConsumidoHotel::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoConsumidoHotel::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoConsumidoHotel::PRODUCTO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoConsumidoHotel::PRODUCTO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Producto
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoConsumidoHotel::HOTEL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoConsumidoHotel::HOTEL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Hotel
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoConsumidoHotel::CANTIDAD))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoConsumidoHotel::CANTIDAD): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cantidad
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ProductoConsumidoHotel::MONTO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ProductoConsumidoHotel::MONTO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Monto
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($producto_consumido_hotels as $key => $producto_consumido_hotel): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($producto_consumido_hotel->getId()) ?>&nbsp;</td>
			<td><?php echo h($producto_consumido_hotel->getProductosRelatedByProductoId()) ?>&nbsp;</td>
			<td><?php echo h($producto_consumido_hotel->getVentasHotelRelatedByHotel()) ?>&nbsp;</td>
			<td><?php echo h($producto_consumido_hotel->getCantidad()) ?>&nbsp;</td>
			<td><?php echo h($producto_consumido_hotel->getMonto()) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Producto_consumido_hotel"
					href="<?php echo site_url('producto-consumido-hotels/show/' . $producto_consumido_hotel->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Producto_consumido_hotel"
					href="<?php echo site_url('producto-consumido-hotels/edit/' . $producto_consumido_hotel->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Producto_consumido_hotel"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('producto-consumido-hotels/delete/' . $producto_consumido_hotel->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>