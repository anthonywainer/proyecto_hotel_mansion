<h1>
	<a href="<?php echo site_url('producto-consumido-hotels/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Producto consumido hotel">
		New Producto consumido hotel
	</a>
	Producto Consumido Hotels
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('producto-consumido-hotels/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>