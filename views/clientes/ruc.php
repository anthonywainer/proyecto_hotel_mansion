<div id="usa">
<h3 align="center"><?php echo $clientes->isNew() ? "Registrar" : "Editar" ?> Clientes</h3>
<form method="post" id="modal-form" onsubmit="registrarR(this); return false;" action="<?php echo site_url('clientes/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($clientes->getId()) ?>" />

        <div class="form-field-wrapper">
            <label id="nombrel" class="form-field-label" for="clientes_nombres">Razón Social</label>
            <input required class="form-control " id="nombre" type="text"
                   onkeypress="return soloLetras(event)"
                   name="nombres"
                   value="<?php echo h($clientes->getNombres()) ?>" />
        </div>

		<div class="form-field-wrapper">
			<label id="documentol" class="form-field-label" for="clientes_dni"></label><span id="id1">Ruc</span>
			<input required class="form-control" id="ruc" type="number"
                   onkeypress="return numeros(event)" name="ruc"
                   value="<?php echo h($clientes->getRuc()) ?>" /><span id="mensaje"></span>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_direccion">Direccion</label>
			<input class="form-control" id="clientes_direccion" type="text"
                   name="direccion" value="<?php echo h($clientes->getDireccion()) ?>" />
		</div>

	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $clientes->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
