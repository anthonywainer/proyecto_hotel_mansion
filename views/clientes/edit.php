
<div id="usa">
<h3 align="center"><?php echo $clientes->isNew() ? "Registrar" : "Editar" ?> Clientes</h3>
<form method="post" id="modal-form" onsubmit="registrar(this); return false;" action="<?php echo site_url('clientes/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($clientes->getId()) ?>" />
		<div class="form-field-wrapper">
			<label id="tipo" class="form-field-label" for="clientes_nombres">Tipo</label>
            <select class="form-control" id="tipo" name="clientes_tipo" onchange="tiposclientes(this)">
                <option value="1">Natural</option>
                <option value="2">Juridico</option>
            </select>
		</div>
        <div class="form-field-wrapper">
            <label id="nombrel" class="form-field-label" for="clientes_nombres">Nombres</label>
            <input required class="form-control " id="nombre" type="text"
                   onkeypress="return soloLetras(event)"
                   name="nombres"
                   value="<?php echo h($clientes->getNombres()) ?>" />
        </div>
		<div class="form-field-wrapper">
			<label id="apellidosl" class="form-field-label juridico" for="clientes_apellidos">Apellidos</label>
			<input  required class="form-control juridico" id="ape"
                    onkeypress="return soloLetras(event)" type="text"
                    name="apellidos"
                    value="<?php echo h($clientes->getApellidos()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label id="documentol" class="form-field-label" for="clientes_dni"></label><span id="id1">DNI</span>
			<input required class="form-control" id="dni" type="number"
                   onkeypress="return numeros(event)" name="dni"
                   value="<?php echo h($clientes->getDni()) ?>" /><span id="mensaje"></span>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_direccion">Direccion</label>
			<input class="form-control" id="clientes_direccion" type="text"
                   name="direccion" value="<?php echo h($clientes->getDireccion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label " for="clientes_telefono">Telefono</label>
			<input class="form-control" id="clientes_telefono" type="text" onkeypress="return numeros(event)" name="telefono" value="<?php echo h($clientes->getTelefono()) ?>" />
		</div><br>
		<div class="form-inline">
			<label id="procedencia" class="form-field-label" for="clientes_procedencia">Procedencia</label>
			<input class="form-control" id="clientes_procedencia" type="text" name="procedencia" value="<?php echo h($clientes->getProcedencia()) ?>" />

            <label id="sexo" class="form-field-label juridico" for="clientes_sexo" style="margin-left: 5%;">Sexo</label>
            <select required class="form-control juridico" id="clientes_sexo" onkeypress="return soloLetras(event)" name="sexo" style="width: 32%">
                <option id="clientes_sexo" value="<?php echo h($clientes->getSexo()) ?>">Masculino</option>
                <option id="clientes_sexo" value="<?php echo h($clientes->getSexo()) ?>">Femenino</option>
            </select>
        </div>

	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $clientes->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
