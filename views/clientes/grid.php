<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid clientes-grid table table-condensed table-bordered" cellspacing="0" id="table1">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					#
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::NOMBRES))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::NOMBRES): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Nombres
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::APELLIDOS))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::APELLIDOS): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Apellidos
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::DNI))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::DNI): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Dni
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::DIRECCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::DIRECCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Direccion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::TELEFONO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::TELEFONO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Telefono
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::PROCEDENCIA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::PROCEDENCIA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Procedencia
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Clientes::SEXO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Clientes::SEXO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Sexo
				</a>
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($clientes as $key => $clientes): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php  echo $key+1;//echo h($clientes->getId()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getApellidos()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getDni()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getDireccion()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getTelefono()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getProcedencia()) ?>&nbsp;</td>
			<td><?php echo h($clientes->getSexo()) ?>&nbsp;</td>
			<!--<td><?php //echo h($clientes->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php //echo h($clientes->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php //echo h($clientes->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>-->
			<td>

            <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('clientes/editar/' . $clientes->getId()) ?>">
                <i class="fa fa-edit fa-lg" ></i>
            </i>
            <a class="btn btn-outline-danger" href="#"
               onclick="conf_eliminar('Eliminar Cliente','¿Está seguro de Eliminar?','<?php echo site_url('clientes/eliminar/' . $clientes->getId()) ?>')">
                <i class="fa fa-trash-o fa-lg" ></i>
            </a>
				<!--<a
					class="button"
					data-icon="carat-1-e"
					href="<?php //echo site_url('clientes-estadias?cliente_id=' . $clientes->getId()) ?>">
					Clientes Estadias
				</a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php //echo site_url('clientes-habitaciones?cliente_id=' . $clientes->getId()) ?>">
					Clientes Habitaciones
				</a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php //echo site_url('estadias?cliente_id=' . $clientes->getId()) ?>">
					Estadias
				</a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php //echo site_url('ventas?cliente_id=' . $clientes->getId()) ?>">
					Ventas
				</a>-->
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>