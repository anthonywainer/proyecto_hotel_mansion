<div class="modal fade" id="registrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrar Clientes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="modal-form" action="<?= site_url('clientes/guardar') ?>"
                  onsubmit="registrar(this); return false;"
                  method="post">
                <div class="modal-body" style="padding: 0 15px">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nombres</label>
                        <input type="text" class="form-control" name="nombres" id="nombre"
                               placeholder="nombres" onkeypress="return soloLetras(event)" required>
                    </div>
                </div>
                <div class="modal-body" style="padding: 0 15px">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Apellidos</label>
                        <input type="text" class="form-control" name="apellidos" id="ape"
                               placeholder="apellidos" onkeypress="return soloLetras(event)" required>
                    </div>
                </div>

                <div class="modal-body" style="padding: 0 15px">
                    <div class="form-group">
                        <label for="exampleInputPassword1">DNI</label>
                        <input type="number"  class="form-control" name="dni" id="dni"
                               placeholder="numerodocumento" onkeyup="validar_dni()" required><span id="mensaje"></span>
                    </div>
                </div>
                <div class="modal-body" style="padding: 0 15px">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Direccion</label>
                        <input type="text" class="form-control" name="direccion" id="exampleInputPassword1" placeholder="direccion" required>
                    </div>
                </div>
                <div class="modal-body" style="padding: 0 15px">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Telefono</label>
                        <input type="text" class="form-control" name="telefono" id="miInput" placeholder="telefono" onkeypress="return numeros(event)" required>
                    </div>
                </div>
                <div class="modal-body" ">
                    <div class="form-inline">
                        <label for="exampleInputPassword1">Procedencia</label>
                        <input type="text" class="form-control" name="procedencia" id="exampleInputPassword1" placeholder="procedencia" required>
                        <label style="margin-left: 2%" for="exampleInputPassword1">Sexo</label>
                        <select class="form-control" name="sexo" id="exampleInputPassword1" required>
                            <option value="m">Masculino</option>
                            <option value="f">Femenino</option>
                        </select>
                        <!--<input type="text" class="form-control" name="sexo" id="exampleInputPassword1" placeholder="sexo" required>-->
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btnEnviar" >Guardar</button>
                    <button type="button" class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>