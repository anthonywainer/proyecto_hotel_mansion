<h1>View Hoteles</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('hoteles/edit/' . $hoteles->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Hoteles">
		Edit	</a>
	<a href="<?php echo site_url('hoteles/delete/' . $hoteles->getId()) ?>"
		class="button" data-icon="trash" title="Delete Hoteles"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($hoteles->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($hoteles->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Direccion</span>
		<?php echo h($hoteles->getDireccion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Ruc</span>
		<?php echo h($hoteles->getRuc()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Telefono</span>
		<?php echo h($hoteles->getTelefono()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Colores</span>
		<?php echo h($hoteles->getColores()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($hoteles->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($hoteles->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($hoteles->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>