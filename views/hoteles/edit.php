<div id="usa">
<h3><div align="center" >
<?php echo $hoteles->isNew() ? "Registrar" : "Editar" ?> Hotel</h3>
<form method="post" action="<?php echo site_url('hoteles/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($hoteles->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="hoteles_nombre">Nombre</label>
			<input class="form-control" onkeypress="return soloLetras(event)" id="hoteles_nombre" type="text" name="nombre" value="<?php echo h($hoteles->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="hoteles_descripcion">Descripcion</label>
            <textarea class="form-control"  id="hoteles_descripcion" type="text" name="descripcion" >
             <?php echo h($hoteles->getDescripcion()) ?>
                </textarea>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="hoteles_direccion">Direccion</label>
			<input class="form-control" onkeypress="return soloLetras(event)" id="hoteles_direccion" type="text" name="direccion" value="<?php echo h($hoteles->getDireccion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="hoteles_ruc">Ruc</label>
			<input class="form-control" id="hoteles_ruc" type="text" name="ruc" value="<?php echo h($hoteles->getRuc()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="hoteles_telefono">Telefono</label>
			<input class="form-control" id="hoteles_telefono" type="text" name="telefono" value="<?php echo h($hoteles->getTelefono()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="hoteles_colores">Colores</label>
			<input class="form-control" id="hoteles_colores" type="text" name="colores" value="<?php echo h($hoteles->getColores()) ?>" />
		</div>
    </div>
    <br>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $hoteles->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>