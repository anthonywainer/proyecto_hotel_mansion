<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid permisos-grid table table-condensed table-bordered table table-striped" cellspacing="0" id="table-responsive">
	<thead>
        <tr>
            <th class="ui-widget-header ui-corner-tl">
                #
            </th>

            <th class="ui-widget-header ">
                Nombre
            </th>

            <th class="ui-widget-header ">
                Descripción
            </th>

            <th class="ui-widget-header ">
                Dirección
            </th>

            <th class="ui-widget-header ">
                RUC
            </th>

            <th class="ui-widget-header ">
                Teléfono
            </th>

            <th class="ui-widget-header ">
                Colores
            </th>

            <th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
        </tr>
	</thead>
	<tbody>
<?php foreach ($hoteles as $key => $hoteles): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo $key+1;//echo h($hoteles->getId())?>&nbsp;</td>
			<td><?php echo h($hoteles->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($hoteles->getDescripcion()) ?>&nbsp;</td>
			<td><?php echo h($hoteles->getDireccion()) ?>&nbsp;</td>
			<td><?php echo h($hoteles->getRuc()) ?>&nbsp;</td>
			<td><?php echo h($hoteles->getTelefono()) ?>&nbsp;</td>
			<td><?php echo h($hoteles->getColores()) ?>&nbsp;</td>
			<!--<td><?php //echo h($hoteles->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php //echo h($hoteles->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php //echo h($hoteles->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>-->
			<td>
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('hoteles/editar/' . $hoteles->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Hoteles','¿Está seguro de Eliminar?', '<?php echo site_url('hoteles/eliminar/' . $hoteles->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>
                </a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>