
<div class="container-fluid">
<h1>
	<a href="<?php echo site_url('clientes-estadias/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Clientes estadias">

	</a>
	Clientes Estadias
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('clientes-estadias/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>
</div>
