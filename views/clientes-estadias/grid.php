<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid clientes_estadias-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::CLIENTE_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::CLIENTE_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cliente
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::HABITACION_ESTADIA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::HABITACION_ESTADIA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Habitacion Estadia
				</a>
			</th>
			<!--<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::CREATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::CREATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Created At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::UPDATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::UPDATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Updated At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::DELETED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::DELETED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Deleted At
				</a>
			</th>-->
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => ClientesEstadias::ESTADO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == ClientesEstadias::ESTADO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Estado
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($clientes_estadias as $key => $clientes_estadias): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($clientes_estadias->getId()) ?>&nbsp;</td>
			<td><?php echo h($clientes_estadias->getClientesRelatedByClienteId()) ?>&nbsp;</td>
			<td><?php echo h($clientes_estadias->getEstadiasHabitacionesRelatedByHabitacionEstadiaId()) ?>&nbsp;</td>
			<td><?php echo h($clientes_estadias->getEstado()) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Clientes_estadias"
					href="<?php echo site_url('clientes-estadias/show/' . $clientes_estadias->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Clientes_estadias"
					href="<?php echo site_url('clientes-estadias/edit/' . $clientes_estadias->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Clientes_estadias"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('clientes-estadias/delete/' . $clientes_estadias->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>