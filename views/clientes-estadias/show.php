<h1>View Clientes Estadias</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('clientes-estadias/edit/' . $clientes_estadias->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Clientes_estadias">
		Edit	</a>
	<a href="<?php echo site_url('clientes-estadias/delete/' . $clientes_estadias->getId()) ?>"
		class="button" data-icon="trash" title="Delete Clientes_estadias"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Cliente</span>
		<?php echo h($clientes_estadias->getClientesRelatedByClienteId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Habitacion Estadia</span>
		<?php echo h($clientes_estadias->getEstadiasHabitacionesRelatedByHabitacionEstadiaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($clientes_estadias->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($clientes_estadias->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($clientes_estadias->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado</span>
		<?php echo h($clientes_estadias->getEstado()) ?>
	</div>
</div>