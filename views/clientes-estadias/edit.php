
<div id="usa">
<h3><?php echo $clientes_estadias->isNew() ? "Registrar" : "Editar" ?> Clientes Estadias</h3>
<form method="post" action="<?php echo site_url('clientes-estadias/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($clientes_estadias->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_estadias_cliente_id">Clientes</label>
			<select id="clientes_estadias_cliente_id" name="cliente_id">
			<?php foreach (Clientes::doSelect() as $clientes): ?>
				<option <?php if ($clientes_estadias->getClienteId() === $clientes->getId()) echo 'selected="selected"' ?> value="<?php echo $clientes->getId() ?>"><?php echo $clientes?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_estadias_habitacion_estadia_id">Estadias Habitaciones</label>
			<select id="clientes_estadias_habitacion_estadia_id" name="habitacion_estadia_id">
			<?php foreach (EstadiasHabitaciones::doSelect() as $estadias_habitaciones): ?>
				<option <?php if ($clientes_estadias->getHabitacionEstadiaId() === $estadias_habitaciones->getId()) echo 'selected="selected"' ?> value="<?php echo $estadias_habitaciones->getId() ?>"><?php echo $estadias_habitaciones?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_estadias_created_at">Created At</label>
			<input id="clientes_estadias_created_at" type="text" name="created_at" value="<?php echo h($clientes_estadias->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_estadias_updated_at">Updated At</label>
			<input id="clientes_estadias_updated_at" type="text" name="updated_at" value="<?php echo h($clientes_estadias->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_estadias_deleted_at">Deleted At</label>
			<input id="clientes_estadias_deleted_at" type="text" name="deleted_at" value="<?php echo h($clientes_estadias->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="clientes_estadias_estado">Estado</label>
			<input id="clientes_estadias_estado" type="text" name="estado" value="<?php echo h($clientes_estadias->getEstado()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $clientes_estadias->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>
</div>