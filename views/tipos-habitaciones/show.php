<h1>View Tipos Habitaciones</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('tipos-habitaciones/edit/' . $tipos_habitaciones->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Tipos_habitaciones">
		Edit	</a>
	<a href="<?php echo site_url('tipos-habitaciones/delete/' . $tipos_habitaciones->getId()) ?>"
		class="button" data-icon="trash" title="Delete Tipos_habitaciones"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($tipos_habitaciones->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Precio</span>
		<?php echo h($tipos_habitaciones->getPrecio()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($tipos_habitaciones->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($tipos_habitaciones->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($tipos_habitaciones->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>