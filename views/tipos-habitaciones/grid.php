<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid tipos_habitaciones-grid table table-condensed table-bordered" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TiposHabitaciones::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TiposHabitaciones::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TiposHabitaciones::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TiposHabitaciones::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descripcion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TiposHabitaciones::PRECIO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TiposHabitaciones::PRECIO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Precio
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr"> Acciones</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($tipos_habitaciones as $key => $tipos_habitaciones): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($tipos_habitaciones->getId()) ?>&nbsp;</td>
			<td><?php echo h($tipos_habitaciones->getDescripcion()) ?>&nbsp;</td>
			<td><?php echo h($tipos_habitaciones->getPrecio()) ?>&nbsp;</td>
			<td>
                <i  class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('tipos-habitaciones/editar/' . $tipos_habitaciones->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger"  href="#"
                   onclick="conf_eliminar('Eliminar Tipo Habitaciones','¿Está seguro de Eliminar?', '<?php echo site_url('tipos-habitaciones/eliminar/' . $tipos_habitaciones->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>