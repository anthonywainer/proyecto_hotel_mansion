<div id="usa">
<h3 align="center"><?php echo $tipos_habitaciones->isNew() ? "Registrar" : "Editar" ?> Tipos Habitaciones</h3>
<form method="post" action="<?php echo site_url('tipos-habitaciones/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($tipos_habitaciones->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipos_habitaciones_descripcion">Descripcion</label>
			<input class="form-control"  id="tipos_habitaciones_descripcion" type="text" name="descripcion" value="<?php echo h($tipos_habitaciones->getDescripcion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipos_habitaciones_precio">Precio</label>
			<input class="form-control" id="tipos_habitaciones_precio" type="number" name="precio" value="<?php echo h($tipos_habitaciones->getPrecio()) ?>" />
		</div>

	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $tipos_habitaciones->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>