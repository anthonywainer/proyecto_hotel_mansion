<style>
    .circle-user{
        width: 90%;
        border-radius: 60%;

    }
    .card{
     min-height:40em ;

    }
    .sidebar-fixed .sidebar {

        height: calc(100vh - 20px);
    }
    /*.sidebar-fixed .sidebar {
        position: fixed;
        z-index: 1019;
        width: 200px;
        height: calc(100vh - 20px);
    }
    @media (max-width: 991px)

        .sidebar .sidebar-nav, .sidebar .nav {
            width: 200px;
            min-height: calc(100vh - 20px);
        }*/

        /*.sidebar .nav {
            width: 300px;
</style>

<header class="app-header navbar" style="background: #3C8DBC; margin-top: -60px">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button" style="margin-top: 4px;border: none; margin-left: 2%">
        <span class=" fa fa-bars" style="color: white; margin-top: 1%; font-size: 2em;margin-left:-1%"></span>
    </button>
    <h3 style="color: white; font-weight: 900; margin-left: 5px">LA MANSIÓN</h3>
    <button class="navbar-toggler sidebar-toggler d-md-down-none h-100 b-r-1" type="button" style="margin-left:2%; margin-top: -0.1%;border: none" >
        <span class="fa fa-bars" style="color: white; margin-top: -0.8%; margin-left:-1%;font-size: 2em"></span>
    </button>

    <ul class="nav navbar-nav ml-auto" >


        <li class="nav-item dropdown d-md-down-none">
            <!--<a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div class="dropdown-header text-center">
                    <strong>You have 5 notifications</strong>
                </div>
                <a href="#" class="dropdown-item">
                    <i class="icon-user-follow text-success"></i> New user registered
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-user-unfollow text-danger"></i> User deleted
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-chart text-info"></i> Sales report is ready
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-basket-loaded text-primary"></i> New client
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-speedometer text-warning"></i> Server overloaded
                </a>
                <div class="dropdown-header text-center">
                    <strong>Server</strong>
                </div>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-1">
                        <small><b>CPU Usage</b></small>
                    </div>
                    <span class="progress progress-xs">
              <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </span>
                    <small class="text-muted">348 Processes. 1/4 Cores.</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-1">
                        <small><b>Memory Usage</b></small>
                    </div>
                    <span class="progress progress-xs">
              <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
            </span>
                    <small class="text-muted">11444GB/16384MB</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-1">
                        <small><b>SSD 1 Usage</b></small>
                    </div>
                    <span class="progress progress-xs">
              <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </span>
                    <small class="text-muted">243GB/256GB</small>
                </a>
            </div>-->
        </li>
        <li class="nav-item dropdown d-md-down-none">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-envelope-letter" style="color: white; font-size: 16px"></i><span class="badge badge-pill badge-info" style="color: white">
                    <?php $c=Contactenos::getAll('where estado="no_leido"'); echo count($c) ?>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div class="dropdown-header text-center">
                    <strong>Usted Tiene <?= count($c) ?></strong>
                </div>
                <a href="#" class="dropdown-item">
                    <?php foreach ($c as $i ):?>
                        <div class="message">
                            <?= $i->getEmail().' '.$i->getAsunto() ?>
                        </div>
                    <?php endforeach; ?>
                </a>
                <a href="<?= site_url('contactenos/index')?>" class="dropdown-item text-center">
                    <strong>Ver todos los mensajes</strong>
                </a>
            </div>
        </li>
     <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-settings" style="color: white; font-size: 16px"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="<?= site_url('logout') ?>"><i class="fa fa-lock"></i> Cerrar Sesión</a>
                <a class="dropdown-item" href="<?= site_url('/') ?>"><i class="fa fa-www"></i> Web</a>
            </div>
        </li>


    </ul>
</header>

<div class="app-body">
    <div class="sidebar" style="margin-top: -35px;  ">
        <div class="sidebar-header">
           <!-- <li class="divider" style="list-style: none"></li>-->
            <li class="nav-item" align="center" style="list-style: none">
                <img style="width: 60%" src="<?php echo site_url('imagenes/user.png') ?>" class="img-avatar circle-user">
            </li>
            <li class="nav-title text-center" style="list-style: none; margin-left: -12%">
                <span><?php echo $_SESSION['user']->correo ?></span>
            </li>
        </div>
        <nav class="sidebar-nav">
            <li class="nav-item" style="list-style: none">
                <a class="nav-link" href="<?php echo site_url('admin')?>"><i class="fa fa-power-off fa-lg"></i> INICIO </a>
            </li>
                <ul class="nav">

                <!--<li class="divider"></li>-->
                 <?php  foreach ($modulosPrincipal as $m): ?>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#" >
                            <i  class="<?php echo $m["icon"] ?>"></i> <?php echo $m["nombre"] ?>
                        </a>
                        <ul class="nav-dropdown-items">
                            <?php foreach ($SubmodulosPrincipal as $sm){ if($sm['idmodulo']==$m["id"]){?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url($sm["url"]) ?>">
                                    <i class="<?php echo $sm["icon"] ?>"></i> <?php echo $sm["nombre"] ?>
                                </a>
                            </li>
                            <?php }} ?>
                        </ul>
                    </li>
                    <?php endforeach; ?>
                </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <!-- Main content -->
    <main class="main" id="main" >

        <div class="container-fluid" style="padding:1%;" >
            <div class="animated fadeIn mostrar">
                <div class="card" style="margin-left: 10px; margin-top: 0px">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <?php echo $params['u'] ?>
                        </li>
                    </ol>

                    <div class="card-block" style="padding: 3px" >
                        <div class="row">
                            <div class="col-sm-12">
                                <?php View::load($params['u'], $params) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

</div>
