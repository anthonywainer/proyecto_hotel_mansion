<div id="usa">
<h6 align="center"><?php echo $usuarios->isNew() ? "NUEVO" : "EDITAR" ?> USUARIO</h6>
<form method="post"  action="<?php echo site_url('usuarios/guardar') ?>" >
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($usuarios->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_nombres">Nombres</label>
			<input required class="form-control" id="usuarios_nombres" type="text" name="nombres" onkeypress="return soloLetras(event)" value="<?php echo h($usuarios->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_apellidos">Apellidos</label>
			<input required class="form-control" id="usuarios_apellidos" type="text" name="apellidos" onkeypress="return soloLetras(event)" value="<?php echo h($usuarios->getApellidos()) ?>" />
		</div>


        <div class="form-field-wrapper">
            <label class="form-field-label" for="grupos">Grupo</label>
            <select name="grupos" id="grupo" class="form-control">
                <?php foreach ($grupos as $g):  ?>
                    <option <?php if (isset($idgrupo )){ if ($idgrupo[0]->grupo_id == $g->id){echo "selected"; } }  ?> value="<?php echo $g->id ?>"><?php echo $g->descripcion ?></option>
                <?php endforeach ?>
            </select>
        </div>

		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_correo">Correo</label>
			<input type="email" class="form-control" id="usuarios_correo"  name="correo" value="<?php echo h($usuarios->getCorreo()) ?>" required/>
		</div>
        <div class="form-field-wrapper">
            <label class="form-field-label" for="usuarios_clave">Contraseña</label>
            <input required class="form-control" id="usuarios_clave" type="password" onkeyup="validar()" name="clave" placeholder="Contraseña" value="<?php echo h($usuarios->getClave()) ?>" />
        </div>
        <div class="form-field-wrapper">
            <label class="form-field-label" for="usuarios_clave">Confirmar Contraseña</label>
            <input required class="form-control" id="usuarios_claves" onkeyup="validarusuario()" type="password" name="clave" placeholder="Confirmar Contraseña" value="<?php echo h($usuarios->getClave()) ?>" />
            <p id="mensaje"></p>
        </div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_telefono">Teléfono</label>
			<input required class="form-control" id="usuarios_telefono" type="number" name="telefono" value="<?php echo h($usuarios->getTelefono()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_direccion">Dirección</label>
			<input required class="form-control" id="usuarios_direccion" type="text" name="direccion" value="<?php echo h($usuarios->getDireccion()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <span data-icon="disk" >
			<input style="background: #3C8DBC; color: white" id ="btnEnviar" class="btn btn-primary"  type="submit" value="<?php echo $usuarios->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-danger" data-icon="cancel" data-dismiss="modal" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>

    </div>

</form>

    <script>
        $(function(){
            $('#user').keyup(function(){
                var _this = $('#usuarios_correo');
                var _user = $('#usuarios_correo').val();
                _this.attr('style', 'background:white');
                if(_user.indexOf(' ') >= 0){
                    _this.attr('style', 'background:#FF4A4A');
                }

                if(_user.indexOf("'") >= 0){
                    _this.attr('style', 'background:#FF4A4A');
                }

                if(_user.val() == ''){
                    _this.attr('style', 'background:#FF4A4A');
                }
            });
        });

        $('#usuarios_clave').keyup(function(){
            var _this = $('#usuarios_clave');
            var pass_1 = $('#usuarios_clave').val();
            _this.attr('style', 'background:white');
            if(pass_1.charAt(0) == ' '){
                _this.attr('style', 'background:#FF4A4A');
            }

            if(_this.val() == ''){
                _this.attr('style', 'background:#FF4A4A');
            }
        });

        $('#usuarios_claves').keyup(function(){
            var pass_1 = $('#usuarios_clave').val();
            var pass_2 = $('#usuarios_claves').val();
            var _this = $('#usuarios_claves');
            _this.attr('style', 'background:white');
            if(pass_1 != pass_2 && pass_2 != ''){
                _this.attr('style', 'background:red');


            }
        });



    </script>