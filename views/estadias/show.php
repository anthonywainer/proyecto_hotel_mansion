<h1>View Estadias</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('estadias/edit/' . $estadias->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Estadias">
		Edit	</a>
	<a href="<?php echo site_url('estadias/delete/' . $estadias->getId()) ?>"
		class="button" data-icon="trash" title="Delete Estadias"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('cronogramas-pagos?estadia_id=' . $estadias->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Cronogramas Pagos Estadias">
		Cronogramas Pagos	</a>
	<a href="<?php echo site_url('estadias-habitaciones?estadia_id=' . $estadias->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Estadias Habitaciones Estadias">
		Estadias Habitaciones	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Fecha Reserva</span>
		<?php echo h($estadias->getFechaReserva(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Ingreso</span>
		<?php echo h($estadias->getFechaIngreso(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Salida</span>
		<?php echo h($estadias->getFechaSalida(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado</span>
		<?php echo h($estadias->getEstado()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cliente</span>
		<?php echo h($estadias->getClientesRelatedByClienteId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($estadias->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($estadias->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($estadias->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Usuario Id</span>
		<?php echo h($estadias->getUsuarioId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Precio</span>
		<?php echo h($estadias->getPrecio()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descuento</span>
		<?php echo h($estadias->getDescuento()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Observacion</span>
		<?php echo h($estadias->getObservacion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Monto Pagar</span>
		<?php echo h($estadias->getMontoPagar()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Cuota</span>
		<?php echo h($estadias->getCuota()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Resto</span>
		<?php echo h($estadias->getResto()) ?>
	</div>
</div>