<link rel="stylesheet" href="<?php echo site_url('css/Sestadia.css', true) ?>">
<?php
$sqlc= "
        SELECT
        clientes.nombres,
        clientes.apellidos,
        clientes.dni,
        clientes.id as idu,
        clientes_estadias.id
        FROM
        clientes_estadias
        INNER JOIN clientes ON clientes_estadias.cliente_id = clientes.id
        WHERE
        clientes_estadias.habitacion_estadia_id = 
        ".$ideh;
$clientes = Clientes::getConnection()->query($sqlc)->fetchAll();
if ($clientes):
    ?>

    <table class="table table-bordered table-hover table-sm" >
        <thead id="thea">
        <tr>
            <th id="nuevo">#</th>
            <th class="tc">Nombres</th>
            <th class="tc">DNI</th>
            <th class="tc">Acciones</th>
        </tr>
        </thead>
        <tbody id="list_ocu" class="tbod">
        <?php $i = 1;
        foreach ($clientes as $c): ?>
            <tr>
                <td id="nuevo">
                    <?php echo $i; $i++; ?>
                </td>
                <td class="tc">
                    <?=$c['nombres'].' '.$c['apellidos'] ?>
                </td>
                <td class="tc">
                    <?= $c['dni'] ?>
                </td>
                <td class="tc">
                    <a class="btn btn-outline-danger" href="#"
                       onclick="conf_eliminar('Eliminar Cliente','¿Está seguro de Eliminar?','<?php echo site_url('clientes-estadias/eliminar/' . $c['id']."/?ideh=".$ideh) ?>')">
                        <i class="fa fa-trash-o fa-lg" ></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>