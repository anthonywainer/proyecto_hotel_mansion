<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid modulos-grid table table-condensed table-bordered table table-striped" id="table1" >
    <thead>
    <tr>
        <th class="ui-widget-header ">
            ID
        </th>
        <th class="ui-widget-header ">
         Cliente Responsable
        </th>
        <th class="ui-widget-header ">
            Habitación
        </th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">Estado</th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">Fecha Reserva</th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">Fecha Ingreso</th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">Fecha Salida</th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
    </tr>
    </thead>
    <tbody>
<?php foreach ($estadias as $key => $estadias): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo $key+1?>&nbsp;</td>
            <!--<td><?php echo h($estadias->getClientesRelatedByClienteId()) ?>&nbsp;</td>-->
            <td>

                <?php if( $estadias->cliente_id) echo $estadias->getCliente($estadias->getClienteId())->nombres.' '.$estadias->getCliente($estadias->getClienteId())->apellidos?>&nbsp;</td>
            <td>
                <?php foreach (EstadiasHabitaciones::getAll('where estadia_id='.$estadias->id) as $eh): ?>
                    <li style="list-style: circle">
                        <?= "Habitación: ".$eh->getHabitaciones()->numero." - ".$eh->getHabitaciones()->getTipoHabitacion()->descripcion ?>

                    </li>
                <?php endforeach; ?>
            </td>
            <td>
                <?php foreach (EstadiasHabitaciones::getAll('where estadia_id='.$estadias->id) as $eh): ?>
                    <li style="list-style: none">
                        <?php if ($eh->getHabitaciones()->getEstado()=='disponible'){
                            echo "Finalizado";
                        }elseif($eh->getHabitaciones()->getEstado()=='ocupado'){
                            echo "Ocupado";
                        }
                        elseif ($eh->getHabitaciones()->getEstado()=='reservado'){
                            echo "Reserva";
                        }
                        ?>

                    </li>
                <?php endforeach; ?>
            </td>
            <td>
                <?php foreach (EstadiasHabitaciones::getAll('where estadia_id='.$estadias->id) as $eh): ?>
                    <li style="list-style: none">
                        <?= $eh->getEstadias()->fecha_reserva ?>

                    </li>
                <?php endforeach; ?>
            </td>
            <td>
                <?php foreach (EstadiasHabitaciones::getAll('where estadia_id='.$estadias->id) as $eh): ?>
                    <li style="list-style: none">
                        <?= $eh->getEstadias()->fecha_ingreso ?>

                    </li>
                <?php endforeach; ?>
            </td>
            <td>
                <?php foreach (EstadiasHabitaciones::getAll('where estadia_id='.$estadias->id) as $eh): ?>
                    <li style="list-style: none">
                        <?= $eh->getEstadias()->fecha_salida ?>

                    </li>
                <?php endforeach; ?>
            </td>

            <!--<td><?php //echo h($estadias->getEstado()) ?>&nbsp;</td>-->
             <!--<td><?php echo h($estadias->getUsuarioId()) ?>&nbsp;</td>-->
			<td>
                <a class="btn btn-outline-secondary" href="<?php echo site_url('estadias-habitaciones/?id='. $estadias->id) ?>">
                    <i class="fa fa-plus-circle"></i>
                </a>

                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Estadias','¿Está seguro de Eliminar?', '<?php echo site_url('estadias/eliminar/' . $estadias->getId()) ?>')">
                    <i class="fa fa-trash-o "></i>
                </a>

                <a class="btn btn-outline-success" href="<?= site_url('movimientos-de-dineros/index/'.$estadias->getId()) ?>">
                    <i class="fa fa-usd" aria-hidden="true"></i></a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>