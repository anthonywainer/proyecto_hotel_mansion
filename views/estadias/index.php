<div class="container-fluid">
 <div class="row">
     <div class="col-md-12">
         <h4 align="center">
             LISTA DE ESTADIAS
         </h4>
     </div>

 </div>


<div class="row">
    <div class="form-group col-md-12">

        <div class="input-group">

	<a href="<?php echo site_url('estadias-habitaciones/?id=') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Estadias">
        <button class="btn btn-primary" style="color: white;margin-right:4px">
           Registrar
        </button>
       <button type="button" class="btn btn-outline-info" ><a style="color: black" href="<?= site_url('habitaciones/?ide=')?>">Habitaciones</a></button>
	</a>


            <input id="filtrar" type="search" onkeyup="buscar_tabla_html(this)" class="form-control form-inline" placeholder="Buscar Estadias" style="margin-left: 40%">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
    </div>
</div>


<div class="ui-widget-content ui-corner-all">
	<?php View::load('estadias/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


