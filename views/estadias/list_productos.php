<link rel="stylesheet" href="<?php echo site_url('css/Sestadia.css', true) ?>">
<?php $sqlc= "
SELECT
productos.nombre,
productos.precio,
productos.id as idp,
producto_consumido.id,
producto_consumido.monto,
producto_consumido.cantidad
FROM
producto_consumido
INNER JOIN productos ON producto_consumido.producto_id = productos.id
WHERE
producto_consumido.habita_estadia_id = 
".$ideh;
$productos = ProductoConsumido::getConnection()->query($sqlc)->fetchAll();
if ($productos): ?>

    <table class="table table-bordered table-sm" >
        <thead>
        <tr>
            <th id="uno">cantidad</th>
            <th id="th">Producto</th>
            <th id="th">Precio</th>
            <!--<th id="th">Descuento</th>-->
            <th id="th">Total</th>
            <th id="th">Acciones</th>
        </tr>
        </thead>
        <tbody class="tbod">
        <?php

        $m = 0;
        foreach ($productos as $c): ?>
            <tr>
                <!--<td>
        <?//= $c['idp'] ?>
        </td>-->
                <td id="uno">
                    <?= $c['cantidad'] ?>
                </td>
                <td class="celda"> <?= $c['nombre'] ?></td>

                <td class="celda">
                    <?= "S/. ".$c['precio'] ?>
                </td>
                <!--<td class="celda">
                </td>-->
                <td class="celda"> <?php $m+=$c['cantidad']*$c['precio']; echo "S/. ".$c['cantidad']*$c['precio'] ?>  </td>
                <td class="celda">
                    <a class="btn btn-outline-danger" href="#"
                       onclick="conf_eliminar('Eliminar Producto','¿Está seguro de Producto?','<?php echo site_url('productos-consumidos/eliminar/' . $c['id']."/?ideh=".$ideh) ?>')">
                        <i class="fa fa-trash-o fa-lg" ></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        <input type="hidden" id="monto_pro" value="<?= $m ?>">
        </tbody>
    </table>
<?php endif; ?>
