<link rel="stylesheet" href="<?php echo site_url('css/Sestadia.css', true) ?>">
<form action="<?= site_url('estadias/guardar/') ?>" method="POST" id="form_estadia">
    <div id="page-wrapper">
        <div class="container-fluid">

                                <div class="back" align="left">
                                    <label  class="letra"> <a href="<?=site_url('estadias-habitaciones/?id='.EstadiasHabitaciones::getAll('where id='.$ideh)[0]->
                                            getEstadia_id())?>">
                                            <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
                                        </a></label>
                                </div>

                                <h2  style="margin-top: -3%" align="center">HABITACIÓN:
                                    <?= $habitacion[0]['numero']." ".$habitacion[0]['descripcion'] ?> <br>
                                    <span id="tph"> <?= $habitacion[0]['tipo'] ?></span>
                                </h2>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card" style="background-color: #E0FFFF">
                                                <div class="card-header">
                                                    <i class="icon-note"></i><label>
                                                        <?php if($habitacion[0]['cliente_id']): ?>
                                                            <?php $ch = Clientes::getAll('where id='.$habitacion[0]['cliente_id']); ?>
                                                            <b>Responsable:</b> <?= $ch[0]->nombres.' '.$ch[0]->apellidos.'  <Strong>DNI:  </Strong>'.' '.$ch[0]->dni?>
                                                        <?php else: ?>
                                                        Cliente: <br>
                                                        <select required name="cliente_id" id="cliente_n_id">
                                                            <option value="">--> Seleccione un cliente <--</option>
                                                            <?php foreach (Clientes::getAll() as $u): ?>
                                                                <option value="<?= $u->id ?>"><?php echo $u->dni.', '.$u->nombres.' '.$u->apellidos ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <i style="color: black; cursor: pointer " href="<?php echo site_url('clientes/editar') ?>"
                                                           onclick="openmodal_c(this)"
                                                           class="fa fa-plus-circle fa-2x"></i>
                                                        <?php endif; ?>
                                                    </label>
                                                    <div class="card-actions">

                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <form>

                                            <div class="form-inline">
                                                <select  id="ocupante" onchange="registrarOcupante(this); return false;" href="<?= site_url("clientes-estadias/guardar/?habitacion_estadia_id=".$ideh) ?>">
                                                    <option value="">--> Seleccione un Ocupante <--</option>
                                                    <?php foreach ($clientes as $u): ?>
                                                        <option value="<?= $u["id"] ?>"><?php echo $u["dni"].', '.$u["nombres"].' '.$u["apellidos"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <i style="color: #3C8DBC;  cursor: pointer "
                                                            href="<?php echo site_url('clientes/editar') ?>"
                                                            onclick="openmodal_o(this)"
                                                            class="btn btn-default">
                                                    <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></i><br><br>
                                            </div>
                                            <div id="tabla_clientes">
                                                <?php View::load('estadias/list_clientes',['ideh'=>$ideh]) ?>
                                            </div>

                                            <input type="hidden" name="ideh" value="<?= $ideh ?>">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <strong>Tipo Precio:</strong>
                                                    <div class="form-inline">
                                                        <div class="form-check">

                                                            <label class="form-check-label"
                                                            onclick="

                                                                    $('#pre').val($('#tipo_h option:selected').attr('precio'));
                                                                    $('#tPoo').removeClass('oculta');
                                                                    calcular_precio();

                                                            ">
                                                                <input type="radio" class="form-check-input"
                                                                    <?php if ((boolean)$habitacion[0]['tipo_precio'] == 1){echo 'checked'; } ?>
                                                                       name="tipo_precio" id="optionsRadios1" value="0"  >
                                                                Habitación
                                                            </label>
                                                        </div>
                                                        <div class="form-check"
                                                             onclick="num_personas = $('#list_ocu tr').length;
                                                             $('#tPoo').addClass('oculta');
                                                             pre = $('#tipo_h option:selected').attr('precio');
                                                             $('#pre').val(num_personas*pre); calcular_precio();">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input"
                                                                    <?php if ((boolean)$habitacion[0]['tipo_precio'] == 0){echo 'checked'; } ?>
                                                                       name="tipo_precio" id="optionsRadios2" value="1">
                                                                Persona
                                                            </label>
                                                        </div>
                                                        <input type="hidden" disabled name="tipo_precio"
                                                               value="0" id="tipo_precio_c"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <strong class="form-inline" style="display: inline"> Precio:</strong><br>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><STRONG>S/.</STRONG></span>
                                                    <input type="number" onkeyup="calcular_precio()"
                                                           min="1" onblur="precio_persona(this)"
                                                           name="precio" style="width:90%" id="pre"
                                                           value="<?php if ($habitacion[0]['precio']){echo $habitacion[0]['precio'];}else{ echo $habitacion[0]['precioh'];} ?>"
                                                           class="form-control-sm">
                                                </div>
                                                </div>
                                                <div class="col-md-4" id="tPoo">
                                                    <strong class="form-imline" style="display: inline">
                                                        Tipo: </strong><br>
                                                    <select id="tipo_h" name="tipo_habitacion"
                                                            onchange="cambiar_tipo(this,'<?=site_url('estadias/tipohabitacion')?>'); return false;" style="width:90%">
                                                        <?php foreach (TiposHabitaciones::getAll() as $t): ?>
                                                            <option  value="<?= $t->id ?>" precio="<?= $t->getPrecio() ?>" max_personas="<?= $t->getMaxPersonas() ?>"
                                                                <?php if($habitacion[0]['tipo_habitacion']){
                                                                    if($t->id == $habitacion[0]['tipo_habitacion']){echo "selected";}
                                                                }else{
                                                                    if($t->id == $habitacion[0]['idth']){echo "selected";}
                                                                }?>><?= $t->getDescripcion() ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <input type="hidden" disabled name="tipo_habitacion" id="ntipo" value="<?php if($habitacion[0]['tipo_habitacion']){
                                                        echo $habitacion[0]['tipo_habitacion'];
                                                    }else{
                                                        echo $habitacion[0]['idth'];
                                                    }?>">
                                                </div>

                                            </div>
                                            <br>

                                            <strong>Productos:</strong> <br>
                                            <div class="form-inline">
                                                <select class="form-control-sm" id="producto_id">
                                                    <option value="">Seleccione producto...</option>
                                                    <?php foreach (Productos::getAll() as $pp): ?>
                                                        <option value="<?= $pp->id ?>" ><?= $pp->getNombre().' S/.'.$pp->getPrecio()  ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <input id="canti" class="form-control-sm"
                                                       onkeypress=" if(event.keyCode==13){agregar_pro(this); return false}"
                                                       href="<?= site_url('productos-consumidos/guardar/?habita_estadia_id='.$ideh) ?>"
                                                       type="number"
                                                       min="1"
                                                       value="1"
                                                       placeholder="cantidad:">
                                                <i style="color: #3C8DBC;  cursor: pointer "
                                                   href="<?= site_url('productos-consumidos/guardar/?habita_estadia_id='.$ideh) ?>"
                                                   onclick="agregar_pro(this)"
                                                   class="btn btn-default">
                                                    <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></i>
                                            </div>

                                            <div id="list_prod">
                                                <?php View::load('estadias/list_productos',['ideh'=>$ideh]) ?>
                                            </div>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="card" style="background-color: #E0FFFF">
                                                <div class="card-body">
                                                    <form>
                                                        <strong> Reserva:</strong>
                                            <div class="form-inline">
                                                <input type="checkbox" id="check_reserva" onclick="fecha_reserva_c(this);"
                                                       <?php if($habitacion[0]['fecha_reserva']){echo "checked disabled";}?>
                                                       class="form-control" />
                                            </div>
                                           <strong>Fecha: </strong>
                                                        <div class="form-inline">
                                                <input type="<?php if ($habitacion[0]['fecha_reserva']){ echo 'text';}else{echo 'hidden';} ?>" required <?php if (!$habitacion[0]['fecha_reserva']){ echo 'disabled';} ?>
                                                       class="form-control fecha_reserva"
                                                       id="fecha_reserva"
                                                       href="<?= site_url('estadias/mostrar/'.$habitacion[0]['idh']) ?>"
                                                       placeholder="dd/mm/aaaa"
                                                       value="<?php  if($habitacion[0]['fecha_reserva']){echo DateTime::createFromFormat('Y-m-d', $habitacion[0]['fecha_reserva'])->format('d/m/Y');} ?>"
                                                       name="fecha_reserva" /><p style="color: white">-</p>
                                                <input type="hidden" <?php if ($habitacion[0]['fecha_reserva']){ echo 'disabled';} ?>
                                                       value="<?php  $fi = $habitacion[0]['fecha_ingreso'];
                                                       if($fi){ echo $fi.'"';}
                                                       else{echo date('Y-m-d').'"  required';}
                                                       ?>
                                                       id = "fecha_ingreso"
                                                       placeholder="dd/mm/aaaa"
                                                       name="fecha_ingreso">
                                                <input type="text" id="fecha_actual" class="form-control" value="<?php echo (date("d/m/Y")); ?>" disabled>
                                                <input type="text" required
                                                       value="<?php if($habitacion[0]['fecha_salida']){echo DateTime::createFromFormat('Y-m-d', $habitacion[0]['fecha_salida'])->format('d/m/Y');}else{
                                                           $fecha = date('Y-m-j');
                                                           $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
                                                           $nuevafecha = date ( 'Y-m-d' , $nuevafecha );

                                                           echo DateTime::createFromFormat('Y-m-d', $nuevafecha)->format('d/m/Y');
                                                       } ?>"
                                                       placeholder="dd/mm/aaaa"
                                                       id = "fecha_salida"
                                                       class="form-control date fecha_reserva" <?php if (!$habitacion[0]['fecha_reserva']){ echo ' onchange="calcular_dias_s($(\'#fecha_ingreso\').val(),$(this).val())"';}?>
                                                       name="fecha_salida" />
                                            </div>
                                            <br>
                                            <div class="form-inline">
                                                Número de Días:
                                                <div style="margin-left: 2%" class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                                                    <span  class="form-control" id="diasS" > <?php if($habitacion[0]['dias']){ echo ($habitacion[0]['dias']);}else{ echo(1);} ?> </span>
                                                    <input required min="1" value="<?php if($habitacion[0]['dias']){ echo ($habitacion[0]['dias']);}else{ echo(1);} ?> "
                                                           name="dias" type="hidden" id="dias" >
                                                </label>
                                                </div>
                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <strong>Descuento:</strong><br>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><STRONG>S/.</STRONG></span>
                                                    <input  name="descuento" value="<?= $habitacion[0]['descuento'] ?>"
                                                           type="number" min="0" step="any"
                                                           onclick=" $('#aum').prop('disabled',true); calcular_precio()"
                                                           onkeyup="calcular_precio()"id="des" class="form-control-sm"
                                                           placeholder="descuento en soles">
                                                </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <strong>Aumento:</strong><br>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><STRONG>S/.</STRONG></span>
                                                    <input name="aumento" step="any" value="<?= $habitacion[0]['aumento'] ?>"
                                                           type="number" min="0" onclick="
                                                           $('#des').prop('disabled',true);
                                                           calcular_precio()"
                                                           onkeyup="calcular_precio()" id="aum" class="form-control-sm"
                                                           placeholder="aumento en soles">
                                                </div>
                                                </div>
                                            </div>
                                            <strong>Observación:</strong>
                                            <textarea name="observacion" value="<?= $habitacion[0]['observacion'] ?>"
                                                      class="form-control" placeholder="ingresar observación">
                                            </textarea>
                                            <br>
                                            <strong>Precio Total:</strong>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><STRONG>S/.</STRONG></span>
                                                <strong class="form-control" id="totS"> <?php if($habitacion[0]['precio_total']){ echo $habitacion[0]['precio_total'];}else{ echo $habitacion[0]['precioh'];}  ?> </strong>
                                                <input  id="tot" step="any" type="hidden" required value="<?php if($habitacion[0]['precio_total']){ echo $habitacion[0]['precio_total'];}else{ echo $habitacion[0]['precioh'];}  ?>" name="precio_total">
                                            </div> <br>
                                                    </form>
                                                </div>
                                        </div>
                                    </div>
                                </div>

    <div align="right">
        <?php if($habitacion[0]['fecha_reserva']){?>
            <a  class="btn btn-success" onclick="check_in()">Check In</a>
        <?php }?>
        <button type="button" id="boton_pagar" onclick="botonpagar(this)" class="btn btn-success" href="<?= site_url("estadias/guardar/?pagar=1")?>" >Pagar</button>
        <button id="nfe" type="submit" class="btn btn-primary">Guardar</button>
        <a class="btn btn-danger" href="<?= site_url("estadias-habitaciones/eliminar/".$ideh)?>">Cerrar</a>

    </div>
                                    </div>
        </div>

    </div>
</form>
<div id="clienteModal">

</div>

<script>

    function cambiar_tipo(t,url) {
        $.get(url).done(
            function () {
                $('#tph').html($('option:selected', t).text());
                $('#pre').val($('option:selected', t).attr('precio'));
                calcular_precio();
            }
        ).fail(
            function () {
                $("#tipo_h").prop('disabled',true);
                $("#ntipo").prop('disabled',false);
                toastr.warning('no tiene permiso!');
            }
        )

    }
    function descuento(t) {
        $.get(url).done(
            function () {
                $('#des').html($('input', t).text());
                calcular_precio();
            }
        ).fail(
            function () {
                $("#des").prop('disabled',true);
                toastr.warning('no tiene permiso!');
            }
        )

    }

    function botonpagar(t) {
        var href=$(t).attr('href');
        var datos=$("#form_estadia").serialize();

        if($("#cliente_n_id").length){
            if($("#cliente_n_id").val()!=""){
                window.location.href =href+"&"+datos;
            }else toastr.warning('Falta llenar el campo de cliente');
        }else {
            window.location.href =href+"&"+datos;
        }
    }
    function openmodal_c(t) {
        u = $(t).attr('href')+'?cli=1';
        $("#clienteModal").empty().load(u, function () {
            $('#registrar').modal('show');
            $('#modal-form').attr('onsubmit','registrar_n_c(this); return false;');
        });
    }
    function openmodal_o(t) {
        u = $(t).attr('href')+'?cli=1';
        $("#clienteModal").empty().load(u, function () {
            $('#registrar').modal('show');
        });
    }
    function check_in() {
        fr = $("#fecha_reserva").val();
        $("#fecha_reserva").val('').prop('required',false);
        $("#check_reserva").prop('disabled',false).prop('checked',false);
        $("#fecha_ingreso").prop('disabled',false).val(fr);
       $("#nfe").click();
    }


    function registrar_n_c(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            if(id!="existe"){
                toastr.success('Guardado Correctamente!');
                nom = $("#dni").val()+","+$("#nombre").val()+" "+$("#ape").val();
                var option = new Option(nom, id);
                $("#cliente_n_id").append(option);
                $("#cliente_n_id").val(id).trigger('change');
                $("#registrar").modal("hide");
            }else {
                toastr.error('DNI existente');
            }
        });
    }
    function fecha_reserva_c(t) {
        ch = $(t).prop('checked');
        ty = 'text';
        if (!ch){
            ty = 'hidden';
            $('.date').datepicker({
                format: "dd/mm/yyyy",
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome'

            });
            if ($('#fecha_reserva').data('daterangepicker')) {
                $("#fecha_actual").removeAttr("hidden","hidden");
                $('#fecha_reserva').data('daterangepicker').container.remove();
                $('#fecha_salida').data('daterangepicker').container.remove();
            }

        }else{
            $("#fecha_actual").attr("hidden","hidden");
            $("#fecha_salida").datepicker('destroy').addClass('fecha_reserva form-control date').val("");
            var separator = ' - ', dateFormat = "DD/MM/YYYY";
            var options = {
                language: 'en',
                autoUpdateInput: false,
                autoApply: true,
                locale: {
                    format: dateFormat,
                    separator: separator,
                },
                minDate: moment(),
                maxDate: moment().add(359, 'days'),
                opens: "left",
                disableTouchKeyboard: true
            };
            $('.fecha_reserva')
                .daterangepicker(options)
                .on('apply.daterangepicker' ,function(ev, picker) {
                    var boolStart = this.name.match(/fecha_reserva/g) == null ? false : true;
                    var boolEnd = this.name.match(/fecha_salida/g) == null ? false : true;

                    var mainName = this.name.replace('fecha_reserva', '');
                    if(boolEnd) {
                        mainName = this.name.replace('fecha_salida', '');
                        $(this).closest('form').find('[name=fecha_salida'+ mainName +']').blur();
                    }

                    fs = picker.endDate.format(dateFormat);
                    $(this).trigger('change').trigger('keyup');
                    $(this).closest('form').find('[name=fecha_reserva' + mainName + ']')
                        .val(picker.startDate.format(dateFormat));
                    $(this).closest('form').find('[name=fecha_salida' + mainName + ']')
                        .val(fs);

                    //fs = $(this).closest('form').find('[name=fecha_salida'+ mainName +']').val();
                    $.get($('#fecha_reserva').attr('href'),{'f':'','fecha':fs}).done(function (da) {
                        if (da){
                            calcular_dias(picker.startDate.format(dateFormat), fs);
                        }else{
                            toastr.warning('fecha no valida!');
                            $('.fecha_reserva').val('');
                        }
                    });

                })
                .on('show.daterangepicker', function(ev, picker) {
                    var boolStart = this.name.match(/fecha_reserva/g) == null ? false : true;
                    var boolEnd = this.name.match(/fecha_salida/g) == null ? false : true;
                    var mainName = this.name.replace('fecha_reserva', '');
                    if(boolEnd) {
                        mainName = this.name.replace('fecha_salida', '');
                    }

                    var startDate = $(this).closest('form').find('[name=fecha_reserva'+ mainName +']').val();
                    var endDate = $(this).closest('form').find('[name=fecha_salida'+ mainName +']').val();

                    $('[name=daterangepicker_start]').val(startDate).trigger('change').trigger('keyup');
                    $('[name=daterangepicker_end]').val(endDate).trigger('change').trigger('keyup');
                    if(boolEnd) {
                        $('[name=daterangepicker_end]').focus();
                    }
                })
            ;
        }
        $('#fecha_reserva').prop('disabled', !ch).attr('type',ty);


    }

    function precio_persona(t) {
        if(!$('.form-check-input').prop('checked')){
            preci = $(t).val();
            num_personas = $('#list_ocu tr').length;
            $(t).val(preci*num_personas);
            calcular_precio();
        }

    }
    function parseDate(str) {
        if (str.indexOf('/') > -1) {
            console.log(str);
            var mdy = str.split('/');
           return new Date(mdy[2], mdy[1] - 1, mdy[0]);
        }else{
            var mdy = str.split('-');
            return new Date(mdy[0], mdy[1] - 1, mdy[2]);
        }

    }
    function calcular_dias(a,b) {
        dias = daydiff(parseDate(a), parseDate(b));
        if (dias<=0){
            toastr.warning('no puede ingresar esta fecha!');
            $('.fecha_reserva').val('');
        }else {
            $('#dias').val(dias);
            $('#diasS').html(dias);
            calcular_precio();
        }
    }
    function calcular_dias_s(a,b) {
        $.get($('#fecha_reserva').attr('href'),{'f':'','fecha':b}).done(function (da) {
            if (da){
                calcular_dias(a, b);
            }else{
                toastr.warning('fecha no valida!');
                $('.fecha_reserva').val('');
            }
        });
    }

    function agregar_pro(t) {
        var p = $("#producto_id").val();
        if (!p){
            toastr.warning('seleccione un producto!');
        }

        if (!$('#canti').val()){
            toastr.warning('seleccione cantidad!');
            return false;
        }else {
            $.get($(t).attr('href') + '&producto_id=' + p + '&cantidad=' + $('#canti').val()).done(
                function (d) {
                    $("#list_prod").html(d);
                    calcular_precio();
                }
            );
        }
        return false;
    }

    function daydiff(first, second) {
        return (second-first)/(1000*60*60*24)
    }




    function calcular_precio() {
        precio = $('#pre').val();
        descuento = $("#des").val();
        dias = $('#dias').val();
        aumento = $("#aum").val();
        monto_prod = $("#monto_pro").val();
        monto_prod =  (monto_prod == null) ? 0 : monto_prod;

        if (descuento == "")  {
            descuento = 0;
        }
        if (aumento == ""){
            aumento = 0;
        }
        dias =  (dias == "") ? 1 : dias;

        tot = ((precio*dias)-descuento+parseFloat(aumento)  );

        //if (descuento<tot){
        $('#tot').val(tot+parseFloat(monto_prod));
        $('#totS').html(tot+parseFloat(monto_prod));
        /*}
        else{
            toastr.warning('Ingrese descuento mayor que cero y menor al precio total!');
        }*/
    }

    function registrarOcupante(t) {
        v = $(t).val();
        th = parseInt($('#tipo_h option:selected').attr('max_personas'));
        num_personas = $('#list_ocu tr').length;
        if (num_personas>=th){
            toastr.warning('Ha superado el Número de personas, para poder registrar más, realize cambio de habitación!');
        }else {
            $("#ocupante option[value='" + v + "']").remove();
            $.get($("#ocupante").attr('href') + '&cliente_id=' + v).done(
                function (d) {

                    $("#tabla_clientes").html(d);
                    num_personas = $('#list_ocu tr').length;
                    $('#tPoo').addClass('oculta');
                    pre = $('#tipo_h option:selected').attr('precio');
                    $('#pre').val(num_personas*pre); calcular_precio()
                }
            );
        }
        return false;
    }
    function registrar(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            toastr.success('Guardado Correctamente!');
            nom = $("#dni").val()+","+$("#nombre").val()+" "+$("#ape").val();
            var option = new Option(nom, id);
            $("#cliente_n_id").append(option);
            $("#ocupante").val(id).trigger("change");
            $("#registrar").modal("hide");
        });
    }


</script>