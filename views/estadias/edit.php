<div class="container-fluid">
<div id="usa">
<h1><?php echo $estadias->isNew() ? "New" : "Edit" ?> Estadias</h1>
<form method="post" action="<?php echo site_url('estadias/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($estadias->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_fecha_reserva">Fecha Reserva</label>
			<input id="estadias_fecha_reserva" type="text" name="fecha_reserva" value="<?php echo h($estadias->getFechaReserva(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_fecha_ingreso">Fecha Ingreso</label>
			<input id="estadias_fecha_ingreso" type="text" name="fecha_ingreso" value="<?php echo h($estadias->getFechaIngreso(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_fecha_salida">Fecha Salida</label>
			<input id="estadias_fecha_salida" type="text" name="fecha_salida" value="<?php echo h($estadias->getFechaSalida(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_estado">Estado</label>
			<input id="estadias_estado" type="text" name="estado" value="<?php echo h($estadias->getEstado()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_cliente_id">Clientes</label>
			<select id="estadias_cliente_id" name="cliente_id">
			<?php foreach (Clientes::doSelect() as $clientes): ?>
				<option <?php if ($estadias->getClienteId() === $clientes->getId()) echo 'selected="selected"' ?> value="<?php echo $clientes->getId() ?>"><?php echo $clientes?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_created_at">Created At</label>
			<input id="estadias_created_at" type="text" name="created_at" value="<?php echo h($estadias->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_updated_at">Updated At</label>
			<input id="estadias_updated_at" type="text" name="updated_at" value="<?php echo h($estadias->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_deleted_at">Deleted At</label>
			<input id="estadias_deleted_at" type="text" name="deleted_at" value="<?php echo h($estadias->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_usuario_id">Usuario Id</label>
			<input id="estadias_usuario_id" type="text" name="usuario_id" value="<?php echo h($estadias->getUsuarioId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_precio">Precio</label>
			<input id="estadias_precio" type="text" name="precio" value="<?php echo h($estadias->getPrecio()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_descuento">Descuento</label>
			<input id="estadias_descuento" type="text" name="descuento" value="<?php echo h($estadias->getDescuento()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_observacion">Observacion</label>
			<input id="estadias_observacion" type="text" name="observacion" value="<?php echo h($estadias->getObservacion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_monto_pagar">Monto Pagar</label>
			<input id="estadias_monto_pagar" type="text" name="monto_pagar" value="<?php echo h($estadias->getMontoPagar()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_cuota">Cuota</label>
			<input id="estadias_cuota" type="text" name="cuota" value="<?php echo h($estadias->getCuota()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_resto">Resto</label>
			<input id="estadias_resto" type="text" name="resto" value="<?php echo h($estadias->getResto()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $estadias->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>
</div>
</div>