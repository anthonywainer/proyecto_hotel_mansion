<style>
    #comprobante{
        background: white;
        border: solid 1px skyblue;
        border-radius: 10px;
        padding: 60px;
    }
    .logo{
        width: 20%;
        margin-left: 2%;
    }
    #cabecera{
        margin-top: -8%;
    }
    label{
        border-bottom: solid 2px black;
    }
    #descripcion{
        margin-top: 1%;
        text-align: center;
    }
</style>
<div class="container" id="comprobante">
    <img src="<?php echo site_url('imagenes/mansion.jpg') ?>" class="logo" alt="" >
    <h2 align="center" id="cabecera">"HOTEL MANSIÓN"</h2>
    <?php $mmm = Ventas::getAll('where id='.$id)[0]; ?>
    <h3 align="center">
        <?php if ($mmm->getMovimiento()->getTipoComprobante()->id==1){echo "Boleta: ";}else{echo "Factura";} ?>
        N° 00000 <?= $mmm->id ?></h3>
    <p id="descripcion">Jr.Maynas 286  <i class="fa fa-phone-square" aria-hidden="true"></i>(042) 532227 San Martín-Tarapoto-Peru</p>

    <p >
        <?php if ($mmm->getMovimiento()->getTipoComprobante()->id==1){?>
       <strong>Cliente:</strong>
            <label><?= $mmm->getMovimiento()->getCliente()->nombres.' '.$mmm->getMovimiento()->getCliente()->apellidos ?></label>
        <strong style="margin-left: 20%">Dni:</strong> <label><?= $mmm->getMovimiento()->getCliente()->dni ?></label>
            <br>
            <strong> Dirección:</strong> <label> <?= $mmm->getMovimiento()->getCliente()->direccion ?></label>
        <?php }else{ ?>
                <strong>Razón Social:</strong><label> <?= $mmm->getMovimiento()->getCliente()->nombres ?></label>
        <strong style="margin-left: 20%"> Ruc:</strong>  <label><?= $mmm->getMovimiento()->getCliente()->ruc ?></label>
            <br>
                    <strong>Dirección:</strong> <label> <?= $mmm->getMovimiento()->getCliente()->direccion ?></label>

        <?php } ?>
    </p>
        <?php $mmt=0; foreach (EstadiasHabitaciones::getAll('where estadia_id='.$mmm->getEstadia_id()) as $eh): ?>
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <td colspan="4"><?php $ha = Habitaciones::getAll('where id='.$eh->getHabitacion_id()); echo "Habitación: ".$ha[0]->getNumero()?> </td>
                </tr>
                <tr>
                    <th>producto</th>
                    <th>precio</th>
                    <th>cantidad</th>
                    <th>monto</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (ProductoConsumido::getAll('where habita_estadia_id='.$eh->id) as $pc){?>
                    <tr>
                        <td><?php $pp = Productos::getAll('where id='.$pc->getProducto_id()); echo $pp[0]->getNombre() ?> </td>
                        <td>S/. <?= $pp[0]->getPrecio() ?></td>
                        <td><?= $pc->getCantidad() ?></td>
                        <td>S/. <?= $pc->getMonto() ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>

                <tr>
                    <td><?php $th =TiposHabitaciones::getAll('where id='.$ha[0]->getTipo_habitacion_id());
                    echo "Hotel ".$th[0]->getDescripcion(); ?></td>
                    <td>S/. <?= $eh->precio ?></td>
                    <td><?= $eh->getDias() ?> días</td>
                    <td>S/. <?= $eh->getPrecio()*$eh->getDias() ?></td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Descuento:</td>
                    <td>S/. <?= $eh->getDescuento() ?></td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Aumento:</td>
                    <td>S/. <?= $eh->getAumento() ?></td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Total</td>
                    <td>S/. <?php  echo $eh->getPrecioTotal(); $mmt+=$eh->getPrecioTotal(); ?></td>
                </tr>
                </tfoot>

            </table>
        <?php endforeach; ?>

        <table>
            <tr>
                <td colspan="3">Cuota</td>
                <td >S/.<?= $mmm->getMonto() ?></td>
            </tr>
            <tr>
                <td colspan="3">Efectivo</td>
                <td >S/.<?= $mmm->getCuota() ?></td>
            </tr>
            <tr>
                <td colspan="3">
                    <?php if ($mmm->getResto()>0){echo 'Deuda';}else{
                        echo 'Vuelto:';
                    } ?>
                </td>
                <td >S/.<?php if ($mmm->getResto()>0){
                    echo $mmm->getResto();
                    }else{
                    echo $mmm->getResto()*-1;
                    } ?></td>
            </tr>
        </table>


</div>



