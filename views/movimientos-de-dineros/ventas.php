<!--hotel Mansion @v1-->
<style>
    #mas{
        color: black; cursor: pointer;margin-top: -10%; margin-left: 100%
    }
    .scroll{
        height: 30px;
        overflow-y: auto;
    }
    .select{
        width: 80%;

    }
    .select1{
        width: 94%;
    }
    @media only screen and (min-width : 800px) and (max-width : 1024px){
        .select1{
            width:100%;
            margin-right: 2%;
        }
        .select{
            width: 70%;
        }
        }
     .oculta{
         display: none;
     }
</style>
<div class="container-fluid">
    <div class="back" align="left">
        <label> <a href="<?=site_url('estadias')?>">
                <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
            </a></label>
    </div>
<div class="row">
    <div class="col-md-6">
        <div class="card" style="min-height: 30em;">
            <div class="card-header">
                <i class="fa fa-align-justify"></i><label style="font-size: 18px; font-weight: bold">Pago por Estadia y productos consumidos</label>
            </div>
            <div class="card-body scroll">

       <!-- <a href="<?= site_url('estadias')?>" class="btn btn-info">Atras</a>-->

        <?php $mmt=0; foreach (EstadiasHabitaciones::getAll('where estadia_id='.$ide) as $eh): ?>
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <td colspan="4"><?php $ha = Habitaciones::getAll('where id='.$eh->getHabitacion_id()); echo "Habitación: ".$ha[0]->getNumero()?> </td>
                </tr>
                <tr>
                    <th>producto</th>
                    <th>precio</th>
                    <th>cantidad</th>
                    <th>monto</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (ProductoConsumido::getAll('where habita_estadia_id='.$eh->id) as $pc){?>
                    <tr>
                        <td><?php $pp = Productos::getAll('where id='.$pc->getProducto_id()); echo $pp[0]->getNombre() ?> </td>
                        <td>S/. <?= $pp[0]->getPrecio() ?></td>
                        <td><?= $pc->getCantidad() ?></td>
                        <td>S/. <?= $pc->getMonto() ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>

                <tr>
                    <td><?php $th =TiposHabitaciones::getAll('where id='.$ha[0]->getTipo_habitacion_id());  echo "Hotel ".$th[0]->getDescripcion(); ?></td>
                    <td>S/. <?= $eh->precio ?></td>
                    <td><?= $eh->getDias() ?> días</td>
                    <td>S/. <?= $eh->getPrecio()*$eh->getDias() ?></td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Descuento:</td>
                    <td>S/. <?= $eh->getDescuento() ?></td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Aumento:</td>
                    <td>S/. <?= $eh->getAumento() ?></td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Total</td>
                    <td>S/. <?php  echo $eh->getPrecioTotal(); $mmt+=$eh->getPrecioTotal(); ?></td>
                </tr>
                </tfoot>

            </table>
        <?php endforeach; ?>
        <hr>
        <table class="table table-bordered table-sm">
            <tbody>
            <!---  <tr>
                    <td align="right" colspan="3">Subtotal</td>
                    <td>
                        S/. <?#= $mmt ?>
                        <input type="hidden" value=" <?#= $mmt ?>" id="sbt">
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">Descuento:</td>
                    <td>
                        <input type="number" onkeyup="monto_total(this)" onclick="monto_total(this)"
                               id="ds" min="1" class="form-control-sm" step="any"
                               value=" <?#= $mmt ?>" name="descuento">
                    </td>
                </tr> -->
            <tr>
                <td align="right" colspan="3">Total_Final:</td>
                <td align="right">

                    <strong id="mp">S/. <?= $mmt ?></strong>

                    <input type="hidden" value=" <?= $mmt ?>" name="monto" class="mp" >
                </td>
            </tr>
            </tbody>
        </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card" style="min-height: 30em;">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Pago
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                       <strong> Tipo Comprobante:</strong> <br>
                        <select name="tipo_comprobante_id" onchange="cambiarcliente(this)"
                                id="tipo_comprobante_id" class="form-control amorcito">
                            <?php foreach (TiposComprobantes::getAll() as $tc){?>
                                <option value="<?= $tc->id ?>"><?= $tc->getDescripcion() ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-6 mp amorcito" id="cdni" >
                       <strong> Cliente: </strong><br>
                        <select name="cliente_id" class="form-control amorcito" id="cliente_n_dni"  >
                            <option value="">--> Seleccione cliente <--</option>
                            <?php foreach (Clientes::getAll('where clientes.dni is not null') as $u): ?>
                                <option value="<?= $u->id ?>"><?php echo $u->dni.', '.$u->nombres.' '.$u->apellidos ?></option>
                            <?php endforeach; ?>
                        </select>
                        <i id="mas"
                           href="<?php echo site_url('clientes/editar?cli=dni') ?>"
                           onclick="openmodal(this)"
                           class="fa fa-plus-circle fa-lg form-inline"></i>
                    </div>
                    <div class="col-md-6 oculta"  id="cruc">
                        <input type="hidden" value="<?= $mmt ?>" name="monto" class="mp amorcito">
                       <strong>Cliente:</strong>  <br>
                        <select name="cliente_id" disabled id="cliente_n_ruc" class="amorcito" >
                            <option value="">--> Seleccione cliente <--</option>
                            <?php $ruc = Clientes::getAll('where clientes.ruc is not null');
                            if ($ruc)
                                foreach ($ruc as $u): ?>
                                    <option value="<?= $u->id ?>"><?php echo $u->ruc.', '.$u->nombres ?></option>
                                <?php endforeach; ?>
                        </select>
                        <i id="mas" " href="<?php echo site_url('clientes/editar?cli=ruc') ?>"onclick="openmodal(this)"
                           class="fa fa-plus-circle form-inline"></i><br>

                    </div>
                    <div class="col-md-6">
                        <strong>Forma de pago: </strong><br>
                        <select name="forma_pago_id" id="forma_pago_id" required class="select1 amorcito">
                            <?php foreach (FormasPagos::getAll() as $fp){  ?>
                                <option value="<?= $fp->id ?>"><?= $fp->getDescripcion() ?></option>
                            <?php } ?>
                        </select>
                    </div>

                </div>
               <br>

            <div id="amorti">
                <?php View::load('movimientos-de-dineros/amortizaciones', $params) ?>
            </div>
            </div>
        </div>

    </div>

</div>
</div>

</div>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <!--<div class="modal-footer">

            </div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    function guardar_amor(id,t) {
        $.post($(t).attr('action'),$(id).serialize()).done(
            function (datos) {
                $("#amorti").html(datos);
            }
        ).error(function (datos) {
            toastr.error('Falta aperturar caja');
        })
    }
    function guardar_amor2(id,t) {
        $.post($(t).attr('action'),$(id).serialize()).done(
            function (datos) {
                $("#amorti").html(datos);
            }
        )
    }

    function checkout(t) {
        if($("#amortizacionvalidacion").length>0){
            toastr.error('Falta pagar');
        }else{
            window.location.href = $(t).attr("href");
        }
    }
    function Resto(t,id) {
        var cuota = $(t).val();
        var mo = $(".resto"+id+" #mon").val();
        $(".resto"+id+" #res").val(mo-cuota);
        $(".resto"+id+" #reS").html(mo-cuota);
    }

    function registrar(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            alert("Guardado Correctamente");
            nom = $("#dni").val()+","+$("#nombre").val()+" "+$("#ape").val();
            var option = new Option(nom, id);
            $("select").append(option);
            $("#usuario_id").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }

    function monto_total(t) {
        d = $("#ds").val();
        sb = $("#sbt").val();
        $(".mp").val(sb-d);
        $("#mp").html("S/. "+(sb-d));
    }
    function registrarD(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            toastr.success('Guardado Correctamente!');
            nom = $("#dni").val()+","+$("#nombre").val()+" "+$("#ape").val();
            var option = new Option(nom, id);
            $("#cliente_n_dni").append(option);
            $("#cliente_n_dni").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function registrarR(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            toastr.success('Guardado Correctamente!');
            nom = $("#ruc").val()+","+$("#nombre").val();
            var option = new Option(nom, id);
            $("#cliente_n_ruc").append(option);
            $("#cliente_n_ruc").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function cambiarcliente(t) {
        cliente = $(t).val();
        if(cliente == 1){
            $("#cruc").addClass('oculta');
            $("#cdni").removeClass('oculta');
            $("#cliente_n_ruc").prop('disabled',true);
            $("#cliente_n_dni").prop('disabled',false);
        }else{
            $("#cruc").removeClass('oculta');
            $("#cdni").addClass('oculta');
            $("#cliente_n_dni").prop('disabled',true);
            $("#cliente_n_ruc").prop('disabled',false);
        }
    }
</script>