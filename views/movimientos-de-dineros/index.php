<h1>
	<a href="<?php echo site_url('movimientos-de-dineros/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Movimientos de dinero">
		New Movimientos de dinero
	</a>
	Movimientos De Dineros
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('movimientos-de-dineros/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>