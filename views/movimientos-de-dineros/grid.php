<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid movimientos_de_dinero-grid table table-condensed table-bordered" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::NUMERO_VOUCHER))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::NUMERO_VOUCHER): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Numero Voucher
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::VOUCHER_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::VOUCHER_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Voucher
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::CONCEPTO_DE_PAGO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::CONCEPTO_DE_PAGO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Concepto De Pago
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::CAJA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::CAJA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Caja
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::USUARIO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::USUARIO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Usuario
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::FORMA_PAGO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::FORMA_PAGO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Forma Pago
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::CREATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::CREATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Created At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::UPDATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::UPDATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Updated At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MovimientosDeDinero::DELETED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MovimientosDeDinero::DELETED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Deleted At
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($movimientos_de_dineros as $key => $movimientos_de_dinero): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($movimientos_de_dinero->getId()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getNumeroVoucher()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getVouchersRelatedByVoucherId()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getConceptosDePagoRelatedByConceptoDePagoId()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getCajasRelatedByCajaId()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getUsuariosRelatedByUsuarioId()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getFormasPagoRelatedByFormaPagoId()) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($movimientos_de_dinero->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Movimientos_de_dinero"
					href="<?php echo site_url('movimientos-de-dineros/show/' . $movimientos_de_dinero->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Movimientos_de_dinero"
					href="<?php echo site_url('movimientos-de-dineros/edit/' . $movimientos_de_dinero->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Movimientos_de_dinero"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('movimientos-de-dineros/delete/' . $movimientos_de_dinero->getId()) ?>'; } return false">
					Delete
				</a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php echo site_url('amortizaciones?movimiento_de_dinero_id=' . $movimientos_de_dinero->getId()) ?>">
					Amortizaciones
				</a>
				<a
					class="button"
					data-icon="carat-1-e"
					href="<?php echo site_url('ventas-movimientos?movimiento_de_dinero_id=' . $movimientos_de_dinero->getId()) ?>">
					Ventas Movimientos
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>