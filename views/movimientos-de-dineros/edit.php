<h1><?php echo $movimientos_de_dinero->isNew() ? "New" : "Edit" ?> Movimientos De Dinero</h1>
<form method="post" action="<?php echo site_url('movimientos-de-dineros/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($movimientos_de_dinero->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_numero_voucher">Numero Voucher</label>
			<input id="movimientos_de_dinero_numero_voucher" type="text" name="numero_voucher" value="<?php echo h($movimientos_de_dinero->getNumeroVoucher()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_voucher_id">Vouchers</label>
			<select id="movimientos_de_dinero_voucher_id" name="voucher_id">
			<?php foreach (Vouchers::doSelect() as $vouchers): ?>
				<option <?php if ($movimientos_de_dinero->getVoucherId() === $vouchers->getId()) echo 'selected="selected"' ?> value="<?php echo $vouchers->getId() ?>"><?php echo $vouchers?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_concepto_de_pago_id">Conceptos De Pago</label>
			<select id="movimientos_de_dinero_concepto_de_pago_id" name="concepto_de_pago_id">
			<?php foreach (ConceptosDePago::doSelect() as $conceptos_de_pago): ?>
				<option <?php if ($movimientos_de_dinero->getConceptoDePagoId() === $conceptos_de_pago->getId()) echo 'selected="selected"' ?> value="<?php echo $conceptos_de_pago->getId() ?>"><?php echo $conceptos_de_pago?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_caja_id">Cajas</label>
			<select id="movimientos_de_dinero_caja_id" name="caja_id">
			<?php foreach (Cajas::doSelect() as $cajas): ?>
				<option <?php if ($movimientos_de_dinero->getCajaId() === $cajas->getId()) echo 'selected="selected"' ?> value="<?php echo $cajas->getId() ?>"><?php echo $cajas?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_usuario_id">Usuarios</label>
			<select id="movimientos_de_dinero_usuario_id" name="usuario_id">
			<?php foreach (Usuarios::doSelect() as $usuarios): ?>
				<option <?php if ($movimientos_de_dinero->getUsuarioId() === $usuarios->getId()) echo 'selected="selected"' ?> value="<?php echo $usuarios->getId() ?>"><?php echo $usuarios?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_forma_pago_id">Formas Pago</label>
			<select id="movimientos_de_dinero_forma_pago_id" name="forma_pago_id">
			<?php foreach (FormasPago::doSelect() as $formas_pago): ?>
				<option <?php if ($movimientos_de_dinero->getFormaPagoId() === $formas_pago->getId()) echo 'selected="selected"' ?> value="<?php echo $formas_pago->getId() ?>"><?php echo $formas_pago?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_created_at">Created At</label>
			<input id="movimientos_de_dinero_created_at" type="text" name="created_at" value="<?php echo h($movimientos_de_dinero->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_updated_at">Updated At</label>
			<input id="movimientos_de_dinero_updated_at" type="text" name="updated_at" value="<?php echo h($movimientos_de_dinero->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="movimientos_de_dinero_deleted_at">Deleted At</label>
			<input id="movimientos_de_dinero_deleted_at" type="text" name="deleted_at" value="<?php echo h($movimientos_de_dinero->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $movimientos_de_dinero->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>