<h1>View Habitaciones</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('habitaciones/edit/' . $habitaciones->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Habitaciones">
		Edit	</a>
	<a href="<?php echo site_url('habitaciones/delete/' . $habitaciones->getId()) ?>"
		class="button" data-icon="trash" title="Delete Habitaciones"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('clientes-habitaciones?habitacion_id=' . $habitaciones->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Clientes Habitaciones Habitaciones">
		Clientes Habitaciones	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Numero</span>
		<?php echo h($habitaciones->getNumero()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($habitaciones->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado</span>
		<?php echo h($habitaciones->getEstado()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Piso</span>
		<?php echo h($habitaciones->getPiso()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Televisor</span>
		<?php echo h($habitaciones->getTelevisoresRelatedByTelevisorId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Tipo Habitacion</span>
		<?php echo h($habitaciones->getTiposHabitacionesRelatedByTipoHabitacionId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($habitaciones->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($habitaciones->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($habitaciones->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>