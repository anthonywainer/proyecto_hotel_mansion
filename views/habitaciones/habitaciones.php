<link rel="stylesheet" href="<?php echo site_url('css/EstilosHabitaciones.css', true) ?>">
<link rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<style>
    .content-box-blue {
        background-color: #d8ecf7;
        border: 1px solid #afcde3;
        padding: 10px;
        width: 50%;
        margin-bottom: 1%;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row" id="main" >
            <div class="col-sm-12 col-md-12 well" id="content">
                <div class="content-wrapper">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <a href="<?= site_url('estadias-habitaciones/?id='.$_GET['ide'])?>">atras</a>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 align="center">RECEPCIÓN</h3>
                                        <label  class="volver"> <a href="<?= site_url('estadias-habitaciones/?id='.$_GET['ide'])?>">
                                                <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
                                            </a></label>
                                    </div>
                                    <!--<div class="col-md-2">
                                        <button class="btn btn-primary" id="adelante">-></button>
                                    </div>-->
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <input type='text'href="<?=site_url('habitaciones/?ide='.$_GET['ide'])?>"
                                                   value="<?= $fechai ?> - <?= $fechae ?>"
                                                   class="form-control datepicker" />
                                        </div>
                                    </div>
                                </div>
                                <div class="content-box-blue">
                                    <strong>Fecha:</strong> <?=$date_i?> <br>
                                    <?php if($resp):  ?>
                                    <strong>Nombre del responsable:</strong> <?=  $resp[0]->nombres.' '.$resp[0]->apellidos.' DNI : '.$resp[0]->dni ?>
                                    <?php endif; ?>
                                </div>

                                <div class="row">
                                    <?php $dis= $ocu= $res = 0; foreach (Habitaciones::getAll() as $h): ?>
                                        <div class="col-md-2 habitacion">
                                            <div estado="<?=$h->estado?>" onclick="formu_estadia(this)"
                                                 href="<?= site_url('estadias-habitaciones/guardar/?ide='.$_GET['ide'].'&'.'idh='.$h->id) ?>"

                                                 class="fa fa-bed fa-4x ha-father
                                                 <?php
                                                 $dis+=1;
                                                 foreach ($estadia_habitacion as $eh){
                                                     $fecha_tool='';
                                                     if($eh['habitacion_id']==$h->id){
                                                         if (($eh['fecha_ingreso'])){ $fecha_tool = $eh['fecha_ingreso'].' / '.$eh['fecha_salida'];
                                                             $ocu+=1;
                                                             $dis-=1;
                                                             echo 'ha-red" onclick= "mostrar(\''.site_url('estadias/editar/'.$eh[0]->id).'\')"
                                                             " data-toggle="tooltip" data-placement="auto" title="'.$fecha_tool.'"'
                                                             ;
                                                         }elseif (($eh['fecha_reserva'])){ $fecha_tool = $eh['fecha_reserva'].' / '.$eh['fecha_salida'];
                                                             $res+=1;
                                                             $dis-=1;
                                                                 echo 'ha-amarillo" 
                                                                 " data-toggle="tooltip" data-placement="auto" title="'.$fecha_tool.'"
                                                                 ';
                                                         }
                                                     }
                                                 }?>
                                            " >
                                                <span><?= $h->numero//." ".$h->descripcion ?></span>
                                                <tipo><?= TiposHabitaciones::getAll('where id='.$h->getTipo_habitacion_id())[0]->getDescripcion() ?></tipo>

                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="social-bar">
                                    <a href="#" class="icon icon-facebook fa fa-bed" target="_blank">
                                        <strong style="font-size: 10px;">Disponible<br>
                                            <p align="center"><?= $dis ?></p>
                                        </strong>
                                    </a>
                                    <a href="#" class="icon icon-twitter fa fa-bed" target="_blank">
                                        <strong style="font-size: 12px;">Ocupado<br><p align="center">
                                                <?= $ocu ?></p>
                                        </strong>
                                    </a>
                                    <a href="#" class="icon icon-instagram fa fa-bed" target="_blank">
                                        <strong style="font-size: 10px; color: black">Reservado<br><p align="center">
                                                <?= $res ?></p>
                                        </strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function formu_estadia(t) {
            //es = $(t).attr('estado');
            //if (es == "disponible"){
                window.location.href = $(t).attr("href")
           /* }else{
                alert("habitación "+es);
            }*/

        }
    </script