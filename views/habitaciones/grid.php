<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid habitaciones-grid table table-condensed table-bordered table table-striped" cellspacing="0" id="table-responsive">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::NUMERO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::NUMERO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Numero
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descripcion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::ESTADO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::ESTADO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Estado
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::PISO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::PISO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Piso
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::TELEVISOR_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::TELEVISOR_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Televisor
            </a>
            </th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Habitaciones::TIPO_HABITACION_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Habitaciones::TIPO_HABITACION_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
                    Tipo Habitación
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($habitaciones as $key => $habitaciones): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($habitaciones->getId()) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getNumero()) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getDescripcion()) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getEstado()) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getPiso()) ?>&nbsp;</td>
			<!--<td><?php echo h($habitaciones->getTelevisoresRelatedByTelevisorId()) ?>&nbsp;</td>-->
            <td><?php echo $habitaciones->getTelevisor($habitaciones->getTelevisorId())->descripcion ?>&nbsp;</td>
            <td><?php echo $habitaciones->getTipoHabitacion($habitaciones->getTipoHabitacionId())->descripcion ?>&nbsp;</td>
            <!--<td><?php echo h($habitaciones->getTiposHabitacionesRelatedByTipoHabitacionId()) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($habitaciones->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>-->
			<td>
				<!--<a
					class="button"
					data-icon="search"
					title="Show Habitaciones"
					href="<?php echo site_url('habitaciones/show/' . $habitaciones->getId()) ?>">
					Show
				</a>-->
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('habitaciones/editar/' . $habitaciones->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Habitaciones','¿Está seguro de Eliminar?', '<?php echo site_url('habitaciones/eliminar/' . $habitaciones->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>
                </a>
				<!--<a
					class="button"
					data-icon="carat-1-e"
					href="<?php echo site_url('clientes-habitaciones?habitacion_id=' . $habitaciones->getId()) ?>">
					Clientes Habitaciones
				</a>-->
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>