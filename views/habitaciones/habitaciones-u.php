<link rel="stylesheet" href="<?php echo site_url('css/EstilosHabitaciones.css', true) ?>">
<link rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<style>
    .back{
        display: inline-block;

    }
    .letra:hover, .fa-arrow-circle-left:hover{
        color:black;
    }
    .fa-arrow-circle-left{
        cursor: pointer;
        color: black;
    }
    .volver{
        font-size: 15px;
        color: #8c8c8c;
        display: inline-block;
        cursor: pointer;
        margin-top: -60px;
    }
    .Iwhite{
        color:#d1d4d7;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row" id="main" >
            <div class="col-sm-12 col-md-12 well" id="content">
                <div class="content-wrapper">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 align="center">RECEPCIÓN</h3>
                                        <label  class="volver"> <a href="<?=site_url('estadias') ?>">
                                                <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
                                            </a></label>
                                    </div>
                                    <br>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 ">

                                           <strong> Fecha: <?=$date_i?></strong>

                                    </div>


                                    <div class="col-md-4 offset-md-5 form-inline" >
                                        <button class="btn btn-primary" href="<?=site_url('habitaciones/?ide=&dia=-1')?>" onclick="fecha_atras(this)"><-</button>
                                        <div class="form-group" style="border-style: groove">
                                            <input type='text' href="<?=site_url('habitaciones/?ide=')?>" value="<?= $fechai ?>" id="fecha_view"
                                                   onclick="datedate(this)" onchange="buscardatoshabitaciones(this)" class="form-control datedate" />
                                        </div>
                                        <button class="btn btn-primary" href="<?=site_url('habitaciones/?ide=&dia=+1')?>" onclick="fecha_adelante(this)">-></button>
                                    </div>

                                </div>
                                </div>
                            <br>
                           <!-- <div class="back" align="left">
                                Fecha: <?=$date_i?> <br>

                            </div>-->
                                <!--<a href="<?= site_url('estadias') ?>">atras</a>-->
                                <div class="row">
                                    <?php $dis= $ocu= $res = 0; $xxx = true; foreach (Habitaciones::getAll() as $h): ?>
                                        <div class="col-md-2 habitacion ">
                                            <div onclick="formu_estadia(this)"
                                                 class="fa fa-bed fa-4x ha-father
                                                <?php  $dis+=1; foreach ($estadia_habitacion as $eh){
                                                    $fecha_tool='';
                                                    if($eh['habitacion_id']== $h->id){
                                                        $idestadia = $eh['id'];
                                                        if (($eh['fecha_ingreso'])){ $fecha_tool = $eh['fecha_ingreso'].' / '.$eh['fecha_salida'];
                                                            echo 'ha-red" onclick= "mostrar(\''.site_url('estadias/editar/'.$eh['id']).'\')"
                                                            " data-toggle="tooltip" data-placement="auto" title="'.$fecha_tool.'"
                                                            ';
                                                            $ocu+=1;
                                                            $dis-=1;
                                                            $xxx = false;
                                                         }elseif (($eh['fecha_reserva'])){ $fecha_tool = $eh['fecha_reserva'].' / '.$eh['fecha_salida'];
                                                                    echo 'ha-amarillo" onclick= "mostrar(\''.site_url('estadias/editar/'.$eh['id']).'\')"
                                                                    " data-toggle="tooltip" data-placement="auto" title="'.$fecha_tool.'"
                                                                    ';
                                                                    $res+=1;
                                                                    $dis-=1;
                                                            $xxx = false;
                                                 }}}?>
                                            " <?php if($xxx): ?>
                                                 href="<?= site_url('estadias-habitaciones/guardar/?ide=&idh='.$h->id) ?>">
                                                <?php else:?>
                                                    href="<?php echo site_url('estadias/editar/'.$idestadia); $xxx=true; ?>">
                                                <?php endif;?>

                                                <span><?= $h->numero//." ".$h->descripcion ?></span>
                                                <tipo><?= TiposHabitaciones::getAll('where id='.$h->getTipo_habitacion_id())[0]->getDescripcion() ?></tipo>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>

                                </div>
                           <!--<div class="social-bar">
                                <a href="#" class="icon icon-facebook fa fa-bed" target="_blank">
                                    <strong style="font-size: 10px;">Disponible<br>
                                        <p align="center"><?= $dis ?></p>
                                    </strong>
                                </a>
                                <a href="#" class="icon icon-twitter fa fa-bed" target="_blank">
                                    <strong style="font-size: 12px;">Ocupado<br><p align="center">
                                            <?= $ocu ?></p>
                                    </strong>
                                </a>
                                <a href="#" class="icon icon-instagram fa fa-bed" target="_blank">
                                    <strong style="font-size: 10px; color: black">Reservado<br><p align="center">
                                            <?= $res ?></p>
                                    </strong></a>
                            </div>
                            </div>-->
                            <div class="row" align="">
                            <div class="col-md-2 offset-md- " id="capa2" >
                                <a href="#" class="icon icon-facebook fa fa-bed cama " target="_blank">
                                    <strong style="font-size: 12px;">Disponible
                                        <p align="center"><?= $dis ?></p>
                                    </strong>
                                </a>
                            </div>
                            <div class="col-md-2 offset-md-3" id="capa3" >
                                <a  href="#" class="icon icon-twitter fa fa-bed cama" target="_blank">
                                    <strong style="font-size: 12px;">Ocupado
                                        <p align="center">
                                             <?= $ocu ?></p>
                                    </strong>
                                </a>
                            </div>
                            <div class="col-md-2 offset-md-3" id="capa1" >
                                <a href="#" class="icon icon-instagram fa fa-bed cama" target="_blank">
                                    <strong style="font-size: 12px; color: black">Reservado
                                        <p align="center">
                                            <?= $res ?></p>
                                    </strong></a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function mostrar(u) {
            window.location = u;
        }
        function formu_estadia(t) {
            //es = $(t).attr('estado');
            //if (es == "disponible"){
            window.location.href = $(t).attr("href")
            /* }else{
                 alert("habitación "+es);
             }*/

        }
        function fecha_atras(t) {
            var fecha=$("#fecha_view").val();

            window.location=$(t).attr('href')+"&date_ini="+fecha+"&date_end="+fecha;
        }
        function fecha_adelante(t) {
            var fecha=$("#fecha_view").val();
            window.location=$(t).attr('href')+"&date_ini="+fecha+"&date_end="+fecha;
        }
        function buscardatoshabitaciones(t) {
            var fecha=$(t).val();
            window.location=$('.datedate').attr('href')+"&date_ini="+fecha+"&date_end="+fecha;
        }
    </script