<div  id="usa">
<h3 align="center"><?php echo $habitaciones->isNew() ? "Registrar" : "Editar" ?> Habitaciones</h3>
<form method="post" action="<?php echo site_url('habitaciones/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($habitaciones->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="habitaciones_numero">Numero</label>
			<input class="form-control" id="habitaciones_numero" type="text" name="numero" value="<?php echo h($habitaciones->getNumero()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="habitaciones_descripcion">Descripcion</label>
			<input class="form-control" id="habitaciones_descripcion"  type="text" name="descripcion" value="<?php echo h($habitaciones->getDescripcion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="habitaciones_estado">Estado</label>
			<input class="form-control" onkeypress="return soloLetras(event)" id="habitaciones_estado" type="text" name="estado" value="<?php echo h($habitaciones->getEstado()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="habitaciones_piso">Piso</label>
			<input class="form-control" id="habitaciones_piso" type="text" name="piso" value="<?php echo h($habitaciones->getPiso()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="habitaciones_televisor_id">Televisores</label>
			<select class="form-control" id="habitaciones_televisor_id" name="televisor_id">
			<?php foreach (Televisores::doSelect() as $televisores): ?>
				<option <?php if ($habitaciones->getTelevisorId() === $televisores->getId()) echo 'selected="selected"' ?> value="<?php echo $televisores->getId() ?>"><?php echo $televisores->getDescripcion()?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="habitaciones_tipo_habitacion_id">Tipos Habitaciones</label>
			<select class="form-control" id="habitaciones_tipo_habitacion_id" name="tipo_habitacion_id">
			<?php foreach (TiposHabitaciones::doSelect() as $tipos_habitaciones): ?>
				<option <?php if ($habitaciones->getTipoHabitacionId() === $tipos_habitaciones->getId()) echo 'selected="selected"' ?> value="<?php echo $tipos_habitaciones->getId() ?>"><?php echo $tipos_habitaciones->getDescripcion()?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $habitaciones->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>