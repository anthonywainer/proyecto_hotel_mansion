<div class="container-fluid">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="email-app mb-4">
                <nav>
                    <h3 align="center"> Contactenos</h3>
                    <div class="back" align="center">
                        <label  class="letra"> <a href="<?=site_url('contactenos')?>">
                                <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
                            </a></label>
                    </div>

                    <div align="center" style="margin-top: 2%">
                        <a  href="<?php echo site_url('contactenos/eliminar/' . $contactenos->getId()) ?>"
                             class="button btn btn-outline-danger" data-icon="trash" title="Delete Contactenos"
                             onclick="return confirm('Are you sure?');">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          	</a>
                    </div>
                </nav>

<div class="action-buttons ui-helper-clearfix">
	<!--<a href="<?php echo site_url('contactenos/edit/' . $contactenos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Contactenos">
		Edit	</a>-->
	<!--<a href="<?php echo site_url('contactenos/eliminar/' . $contactenos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Contactenos"
		onclick="return confirm('Are you sure?');">
		Eliminar	</a>-->
</div>
                <div class="container-fluid">
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
<br>
    <div class="details">
        <div class="title">
            <strong class="field-label">Nombres:</strong>
            <?php echo h($contactenos->getNombres()) ?>
        </div>
        <div class="header">

            <div class="from">
                <div class="field-wrapper">
                    <strong class="field-label">Email:</strong>
                    <?php echo h($contactenos->getEmail()) ?>
                </div>
            </div>

        </div>

	<div class="field-wrapper">
		<strong class="field-label">Telefono:</strong>
		<?php echo h($contactenos->getTelefono()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Email:</strong>
		<?php echo h($contactenos->getEmail()) ?>
	</div>
	<div class="field-wrapper" style="width: 60%">
        <strong class="field-label">Asunto:</strong>

		<?php echo h($contactenos->getAsunto()) ?>

	</div>
	<div class="field-wrapper">
		<strong class="field-label">Estado:</strong>
		<?php echo h($contactenos->getEstado()) ?>
	</div>
        <br>
</div>
                </div>
</div>
        </div>
</div>