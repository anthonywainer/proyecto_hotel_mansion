<h1><?php echo $contactenos->isNew() ? "New" : "Edit" ?> Contactenos</h1>
<form method="post" action="<?php echo site_url('contactenos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($contactenos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="contactenos_nombres">Nombres</label>
			<input id="contactenos_nombres" type="text" name="nombres" value="<?php echo h($contactenos->getNombres()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="contactenos_telefono">Telefono</label>
			<input id="contactenos_telefono" type="text" name="telefono" value="<?php echo h($contactenos->getTelefono()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="contactenos_email">Email</label>
			<input id="contactenos_email" type="text" name="email" value="<?php echo h($contactenos->getEmail()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="contactenos_asunto">Asunto</label>
			<input id="contactenos_asunto" type="text" name="asunto" value="<?php echo h($contactenos->getAsunto()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="contactenos_estado">Estado</label>
			<input id="contactenos_estado" type="text" name="estado" value="<?php echo h($contactenos->getEstado()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $contactenos->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>