<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
    unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
    $_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid conceptos_de_pago-grid  table table-condensed table-striped table-bordered" cellspacing="0" id="table2">
    <thead>
    <tr>
        <th class="ui-widget-header ui-corner-tl">
            ID
        </th>
        <th class="ui-widget-header ">
            DESCRIPCIÓN
        </th>
        <th class="ui-widget-header ">
            TIPO DE PAGO
        </th>
        <th>
            Monto
        </th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($concepto_pagos as $key => $concepto_pagos): ?>
        <tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
            <td><?php echo h($concepto_pagos->getId()) ?>&nbsp;</td>
            <td><?php echo h($concepto_pagos->getDescripcion()) ?>&nbsp;</td>
            <td><?= TiposConceptoPago::getAll('where id='.$concepto_pagos->getMovimiento()->concepto_pago_id)[0]->getDescripcion() ?>&nbsp;</td>
            <th>
                <?php echo h($concepto_pagos->getMonto()) ?>&nbsp;
            </th>
            <td>
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('concepto-pagos/editar/' . $concepto_pagos->getId()) ?>">
                    <i class="fa fa-edit "></i>
                </i>
                <a class="btn btn-outline-danger" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('concepto-pagos/eliminar/' . $concepto_pagos->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>

            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>