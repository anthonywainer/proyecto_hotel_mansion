<div id="usa">
    <h6 align="center"><?php echo $concepto_pago->isNew() ? "NUEVO" : "EDITAR" ?> CONCEPTOS DE PAGO</h6>
    <form method="post" action="<?php echo site_url('concepto-pagos/guardar') ?>">
        <input type="hidden" name="movimiento_id" value="<?=$concepto_pago->movimiento_id?>">

        <div class="form-inline">
            <?php foreach(TiposConceptoPago::getAll() as $tp): ?>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" required value="<?= $tp->id ?>"
                               class="form-check-input conpag"
                            <?php if($concepto_pago->movimiento_id) if(MovimientosDeDinero::getAll('where id='.$concepto_pago->movimiento_id)[0]->getConcepto_pago_id()==$tp->id){echo "checked";} ?>
                               name="concepto_pago_id">
                        <?= $tp->getDescripcion() ?>
                    </label>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="ui-widget-content ui-corner-all ui-helper-clearfix">
            <input type="hidden" name="id" value="<?php echo h($concepto_pago->getId()) ?>" />
            <div class="form-field-wrapper">
                <label class="form-field-label" for="conceptos_pago_descripcion">Descripcion</label>
                <input  type="text" name="descripcion" required
                        class="form-control"
                        value="<?php echo h($concepto_pago->getDescripcion()) ?>" />
            </div>
            <div class="form-field-wrapper">
                <label class="form-field-label" for="conceptos_de_pago_tipo_de_pago_id">Monto</label>
                <input class="form-control" type="number"
                       title="Monto insuficiente"
                       min="0" required onchange="check_max(this,<?=$max?>)" onkeyup="check_max(this,<?=$max?>)" name="monto">
            </div>

        </div>
        <hr>
        <div class="form-action-buttons ui-helper-clearfix" align="right">
            <span class="button" data-icon="disk">
			<input type="submit"
                   class="btn btn-info" style="background: #3C8DBC; color: white"
                   value="<?php echo $concepto_pago->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
            <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
                <a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                    Cancelar
                </a>
            <?php endif ?>


        </div>
    </form>
</div>
