<h1>View Concepto Pago</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('concepto-pagos/edit/' . $concepto_pago->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Concepto_pago">
		Edit	</a>
	<a href="<?php echo site_url('concepto-pagos/delete/' . $concepto_pago->getId()) ?>"
		class="button" data-icon="trash" title="Delete Concepto_pago"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($concepto_pago->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Movimiento</span>
		<?php echo h($concepto_pago->getMovimientosDeDineroRelatedByMovimientoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($concepto_pago->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($concepto_pago->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($concepto_pago->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Monto</span>
		<?php echo h($concepto_pago->getMonto()) ?>
	</div>
</div>