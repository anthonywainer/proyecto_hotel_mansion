<div class="container-fluid">
    <div id="usa">
<h1 align="center">Asignar Acciones</h1>
<form action="<?= site_url($u.$id) ?>" method="post">

<table class="table-bordered table-light table-hover align-content-center " style="margin-left: 2%">
    <tr>
        <td style="width: 8%">NOMBRE</td>
        <td style="width: 6%">ACCIONES</td>
    </tr>
   <?php foreach ( Acciones::getAll() as $a): ?>

      <tr><td><label for=""><?= $a->accion ?></label></td>
      <td><input type="checkbox" name="acciones[]" <?php
        $p = Permisos::getAll('WHERE '.$query.' and idaccion= '.$a->id);
        if (!empty($p)){
            echo "checked";
        }
        ?> value="<?= $a->id ?>"></td>
       <?php endforeach; ?>
    </tr>
</table>
        <br>
    <span data-icon="disk" >
			<input  style="background: #3C8DBC; color: white" class="btn btn-primary"  type="submit" value="Guardar" />
    </span>
    <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
        <a class="btn btn-danger" data-icon="cancel" data-dismiss="modal" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
            Cancelar
        </a>
    <?php endif ?>



</form>
</div>
</div>