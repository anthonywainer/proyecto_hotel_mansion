<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid submodulo-grid table table-condensed table-bordered table table-striped "  cellspacing="0" id="table-responsive">
	<thead >
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					URL
			</th>
			<th class="ui-widget-header ">
					ICON
			</th>
			<th class="ui-widget-header ">
					IDMÓDULO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($submodulos as $key => $submodulo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($submodulo->getId()) ?>&nbsp;</td>
			<td><?php echo h($submodulo->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($submodulo->getUrl()) ?>&nbsp;</td>
			<td><?php echo h($submodulo->getIcon()) ?>&nbsp;</td>
			<td><?php echo Modulos::getAll('where id='.$submodulo->getIdModulo())[0]->nombre ?>&nbsp;</td>
			<td>
                <i  class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('submodulos/editar/' . $submodulo->getId()) ?>">
                    <i  class="fa fa-edit fa-lg  "></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Submodulo','¿Está seguro de Eliminar?','<?php echo site_url('submodulos/eliminar/' . $submodulo->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg  color-icon"></i>
                </a>
                <i class="btn btn-outline-secondary" onclick="openmodal(this)" href="<?= site_url('submodulos/permisos/' . $submodulo->getId()) ?>">
                    <i class="fa fa-unlock-alt fa-lg" aria-hidden="true"></i>

                </i>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>