<header>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php echo site_url('css/StylePrincipal.css', true) ?>s">
    <link rel="stylesheet" href="<?php echo site_url('css/font-awesome.css', true) ?>">
</header>

<body>

<ul class="horizontal" style="margin-top: 1%;margin-bottom: 8%">
    <li class="inicio"><a href="<?php echo site_url('admin') ?>"><i class="fa fa-info-circle " aria-hidden="true"></i>INICIO
            </i></a></li>
    <li class="clientes" ><a href="#"><i class="fa fa-user " aria-hidden="true"></i>CLIENTES
            <i class="izquierda fa fa-chevron-down" aria-hidden="true"></i></a>
        <ul><li class="sub-menu-clientes"><a href="<?php echo site_url('clientes') ?>" class="subtitulos">Registrar Cliente</a></li></ul>

    </li>
    <li class="ventas"><a href="#"><i class="fa fa-shopping-cart " aria-hidden="true"></i>VENTAS
            <i class="izquierda fa fa-chevron-down" aria-hidden="true"></i></a>
        <ul><li class="sub-menu-ventas"><a href="<?php echo site_url('ventas-hotels') ?>" class="subtitulos">Registrar Venta</a></li></ul>

    </li>
    <li class="recepcion"><a href="<?php echo site_url('habitaciones/?ide=')?>"><img class="recepcionista" src="<?php echo site_url('imagenes/recepcionista.png') ?>"> RECEPCIÓN
        </a></li>
    <li class="informes"><a href="#"><img  class="papel" src="<?php echo site_url('imagenes/papel.png') ?>"> INFORMES
            <i class=" izquierda fa fa-chevron-down" aria-hidden="true"></i></a>
        <ul>
          <!--  <li class="sub-menu-informe"><a href="" class="subtitulos">Reporte Ingreso</a></li>-->
            <li class="sub-menu-informe"><a href="<?php echo site_url('reporteestadia') ?>" class="subtitulos">Reporte Estadía</a></li>

        </ul></li>
</ul>
<div class="row informacion">
    <div class="col-xs-12 col-sm-12 col-md-4">
        <img src="<?php echo site_url('imagenes/group.png') ?>" class="iconos" alt="">
        <span class="information-title">Clientes Registrados</span>
        <span class="numeros">
            <?= count(Clientes::getAll())?>
        </span>
    </div>
    <div class=" col-xs-12 col-sm-12 col-md-4">
        <img src="<?php echo site_url('imagenes/slipin.png') ?>" class="iconos" alt="">
        <span class="information-title">Habitaciones Ocupadas</span>
        <span class="numeros">
            <?= count(Habitaciones::getAll( 'where estado="ocupado"'))?>
        </span>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <img src="<?php echo site_url('imagenes/camita.png') ?>" class="iconos" alt="">
        <span class="information-title">Habitaciones Reservadas</span>
        <span class="numeros">
            <?= count(Habitaciones::getAll('where estado = "reservado"'))?>
        </span>
    </div>

</div>

<div class="row informacion secundary" style="margin-bottom: 1%">

    <div class="col-xs-12 col-sm-6 col-md-4 ">
        <img src="<?php echo site_url('imagenes/camita.png') ?>" class="iconos" alt="">
        <span class="information-title">Habitaciones Disponibles</span>
        <span class="numeros">
            <?= count(Habitaciones::getAll('where estado= "disponible"')) ?>
        </span>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <img src="<?php echo site_url('imagenes/carrito.png') ?>" class="iconos" alt="">
        <span class="information-title">Ingresos Diarios</span>
        <span class="numeros">40</span>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <img src="<?php echo site_url('imagenes/bolso .png') ?>" class="iconos" alt="">
        <span class="information-title">Caja Diaria</span>
        <span class="numeros">
            <?= count(Cajas::getAll('where estado = "1"'))?>
        </span>

    </div>
</div>
</body>
