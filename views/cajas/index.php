<ul class="nav nav-tabs justify-content-end">
    <li class="nav-item">
        <a class="nav-link" href="<?= site_url('cajas/mostrar') ?>">
            <i class="icon-calculator"></i> Caja</a>

    </li>
    <li class="nav-item">
        <a class="nav-link active" href="<?= site_url('cajas') ?>"> <i class="icon-doc" aria-hidden="true"></i>  Historial Caja</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link " href="<?= site_url('concepto-pagos') ?>">Pagos</a>
    </li>
</ul>

<div class="container-fluid">
<div class="ui-widget-content ui-corner-all">
    <div class="row">
        <div class="form-group  col-md-4">

            <div class="input-group"  style="margin-left: 5%">

                <input id="filtrar" type="search" onkeyup="buscar_tabla_html(this)" class="form-control" placeholder="Buscar Cajas">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>

            </div>
        </div>
    </div>
    <?php View::load('cajas/grid', $params) ?>
</div>
</div>

<?php View::load('pager', compact('pager')) ?>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>