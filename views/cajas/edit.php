<div id="usa">
<h6>
    <div align="center" >
    <?php echo $estado ? "APERTURAR" : "CERRAR" ?> CAJA</h6>


<form method="post" action="<?php echo site_url('cajas/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($cajas->getId()) ?>" />
        <?php if ($estado): ?>
		<div class="form-field-wrapper">
            <input type="hidden" name="estado" value="1">
			<label class="form-field-label" for="cajas_saldo_inicial">Monto Apertura</label>
            <!-- por el momento agregare un if que valide si quiere con cero o con el monto cerrado el dia anterior pero no mandare nada
             que lea el if por el momento siempre ira a 0-->
			<input required class="form-control" min="0"  id="cajas_saldo_inicial" type="number" name="saldo_inicial" value="<?php if(!$monto_ap){echo $monto_ap;}else{echo 0;}  ?>" />
		</div>
        <input type="hidden" name="fecha_apertura" value="<?= date("Y-m-d H:i:s") ?>" />
        <?php else: ?>
            <div class="form-field-wrapper">
                <label class="form-field-label" for="cajas_saldo_final">Monto Cerrado</label>
                <input required class="form-control" min="0"  id="cajas_saldo_final" type="number" name="saldo_final"
                       value="<?php echo h($cajas->getSaldoInicial()) ?>" />
            </div>
            <input type="hidden" name="estado" value="0">
            <input type="hidden" name="fecha_cierre" value="<?= date("Y-m-d H:i:s") ?>" />
        <?php endif; ?>
		<input type="hidden" value="<?= $_SESSION['user']->id ?>" name="usuario_id">
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="button" data-icon="disk">
			<input class="btn btn-primary" type="submit" value="<?php echo $cajas->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>