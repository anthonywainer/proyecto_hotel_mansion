<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
    unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
    $_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid cajas-grid table  table-condensed table-striped table-bordered" cellspacing="0" id="table-responsive">
    <thead>
    <tr>
        <th class="ui-widget-header ui-corner-tl">
            ID
        </th>
        <th class="ui-widget-header ">
            Saldo Inicial
        </th>
        <th class="ui-widget-header ">
            Saldo Final
        </th>
        <th class="ui-widget-header ">
            FECHA CERRADA
        </th>
        <th class="ui-widget-header ">
            FECHA ABIERTA
        </th>
        <th class="ui-widget-header ">
            USUARIO
        </th>


    </tr>
    </thead>
    <tbody>
    <?php foreach ($cajas as $key => $cajas): ?>
        <tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
            <td><?php echo h($cajas->getId()) ?>&nbsp;</td>
            <td><?php echo h($cajas->getSaldo_Inicial()) ?>&nbsp;</td>
            <td><?php echo h($cajas->getSaldo_Final()) ?>&nbsp;</td>
            <td><?php echo h($cajas->getFechaCierre()) ?>&nbsp;</td>
            <td><?php echo h($cajas->getFechaApertura()) ?>&nbsp;</td>
            <td><?php $idu=$cajas->getUsuarioId(); $u = Usuarios::getAll('WHERE id='.$idu)[0]; echo($u->nombres.' '.$u->apellidos);?>&nbsp;</td>

        </tr>
    <?php endforeach ?>
    </tbody>
</table>