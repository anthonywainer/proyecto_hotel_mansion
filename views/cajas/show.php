<?php
if ($estado) {
    $ccc = new Cajas;
    $in = $ccc->getIngresos($estado);
    $ni = $ccc->getIngresosN($estado);
    $nih = $ccc->getIngresosH($estado);

    $eg = $ccc->getEgresos($estado);

    $in[0]+=$ni[0]+$nih[0];
    $in[1]+=$ni[1]+$nih[1];
}
?>

<style>
    .card_caja{
        height: 200px;
    }
    .card-body{
        text-align: center;
    }
    .caja_center{
        line-height: 70px;
    }
    .card {
        min-height: 15em;
    }
</style>
<ul class="nav nav-tabs justify-content-end">
    <li class="nav-item">
        <a class="nav-link active" href="<?= site_url('cajas/mostrar') ?>">
            <i class="icon-calculator"></i> Caja</a>

    </li>
    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('cajas') ?>"> <i class="icon-doc" aria-hidden="true"></i>  Historial Caja</a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="<?= site_url('concepto-pagos') ?>">Pagos</a>
    </li>
</ul>
<br><br>
<div class="container-fluid">

<div class="col-md-12" align="center">
<?php if(isset($estado)) if (empty($estado)): ?>
    <i href="<?php echo site_url('cajas/aperturar') ?>" onclick="openmodal(this)"
       class="button"
       data-icon="plusthick"
       title="Aperturar Cajas">
        <button class="btn btn-outline-primary" >
            Aperturar Caja
        </button>
    </i>
<?php else: ?>
    <?php $moc = ($estado[0]->saldo_inicial+$in[0]-$eg[0]);

    ?>
    <a
            class="btn btn-danger"
            data-icon="carat-1-e"
            href="<?php echo site_url('cajas/cerrar/' . $estado[0]->getId().'?saldo_final='.$moc) ?>">
        Cerrar Caja
    </a>
<?php endif; ?>
</div>
<?php if($estado): ?>

    <div class="row">

        <div class="col-sm-6 col-md-4 card_caja">
            <div class="card border-primary card_caja">
                <div class="card-header">
                    <strong>Fecha de Apertura:</strong> <?= $estado[0]->fecha_apertura ?>
                </div>
                <div class="card-body" style="background-color: #40E0D0">
                    <strong>
                        <h1 class="caja_center">
                            S/.  <?= $estado[0]->saldo_inicial+$in[0]-$eg[0] ?>
                        </h1>
                    </strong>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-4 card_caja">
            <div class="card border-success card_caja">
                <div class="card-header">
                    Monto Total de Ingresos
                </div>
                <div class="card-body bg-success" >
                    <div class="row">
                        <div class="col-md-6">
                            Efectivo:
                            <strong>

                                <?php
                                if ($in[0]){
                                    echo "<h3 class=\"caja_center\"> S/. ".$in[0]."</h3>";
                                }else{
                                    echo "<p class=\"caja_centro\" style='font-size: 14px; color:white; text-align: justify; line-height: 22px'>No tiene ninguna venta en efectivo</p>";
                                }
                                ?>
                            </strong>
                        </div>
                        <div class="col-md-6">
                            Tarjeta:
                            <?php $nq = "
                                    SELECT
                                        sum(ventas.cuota) AS monto,
                                        count(ventas.cuota) cant
                                    FROM
                                        movimientos_de_dinero
                                    INNER JOIN ventas ON ventas.movimiento_id = movimientos_de_dinero.id
                                    WHERE
                                        movimientos_de_dinero.caja_id = " . $estado[0]->id . "
                                    AND movimientos_de_dinero.concepto_pago_id = 1
                                    AND movimientos_de_dinero.forma_pago_id = 2   
                        ";
                            $inT = Cajas::getConnection()->query($nq)->fetch();
                            if ($inT[0]){
                                echo '<h3 class="caja_center"> S/. '.$inT[0].'</h3>';
                            }else{
                                echo '<p class="caja_centro" style=" color: white; font-size: 14px; color:white; text-align: justify; line-height: 22px">No tiene ninguna venta con Tarjeta</p>';
                            }
                            ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>



        <div class="col-sm-6 col-md-4 card_caja">
            <div class="card border-warning card_caja">
                <div class="card-header">
                    Monto Total de Egresos
                </div>
                <div class="card-body " style="background-color: #e9c341;">
                    <strong>

                        <?php

                        if ($eg[0]){
                            echo '<h1 class="caja_center"> S/. '.$eg[0].'</h1>';
                        }else{
                            echo '<h5 class="caja_center" style="color:white;">No tiene ningún egreso</h5>';
                        }
                        ?>

                    </strong>
                </div>
            </div>
        </div>

        <div class="col-md-6  offset-md-3" >
            <br><br>
            <table class="table table-hover table-sm">
                <thead>
                <tr>
                    <th colspan="2">
                        Monto Aperturado:
                    </th>
                    <th>
                        S/.  <?= $estado[0]->saldo_inicial ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        Forma de Pago
                    </th>
                    <th>
                        Cant. Transacciones
                    </th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Efectivo</td>
                    <td>
                        <?php
                        if ($in[1]){
                            echo $in[1];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($in[0]){
                            echo "S/. ".$in[0];
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Tarjeta</td>
                    <td>
                        <?php
                        if ($inT[1]){
                            echo $inT[1];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($inT[0]){
                            echo "S/. ".$inT[0];
                        }
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php endif; ?>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>