<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand navbar-link"><img src="<?= site_url("assets/img/logo.jpg", true)  ?>"></a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" ><a href="<?= site_url('/') ?>" class="navbar-1">INICIO </a></li>
                <li role="presentation" class="active"><a href="<?= site_url('/nosotros') ?>" class="navbar-1">NOSOTROS </a></li>
                <li role="presentation" class="navbar-1"><a href="<?= site_url('/habitacion') ?>" class="navbar-1">HABITACIONES </a></li>
                <li role="presentation"><a href="<?=site_url('/servicios') ?>" class="navbar-1">SERVICIOS </a></li>
                <li role="presentation"><a href="<?=site_url('/galeria') ?>" class="navbar-1">GALERÍA </a></li>
                <li role="presentation" ><a href="<?= site_url('/contactenos') ?>" class="navbar-1">CONTÁCTENOS </a></li>
                <li role="presentation" id="lista-login">
                    <div id="cuadrito-login">
                        <?php if ($_SESSION): ?>
                            <a href="<?= site_url('/admin') ?>" class="">Aceder al Sistema</a>
                        <?php else: ?>
                            <a href="<?= site_url('/login') ?>" class="login">INICIAR SESIÓN</a>
                        <?php endif; ?>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <div class="col-md-12" id="nosotros" align="center">
        <h2>NOSOTROS</h2>
    </div>
</div>
<div class="row" id="nosotros-c">
    <div class="col-md-8" id="descripcion-no" align="center">
        <p align="justify">
            DUEÑOS:
            Hoteles GMR tiene como propietarios a: Lic. Gabriel  Chacón Quito y Marco Antonio Bartra Leveu.

            HISTORIA:
            Hoteles GMR (EL HOSPEDAJE LA MANSION) A sido creado el 21 de enero  del 2016  como una sociedad anónima cerrada conformada por dos  socios: el licenciado Gabriel Chacón Quito y Marco Antonio Bartra Leveu, dos  grandes amigos que  decidieron  emprender su propio negocio al que llamaron GMR el nombre se debe a que son las iniciales de los socios; como no contaban  con recursos económicos  suficientes  para construir su propio local, se les presento la oportunidad  de  alquilar el local del hospedaje “LA MANSION” ubicado en JR MAYNAS # 286 quienes sus  dueños son personas de avanzada edad debido a ello decidieron alejarse de los negocios y dar en alquiler el hospedaje.
            El hospedaje ya tiene en el mercado un aproximado de diez años que brinda el servicio de alquiler de habitaciones diarias; cuenta con cincuenta habitaciones que se clasifican en: SW(simple),MW(matrimoniales),DW(doble),CW(cuadruple),QW(quintuples), cada habitación tiene un precio según el tipo de habitación que sea, los precios están entre 50 y 70,además cuenta con una piscina y cochera
            .
        </p>
    </div>
    <div class="col-md-4" style="padding-right: 30px; padding-left: 20px; padding-top: 20px">
        <div id="cuadrito-no" >
            <br>
            <strong><h4 align="center">CONTACTO</h4></strong><br>
            <div style="padding-left: 40px">
                <a class="contacto"><strong>Teléfono </strong> 042 532227</a><br><br>
                <a class="contacto"><strong>Email</strong>       hospedaj@elamansion.com </a><br><br>
                <a class="contacto"><strong>Dirección</strong>    Jr.Maynas 286 042 Tarapoto</a> <br><br>
                <a class="contacto"><strong>WhattsApp</strong>    957673499</a>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4 servicios" align="center" >

    </div>
    <div class="col-md-4 servicios" align="center">

    </div>
    <div class="col-md-4 servicios" align="left" style="padding-top: 0px">
        <h3>REDES SOCIALES</h3>
        <div class="form-inline">

            <a href="" target="_blank">
                <i class="fa fa-3x fa-twitter-square"></i>
            </a>
            <a href="https://www.facebook.com/pages/Hotel-La-Mansion-Tarapoto/267925833287375" target="_blank">
                <i  class="fa fa-3x fa-facebook-square" style="color: blue"></i>
            </a>
            <a href="" target="_blank">
                <i class="fa fa-3x fa-instagram" style="color: #2A5B83"></i>
            </a>
        </div>

    </div>
</div>