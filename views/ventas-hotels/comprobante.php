<style>
    #comprobante{
        background: white;
        border: solid 1px skyblue;
        border-radius: 10px;
        padding: 60px;
    }
    .logo{
        width: 20%;
margin-left: 2%;
    }
    #cabecera{
        margin-top: -8%;
    }
label{
    border-bottom: solid 2px black;
}
    #descripcion{
        margin-top: 1%;
        text-align: center;
    }
</style>
<div class="container" id="comprobante">
    <a href="<?=site_url('ventas-hotels')?>">Atras</a>
    <img src="<?php echo site_url('imagenes/mansion.jpg') ?>" class="logo" alt="" >

<h2 align="center" id="cabecera">"HOTEL MANSIÓN"</h2>
    <?php $mmm = VentasHotel::getAll('where id='.$id)[0]; ?>
    <h3 align="center">
        <?php if ($mmm->getMovimiento()->getTipoComprobante()->id==1){echo "Boleta: ";}else{echo "Factura";} ?>
        N° 00000 <?= $mmm->id ?></h3>
    <p id="descripcion">Jr.Maynas 286  <i class="fa fa-phone-square" aria-hidden="true"></i>(042) 532227 San Martín-Tarapoto-Peru</p>
<p>
        <?php if ($mmm->getMovimiento()->getTipoComprobante()->id==1){?>
        <strong>Cliente:</strong>
            <label><?= $mmm->getMovimiento()->getCliente()->nombres.' '.$mmm->getMovimiento()->getCliente()->apellidos ?></label>
            <strong  style="margin-left: 20%"> Dni:</strong>
            <label><?= $mmm->getMovimiento()->getCliente()->dni ?></label>
            <br>
                <strong>Dirección:</strong>
                <label><?= $mmm->getMovimiento()->getCliente()->direccion ?></label>
        <?php }else{ ?>
            <strong >Razón Social:</strong>
            <label><?= $mmm->getMovimiento()->getCliente()->nombres ?></label>

        <strong style="margin-left: 20%">Ruc:</strong>
            <label><?= $mmm->getMovimiento()->getCliente()->ruc ?></label>
            <br>
            <strong>Dirección: </strong>
            <label><?= $mmm->getMovimiento()->getCliente()->direccion ?></label>

        <?php } ?>
    </p>

<br>
<table class="table table-sm table-bordered">
    <thead>
        <tr>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>P/U</th>
            <th>Monto</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(ProductoConsumidoHotel::getAll('where hotel='.$id) as $pc): ?>
            <tr>
                <td><?= $pc->getProducto()->nombre ?></td>
                <td><?= $pc->getCantidad() ?></td>
                <td><?= $pc->getProducto()->precio ?></td>
                <td><?= $pc->getProducto()->precio*$pc->getCantidad() ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfooter>
        <tr>
            <td colspan="3">SubTotal</td>
            <td >S/.<?= $mmm->monto ?></td>
        </tr>
        <tr>
            <td colspan="3">Efectivo</td>
            <td >S/.<?= $mmm->efectivo ?></td>
        </tr>
        <tr>
            <td colspan="3">Vuelto</td>
            <td >S/.<?= $mmm->efectivo-$mmm->monto ?></td>
        </tr>
    </tfooter>
</table>
</div>


