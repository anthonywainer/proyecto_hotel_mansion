<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid ventas_hotel-grid table table-condensed table-bordered" cellspacing="0" id="table2">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasHotel::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasHotel::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>

			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasHotel::MOVIMIENTO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasHotel::MOVIMIENTO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha
				</a>
			</th>

			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => VentasHotel::MONTO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == VentasHotel::MONTO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Monto
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">Aciones&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ventas_hotels as $key => $ventas_hotel): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($ventas_hotel->getId()) ?>&nbsp;</td>
			<!--<td><?php echo h($ventas_hotel->getMovimientosDeDineroRelatedByMovimientoId()) ?></td>-->
            <td><?php echo $ventas_hotel->getMovimientosDeDinero($ventas_hotel->getMovimientoId())->created_at ?>&nbsp;</td>
			<td><?php echo h($ventas_hotel->getEfectivo()) ?></td>
			<td>
				<a
					class="button btn btn-outline-primary"
					data-icon="pencil"
					title="Edit Ventas_hotel"
					href="<?php echo site_url('ventas-hotels/editar/' . $ventas_hotel->getId()) ?>">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
				<a
					class="button btn btn-outline-danger"
					data-icon="trash"
					title="Delete Ventas_hotel"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('ventas-hotels/eliminar/' . $ventas_hotel->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>