<link rel="stylesheet" href="<?php echo site_url('css/Sestadia.css', true) ?>">

<?php $sqlc= "
SELECT
productos.nombre,
productos.precio,
productos.id as idp,
producto_consumido_hotel.id,
producto_consumido_hotel.monto,
producto_consumido_hotel.cantidad
FROM
producto_consumido_hotel
INNER JOIN productos ON producto_consumido_hotel.producto_id = productos.id
WHERE
producto_consumido_hotel.hotel= 
".$id;
$productos = ProductoConsumido::getConnection()->query($sqlc)->fetchAll();
if ($productos): ?>

    <table class="table table-bordered table-sm">
        <thead>
        <tr>
            <th id="uno">cantidad</th>
            <th id="th">Producto</th>
            <th id="th">Precio</th>
            <th id="th">Total</th>
            <th id="th">Acciones</th>
        </tr>
        </thead>
        <tbody class="tbod">
        <?php

        $m = 0;
        foreach ($productos as $c): ?>
            <tr>
                <!--<td>
        <?//= $c['idp'] ?>
        </td>-->
                <td id="uno">
                    <?= $c['cantidad'] ?>
                </td>
                <td class="celda"> <?= $c['nombre'] ?></td>

                <td class="celda">
                    <?= "S/. ".$c['precio'] ?>
                </td>
                <td class="celda"> <?php $m+=$c['cantidad']*$c['precio']; echo "S/. ".$c['cantidad']*$c['precio'] ?>  </td>
                <td class="celda">
                    <a class="btn btn-outline-danger" href="#"
                       onclick="conf_eliminar('Eliminar Producto','¿Está seguro de Producto?','<?php echo site_url('ventas-hotels/eliminar/'.$c['id']."/?elip=") ?>')">
                        <i class="fa fa-trash-o fa-lg" ></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        <input type="hidden" id="monto" name="monto" value="<?= $m ?>">
        </tbody>
    </table>
<?php endif; ?>
<div class="row">
<div class="col-md-4">
    <?php $mmm =  MovimientosDeDinero::getAll('where id='.$ventas->movimiento_id)[0]; ?>
   <strong> Forma de pago:</strong> <br>

    <select name="forma_pago_id" id="forma_pago_id" required class="form-control">
        <?php foreach (FormasPagos::getAll() as $fp){  ?>
            <option <?php if($mmm->forma_pago_id == $fp->id){echo "selected";} ?>
                    value="<?= $fp->id ?>"><?= $fp->getDescripcion() ?></option>
        <?php } ?>
    </select>
</div>
<div class="col-md-4">
    <strong>Efectivo:</strong> <br>
    <div class="input-group">
        <span class="input-group-addon"><STRONG>S/.</STRONG></span>
    <input onchange="vuelto(this);" onkeyup="vuelto(this);" type="number"
           step="any"
           name="efectivo"
           value="<?php if($ventas->getEfectivo() > 0) {
               echo $ventas->getEfectivo();
           }else{
               echo $m;
           }
           ?>"
        class="form-control-sm"
    >
    </div>
</div>
    <div class="col-md-2" id="vuelt">
       <strong> Vuelto:</strong>
        <div class="input-group">
            <span class="input-group-addon"><STRONG>S/.</STRONG></span>
        <span class="form-control" id="resto">

        </span>
            </div>
    </div>
</div>
<script>
    function vuelto(t) {
        var monto = $("#monto").val();
        var rest=$(t).val();
        var montototal=rest-monto;
        $("#resto").html(montototal);
        if(rest<monto){
            alert("ingresar monto mayor al total ");
        }
    }
</script>