<style>
    .oculta{
        display: none;
    }
    .ti{
        font-size: 20px; margin-left: 30%;
        font-weight: bold;
    }
    #right{
      margin-left: 45%;
    }
    #plus{
        display:inherit; margin-top: -8%; margin-left: 102%
    }
    @media only screen and (max-width : 320px) {
        #list_prod{
            display: block;
            overflow-x: auto;
            overflow-y: auto;

        }
    }
</style>
<div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header" style="background-color: #c0c0c0">

                    <label> <a href="<?=site_url('ventas-hotels/')?>">
                            <i style="color: black" class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
                        </a></label>

              <label class="ti"><?php echo $ventas_hotel->isNew() ? "" : "" ?> Ventas Hotel</label>
                <i class="fa fa-edit" id="right"></i>
                <div class="card-actions">
                </div>
            </div>
            <div class="card-body">


<form method="post" action="<?php echo site_url('ventas-hotels/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ventas_hotel->getId()) ?>" />
        <input type="hidden" name="movimiento_id" value="<?php echo h($ventas_hotel->movimiento_id) ?>" />
        <?php $mmm =  MovimientosDeDinero::getAll('where id='.$ventas_hotel->movimiento_id)[0]; ?>
        <div class="row">



            <div class="col-md-3">
              <strong> Tipos Comprobantes:</strong>  <br>
                <select name="tipo_comprobante_id" onchange="cambiartipo(this)"
                        id="tipo_comprobante_id" class="form-control">
                    <?php foreach (TiposComprobantes::getAll() as $tc){?>
                        <option <?php if( $tc->id){echo "selected";} ?> value="<?= $tc->id ?>"><?= $tc->getDescripcion() ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-4 <?php if ($mmm->tipo_comprobante_id==3){echo 'oculta';}?>" id="cdni" >
                <strong>Cliente: </strong><br>
                <select name="cliente_id" <?php if ($mmm->tipo_comprobante_id==3){echo 'disabled';}?>
                        class="form-control" id="cliente_n_dni" >
                    <option value="">--> Seleccione un cliente <--</option>
                    <?php foreach (Clientes::getAll('where clientes.dni is not null') as $u): ?>
                        <option <?php if($mmm->cliente_id == $u->id){echo "selected";} ?>
                                value="<?= $u->id ?>"><?php echo $u->dni.', '.$u->nombres.' '.$u->apellidos ?></option>
                    <?php endforeach; ?>
                </select>
                <span  id="plus" >
                <i style="color: black; cursor: pointer; "
                   href="<?php echo site_url('clientes/editar?cli=dni') ?>"
                   onclick="openmodal(this)"
                   class="fa fa-plus-circle fa-lg"></i>
                    </span>
            </div>
            <div class="col-md-4 <?php if ($mmm->tipo_comprobante_id==1){echo 'oculta';}?>"  id="cruc">
               <strong> Cliente:</strong> <br>
                <select name="cliente_id" <?php if ($mmm->tipo_comprobante_id==1){echo 'disabled';}?>  class="form-control-sm" id="cliente_n_ruc"  style="width: 60%">
                    <option value="">--> Seleccione un cliente <--</option>
                    <?php $ruc = Clientes::getAll('where clientes.ruc is not null');
                    if ($ruc)
                        foreach ($ruc as $u): ?>
                            <option <?php if($mmm->cliente_id == $u->id){echo "selected";} ?> value="<?= $u->id ?>"><?php echo $u->ruc.', '.$u->nombres ?></option>
                        <?php endforeach; ?>
                </select>
                <span id="plus">
                <i style="color: black; cursor: pointer "
                   href="<?php echo site_url('clientes/editar?cli=ruc') ?>"
                   onclick="openmodal(this)"
                   class="fa fa-plus-circle fa-lg"></i></span>
            </div>
        </div>
        <br>
        <strong>Productos:</strong> <br>
        <div class="form-inline">
            <select class="form-control-sm" id="producto_id" >
                <option value="">Seleccione producto...</option>
                <?php foreach (Productos::getAll() as $pp): ?>
                    <option value="<?= $pp->id ?>" ><?= $pp->getNombre().' S/.'.$pp->getPrecio()  ?></option>
                <?php endforeach; ?>
            </select>

            <input id="canti" class="form-control-sm"
                   onblur="agregar_pro_ven(this); return false"
                   href="<?= site_url('ventas-hotels/guardar/'.$ventas_hotel->id.'/?sa=1') ?>"
                   type="number"
                   min="1"
                   placeholder="cantidad:" style="margin-left: 4%">
        </div>

        <br>
        <div id="list_prod">
            <?php View::load('ventas-hotels/list_productos',['id'=>$ventas_hotel->id,'ventas'=>$ventas_hotel]) ?>
        </div>

	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input class="btn btn-outline-primary" type="submit" value="<?php echo $ventas_hotel->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button btn btn-outline-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</div>

</form>
        </div>
    </div>
    </div>
</div>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <!--  <div class="modal-footer">
             </div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</div>

<script>
    function agregar_pro_ven(t) {
        var p = $("#producto_id").val();
        if (!p){
            toastr.warning('seleccione un producto!');
        }

        if (!$('#canti').val()){
            toastr.warning('seleccione cantidad!');
            return false;
        }else {
            $.get($(t).attr('href') + '&producto_id=' + p + '&cantidad=' + $('#canti').val()).done(
                function (d) {
                    $("#list_prod").html(d);
                    calcular_precio();
                }
            );
        }
        return false;
    }
    function registrarD(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            toastr.success('Guardado Correctamente!');
            nom = $("#dni").val()+","+$("#nombre").val()+" "+$("#ape").val();
            var option = new Option(nom, id);
            $("#cliente_n_dni").append(option);
            $("#cliente_n_dni").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function registrarR(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            toastr.success('Guardado Correctamente!');
            nom = $("#ruc").val()+","+$("#nombre").val();
            var option = new Option(nom, id);
            $("#cliente_n_ruc").append(option);
            $("#cliente_n_ruc").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function cambiartipo(t) {
        ti = $(t).val();
        if(ti == 1){
            $("#cruc").addClass('oculta');
            $("#cdni").removeClass('oculta');
            $("#cliente_n_ruc").prop('disabled',true);
            $("#cliente_n_dni").prop('disabled',false);
        }else{
            $("#cruc").removeClass('oculta');
            $("#cdni").addClass('oculta');
            $("#cliente_n_dni").prop('disabled',true);
            $("#cliente_n_ruc").prop('disabled',false);
        }
    }
</script>