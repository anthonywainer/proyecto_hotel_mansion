<h1><?php echo $ventas->isNew() ? "New" : "Edit" ?> Ventas</h1>
<form method="post" action="<?php echo site_url('ventas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ventas->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_fecha_venta">Fecha Venta</label>
			<input id="ventas_fecha_venta" type="text" name="fecha_Venta" value="<?php echo h($ventas->getFechaVenta(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_estado">Estado</label>
			<input id="ventas_estado" type="text" name="estado" value="<?php echo h($ventas->getEstado()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_estadia_id">Estadia Id</label>
			<input id="ventas_estadia_id" type="text" name="estadia_id" value="<?php echo h($ventas->getEstadiaId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_created_at">Created At</label>
			<input id="ventas_created_at" type="text" name="created_at" value="<?php echo h($ventas->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_updated_at">Updated At</label>
			<input id="ventas_updated_at" type="text" name="updated_at" value="<?php echo h($ventas->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_deleted_at">Deleted At</label>
			<input id="ventas_deleted_at" type="text" name="deleted_at" value="<?php echo h($ventas->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ventas_movimiento_id">Movimientos De Dinero</label>
			<select id="ventas_movimiento_id" name="movimiento_id">
			<?php foreach (MovimientosDeDinero::doSelect() as $movimientos_de_dinero): ?>
				<option <?php if ($ventas->getMovimientoId() === $movimientos_de_dinero->getId()) echo 'selected="selected"' ?> value="<?php echo $movimientos_de_dinero->getId() ?>"><?php echo $movimientos_de_dinero?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $ventas->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>