<h1>
	<a href="<?php echo site_url('ventas/editar') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Ventas">
		Nueva Venta
	</a>
	Ventas
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('ventas/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>