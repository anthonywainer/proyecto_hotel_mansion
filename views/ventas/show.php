<h1>View Ventas</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('ventas/edit/' . $ventas->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Ventas">
		Edit	</a>
	<a href="<?php echo site_url('ventas/delete/' . $ventas->getId()) ?>"
		class="button" data-icon="trash" title="Delete Ventas"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Fecha Venta</span>
		<?php echo h($ventas->getFechaVenta(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estado</span>
		<?php echo h($ventas->getEstado()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estadia Id</span>
		<?php echo h($ventas->getEstadiaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($ventas->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($ventas->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($ventas->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Movimiento</span>
		<?php echo h($ventas->getMovimientosDeDineroRelatedByMovimientoId()) ?>
	</div>
</div>