<style>

    .btn-toolbar{
        margin-top:-38%;

        margin-left: 120%;
        margin-bottom: -10%;
    }

    .btn .btn-default .xlsx{
        z-index: 1000;
    margin-top: 5%;
        display: inline;
    }
</style>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">

<h1 align="center">Reportes Estadias</h1>
<div class="col-md-4">
    <div class="form-group">
        <input type='text'href="<?=site_url('reporteestadia')?>"
               value="<?= $fechai ?> - <?= $fechae ?>"
               class="form-control datepicker" />
    </div>
</div>

<hr>
<div class="container-fluid">

<table id="tableestadia" class=" object-grid table table-sm table-hover table table-condensed table-striped table-bordered" >
    <thead>
    <tr>
        <th >#</th>
        <th >Fecha_Reserva</th>
        <th>Fecha_Ingreso</th>
        <th>Fecha_Salida</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Dias</th>
        <th>Precio total</th>
        <th>Numero</th>
        <th>Descripción</th>
        <th>Forma Pago</th>
        <th>DOC.</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($estadias as $e):?>
            <tr>
                <td><?= $e['id'] ?></td>
                <td><?= $e['fecha_reserva'] ?></td>
                <td><?= $e['fecha_ingreso'] ?></td>
                <td><?= $e['fecha_salida'] ?></td>
                <td><?= $e['nombres'] ?></td>
                <td><?= $e['apellidos'] ?></td>
                <td><?= $e['dias'] ?></td>
                <td><?= $e['precio_total'] ?></td>
                <td><?= $e['numero'] ?></td>
                <td><?= $e['descripcion'] ?></td>
                <td><?= $e['forma_pago'] ?></td>
                <td><?= $e['comprobante'] ?></td>

            </tr>
        <?php endforeach; ?>
    </tbody >
<div id="scroll"></div>
</table>
</div>