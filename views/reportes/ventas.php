<h1 align="center">Reportes Ventas</h1>
<div class="col-md-4">
    <div class="form-group">
        <input type='text'href="<?=site_url('reporteventas')?>"
               value="<?= $fechai ?> - <?= $fechae ?>"
               class="form-control datepicker" />
    </div>
</div>

<hr>

<div class="container-fluid">
<table id="tableventas" class=" object-grid table table-sm table-hover table table-condensed table-striped table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Fecha</th>
        <th>nombres</th>
        <th>apellidos</th>
        <th>forma de pago</th>
        <th>comprobante</th>
        <th>precio total</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($ventas as $m):?>
    <tr>
        <td><?= $m['id'] ?></td>
        <td><?= $m['created_at'] ?></td>
        <td><?= $m['nombres'] ?></td>
        <td><?= $m['apellidos'] ?></td>
        <td><?= $m['tipoc'] ?></td>
        <td><?= $m['formap'] ?></td>
        <td><?= $m['monto'] ?></td>

    </tr>
<?php endforeach; ?>
</tbody>

</table>
</div>
