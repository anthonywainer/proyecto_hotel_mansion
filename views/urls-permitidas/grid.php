<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid urls_permitidas-grid table tab-content table-bordered"  cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UrlsPermitidas::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UrlsPermitidas::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UrlsPermitidas::URL))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UrlsPermitidas::URL): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Url
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($urls_permitidas as $key => $urls_permitidas): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($urls_permitidas->getId()) ?>&nbsp;</td>
			<td><?php echo h($urls_permitidas->getUrl()) ?>&nbsp;</td>
			<td>
				<a
                        class="btn btn-success"
					title="Show Urls_permitidas"
					href="<?php echo site_url('urls-permitidas/show/' . $urls_permitidas->getId()) ?>">
                    <i class="fa fa-search "></i>

                </a>
				<a
                        class="btn btn-info"
                        data-icon="pencil"
					title="Edit Urls_permitidas"
					href="<?php echo site_url('urls-permitidas/edit/' . $urls_permitidas->getId()) ?>">
                    <i class="fa fa-edit "></i>

                </a>
				<a     class="btn btn-danger"
					data-icon="trash"
					title="Delete Urls_permitidas"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('urls-permitidas/delete/' . $urls_permitidas->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o "></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>