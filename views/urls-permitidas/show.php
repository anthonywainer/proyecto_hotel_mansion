<h1>View Urls Permitidas</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('urls-permitidas/edit/' . $urls_permitidas->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Urls_permitidas">
		Edit	</a>
	<a href="<?php echo site_url('urls-permitidas/delete/' . $urls_permitidas->getId()) ?>"
		class="button" data-icon="trash" title="Delete Urls_permitidas"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Url</span>
		<?php echo h($urls_permitidas->getUrl()) ?>
	</div>
</div>