<div id="usa">
<h6>
    <div align="center" >
    <?php echo $permisos_grupo->isNew() ? "NUEVO" : "EDITAR" ?> PERMISOS GRUPOS</h6>
<form method="post" action="<?php echo site_url('permisos_grupos/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($permisos_grupo->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_grupo_idpermiso">Permisos</label>
			<select required class="form-control" id="permisos_grupo_idpermiso" name="idpermiso">
			<?php foreach (Permisos::getAll() as $permisos): ?>
				<option <?php if ($permisos_grupo->getIdpermiso() === $permisos->id) echo 'selected="selected"' ?> value="<?php echo $permisos->id ?>"><?php echo $permisos->nombre?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_grupo_idgrupo">Usuarios Grupo</label>
			<select required class="form-control" id="permisos_grupo_idgrupo" name="idgrupo">
			<?php foreach (Grupos::getAll() as $usuarios_grupo): ?>
				<option <?php if ($permisos_grupo->getIdgrupo() === $usuarios_grupo->id) echo 'selected="selected"' ?> value="<?php echo $usuarios_grupo->id ?>"><?php echo $usuarios_grupo->descripcion?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix"align="right">
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-secondary" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>
		<span class="" data-icon="disk">
			<input class="btn btn-primary"type="submit" value="<?php echo $permisos_grupo->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>

	</div>
</form>