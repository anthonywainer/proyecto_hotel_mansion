<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid permisos_grupo-grid table table-condensed table-bordered table table-striped" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					PERMISO
			</th>
			<th class="ui-widget-header ">
					GRUPO
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($permisos_grupos as $key => $permisos_grupo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($permisos_grupo->getId()) ?>&nbsp;</td>
            <td><?php if($permisos_grupo->getIdPermiso()) echo $permisos_grupo->getPermisos($permisos_grupo->getIdPermiso())->nombre ?>&nbsp;</td>
            <td><?php if($permisos_grupo->getIdGrupo()) echo Grupos::getAll('where id='.$permisos_grupo->getIdGrupo())[0]->descripcion ?>&nbsp;</td>

			<td>
                <a style="cursor: pointer" class="" title="ver modulos" href="<?php echo site_url('permisos_grupos/' . $permisos_grupo->getId()) ?>">
                    <i class="fa fa-search "></i>
                </a>
                <i style="cursor: pointer" class="" onclick="openmodal(this)" href="<?php echo site_url('permisos_grupos/editar/' . $permisos_grupo->getId()) ?>">
                    <i style="color:  #0e6498;" class="fa fa-edit fa-lg"></i>
                </i>
                <a style="cursor: pointer" class="" href="#" onclick="if (confirm('Esta seguro de eliminar?')) { window.location.href = '<?php echo site_url('permisos_grupos/eliminar/' . $permisos_grupo->getId()) ?>'; } return false">
                    <i style="color: red" class="fa fa-trash-o fa-lg "></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>