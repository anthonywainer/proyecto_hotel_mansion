<h1><?php echo $usuarios_grupo->isNew() ? "New" : "Edit" ?> Usuarios Grupo</h1>
<form method="post" action="<?php echo site_url('usuarios-grupos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($usuarios_grupo->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_grupo_usuario_id">Usuarios</label>
			<select id="usuarios_grupo_usuario_id" name="usuario_id">
			<?php foreach (Usuarios::doSelect() as $usuarios): ?>
				<option <?php if ($usuarios_grupo->getUsuarioId() === $usuarios->getId()) echo 'selected="selected"' ?> value="<?php echo $usuarios->getId() ?>"><?php echo $usuarios?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="usuarios_grupo_grupo_id">Grupos</label>
			<select id="usuarios_grupo_grupo_id" name="grupo_id">
			<?php foreach (Grupos::doSelect() as $grupos): ?>
				<option <?php if ($usuarios_grupo->getGrupoId() === $grupos->getId()) echo 'selected="selected"' ?> value="<?php echo $grupos->getId() ?>"><?php echo $grupos?></option>
			<?php endforeach ?>
			</select>
		</div>

	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $usuarios_grupo->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>