<h1>View Tipos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('tipos/edit/' . $tipos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Tipos">
		Edit	</a>
	<a href="<?php echo site_url('tipos/delete/' . $tipos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Tipos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('conceptos-pagos?tipo_id=' . $tipos->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Conceptos Pagos Tipos">
		Conceptos Pagos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Tipo</span>
		<?php echo h($tipos->getTipo()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($tipos->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($tipos->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($tipos->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>