
	<i href="<?php echo site_url('tipos/edit') ?> " onclick="openmodal(this)"
	   class="button"
	   data-icon="plusthick"
	   title="New Tipos">
        Registrar
	</i>
    <h1 align="center">
	Listar Tipos
</h1>
    <div class="row">
        <div class="form-group col-md-4">
            <div class="input-group">
                <input id="filtrar" type="search" onkeyup="buscar_tabla(this)" class="form-control" placeholder="Buscar Televisores">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </div>
    </div>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('tipos/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>

    <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #024C51">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
