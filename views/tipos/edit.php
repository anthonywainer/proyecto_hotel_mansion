
<div id="usa">
<h1><?php echo $tipos->isNew() ? "New" : "Edit" ?> Tipos</h1>
<form method="post" action="<?php echo site_url('tipos/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($tipos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipos_tipo">Tipo</label>
			<input id="tipos_tipo" type="text" name="tipo" value="<?php echo h($tipos->getTipo()) ?>" />
		</div>

	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $tipos->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>