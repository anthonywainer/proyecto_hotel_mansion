
<div id="usa">
<h3 align="center"><?php echo $categorias->isNew() ? "Registrar" : "Editar" ?> Categorías</h3>
<form method="post" action="<?php echo site_url('categorias/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($categorias->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="categorias_descripcion">Categoría</label>
			<input class="form-control" onkeypress="return soloLetras(event)" id="categorias_descripcion" type="text" name="descripcion" value="<?php echo h($categorias->getDescripcion()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix"  align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $categorias->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>