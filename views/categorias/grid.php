<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid categorias-grid table table-condensed table-bordered  table table-striped" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Categorias::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Categorias::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					#
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Categorias::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Categorias::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Categoría
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($categorias as $key => $categorias): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo $key+1;//echo h($categorias->getId()) ?>&nbsp;</td>
			<td><?php echo h($categorias->getDescripcion()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('categorias/editar/' . $categorias->getId()) ?>">
                    <i  class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Categoria','¿Está seguro de Eliminar?','<?php echo site_url('categorias/eliminar/' . $categorias->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg"></i>
                </a>
				<!--<a
					class="button"
					data-icon="carat-1-e"
					href="">
					Productos
				</a>-->
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>