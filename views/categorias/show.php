<h1>View Categorias</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('categorias/edit/' . $categorias->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Categorias">
		Edit	</a>
	<a href="<?php echo site_url('categorias/delete/' . $categorias->getId()) ?>"
		class="button" data-icon="trash" title="Delete Categorias"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('productos?categoria_id=' . $categorias->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Productos Categorias">
		Productos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($categorias->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($categorias->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($categorias->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($categorias->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>