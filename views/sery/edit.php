<div id="usa">
<h3 align="center"><?php echo $series->isNew() ? "Registrar" : "Editar" ?> Series</h3><br>
<form method="post" action="<?php echo site_url('sery/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($series->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="series_serie">Serie</label>
			<input class="form-control" id="series_serie" type="text" name="serie" value="<?php echo h($series->getSerie()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="series_correlativo">Correlativo</label>
			<input class="form-control" id="series_correlativo" type="text" name="correlativo" value="<?php echo h($series->getCorrelativo()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="series_tipo_comprobante_id">Tipos Comprobantes</label>
			<select class="form-control" id="series_tipo_comprobante_id" name="tipo_comprobante_id">
			<?php foreach (TiposComprobantes::doSelect() as $tipos_comprobantes): ?>
				<option <?php if ($series->getTipoComprobanteId() === $tipos_comprobantes->getId()) echo 'selected="selected"' ?> value="<?php echo $tipos_comprobantes->getId() ?>"><?php echo $tipos_comprobantes->descripcion;?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $series->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger"  data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>