<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid series-grid table table-condensed table-bordered table-striped" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Series::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Series::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>

				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Series::SERIE))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Series::SERIE): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Serie
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Series::CORRELATIVO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Series::CORRELATIVO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Correlativo
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Series::TIPO_COMPROBANTE_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Series::TIPO_COMPROBANTE_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Tipo Comprobante
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">Acciones</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($sery as $key => $series): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($series->getId()) ?>&nbsp;</td>
			<td><?php echo h($series->getSerie()) ?>&nbsp;</td>
			<td><?php echo h($series->getCorrelativo()) ?>&nbsp;</td>
            <td><?php echo $series->getTiposComprobantes($series->getTipoComprobanteId())->descripcion ?>&nbsp;</td>
			<!--<td><?php echo h($series->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($series->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($series->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>-->                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       '
			<td>
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('sery/editar/' . $series->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Series','¿Está seguro de Eliminar?','<?php echo site_url('sery/eliminar/' . $series->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>