<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand navbar-link"><img src="<?= site_url("assets/img/logo.jpg", true)  ?>"></a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" class="active"><a href="<?= site_url('/') ?>" class="navbar-1">INICIO </a></li>
                <li role="presentation"><a href="<?= site_url('/nosotros') ?>" class="navbar-1">NOSOTROS </a></li>
                <li role="presentation" class="navbar-1"><a href="<?= site_url('/habitacion') ?>" class="navbar-1">HABITACIONES </a></li>
                <li role="presentation"><a href="<?=site_url('/servicios') ?>" class="navbar-1">SERVICIOS </a></li>
                <li role="presentation"><a href="<?=site_url('/galeria') ?>" class="navbar-1">GALERÍA </a></li>
                <li role="presentation"><a href="<?= site_url('/contactenos') ?>" class="navbar-1">CONTÁCTENOS </a></li>
                <li role="presentation" id="lista-login">
                    <div id="cuadrito-login">
                        <?php if ($_SESSION): ?>
                            <a href="<?= site_url('/admin') ?>" class="">Aceder al Sistema</a>
                        <?php else: ?>
                            <a href="<?= site_url('/login') ?>" class="login">INICIAR SESIÓN</a>
                        <?php endif; ?>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>