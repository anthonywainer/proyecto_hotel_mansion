<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0, shrink-to-fit=no">

    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="<?php echo site_url('css/font-awesome.css', true) ?>">
    <link rel="stylesheet" href="<?php echo site_url('css/simple-line-icons.min.css', true) ?>">
    <?php         if ($_SESSION) { ?>
        <link href="<?php echo site_url('css/toastr.min.css', true) ?>" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/bootstrap/css/bootstrap.css', true) ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/style-admin.css', true) ?>">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
        <link href="http://code.gijgo.com/1.6.1/css/gijgo.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <?php }else{ ?>
        <link href="<?php echo site_url('css/toastr.min.css', true) ?>" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/bootstrap3.min.css', true) ?>">

        <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/style.css', true) ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/tableexport.css', true) ?>">


    <?php } ?>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">


<div class="content-wrapper ui-widget">
    <div class="content">
        <?php View::load($content_view, $params) ?>
    </div>
</div>


<?php if ($_SESSION) { ?>

    </script>
    <script src="<?php echo site_url('js/jquery.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/popper.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/bootstrap.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/pace.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/toastr.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/app.js', true) ?>"></script>
    <script src="<?php echo site_url('js/gijgo.js', true) ?>"></script>


    <script src="<?php echo site_url('js/xlsx.core.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/Blob.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/FileSaver.min.js', true) ?>"></script>
    <script src="<?php echo site_url('js/tableexport.js', true) ?>"></script>
    <script src="<?php echo site_url('js/tableexport.min.js', true) ?>"></script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>

        <?php if(isset($errors)): ?>
        toastr.error("<?php View::load('errors', compact('errors')) ?>", 'Eliminado', {
            closeButton: true,
            progressBar: true,
        });
        <?php endif ?>
        <?php if(isset($messages)): ?>
        toastr.success("<?php View::load('messages', compact('messages')) ?>", 'Guardado', {
            closeButton: true,
            progressBar: true,
        });
        <?php endif ?>
    </script>
    <script>
        $(".accordion-titulo").click(function(){

            var contenido=$(this).next(".accordion-content");

            if(contenido.css("display")=="none"){ //open
                contenido.slideDown(250);
                $(this).addClass("open");
            }
            else{ //close
                contenido.slideUp(250);
                $(this).removeClass("open");
            }

        });
    </script>
<?php }else{ ?>

    <script src="<?= site_url('js/jquery.min1.js') ?>"></script>
    <script src="<?= site_url('js/bootstrap3.min.js') ?>" ></script>
    <script src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/mdb3/mdb.min.js"></script>
    <script src="<?php echo site_url('js/toastr.min.js', true) ?>"></script>


    <script>
        $('.materialboxed').materialbox();
        <?php if(isset($errors)): ?>
        toastr.error("<?php View::load('errors', compact('errors')) ?>", 'Eliminado', {
            closeButton: true,
            progressBar: true,
        });
        <?php endif ?>
        <?php if(isset($messages)): ?>
        toastr.success("<?php View::load('messages', compact('messages')) ?>", 'Guardado', {
            closeButton: true,
            progressBar: true,
        });
        <?php endif ?>
    </script>

<?php } ?>



</body>
</html>