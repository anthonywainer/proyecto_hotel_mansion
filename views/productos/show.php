<h1>View Productos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('productos/edit/' . $productos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Productos">
		Edit	</a>
	<a href="<?php echo site_url('productos/delete/' . $productos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Productos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('productos-ventas?producto_id=' . $productos->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Productos Ventas Productos">
		Productos Ventas	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Nombre</span>
		<?php echo h($productos->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($productos->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Precio</span>
		<?php echo h($productos->getPrecio()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Imagen</span>
		<?php echo h($productos->getImagen()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Stock</span>
		<?php echo h($productos->getStock()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Categoria</span>
		<?php echo h($productos->getCategoriasRelatedByCategoriaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($productos->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($productos->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($productos->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>