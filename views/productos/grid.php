<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid productos-grid table table-condensed table-bordered table-striped" cellspacing="0" id="table-responsive">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::NOMBRE))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::NOMBRE): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Nombre
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descripcion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::PRECIO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::PRECIO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Precio
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::IMAGEN))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::IMAGEN): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Imagen
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::STOCK))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::STOCK): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Stock
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Productos::CATEGORIA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Productos::CATEGORIA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Categoria
				</a>
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($productos as $key => $productos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($productos->getId()) ?>&nbsp;</td>
			<td><?php echo h($productos->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($productos->getDescripcion()) ?>&nbsp;</td>
			<td><?php echo h($productos->getPrecio()) ?>&nbsp;</td>
			<td>
                <img width="109" height="109" src="<?= site_url('imagenes_productos/'.$productos->getImagen())?>" alt="">                &nbsp;
            </td>
			<td><?php echo h($productos->getStock()) ?>&nbsp;</td>
			<!--<td><?php echo h($productos->getCategoriasRelatedByCategoriaId()) ?>&nbsp;</td>-->
            <td><?php echo Categorias::getAll('where id='.$productos->categoria_id)[0]->getDescripcion(); ?>&nbsp;</td>

            <!--<td><?php echo h($productos->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($productos->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($productos->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>-->
			<td>
				<!--<a
					class="button"
					data-icon="search"
					title="Show Productos"
					href="<?php echo site_url('productos/show/' . $productos->getId()) ?>">
					Show
				</a>-->
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('productos/editar/' . $productos->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Producto','¿Está seguro de Eliminar?','<?php echo site_url('productos/eliminar/' . $productos->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>

                </a>
				<!--<a
					class="button"
					data-icon="carat-1-e"
					href="<?php echo site_url('productos-ventas?producto_id=' . $productos->getId()) ?>">
					Productos Ventas
				</a>-->
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>