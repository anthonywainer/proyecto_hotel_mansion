<div id="usa">
<h3><?php echo $productos->isNew() ? "New" : "Edit" ?> Productos</h3>
<form method="post" enctype="multipart/form-data" action="<?php echo site_url('productos/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($productos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label"  for="productos_nombre">Nombre</label>
			<input class="form-control" onkeypress="return soloLetras(event)" id="productos_nombre" type="text" name="nombre" value="<?php echo h($productos->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_descripcion">Descripcion</label>
            <textarea class="form-control"  id="productos_descripcion" type="text" name="descripcion" value="<?php echo h($productos->getDescripcion()) ?>" >

            </textarea>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_precio">Precio</label>
			<input class="form-control" id="productos_precio" type="text" name="precio" value="<?php echo h($productos->getPrecio()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_imagen">Imagen</label>
			<input class="form-control" id="productos_imagen" type="file" name="imagen" value="<?php echo h($productos->getImagen()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_stock">Stock</label>
			<input class="form-control" id="productos_stock" type="text" name="stock" value="<?php echo h($productos->getStock()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="productos_categoria_id">Categorias</label>
			<select class="form-control" id="productos_categoria_id" name="categoria_id">
			<?php foreach (Categorias::doSelect() as $categorias): ?>
				<option <?php if ($productos->getCategoriaId() === $categorias->getId()) echo 'selected="selected"' ?> value="<?php echo $categorias->getId() ?>"><?php echo $categorias->getDescripcion()?></option>
			<?php endforeach ?>
			</select>
		</div>
		</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $productos->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>