<h1><?php echo $estadias_habitaciones->isNew() ? "New" : "Edit" ?> Estadias Habitaciones</h1>
<form method="post" action="<?php echo site_url('estadias-habitaciones/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($estadias_habitaciones->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_descuento">Descuento</label>
			<input id="estadias_habitaciones_descuento" type="text" name="descuento" value="<?php echo h($estadias_habitaciones->getDescuento()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_asistente">Asistente</label>
			<input id="estadias_habitaciones_asistente" type="text" name="asistente" value="<?php echo h($estadias_habitaciones->getAsistente()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_estadia_id">Estadias</label>
			<select id="estadias_habitaciones_estadia_id" name="estadia_id">
			<?php foreach (Estadias::doSelect() as $estadias): ?>
				<option <?php if ($estadias_habitaciones->getEstadiaId() === $estadias->getId()) echo 'selected="selected"' ?> value="<?php echo $estadias->getId() ?>"><?php echo $estadias?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_habitacion_id">Habitaciones</label>
			<select id="estadias_habitaciones_habitacion_id" name="habitacion_id">
			<?php foreach (Habitaciones::doSelect() as $habitaciones): ?>
				<option <?php if ($estadias_habitaciones->getHabitacionId() === $habitaciones->getId()) echo 'selected="selected"' ?> value="<?php echo $habitaciones->getId() ?>"><?php echo $habitaciones?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_producto_id">Productos</label>
			<select id="estadias_habitaciones_producto_id" name="producto_id">
			<?php foreach (Productos::doSelect() as $productos): ?>
				<option <?php if ($estadias_habitaciones->getProductoId() === $productos->getId()) echo 'selected="selected"' ?> value="<?php echo $productos->getId() ?>"><?php echo $productos?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_created_at">Created At</label>
			<input id="estadias_habitaciones_created_at" type="text" name="created_at" value="<?php echo h($estadias_habitaciones->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_updated_at">Updated At</label>
			<input id="estadias_habitaciones_updated_at" type="text" name="updated_at" value="<?php echo h($estadias_habitaciones->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_deleted_at">Deleted At</label>
			<input id="estadias_habitaciones_deleted_at" type="text" name="deleted_at" value="<?php echo h($estadias_habitaciones->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_fecha_reserva">Fecha Reserva</label>
			<input id="estadias_habitaciones_fecha_reserva" class="datepicker" type="text" name="fecha_reserva" value="<?php echo h($estadias_habitaciones->getFechaReserva(VIEW_DATE_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_fecha_ingreso">Fecha Ingreso</label>
			<input id="estadias_habitaciones_fecha_ingreso" class="datepicker" type="text" name="fecha_ingreso" value="<?php echo h($estadias_habitaciones->getFechaIngreso(VIEW_DATE_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_fecha_salida">Fecha Salida</label>
			<input id="estadias_habitaciones_fecha_salida" class="datepicker" type="text" name="fecha_salida" value="<?php echo h($estadias_habitaciones->getFechaSalida(VIEW_DATE_FORMAT)) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_observacion">Observacion</label>
			<input id="estadias_habitaciones_observacion" type="text" name="observacion" value="<?php echo h($estadias_habitaciones->getObservacion()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_precio_total">Precio Total</label>
			<input id="estadias_habitaciones_precio_total" type="text" name="precio_total" value="<?php echo h($estadias_habitaciones->getPrecioTotal()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="estadias_habitaciones_dias">Dias</label>
			<input id="estadias_habitaciones_dias" type="text" name="dias" value="<?php echo h($estadias_habitaciones->getDias()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $estadias_habitaciones->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>