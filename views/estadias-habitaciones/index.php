<style>
    .back{
        display: inline-block;

    }
    .letra:hover, .fa-arrow-circle-left:hover{
        color:black;
    }
    .fa-arrow-circle-left{
        cursor: pointer;
        color: #8c8c8c;

    }
    .letra{
        font-size: 15px;
        color: #8c8c8c;
        display: inline-block;
        cursor: pointer;
        margin-top: -50px;
    }
    .Iwhite{
        color:#d1d4d7;
    }
</style>



<?php
function count_c_e($idhe=null){
    $sqlce = "SELECT
            Count(clientes_estadias.id) as c
            FROM
            clientes_estadias
            WHERE
            clientes_estadias.habitacion_estadia_id = ".$idhe;
    return ClientesEstadias::getConnection()->query($sqlce)->fetchAll();
}
?>
<div class="container-fluid">
    <div class="back" align="left">
        <label> <a href="<?=site_url('estadias')?>">
                <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true">Volver</i>
            </a></label>
    </div>

<h1 align="center">
	Estadias Habitaciones
</h1>

<?php if ($no): ?>
    Cliente: <br>
    <select name="usuario_id" id="usuario_id">
        <option value="">--> Seleccione un cliente <--</option>
        <?php foreach ($usuarios as $u): ?>
            <option value="<?= $u->id ?>"><?php echo $u->dni.', '.$u->nombres.' '.$u->apellidos ?></option>
        <?php endforeach; ?>
    </select>
     <i style="color: green; cursor: pointer " href="<?php echo site_url('clientes/editar') ?>"onclick="openmodal(this)"
                class="fa fa-plus-circle"></i>
<?php endif; ?>
<br><br>
<button onclick="registrarHabita(this, '<?php if ($no){echo '';}else{echo 'first';}?>')"
    <?php if ($no): ?>
        href="<?= site_url('estadias/guardar/') ?>"
    <?php else: ?>
        href="<?= site_url('habitaciones/?ide='.$_GET['id']) ?>"
    <?php endif; ?>
        class="btn btn-primary">AGREGAR HABITACIÓN
</button>
<br><br>
<div class="ui-widget-content ui-corner-all">
	<?php if(isset($habitacion_estadia))  View::load('estadias-habitaciones/grid', $params) ?>
</div>

<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <!--<div class="modal-footer">

            </div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</div>
<script>
    function registrar(t) {
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url,param).done(function (id) {
            alert("Guardado Correctamente");
            nom = $("#dni").val()+","+$("#nombre").val()+" "+$("#ape").val();
            var option = new Option(nom, id);
            $("select").append(option);
            $("#usuario_id").select2("val", id);
            $("#primaryModal").modal("hide");
        });
    }

    function registrarHabita(t,s) {
        if(s!="first") {
            idu = $("select").val();
            if (!idu) {
                alert("escoger un cliente");
            } else {
                window.location.href = $(t).attr("href") + "?cliente_id=" + idu;
            }
        }else{
            window.location.href = $(t).attr("href");
        }
    }

</script>