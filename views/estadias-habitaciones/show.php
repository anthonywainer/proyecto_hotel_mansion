<h1>View Estadias Habitaciones</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('estadias-habitaciones/edit/' . $estadias_habitaciones->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Estadias_habitaciones">
		Edit	</a>
	<a href="<?php echo site_url('estadias-habitaciones/delete/' . $estadias_habitaciones->getId()) ?>"
		class="button" data-icon="trash" title="Delete Estadias_habitaciones"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('clientes-estadias?habitacion_estadia_id=' . $estadias_habitaciones->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Clientes Estadias Estadias_habitaciones">
		Clientes Estadias	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descuento</span>
		<?php echo h($estadias_habitaciones->getDescuento()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Asistente</span>
		<?php echo h($estadias_habitaciones->getAsistente()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Estadia</span>
		<?php echo h($estadias_habitaciones->getEstadiasRelatedByEstadiaId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Habitacion</span>
		<?php echo h($estadias_habitaciones->getHabitacionesRelatedByHabitacionId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Producto</span>
		<?php echo h($estadias_habitaciones->getProductosRelatedByProductoId()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($estadias_habitaciones->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($estadias_habitaciones->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($estadias_habitaciones->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Reserva</span>
		<?php echo h($estadias_habitaciones->getFechaReserva(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Ingreso</span>
		<?php echo h($estadias_habitaciones->getFechaIngreso(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Fecha Salida</span>
		<?php echo h($estadias_habitaciones->getFechaSalida(VIEW_DATE_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Observacion</span>
		<?php echo h($estadias_habitaciones->getObservacion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Precio Total</span>
		<?php echo h($estadias_habitaciones->getPrecioTotal()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Dias</span>
		<?php echo h($estadias_habitaciones->getDias()) ?>
	</div>
</div>