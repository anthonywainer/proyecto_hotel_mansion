<table class="table table-bordered table-hover table-condensed">
    <thead>
    <tr>
        <td>#</td>
        <td>Habitación</td>
        <td>Personas</td>
        <td>Estado</td>
        <td>Acciones</td>
    </tr>
    </thead>
    <tbody>
    <?php $tt=0; foreach ($habitacion_estadia as $he): ?>
        <tr>
            <td><?=$he['id']?></td>
            <td><?=$he['numero'].' => tipo: '.$he["tipo"]?></td>
            <td>
                <?php $tt+=$he['total']; print_r(count_c_e($he['id'])[0]['c']) ?>
            </td>
            <td>
                <?= $he['estado'] ?>
            </td>
            <td>
                <a class="btn btn-outline-primary" href="<?= site_url("estadias/editar/".$he['id']) ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Televisores','¿Está seguro de Eliminar?',
                           '<?php echo site_url("estadias-habitaciones/eliminar/".$he['id']) ?>')">
                <i class="fa fa-trash-o fa-lg" ></i>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="4" align="right">Total:</td>
        <td>S/. <?= $tt  ?></td>
    </tr>
    </tfoot>
</table>