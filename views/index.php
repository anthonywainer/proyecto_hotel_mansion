<link rel="stylesheet" href="<?php echo site_url('css/style.css', true) ?>">
<?php include 'layouts/header.php' ?>


<div class="row">
    <div class="col-md-12" id="espacio"></div>
    <div class="carousel slide col-md-12" data-ride="carousel" id="carousel-1">
        <div class="carousel-inner" role="listbox">
            <div class="item active"><img src="<?= site_url("assets/img/foto1.jpg", true)  ?>" alt="Slide Image" class="tales"></div>
            <div class="item"><img src="<?= site_url("assets/img/foto2.jpg", true)  ?>" alt="Slide Image" class="tales"></div>
            <div class="item"><img src="<?= site_url("assets/img/foto33.jpg", true)  ?>" alt="Slide Image" class="tales"></div>
            <div class="item"><img src="<?= site_url("assets/img/foto4.jpg", true)  ?>" alt="Slide Image" class="tales"></div>

        </div>

        <div><a class="left carousel-control" href="#carousel-1" role="button" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i><span class="sr-only">Previous</span></a>
            <a id="right" class="right carousel-control" href="#carousel-1" role="button" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i><span class="sr-only">Next</span></a>
        </div>
        <ol  class="carousel-indicators">
            <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-1" data-slide-to="1"></li>
            <li data-target="#carousel-1" data-slide-to="2"></li>
            <li data-target="#carousel-1" data-slide-to="3"></li>
        </ol>
    </div>
</div>
<div id="bienvenido" class="col-md-4 col-sm-6" align="center">
    <h3 >BIENVENIDOS AL HOSPEDAJE </h3>
    <h1>LA MANSIÓN</h1>
</div>

<div class="row" align="center">
    <div id="habi"class="col-md-4 col-sm-6 col-xs-12 servicios" align="center" >
        <h5>HABITACIONES</h5>
        <img id="ima" class="materialboxed " src="<?= site_url("assets/img/habitacion1.jpg", true)  ?>"
             width="250" height="100"/><br>
        <p align="justify"> Relájate en nuestras habitaciones, cada una diseñada para brindar el mejor descanso
            a nuestros clientes: parejas o familias enteras.  </p>

    </div>
    <div id="habi" class="col-md-4 col-sm-6 col-xs-12 servicios" align="center">
        <h5>SERVICIOS</h5>
        <img id="ima" class="materialboxed " src="<?= site_url("assets/img/segundo.jpg", true)  ?> "
             width="200" height="100"/><br>

        <p align="justify">Disfruta de nuestro restaurante,piscina y la buena atencion que brindamos a cada uno de nuestros clientes.
        </p>

    </div>
    <div id="habi" class="col-md-4 col-sm-6 col-xs-12 servicios" align="center">
        <h5>GALERÍA</h5>
        <img id="ima" class="materialboxed" src="<?= site_url("assets/img/tercero.jpg", true)  ?> "
              width="250" height="100"/><br>
        <p align="justify">Tus mejores momentos tambien son los nuestros.Aca te brindamos una serie de fotos donde nuestros clientes son los protagonistas
        </p>

    </div>

    <div id="habi" class="col-md-4 col-sm-6 col-xs-12 servicios" align="center" >

        <br><br>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12" style="padding-left: 30px">
                <img id="ima" class="materialboxed" src="<?= site_url("assets/img/foto3.jpg", true)  ?> "
                     width="250" height="100"/>
            </div>
        </div>
    </div>


    <div class="row"  >
        <div class="col-md-4 servicios" align="center" >

        </div>
        <div class="col-md-4 servicios" align="center">

        </div>
        <div class="col-md-4 servicios"  style="left: 380px">
            <h3>REDES SOCIALES</h3>
            <div class="form-inline">

                <a href="" target="_blank">
                    <i class="fa fa-3x fa-twitter-square"></i>
                </a>
                <a href="https://www.facebook.com/pages/Hotel-La-Mansion-Tarapoto/267925833287375" target="_blank">
                    <i  class="fa fa-3x fa-facebook-square" style="color: blue"></i>
                </a>
                <a href="" target="_blank">
                    <i class="fa fa-3x fa-instagram" style="color: #2A5B83"></i>
                </a>
            </div>

        </div>
    </div>

</div>



