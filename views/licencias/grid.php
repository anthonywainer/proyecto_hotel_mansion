<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid licencia-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Licencia::LICENCIA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Licencia::LICENCIA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Licencia Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Licencia::FECHA_FIN))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Licencia::FECHA_FIN): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha Fin
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($licencias as $key => $licencia): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($licencia->getLicenciaId()) ?>&nbsp;</td>
			<td><?php echo h($licencia->getFechaFin(VIEW_DATE_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Licencia"
					href="<?php echo site_url('licencias/show/' . $licencia->getLicenciaId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Licencia"
					href="<?php echo site_url('licencias/edit/' . $licencia->getLicenciaId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Licencia"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('licencias/delete/' . $licencia->getLicenciaId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>