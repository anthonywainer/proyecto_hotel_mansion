<h1><?php echo $licencia->isNew() ? "New" : "Edit" ?> Licencia</h1>
<form method="post" action="<?php echo site_url('licencias/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="licencia_id" value="<?php echo h($licencia->getLicenciaId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="licencia_fecha_fin">Fecha Fin</label>
			<input id="licencia_fecha_fin" class="datepicker" type="text" name="fecha_fin" value="<?php echo h($licencia->getFechaFin(VIEW_DATE_FORMAT)) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $licencia->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>