<h1>View Licencia</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('licencias/edit/' . $licencia->getLicenciaId()) ?>"
		class="button" data-icon="pencil" title="Edit Licencia">
		Edit	</a>
	<a href="<?php echo site_url('licencias/delete/' . $licencia->getLicenciaId()) ?>"
		class="button" data-icon="trash" title="Delete Licencia"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Fecha Fin</span>
		<?php echo h($licencia->getFechaFin(VIEW_DATE_FORMAT)) ?>
	</div>
</div>