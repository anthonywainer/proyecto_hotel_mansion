<h1>
	<a href="<?php echo site_url('licencias/edit') ?>"
	   class="button"
	   data-icon="plusthick"
	   title="New Licencia">
		New Licencia
	</a>
	Licencias
</h1>

<?php View::load('pager', compact('pager')) ?>
<div class="ui-widget-content ui-corner-all">
	<?php View::load('licencias/grid', $params) ?>
</div>

<?php View::load('pager', compact('pager')) ?>