<h1>View Tipos Comprobantes</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('tipos-comprobantes/edit/' . $tipos_comprobantes->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Tipos_comprobantes">
		Edit	</a>
	<a href="<?php echo site_url('tipos-comprobantes/delete/' . $tipos_comprobantes->getId()) ?>"
		class="button" data-icon="trash" title="Delete Tipos_comprobantes"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('movimientos?tipo_comprobante_id=' . $tipos_comprobantes->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Movimientos Tipos_comprobantes">
		Movimientos	</a>
	<a href="<?php echo site_url('sery?tipo_comprobante_id=' . $tipos_comprobantes->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Sery Tipos_comprobantes">
		Sery	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($tipos_comprobantes->getDescripcion()) ?>
	</div>

</div>