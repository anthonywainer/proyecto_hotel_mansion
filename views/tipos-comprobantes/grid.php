<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid tipos_comprobantes-grid table table-condensed table-bordered table-striped" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TiposComprobantes::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TiposComprobantes::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TiposComprobantes::DESCRIPCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TiposComprobantes::DESCRIPCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Descripcion
				</a>
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">Acciones;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($tipos_comprobantes as $key => $tipos_comprobantes): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($tipos_comprobantes->getId()) ?>&nbsp;</td>
			<td><?php echo h($tipos_comprobantes->getDescripcion()) ?>&nbsp;</td>
			<!--<td><?php echo h($tipos_comprobantes->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($tipos_comprobantes->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($tipos_comprobantes->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>-->
            <td>

                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('tipos-comprobantes/editar/' . $tipos_comprobantes->getId()) ?>">
                    <i class="fa fa-edit fa-lg" ></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Tipo Comprobante','¿Está seguro de Eliminar?', '<?php echo site_url('tipos-comprobantes/eliminar/' . $tipos_comprobantes->getId()) ?>')">
                    <i class="fa fa-trash-o fa-lg" ></i>
                </a>
            </td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>