<div class="container-fluid">
<h2 align="center">
    LISTA DE FORMAS DE PAGO
</h2>
<br>
<br>
<div class="row">
    <div class="form-group col-md-12">
        <div class="input-group">
            <i href="<?php echo site_url('formas-pagos/editar') ?>"onclick="openmodal(this)"
               class="button"
               data-icon="plusthick"
               title="Nueva Forma Pago">
                <button class="btn btn-primary" style="background: #3C8DBC; color: white">
                    Registrar
                </button>
            </i>
            <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                   href="<?= site_url('formas-pagos/index?search=') ?>"
                   value="<?= $_GET['search'] ?>"
                   class="form-control" placeholder="Buscar forma de pago" style="margin-left: 40%"/>
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
    </div>
</div>
<div id="grilla">
<div class="ui-widget-content ui-corner-all">
	<?php View::load('formas-pagos/grid', $params) ?>
</div>
</div>
<?php View::load('pager', compact('pager')) ?>

<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <!--<div class="modal-footer">

            </div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</div>