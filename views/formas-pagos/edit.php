<div id="usa">
<h3><div align="center" >
<?php echo $formas_pagos->isNew() ? "Registrar" : "Editar" ?> Formas Pagos</h3>
    <br>
<form method="post" action="<?php echo site_url('formas-pagos/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($formas_pagos->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="formas_pagos_descripcion">Descripción</label>
			<input class="form-control" id="formas_pagos_descripcion" type="text" name="descripcion" value="<?php echo h($formas_pagos->getDescripcion()) ?>" />
		</div>

	</div>
    <br>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" type="submit" class="btn btn-primary" value="<?php echo $formas_pagos->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>