<h1>View Formas Pagos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('formas-pagos/edit/' . $formas_pagos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Formas_pagos">
		Edit	</a>
	<a href="<?php echo site_url('formas-pagos/delete/' . $formas_pagos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Formas_pagos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>

</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($formas_pagos->getDescripcion()) ?>
	</div>
</div>