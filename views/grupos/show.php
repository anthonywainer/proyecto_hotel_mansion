<h1>View Grupos</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('grupos/edit/' . $grupos->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Grupos">
		Edit	</a>
	<a href="<?php echo site_url('grupos/delete/' . $grupos->getId()) ?>"
		class="button" data-icon="trash" title="Delete Grupos"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('permisos-grupos?grupo_id=' . $grupos->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Permisos Grupos Grupos">
		Permisos Grupos	</a>
	<a href="<?php echo site_url('usuarios-grupos?grupo_id=' . $grupos->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Usuarios Grupos Grupos">
		Usuarios Grupos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Descripcion</span>
		<?php echo h($grupos->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Created At</span>
		<?php echo h($grupos->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Updated At</span>
		<?php echo h($grupos->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<span class="field-label">Deleted At</span>
		<?php echo h($grupos->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>