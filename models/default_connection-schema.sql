
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- acciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `acciones`;

CREATE TABLE `acciones`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`accion` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- amortizaciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `amortizaciones`;

CREATE TABLE `amortizaciones`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`movimiento_id` INTEGER(10) NOT NULL,
	`cronograma_pago_id` INTEGER(10) NOT NULL,
	`monto` DECIMAL(8,2) NOT NULL,
	`glosa` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `amortizaciones_movimiento_id_foreign` (`movimiento_id`(10)),
	INDEX `amortizaciones_cronograma_pago_id_foreign` (`cronograma_pago_id`(10)),
	CONSTRAINT `amortizaciones_ibfk_1`
		FOREIGN KEY (`cronograma_pago_id`)
		REFERENCES `cronogramas_pagos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `amortizaciones_ibfk_2`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cajas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cajas`;

CREATE TABLE `cajas`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`saldo_inicial` DECIMAL(8,2),
	`saldo_final` DECIMAL(8,2),
	`fecha_apertura` DATETIME,
	`fecha_cierre` DATETIME,
	`estado` VARCHAR(1) DEFAULT '\0' NOT NULL,
	`usuario_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `cajas_usuario_id_foreign` (`usuario_id`(10))
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- categorias
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `categorias`;

CREATE TABLE `categorias`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- clientes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`ruc` VARCHAR(191),
	`nombres` VARCHAR(191),
	`apellidos` VARCHAR(191),
	`dni` VARCHAR(191),
	`direccion` VARCHAR(191) NOT NULL,
	`telefono` VARCHAR(191),
	`procedencia` VARCHAR(191),
	`sexo` VARCHAR(191),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- clientes_estadias
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `clientes_estadias`;

CREATE TABLE `clientes_estadias`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`cliente_id` INTEGER(10) NOT NULL,
	`habitacion_estadia_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`estado` FLOAT DEFAULT 1,
	PRIMARY KEY (`id`),
	INDEX `clientes_estadias_cliente_id_foreign` (`cliente_id`(10)),
	INDEX `clientes_estadias_habitacion_estadia_id_foreign` (`habitacion_estadia_id`(10)),
	CONSTRAINT `clientes_estadias_ibfk_1`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `clientes_estadias_ibfk_2`
		FOREIGN KEY (`habitacion_estadia_id`)
		REFERENCES `estadias_habitaciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- clientes_habitaciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `clientes_habitaciones`;

CREATE TABLE `clientes_habitaciones`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`habitacion_id` INTEGER(10) NOT NULL,
	`cliente_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `clientes_habitaciones_habitacion_id_foreign` (`habitacion_id`(10)),
	INDEX `clientes_habitaciones_cliente_id_foreign` (`cliente_id`(10)),
	CONSTRAINT `clientes_habitaciones_ibfk_1`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `clientes_habitaciones_ibfk_2`
		FOREIGN KEY (`habitacion_id`)
		REFERENCES `habitaciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- concepto_pago
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `concepto_pago`;

CREATE TABLE `concepto_pago`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(60) NOT NULL,
	`movimiento_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`monto` DECIMAL DEFAULT 0.00,
	PRIMARY KEY (`id`),
	INDEX `concepto_pago_ibfk_1` (`movimiento_id`),
	CONSTRAINT `concepto_pago_ibfk_1`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- contactenos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `contactenos`;

CREATE TABLE `contactenos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(50) NOT NULL,
	`telefono` VARCHAR(10) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`asunto` TEXT NOT NULL,
	`estado` VARCHAR(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cronogramas_pagos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cronogramas_pagos`;

CREATE TABLE `cronogramas_pagos`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`fecha` DATETIME NOT NULL,
	`monto_cargado` DECIMAL(8,2) NOT NULL,
	`cuota` DECIMAL(8,2) NOT NULL,
	`estadia_id` INTEGER(10) NOT NULL,
	`venta_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `cronogramas_pagos_estadia_id_foreign` (`estadia_id`(10)),
	INDEX `cronogramas_pagos_venta_id_foreign` (`venta_id`(10)),
	CONSTRAINT `cronogramas_pagos_ibfk_1`
		FOREIGN KEY (`estadia_id`)
		REFERENCES `estadias` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- estadias
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `estadias`;

CREATE TABLE `estadias`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`fecha_reserva` DATETIME,
	`fecha_ingreso` DATETIME,
	`fecha_salida` DATETIME,
	`estado` VARCHAR(191) DEFAULT 'Activo' NOT NULL,
	`cliente_id` INTEGER(10),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`usuario_id` VARCHAR(255),
	`precio` DECIMAL,
	`descuento` DECIMAL,
	`observacion` TEXT,
	`monto_pagar` DECIMAL,
	`cuota` DECIMAL,
	`resto` DECIMAL,
	PRIMARY KEY (`id`),
	INDEX `estadias_cliente_id_foreign` (`cliente_id`(10)),
	CONSTRAINT `estadias_ibfk_1`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- estadias_habitaciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `estadias_habitaciones`;

CREATE TABLE `estadias_habitaciones`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`descuento` DECIMAL(8,2) DEFAULT 0.00,
	`asistente` VARCHAR(191),
	`estadia_id` INTEGER(10) NOT NULL,
	`habitacion_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`fecha_reserva` DATE,
	`fecha_ingreso` DATE,
	`fecha_salida` DATE,
	`observacion` VARCHAR(255),
	`precio_total` DECIMAL,
	`dias` INTEGER,
	`tipo_habitacion` INTEGER,
	`estado_es_ha` TINYINT(1) DEFAULT 0,
	`aumento` DECIMAL(8,2),
	`tipo_precio` TINYINT(1) DEFAULT 0,
	`precio` DECIMAL,
	PRIMARY KEY (`id`),
	INDEX `estadias_habitaciones_estadia_id_foreign` (`estadia_id`(10)),
	INDEX `estadias_habitaciones_habitacion_id_foreign` (`habitacion_id`(10)),
	INDEX `estadias_habitaciones_ibfk_4` (`tipo_habitacion`),
	CONSTRAINT `estadias_habitaciones_ibfk_1`
		FOREIGN KEY (`estadia_id`)
		REFERENCES `estadias` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `estadias_habitaciones_ibfk_3`
		FOREIGN KEY (`habitacion_id`)
		REFERENCES `habitaciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `estadias_habitaciones_ibfk_4`
		FOREIGN KEY (`tipo_habitacion`)
		REFERENCES `tipos_habitaciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- formas_pagos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `formas_pagos`;

CREATE TABLE `formas_pagos`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- grupos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `grupos`;

CREATE TABLE `grupos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(50) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- habitaciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `habitaciones`;

CREATE TABLE `habitaciones`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`numero` INTEGER NOT NULL,
	`descripcion` VARCHAR(191) NOT NULL,
	`estado` VARCHAR(191) NOT NULL,
	`piso` INTEGER NOT NULL,
	`televisor_id` INTEGER(10) NOT NULL,
	`tipo_habitacion_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `habitaciones_televisor_id_foreign` (`televisor_id`(10)),
	INDEX `habitaciones_tipo_habitacion_id_foreign` (`tipo_habitacion_id`(10)),
	CONSTRAINT `habitaciones_ibfk_1`
		FOREIGN KEY (`televisor_id`)
		REFERENCES `televisores` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `habitaciones_ibfk_2`
		FOREIGN KEY (`tipo_habitacion_id`)
		REFERENCES `tipos_habitaciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- hoteles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `hoteles`;

CREATE TABLE `hoteles`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(191) NOT NULL,
	`descripcion` VARCHAR(191) NOT NULL,
	`direccion` VARCHAR(191) NOT NULL,
	`ruc` INTEGER NOT NULL,
	`telefono` INTEGER NOT NULL,
	`colores` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- licencia
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `licencia`;

CREATE TABLE `licencia`
(
	`licencia_id` INTEGER NOT NULL AUTO_INCREMENT,
	`fecha_fin` DATE NOT NULL,
	PRIMARY KEY (`licencia_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- modulos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `modulos`;

CREATE TABLE `modulos`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`icon` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- movimientos_de_dinero
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `movimientos_de_dinero`;

CREATE TABLE `movimientos_de_dinero`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`numero_boleta` VARCHAR(191),
	`monto` DECIMAL(8,2),
	`fecha` DATETIME,
	`reembolso` DECIMAL(8,2),
	`glosa` VARCHAR(191),
	`forma_pago_id` INTEGER(10) NOT NULL,
	`tipo_comprobante_id` INTEGER(10),
	`caja_id` INTEGER(10) NOT NULL,
	`usuario_id` INTEGER(10),
	`concepto_pago_id` INTEGER(10) DEFAULT 1 NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`cliente_id` INTEGER(10),
	PRIMARY KEY (`id`),
	INDEX `movimientos_forma_pago_id_foreign` (`forma_pago_id`(10)),
	INDEX `movimientos_tipo_comprobante_id_foreign` (`tipo_comprobante_id`(10)),
	INDEX `movimientos_caja_id_foreign` (`caja_id`(10)),
	INDEX `movimientos_concepto_pago_id_foreign` (`concepto_pago_id`(10)),
	INDEX `movimientos_de_dinero_ibfk_5` (`cliente_id`(10)),
	CONSTRAINT `movimientos_de_dinero_ibfk_1`
		FOREIGN KEY (`caja_id`)
		REFERENCES `cajas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_3`
		FOREIGN KEY (`forma_pago_id`)
		REFERENCES `formas_pagos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_4`
		FOREIGN KEY (`tipo_comprobante_id`)
		REFERENCES `tipos_comprobantes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `movimientos_de_dinero_ibfk_5`
		FOREIGN KEY (`cliente_id`)
		REFERENCES `clientes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos`;

CREATE TABLE `permisos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idaccion` INTEGER NOT NULL,
	`idsubmodulo` INTEGER,
	`nombre` VARCHAR(255) NOT NULL,
	`idmodulo` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `idaccion` (`idaccion`),
	INDEX `idmodulo` (`idsubmodulo`),
	INDEX `permisos_ibfk_3` (`idmodulo`),
	CONSTRAINT `permisos_ibfk_2`
		FOREIGN KEY (`idaccion`)
		REFERENCES `acciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_ibfk_3`
		FOREIGN KEY (`idmodulo`)
		REFERENCES `modulos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_ibfk_4`
		FOREIGN KEY (`idsubmodulo`)
		REFERENCES `submodulo` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos_grupo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos_grupo`;

CREATE TABLE `permisos_grupo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idpermiso` INTEGER NOT NULL,
	`idgrupo` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `permisos_grupo_ibfk_1` (`idpermiso`),
	INDEX `permisos_grupo_ibfk_2` (`idgrupo`),
	CONSTRAINT `permisos_grupo_ibfk_1`
		FOREIGN KEY (`idpermiso`)
		REFERENCES `permisos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_grupo_ibfk_2`
		FOREIGN KEY (`idgrupo`)
		REFERENCES `grupos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos_usuario
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos_usuario`;

CREATE TABLE `permisos_usuario`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idusuario` INTEGER,
	`idpermiso` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `permisos_usuario_ibfk_1` (`idpermiso`),
	INDEX `permisos_usuario_ibfk_2` (`idusuario`),
	CONSTRAINT `permisos_usuario_ibfk_1`
		FOREIGN KEY (`idpermiso`)
		REFERENCES `permisos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_usuario_ibfk_2`
		FOREIGN KEY (`idusuario`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- producto_consumido
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `producto_consumido`;

CREATE TABLE `producto_consumido`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`producto_id` INTEGER NOT NULL,
	`habita_estadia_id` INTEGER NOT NULL,
	`cantidad` INTEGER,
	`monto` FLOAT(255,0),
	PRIMARY KEY (`id`),
	INDEX `habitacion_estadia_id` (`habita_estadia_id`),
	INDEX `productos_consumidos_ibfk_1` (`producto_id`),
	CONSTRAINT `producto_consumido_ibfk_1`
		FOREIGN KEY (`habita_estadia_id`)
		REFERENCES `estadias_habitaciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `productos_consumidos_ibfk_1`
		FOREIGN KEY (`producto_id`)
		REFERENCES `productos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- producto_consumido_hotel
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `producto_consumido_hotel`;

CREATE TABLE `producto_consumido_hotel`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`producto_id` INTEGER NOT NULL,
	`hotel` INTEGER NOT NULL,
	`cantidad` INTEGER,
	`monto` FLOAT(255,0),
	PRIMARY KEY (`id`),
	INDEX `habitacion_estadia_id` (`hotel`),
	INDEX `productos_consumidos_ibfk_1` (`producto_id`),
	CONSTRAINT `producto_consumido_hotel_ibfk_1`
		FOREIGN KEY (`producto_id`)
		REFERENCES `productos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `producto_consumido_hotel_ibfk_2`
		FOREIGN KEY (`hotel`)
		REFERENCES `ventas_hotel` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- productos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(191) NOT NULL,
	`descripcion` VARCHAR(191) NOT NULL,
	`precio` DECIMAL(8,2) NOT NULL,
	`imagen` VARCHAR(191) NOT NULL,
	`stock` INTEGER NOT NULL,
	`categoria_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `productos_categoria_id_foreign` (`categoria_id`(10)),
	CONSTRAINT `productos_ibfk_1`
		FOREIGN KEY (`categoria_id`)
		REFERENCES `categorias` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- series
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `series`;

CREATE TABLE `series`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`serie` INTEGER NOT NULL,
	`correlativo` INTEGER NOT NULL,
	`tipo_comprobante_id` INTEGER(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `series_tipo_comprobante_id_foreign` (`tipo_comprobante_id`(10)),
	CONSTRAINT `series_ibfk_1`
		FOREIGN KEY (`tipo_comprobante_id`)
		REFERENCES `tipos_comprobantes` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- submodulo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `submodulo`;

CREATE TABLE `submodulo`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`icon` VARCHAR(255) NOT NULL,
	`idmodulo` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `pk_mod` (`idmodulo`),
	CONSTRAINT `submodulo_ibfk_1`
		FOREIGN KEY (`idmodulo`)
		REFERENCES `modulos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- televisores
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `televisores`;

CREATE TABLE `televisores`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(191) NOT NULL,
	`estado` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipos`;

CREATE TABLE `tipos`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`tipo` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipos_comprobantes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipos_comprobantes`;

CREATE TABLE `tipos_comprobantes`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(191) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipos_concepto_pago
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipos_concepto_pago`;

CREATE TABLE `tipos_concepto_pago`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(50) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipos_habitaciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipos_habitaciones`;

CREATE TABLE `tipos_habitaciones`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(191) NOT NULL,
	`precio` DECIMAL(8,2) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`max_personas` INTEGER,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- urls_permitidas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urls_permitidas`;

CREATE TABLE `urls_permitidas`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`url` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usuarios
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(50) NOT NULL,
	`apellidos` VARCHAR(50) NOT NULL,
	`clave` VARCHAR(50) NOT NULL,
	`fecha_ingreso` DATE NOT NULL,
	`fecha_salida` DATE NOT NULL,
	`esta_activo` VARCHAR(20) NOT NULL,
	`correo` VARCHAR(50) NOT NULL,
	`telefono` INTEGER NOT NULL,
	`direccion` VARCHAR(60) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usuarios_grupo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios_grupo`;

CREATE TABLE `usuarios_grupo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER NOT NULL,
	`grupo_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `usuario_id` (`usuario_id`),
	INDEX `grupo_id` (`grupo_id`),
	CONSTRAINT `usuarios_grupo_ibfk_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `usuarios_grupo_ibfk_2`
		FOREIGN KEY (`grupo_id`)
		REFERENCES `grupos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ventas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ventas`;

CREATE TABLE `ventas`
(
	`id` INTEGER(10) NOT NULL AUTO_INCREMENT,
	`estado` FLOAT(191,0) DEFAULT 1 NOT NULL,
	`estadia_id` INTEGER(10),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`movimiento_id` INTEGER(10),
	`monto` DECIMAL DEFAULT 0,
	`cuota` DECIMAL DEFAULT 0.00,
	`resto` DECIMAL DEFAULT 0.00,
	PRIMARY KEY (`id`),
	INDEX `ventas_cliente_id_foreign` (`estadia_id`(10)),
	INDEX `ventas_ibfk_2` (`movimiento_id`(10)),
	CONSTRAINT `ventas_ibfk_2`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `ventas_ibfk_3`
		FOREIGN KEY (`estadia_id`)
		REFERENCES `estadias` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ventas_hotel
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ventas_hotel`;

CREATE TABLE `ventas_hotel`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`movimiento_id` INTEGER NOT NULL,
	`monto` DECIMAL DEFAULT 0.00,
	`efectivo` DECIMAL DEFAULT 0.00,
	PRIMARY KEY (`id`),
	INDEX `habitacion_estadia_id` (`movimiento_id`),
	CONSTRAINT `ventas_hotel_ibfk_3`
		FOREIGN KEY (`movimiento_id`)
		REFERENCES `movimientos_de_dinero` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
