<?php

use Dabl\Query\Query;

abstract class baseHabitacionesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Habitaciones::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return HabitacionesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new HabitacionesQuery($table_name, $alias);
	}

	/**
	 * @return Habitaciones[]
	 */
	function select() {
		return Habitaciones::doSelect($this);
	}

	/**
	 * @return Habitaciones
	 */
	function selectOne() {
		return Habitaciones::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Habitaciones::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Habitaciones::doCount($this);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Habitaciones::isTemporalType($type)) {
			$value = Habitaciones::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Habitaciones::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Habitaciones::isTemporalType($type)) {
			$value = Habitaciones::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Habitaciones::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andId($integer) {
		return $this->addAnd(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdNull() {
		return $this->andNull(Habitaciones::ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Habitaciones::ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Habitaciones::ID, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orId($integer) {
		return $this->or(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdNull() {
		return $this->orNull(Habitaciones::ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Habitaciones::ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Habitaciones::ID, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Habitaciones::ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Habitaciones::ID, $integer);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Habitaciones::ID, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Habitaciones::ID, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupById() {
		return $this->groupBy(Habitaciones::ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumero($integer) {
		return $this->addAnd(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroNot($integer) {
		return $this->andNot(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroLike($integer) {
		return $this->andLike(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroNotLike($integer) {
		return $this->andNotLike(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroGreater($integer) {
		return $this->andGreater(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroGreaterEqual($integer) {
		return $this->andGreaterEqual(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroLess($integer) {
		return $this->andLess(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroLessEqual($integer) {
		return $this->andLessEqual(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroNull() {
		return $this->andNull(Habitaciones::NUMERO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroNotNull() {
		return $this->andNotNull(Habitaciones::NUMERO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroBetween($integer, $from, $to) {
		return $this->andBetween(Habitaciones::NUMERO, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroBeginsWith($integer) {
		return $this->andBeginsWith(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroEndsWith($integer) {
		return $this->andEndsWith(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andNumeroContains($integer) {
		return $this->andContains(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumero($integer) {
		return $this->or(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroNot($integer) {
		return $this->orNot(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroLike($integer) {
		return $this->orLike(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroNotLike($integer) {
		return $this->orNotLike(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroGreater($integer) {
		return $this->orGreater(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroGreaterEqual($integer) {
		return $this->orGreaterEqual(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroLess($integer) {
		return $this->orLess(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroLessEqual($integer) {
		return $this->orLessEqual(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroNull() {
		return $this->orNull(Habitaciones::NUMERO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroNotNull() {
		return $this->orNotNull(Habitaciones::NUMERO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroBetween($integer, $from, $to) {
		return $this->orBetween(Habitaciones::NUMERO, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroBeginsWith($integer) {
		return $this->orBeginsWith(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroEndsWith($integer) {
		return $this->orEndsWith(Habitaciones::NUMERO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orNumeroContains($integer) {
		return $this->orContains(Habitaciones::NUMERO, $integer);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByNumeroAsc() {
		return $this->orderBy(Habitaciones::NUMERO, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByNumeroDesc() {
		return $this->orderBy(Habitaciones::NUMERO, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByNumero() {
		return $this->groupBy(Habitaciones::NUMERO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Habitaciones::DESCRIPCION);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Habitaciones::DESCRIPCION);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Habitaciones::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Habitaciones::DESCRIPCION);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Habitaciones::DESCRIPCION);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Habitaciones::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Habitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Habitaciones::DESCRIPCION, $varchar);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Habitaciones::DESCRIPCION, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Habitaciones::DESCRIPCION, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Habitaciones::DESCRIPCION);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstado($varchar) {
		return $this->addAnd(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoNot($varchar) {
		return $this->andNot(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoLike($varchar) {
		return $this->andLike(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoNotLike($varchar) {
		return $this->andNotLike(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoGreater($varchar) {
		return $this->andGreater(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoLess($varchar) {
		return $this->andLess(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoLessEqual($varchar) {
		return $this->andLessEqual(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoNull() {
		return $this->andNull(Habitaciones::ESTADO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoNotNull() {
		return $this->andNotNull(Habitaciones::ESTADO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoBetween($varchar, $from, $to) {
		return $this->andBetween(Habitaciones::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoBeginsWith($varchar) {
		return $this->andBeginsWith(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoEndsWith($varchar) {
		return $this->andEndsWith(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andEstadoContains($varchar) {
		return $this->andContains(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstado($varchar) {
		return $this->or(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoNot($varchar) {
		return $this->orNot(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoLike($varchar) {
		return $this->orLike(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoNotLike($varchar) {
		return $this->orNotLike(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoGreater($varchar) {
		return $this->orGreater(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoLess($varchar) {
		return $this->orLess(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoLessEqual($varchar) {
		return $this->orLessEqual(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoNull() {
		return $this->orNull(Habitaciones::ESTADO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoNotNull() {
		return $this->orNotNull(Habitaciones::ESTADO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoBetween($varchar, $from, $to) {
		return $this->orBetween(Habitaciones::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoBeginsWith($varchar) {
		return $this->orBeginsWith(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoEndsWith($varchar) {
		return $this->orEndsWith(Habitaciones::ESTADO, $varchar);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orEstadoContains($varchar) {
		return $this->orContains(Habitaciones::ESTADO, $varchar);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByEstadoAsc() {
		return $this->orderBy(Habitaciones::ESTADO, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByEstadoDesc() {
		return $this->orderBy(Habitaciones::ESTADO, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByEstado() {
		return $this->groupBy(Habitaciones::ESTADO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPiso($integer) {
		return $this->addAnd(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoNot($integer) {
		return $this->andNot(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoLike($integer) {
		return $this->andLike(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoNotLike($integer) {
		return $this->andNotLike(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoGreater($integer) {
		return $this->andGreater(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoGreaterEqual($integer) {
		return $this->andGreaterEqual(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoLess($integer) {
		return $this->andLess(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoLessEqual($integer) {
		return $this->andLessEqual(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoNull() {
		return $this->andNull(Habitaciones::PISO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoNotNull() {
		return $this->andNotNull(Habitaciones::PISO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoBetween($integer, $from, $to) {
		return $this->andBetween(Habitaciones::PISO, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoBeginsWith($integer) {
		return $this->andBeginsWith(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoEndsWith($integer) {
		return $this->andEndsWith(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andPisoContains($integer) {
		return $this->andContains(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPiso($integer) {
		return $this->or(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoNot($integer) {
		return $this->orNot(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoLike($integer) {
		return $this->orLike(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoNotLike($integer) {
		return $this->orNotLike(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoGreater($integer) {
		return $this->orGreater(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoGreaterEqual($integer) {
		return $this->orGreaterEqual(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoLess($integer) {
		return $this->orLess(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoLessEqual($integer) {
		return $this->orLessEqual(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoNull() {
		return $this->orNull(Habitaciones::PISO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoNotNull() {
		return $this->orNotNull(Habitaciones::PISO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoBetween($integer, $from, $to) {
		return $this->orBetween(Habitaciones::PISO, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoBeginsWith($integer) {
		return $this->orBeginsWith(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoEndsWith($integer) {
		return $this->orEndsWith(Habitaciones::PISO, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orPisoContains($integer) {
		return $this->orContains(Habitaciones::PISO, $integer);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByPisoAsc() {
		return $this->orderBy(Habitaciones::PISO, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByPisoDesc() {
		return $this->orderBy(Habitaciones::PISO, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByPiso() {
		return $this->groupBy(Habitaciones::PISO);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorId($integer) {
		return $this->addAnd(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdNot($integer) {
		return $this->andNot(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdLike($integer) {
		return $this->andLike(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdNotLike($integer) {
		return $this->andNotLike(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdGreater($integer) {
		return $this->andGreater(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdLess($integer) {
		return $this->andLess(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdLessEqual($integer) {
		return $this->andLessEqual(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdNull() {
		return $this->andNull(Habitaciones::TELEVISOR_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdNotNull() {
		return $this->andNotNull(Habitaciones::TELEVISOR_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdBetween($integer, $from, $to) {
		return $this->andBetween(Habitaciones::TELEVISOR_ID, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdBeginsWith($integer) {
		return $this->andBeginsWith(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdEndsWith($integer) {
		return $this->andEndsWith(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTelevisorIdContains($integer) {
		return $this->andContains(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorId($integer) {
		return $this->or(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdNot($integer) {
		return $this->orNot(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdLike($integer) {
		return $this->orLike(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdNotLike($integer) {
		return $this->orNotLike(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdGreater($integer) {
		return $this->orGreater(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdLess($integer) {
		return $this->orLess(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdLessEqual($integer) {
		return $this->orLessEqual(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdNull() {
		return $this->orNull(Habitaciones::TELEVISOR_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdNotNull() {
		return $this->orNotNull(Habitaciones::TELEVISOR_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdBetween($integer, $from, $to) {
		return $this->orBetween(Habitaciones::TELEVISOR_ID, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdBeginsWith($integer) {
		return $this->orBeginsWith(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdEndsWith($integer) {
		return $this->orEndsWith(Habitaciones::TELEVISOR_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTelevisorIdContains($integer) {
		return $this->orContains(Habitaciones::TELEVISOR_ID, $integer);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByTelevisorIdAsc() {
		return $this->orderBy(Habitaciones::TELEVISOR_ID, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByTelevisorIdDesc() {
		return $this->orderBy(Habitaciones::TELEVISOR_ID, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByTelevisorId() {
		return $this->groupBy(Habitaciones::TELEVISOR_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionId($integer) {
		return $this->addAnd(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdNot($integer) {
		return $this->andNot(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdLike($integer) {
		return $this->andLike(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdNotLike($integer) {
		return $this->andNotLike(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdGreater($integer) {
		return $this->andGreater(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdLess($integer) {
		return $this->andLess(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdLessEqual($integer) {
		return $this->andLessEqual(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdNull() {
		return $this->andNull(Habitaciones::TIPO_HABITACION_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdNotNull() {
		return $this->andNotNull(Habitaciones::TIPO_HABITACION_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdBetween($integer, $from, $to) {
		return $this->andBetween(Habitaciones::TIPO_HABITACION_ID, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdBeginsWith($integer) {
		return $this->andBeginsWith(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdEndsWith($integer) {
		return $this->andEndsWith(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andTipoHabitacionIdContains($integer) {
		return $this->andContains(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionId($integer) {
		return $this->or(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdNot($integer) {
		return $this->orNot(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdLike($integer) {
		return $this->orLike(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdNotLike($integer) {
		return $this->orNotLike(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdGreater($integer) {
		return $this->orGreater(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdLess($integer) {
		return $this->orLess(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdLessEqual($integer) {
		return $this->orLessEqual(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdNull() {
		return $this->orNull(Habitaciones::TIPO_HABITACION_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdNotNull() {
		return $this->orNotNull(Habitaciones::TIPO_HABITACION_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdBetween($integer, $from, $to) {
		return $this->orBetween(Habitaciones::TIPO_HABITACION_ID, $integer, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdBeginsWith($integer) {
		return $this->orBeginsWith(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdEndsWith($integer) {
		return $this->orEndsWith(Habitaciones::TIPO_HABITACION_ID, $integer);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orTipoHabitacionIdContains($integer) {
		return $this->orContains(Habitaciones::TIPO_HABITACION_ID, $integer);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByTipoHabitacionIdAsc() {
		return $this->orderBy(Habitaciones::TIPO_HABITACION_ID, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByTipoHabitacionIdDesc() {
		return $this->orderBy(Habitaciones::TIPO_HABITACION_ID, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByTipoHabitacionId() {
		return $this->groupBy(Habitaciones::TIPO_HABITACION_ID);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Habitaciones::CREATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Habitaciones::CREATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Habitaciones::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Habitaciones::CREATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Habitaciones::CREATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Habitaciones::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Habitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Habitaciones::CREATED_AT, $timestamp);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Habitaciones::CREATED_AT, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Habitaciones::CREATED_AT, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Habitaciones::CREATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Habitaciones::UPDATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Habitaciones::UPDATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Habitaciones::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Habitaciones::UPDATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Habitaciones::UPDATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Habitaciones::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Habitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Habitaciones::UPDATED_AT, $timestamp);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Habitaciones::UPDATED_AT, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Habitaciones::UPDATED_AT, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Habitaciones::UPDATED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Habitaciones::DELETED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Habitaciones::DELETED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Habitaciones::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Habitaciones::DELETED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Habitaciones::DELETED_AT);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Habitaciones::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Habitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Habitaciones::DELETED_AT, $timestamp);
	}


	/**
	 * @return HabitacionesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Habitaciones::DELETED_AT, self::ASC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Habitaciones::DELETED_AT, self::DESC);
	}

	/**
	 * @return HabitacionesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Habitaciones::DELETED_AT);
	}


}