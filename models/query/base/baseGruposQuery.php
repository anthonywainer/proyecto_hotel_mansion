<?php

use Dabl\Query\Query;

abstract class baseGruposQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Grupos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return GruposQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new GruposQuery($table_name, $alias);
	}

	/**
	 * @return Grupos[]
	 */
	function select() {
		return Grupos::doSelect($this);
	}

	/**
	 * @return Grupos
	 */
	function selectOne() {
		return Grupos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Grupos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Grupos::doCount($this);
	}

	/**
	 * @return GruposQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Grupos::isTemporalType($type)) {
			$value = Grupos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Grupos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return GruposQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Grupos::isTemporalType($type)) {
			$value = Grupos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Grupos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return GruposQuery
	 */
	function andId($integer) {
		return $this->addAnd(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdNull() {
		return $this->andNull(Grupos::ID);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Grupos::ID);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Grupos::ID, $integer, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orId($integer) {
		return $this->or(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdNull() {
		return $this->orNull(Grupos::ID);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Grupos::ID);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Grupos::ID, $integer, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Grupos::ID, $integer);
	}

	/**
	 * @return GruposQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Grupos::ID, $integer);
	}


	/**
	 * @return GruposQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Grupos::ID, self::ASC);
	}

	/**
	 * @return GruposQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Grupos::ID, self::DESC);
	}

	/**
	 * @return GruposQuery
	 */
	function groupById() {
		return $this->groupBy(Grupos::ID);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Grupos::DESCRIPCION);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Grupos::DESCRIPCION);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Grupos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Grupos::DESCRIPCION);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Grupos::DESCRIPCION);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Grupos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Grupos::DESCRIPCION, $varchar);
	}

	/**
	 * @return GruposQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Grupos::DESCRIPCION, $varchar);
	}


	/**
	 * @return GruposQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Grupos::DESCRIPCION, self::ASC);
	}

	/**
	 * @return GruposQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Grupos::DESCRIPCION, self::DESC);
	}

	/**
	 * @return GruposQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Grupos::DESCRIPCION);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Grupos::CREATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Grupos::CREATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Grupos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Grupos::CREATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Grupos::CREATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Grupos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Grupos::CREATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Grupos::CREATED_AT, $timestamp);
	}


	/**
	 * @return GruposQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Grupos::CREATED_AT, self::ASC);
	}

	/**
	 * @return GruposQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Grupos::CREATED_AT, self::DESC);
	}

	/**
	 * @return GruposQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Grupos::CREATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Grupos::UPDATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Grupos::UPDATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Grupos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Grupos::UPDATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Grupos::UPDATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Grupos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Grupos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Grupos::UPDATED_AT, $timestamp);
	}


	/**
	 * @return GruposQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Grupos::UPDATED_AT, self::ASC);
	}

	/**
	 * @return GruposQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Grupos::UPDATED_AT, self::DESC);
	}

	/**
	 * @return GruposQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Grupos::UPDATED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Grupos::DELETED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Grupos::DELETED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Grupos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Grupos::DELETED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Grupos::DELETED_AT);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Grupos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Grupos::DELETED_AT, $timestamp);
	}

	/**
	 * @return GruposQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Grupos::DELETED_AT, $timestamp);
	}


	/**
	 * @return GruposQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Grupos::DELETED_AT, self::ASC);
	}

	/**
	 * @return GruposQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Grupos::DELETED_AT, self::DESC);
	}

	/**
	 * @return GruposQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Grupos::DELETED_AT);
	}


}