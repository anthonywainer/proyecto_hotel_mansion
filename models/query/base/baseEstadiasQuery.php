<?php

use Dabl\Query\Query;

abstract class baseEstadiasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Estadias::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return EstadiasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new EstadiasQuery($table_name, $alias);
	}

	/**
	 * @return Estadias[]
	 */
	function select() {
		return Estadias::doSelect($this);
	}

	/**
	 * @return Estadias
	 */
	function selectOne() {
		return Estadias::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Estadias::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Estadias::doCount($this);
	}

	/**
	 * @return EstadiasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Estadias::isTemporalType($type)) {
			$value = Estadias::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Estadias::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return EstadiasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Estadias::isTemporalType($type)) {
			$value = Estadias::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Estadias::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andId($integer) {
		return $this->addAnd(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdNull() {
		return $this->andNull(Estadias::ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Estadias::ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Estadias::ID, $integer, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orId($integer) {
		return $this->or(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdNull() {
		return $this->orNull(Estadias::ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Estadias::ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Estadias::ID, $integer, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Estadias::ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Estadias::ID, $integer);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Estadias::ID, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Estadias::ID, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupById() {
		return $this->groupBy(Estadias::ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReserva($timestamp) {
		return $this->addAnd(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaNot($timestamp) {
		return $this->andNot(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaLike($timestamp) {
		return $this->andLike(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaNotLike($timestamp) {
		return $this->andNotLike(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaGreater($timestamp) {
		return $this->andGreater(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaLess($timestamp) {
		return $this->andLess(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaLessEqual($timestamp) {
		return $this->andLessEqual(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaNull() {
		return $this->andNull(Estadias::FECHA_RESERVA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaNotNull() {
		return $this->andNotNull(Estadias::FECHA_RESERVA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaBetween($timestamp, $from, $to) {
		return $this->andBetween(Estadias::FECHA_RESERVA, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaBeginsWith($timestamp) {
		return $this->andBeginsWith(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaEndsWith($timestamp) {
		return $this->andEndsWith(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaReservaContains($timestamp) {
		return $this->andContains(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReserva($timestamp) {
		return $this->or(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaNot($timestamp) {
		return $this->orNot(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaLike($timestamp) {
		return $this->orLike(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaNotLike($timestamp) {
		return $this->orNotLike(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaGreater($timestamp) {
		return $this->orGreater(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaLess($timestamp) {
		return $this->orLess(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaLessEqual($timestamp) {
		return $this->orLessEqual(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaNull() {
		return $this->orNull(Estadias::FECHA_RESERVA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaNotNull() {
		return $this->orNotNull(Estadias::FECHA_RESERVA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaBetween($timestamp, $from, $to) {
		return $this->orBetween(Estadias::FECHA_RESERVA, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaBeginsWith($timestamp) {
		return $this->orBeginsWith(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaEndsWith($timestamp) {
		return $this->orEndsWith(Estadias::FECHA_RESERVA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaReservaContains($timestamp) {
		return $this->orContains(Estadias::FECHA_RESERVA, $timestamp);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByFechaReservaAsc() {
		return $this->orderBy(Estadias::FECHA_RESERVA, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByFechaReservaDesc() {
		return $this->orderBy(Estadias::FECHA_RESERVA, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByFechaReserva() {
		return $this->groupBy(Estadias::FECHA_RESERVA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngreso($timestamp) {
		return $this->addAnd(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoNot($timestamp) {
		return $this->andNot(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoLike($timestamp) {
		return $this->andLike(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoNotLike($timestamp) {
		return $this->andNotLike(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoGreater($timestamp) {
		return $this->andGreater(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoLess($timestamp) {
		return $this->andLess(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoLessEqual($timestamp) {
		return $this->andLessEqual(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoNull() {
		return $this->andNull(Estadias::FECHA_INGRESO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoNotNull() {
		return $this->andNotNull(Estadias::FECHA_INGRESO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoBetween($timestamp, $from, $to) {
		return $this->andBetween(Estadias::FECHA_INGRESO, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoBeginsWith($timestamp) {
		return $this->andBeginsWith(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoEndsWith($timestamp) {
		return $this->andEndsWith(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaIngresoContains($timestamp) {
		return $this->andContains(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngreso($timestamp) {
		return $this->or(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoNot($timestamp) {
		return $this->orNot(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoLike($timestamp) {
		return $this->orLike(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoNotLike($timestamp) {
		return $this->orNotLike(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoGreater($timestamp) {
		return $this->orGreater(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoLess($timestamp) {
		return $this->orLess(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoLessEqual($timestamp) {
		return $this->orLessEqual(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoNull() {
		return $this->orNull(Estadias::FECHA_INGRESO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoNotNull() {
		return $this->orNotNull(Estadias::FECHA_INGRESO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoBetween($timestamp, $from, $to) {
		return $this->orBetween(Estadias::FECHA_INGRESO, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoBeginsWith($timestamp) {
		return $this->orBeginsWith(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoEndsWith($timestamp) {
		return $this->orEndsWith(Estadias::FECHA_INGRESO, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaIngresoContains($timestamp) {
		return $this->orContains(Estadias::FECHA_INGRESO, $timestamp);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByFechaIngresoAsc() {
		return $this->orderBy(Estadias::FECHA_INGRESO, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByFechaIngresoDesc() {
		return $this->orderBy(Estadias::FECHA_INGRESO, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByFechaIngreso() {
		return $this->groupBy(Estadias::FECHA_INGRESO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalida($timestamp) {
		return $this->addAnd(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaNot($timestamp) {
		return $this->andNot(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaLike($timestamp) {
		return $this->andLike(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaNotLike($timestamp) {
		return $this->andNotLike(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaGreater($timestamp) {
		return $this->andGreater(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaLess($timestamp) {
		return $this->andLess(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaLessEqual($timestamp) {
		return $this->andLessEqual(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaNull() {
		return $this->andNull(Estadias::FECHA_SALIDA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaNotNull() {
		return $this->andNotNull(Estadias::FECHA_SALIDA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaBetween($timestamp, $from, $to) {
		return $this->andBetween(Estadias::FECHA_SALIDA, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaBeginsWith($timestamp) {
		return $this->andBeginsWith(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaEndsWith($timestamp) {
		return $this->andEndsWith(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andFechaSalidaContains($timestamp) {
		return $this->andContains(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalida($timestamp) {
		return $this->or(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaNot($timestamp) {
		return $this->orNot(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaLike($timestamp) {
		return $this->orLike(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaNotLike($timestamp) {
		return $this->orNotLike(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaGreater($timestamp) {
		return $this->orGreater(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaLess($timestamp) {
		return $this->orLess(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaLessEqual($timestamp) {
		return $this->orLessEqual(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaNull() {
		return $this->orNull(Estadias::FECHA_SALIDA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaNotNull() {
		return $this->orNotNull(Estadias::FECHA_SALIDA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaBetween($timestamp, $from, $to) {
		return $this->orBetween(Estadias::FECHA_SALIDA, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaBeginsWith($timestamp) {
		return $this->orBeginsWith(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaEndsWith($timestamp) {
		return $this->orEndsWith(Estadias::FECHA_SALIDA, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orFechaSalidaContains($timestamp) {
		return $this->orContains(Estadias::FECHA_SALIDA, $timestamp);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByFechaSalidaAsc() {
		return $this->orderBy(Estadias::FECHA_SALIDA, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByFechaSalidaDesc() {
		return $this->orderBy(Estadias::FECHA_SALIDA, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByFechaSalida() {
		return $this->groupBy(Estadias::FECHA_SALIDA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstado($varchar) {
		return $this->addAnd(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoNot($varchar) {
		return $this->andNot(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoLike($varchar) {
		return $this->andLike(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoNotLike($varchar) {
		return $this->andNotLike(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoGreater($varchar) {
		return $this->andGreater(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoLess($varchar) {
		return $this->andLess(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoLessEqual($varchar) {
		return $this->andLessEqual(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoNull() {
		return $this->andNull(Estadias::ESTADO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoNotNull() {
		return $this->andNotNull(Estadias::ESTADO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoBetween($varchar, $from, $to) {
		return $this->andBetween(Estadias::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoBeginsWith($varchar) {
		return $this->andBeginsWith(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoEndsWith($varchar) {
		return $this->andEndsWith(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andEstadoContains($varchar) {
		return $this->andContains(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstado($varchar) {
		return $this->or(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoNot($varchar) {
		return $this->orNot(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoLike($varchar) {
		return $this->orLike(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoNotLike($varchar) {
		return $this->orNotLike(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoGreater($varchar) {
		return $this->orGreater(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoLess($varchar) {
		return $this->orLess(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoLessEqual($varchar) {
		return $this->orLessEqual(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoNull() {
		return $this->orNull(Estadias::ESTADO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoNotNull() {
		return $this->orNotNull(Estadias::ESTADO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoBetween($varchar, $from, $to) {
		return $this->orBetween(Estadias::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoBeginsWith($varchar) {
		return $this->orBeginsWith(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoEndsWith($varchar) {
		return $this->orEndsWith(Estadias::ESTADO, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orEstadoContains($varchar) {
		return $this->orContains(Estadias::ESTADO, $varchar);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByEstadoAsc() {
		return $this->orderBy(Estadias::ESTADO, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByEstadoDesc() {
		return $this->orderBy(Estadias::ESTADO, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByEstado() {
		return $this->groupBy(Estadias::ESTADO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(Estadias::CLIENTE_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(Estadias::CLIENTE_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(Estadias::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteId($integer) {
		return $this->or(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(Estadias::CLIENTE_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(Estadias::CLIENTE_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(Estadias::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(Estadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(Estadias::CLIENTE_ID, $integer);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(Estadias::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(Estadias::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(Estadias::CLIENTE_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Estadias::CREATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Estadias::CREATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Estadias::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Estadias::CREATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Estadias::CREATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Estadias::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Estadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Estadias::CREATED_AT, $timestamp);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Estadias::CREATED_AT, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Estadias::CREATED_AT, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Estadias::CREATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Estadias::UPDATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Estadias::UPDATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Estadias::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Estadias::UPDATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Estadias::UPDATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Estadias::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Estadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Estadias::UPDATED_AT, $timestamp);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Estadias::UPDATED_AT, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Estadias::UPDATED_AT, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Estadias::UPDATED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Estadias::DELETED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Estadias::DELETED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Estadias::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Estadias::DELETED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Estadias::DELETED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Estadias::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Estadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Estadias::DELETED_AT, $timestamp);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Estadias::DELETED_AT, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Estadias::DELETED_AT, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Estadias::DELETED_AT);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioId($varchar) {
		return $this->addAnd(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdNot($varchar) {
		return $this->andNot(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdLike($varchar) {
		return $this->andLike(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdNotLike($varchar) {
		return $this->andNotLike(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdGreater($varchar) {
		return $this->andGreater(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdGreaterEqual($varchar) {
		return $this->andGreaterEqual(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdLess($varchar) {
		return $this->andLess(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdLessEqual($varchar) {
		return $this->andLessEqual(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdNull() {
		return $this->andNull(Estadias::USUARIO_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdNotNull() {
		return $this->andNotNull(Estadias::USUARIO_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdBetween($varchar, $from, $to) {
		return $this->andBetween(Estadias::USUARIO_ID, $varchar, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdBeginsWith($varchar) {
		return $this->andBeginsWith(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdEndsWith($varchar) {
		return $this->andEndsWith(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andUsuarioIdContains($varchar) {
		return $this->andContains(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioId($varchar) {
		return $this->or(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdNot($varchar) {
		return $this->orNot(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdLike($varchar) {
		return $this->orLike(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdNotLike($varchar) {
		return $this->orNotLike(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdGreater($varchar) {
		return $this->orGreater(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdGreaterEqual($varchar) {
		return $this->orGreaterEqual(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdLess($varchar) {
		return $this->orLess(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdLessEqual($varchar) {
		return $this->orLessEqual(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdNull() {
		return $this->orNull(Estadias::USUARIO_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdNotNull() {
		return $this->orNotNull(Estadias::USUARIO_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdBetween($varchar, $from, $to) {
		return $this->orBetween(Estadias::USUARIO_ID, $varchar, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdBeginsWith($varchar) {
		return $this->orBeginsWith(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdEndsWith($varchar) {
		return $this->orEndsWith(Estadias::USUARIO_ID, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orUsuarioIdContains($varchar) {
		return $this->orContains(Estadias::USUARIO_ID, $varchar);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByUsuarioIdAsc() {
		return $this->orderBy(Estadias::USUARIO_ID, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByUsuarioIdDesc() {
		return $this->orderBy(Estadias::USUARIO_ID, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByUsuarioId() {
		return $this->groupBy(Estadias::USUARIO_ID);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecio($decimal) {
		return $this->addAnd(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioNot($decimal) {
		return $this->andNot(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioLike($decimal) {
		return $this->andLike(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioNotLike($decimal) {
		return $this->andNotLike(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioGreater($decimal) {
		return $this->andGreater(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioGreaterEqual($decimal) {
		return $this->andGreaterEqual(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioLess($decimal) {
		return $this->andLess(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioLessEqual($decimal) {
		return $this->andLessEqual(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioNull() {
		return $this->andNull(Estadias::PRECIO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(Estadias::PRECIO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioBetween($decimal, $from, $to) {
		return $this->andBetween(Estadias::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioBeginsWith($decimal) {
		return $this->andBeginsWith(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioEndsWith($decimal) {
		return $this->andEndsWith(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andPrecioContains($decimal) {
		return $this->andContains(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecio($decimal) {
		return $this->or(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioNot($decimal) {
		return $this->orNot(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioLike($decimal) {
		return $this->orLike(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioNotLike($decimal) {
		return $this->orNotLike(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioGreater($decimal) {
		return $this->orGreater(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioGreaterEqual($decimal) {
		return $this->orGreaterEqual(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioLess($decimal) {
		return $this->orLess(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioLessEqual($decimal) {
		return $this->orLessEqual(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioNull() {
		return $this->orNull(Estadias::PRECIO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(Estadias::PRECIO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioBetween($decimal, $from, $to) {
		return $this->orBetween(Estadias::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioBeginsWith($decimal) {
		return $this->orBeginsWith(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioEndsWith($decimal) {
		return $this->orEndsWith(Estadias::PRECIO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orPrecioContains($decimal) {
		return $this->orContains(Estadias::PRECIO, $decimal);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(Estadias::PRECIO, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(Estadias::PRECIO, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(Estadias::PRECIO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuento($decimal) {
		return $this->addAnd(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoNot($decimal) {
		return $this->andNot(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoLike($decimal) {
		return $this->andLike(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoNotLike($decimal) {
		return $this->andNotLike(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoGreater($decimal) {
		return $this->andGreater(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoGreaterEqual($decimal) {
		return $this->andGreaterEqual(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoLess($decimal) {
		return $this->andLess(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoLessEqual($decimal) {
		return $this->andLessEqual(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoNull() {
		return $this->andNull(Estadias::DESCUENTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoNotNull() {
		return $this->andNotNull(Estadias::DESCUENTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoBetween($decimal, $from, $to) {
		return $this->andBetween(Estadias::DESCUENTO, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoBeginsWith($decimal) {
		return $this->andBeginsWith(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoEndsWith($decimal) {
		return $this->andEndsWith(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andDescuentoContains($decimal) {
		return $this->andContains(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuento($decimal) {
		return $this->or(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoNot($decimal) {
		return $this->orNot(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoLike($decimal) {
		return $this->orLike(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoNotLike($decimal) {
		return $this->orNotLike(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoGreater($decimal) {
		return $this->orGreater(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoGreaterEqual($decimal) {
		return $this->orGreaterEqual(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoLess($decimal) {
		return $this->orLess(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoLessEqual($decimal) {
		return $this->orLessEqual(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoNull() {
		return $this->orNull(Estadias::DESCUENTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoNotNull() {
		return $this->orNotNull(Estadias::DESCUENTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoBetween($decimal, $from, $to) {
		return $this->orBetween(Estadias::DESCUENTO, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoBeginsWith($decimal) {
		return $this->orBeginsWith(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoEndsWith($decimal) {
		return $this->orEndsWith(Estadias::DESCUENTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orDescuentoContains($decimal) {
		return $this->orContains(Estadias::DESCUENTO, $decimal);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByDescuentoAsc() {
		return $this->orderBy(Estadias::DESCUENTO, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByDescuentoDesc() {
		return $this->orderBy(Estadias::DESCUENTO, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByDescuento() {
		return $this->groupBy(Estadias::DESCUENTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacion($varchar) {
		return $this->addAnd(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionNot($varchar) {
		return $this->andNot(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionLike($varchar) {
		return $this->andLike(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionNotLike($varchar) {
		return $this->andNotLike(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionGreater($varchar) {
		return $this->andGreater(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionLess($varchar) {
		return $this->andLess(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionLessEqual($varchar) {
		return $this->andLessEqual(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionNull() {
		return $this->andNull(Estadias::OBSERVACION);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionNotNull() {
		return $this->andNotNull(Estadias::OBSERVACION);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionBetween($varchar, $from, $to) {
		return $this->andBetween(Estadias::OBSERVACION, $varchar, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionBeginsWith($varchar) {
		return $this->andBeginsWith(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionEndsWith($varchar) {
		return $this->andEndsWith(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andObservacionContains($varchar) {
		return $this->andContains(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacion($varchar) {
		return $this->or(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionNot($varchar) {
		return $this->orNot(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionLike($varchar) {
		return $this->orLike(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionNotLike($varchar) {
		return $this->orNotLike(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionGreater($varchar) {
		return $this->orGreater(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionLess($varchar) {
		return $this->orLess(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionLessEqual($varchar) {
		return $this->orLessEqual(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionNull() {
		return $this->orNull(Estadias::OBSERVACION);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionNotNull() {
		return $this->orNotNull(Estadias::OBSERVACION);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionBetween($varchar, $from, $to) {
		return $this->orBetween(Estadias::OBSERVACION, $varchar, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionBeginsWith($varchar) {
		return $this->orBeginsWith(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionEndsWith($varchar) {
		return $this->orEndsWith(Estadias::OBSERVACION, $varchar);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orObservacionContains($varchar) {
		return $this->orContains(Estadias::OBSERVACION, $varchar);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByObservacionAsc() {
		return $this->orderBy(Estadias::OBSERVACION, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByObservacionDesc() {
		return $this->orderBy(Estadias::OBSERVACION, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByObservacion() {
		return $this->groupBy(Estadias::OBSERVACION);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagar($decimal) {
		return $this->addAnd(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarNot($decimal) {
		return $this->andNot(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarLike($decimal) {
		return $this->andLike(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarNotLike($decimal) {
		return $this->andNotLike(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarGreater($decimal) {
		return $this->andGreater(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarGreaterEqual($decimal) {
		return $this->andGreaterEqual(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarLess($decimal) {
		return $this->andLess(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarLessEqual($decimal) {
		return $this->andLessEqual(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarNull() {
		return $this->andNull(Estadias::MONTO_PAGAR);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarNotNull() {
		return $this->andNotNull(Estadias::MONTO_PAGAR);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarBetween($decimal, $from, $to) {
		return $this->andBetween(Estadias::MONTO_PAGAR, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarBeginsWith($decimal) {
		return $this->andBeginsWith(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarEndsWith($decimal) {
		return $this->andEndsWith(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andMontoPagarContains($decimal) {
		return $this->andContains(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagar($decimal) {
		return $this->or(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarNot($decimal) {
		return $this->orNot(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarLike($decimal) {
		return $this->orLike(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarNotLike($decimal) {
		return $this->orNotLike(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarGreater($decimal) {
		return $this->orGreater(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarGreaterEqual($decimal) {
		return $this->orGreaterEqual(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarLess($decimal) {
		return $this->orLess(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarLessEqual($decimal) {
		return $this->orLessEqual(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarNull() {
		return $this->orNull(Estadias::MONTO_PAGAR);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarNotNull() {
		return $this->orNotNull(Estadias::MONTO_PAGAR);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarBetween($decimal, $from, $to) {
		return $this->orBetween(Estadias::MONTO_PAGAR, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarBeginsWith($decimal) {
		return $this->orBeginsWith(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarEndsWith($decimal) {
		return $this->orEndsWith(Estadias::MONTO_PAGAR, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orMontoPagarContains($decimal) {
		return $this->orContains(Estadias::MONTO_PAGAR, $decimal);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByMontoPagarAsc() {
		return $this->orderBy(Estadias::MONTO_PAGAR, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByMontoPagarDesc() {
		return $this->orderBy(Estadias::MONTO_PAGAR, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByMontoPagar() {
		return $this->groupBy(Estadias::MONTO_PAGAR);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuota($decimal) {
		return $this->addAnd(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaNot($decimal) {
		return $this->andNot(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaLike($decimal) {
		return $this->andLike(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaNotLike($decimal) {
		return $this->andNotLike(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaGreater($decimal) {
		return $this->andGreater(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaGreaterEqual($decimal) {
		return $this->andGreaterEqual(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaLess($decimal) {
		return $this->andLess(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaLessEqual($decimal) {
		return $this->andLessEqual(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaNull() {
		return $this->andNull(Estadias::CUOTA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaNotNull() {
		return $this->andNotNull(Estadias::CUOTA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaBetween($decimal, $from, $to) {
		return $this->andBetween(Estadias::CUOTA, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaBeginsWith($decimal) {
		return $this->andBeginsWith(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaEndsWith($decimal) {
		return $this->andEndsWith(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andCuotaContains($decimal) {
		return $this->andContains(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuota($decimal) {
		return $this->or(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaNot($decimal) {
		return $this->orNot(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaLike($decimal) {
		return $this->orLike(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaNotLike($decimal) {
		return $this->orNotLike(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaGreater($decimal) {
		return $this->orGreater(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaGreaterEqual($decimal) {
		return $this->orGreaterEqual(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaLess($decimal) {
		return $this->orLess(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaLessEqual($decimal) {
		return $this->orLessEqual(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaNull() {
		return $this->orNull(Estadias::CUOTA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaNotNull() {
		return $this->orNotNull(Estadias::CUOTA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaBetween($decimal, $from, $to) {
		return $this->orBetween(Estadias::CUOTA, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaBeginsWith($decimal) {
		return $this->orBeginsWith(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaEndsWith($decimal) {
		return $this->orEndsWith(Estadias::CUOTA, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orCuotaContains($decimal) {
		return $this->orContains(Estadias::CUOTA, $decimal);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByCuotaAsc() {
		return $this->orderBy(Estadias::CUOTA, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByCuotaDesc() {
		return $this->orderBy(Estadias::CUOTA, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByCuota() {
		return $this->groupBy(Estadias::CUOTA);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andResto($decimal) {
		return $this->addAnd(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoNot($decimal) {
		return $this->andNot(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoLike($decimal) {
		return $this->andLike(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoNotLike($decimal) {
		return $this->andNotLike(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoGreater($decimal) {
		return $this->andGreater(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoGreaterEqual($decimal) {
		return $this->andGreaterEqual(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoLess($decimal) {
		return $this->andLess(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoLessEqual($decimal) {
		return $this->andLessEqual(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoNull() {
		return $this->andNull(Estadias::RESTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoNotNull() {
		return $this->andNotNull(Estadias::RESTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoBetween($decimal, $from, $to) {
		return $this->andBetween(Estadias::RESTO, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoBeginsWith($decimal) {
		return $this->andBeginsWith(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoEndsWith($decimal) {
		return $this->andEndsWith(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function andRestoContains($decimal) {
		return $this->andContains(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orResto($decimal) {
		return $this->or(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoNot($decimal) {
		return $this->orNot(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoLike($decimal) {
		return $this->orLike(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoNotLike($decimal) {
		return $this->orNotLike(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoGreater($decimal) {
		return $this->orGreater(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoGreaterEqual($decimal) {
		return $this->orGreaterEqual(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoLess($decimal) {
		return $this->orLess(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoLessEqual($decimal) {
		return $this->orLessEqual(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoNull() {
		return $this->orNull(Estadias::RESTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoNotNull() {
		return $this->orNotNull(Estadias::RESTO);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoBetween($decimal, $from, $to) {
		return $this->orBetween(Estadias::RESTO, $decimal, $from, $to);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoBeginsWith($decimal) {
		return $this->orBeginsWith(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoEndsWith($decimal) {
		return $this->orEndsWith(Estadias::RESTO, $decimal);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orRestoContains($decimal) {
		return $this->orContains(Estadias::RESTO, $decimal);
	}


	/**
	 * @return EstadiasQuery
	 */
	function orderByRestoAsc() {
		return $this->orderBy(Estadias::RESTO, self::ASC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function orderByRestoDesc() {
		return $this->orderBy(Estadias::RESTO, self::DESC);
	}

	/**
	 * @return EstadiasQuery
	 */
	function groupByResto() {
		return $this->groupBy(Estadias::RESTO);
	}


}