<?php

use Dabl\Query\Query;

abstract class baseVentasQuery extends Query {

    function __construct($table_name = null, $alias = null) {
        if (null === $table_name) {
            $table_name = Ventas::getTableName();
        }
        return parent::__construct($table_name, $alias);
    }

    /**
     * Returns new instance of self by passing arguments directly to constructor.
     * @param string $alias
     * @return VentasQuery
     */
    static function create($table_name = null, $alias = null) {
        return new VentasQuery($table_name, $alias);
    }

    /**
     * @return Ventas[]
     */
    function select() {
        return Ventas::doSelect($this);
    }

    /**
     * @return Ventas
     */
    function selectOne() {
        return Ventas::doSelectOne($this);
    }

    /**
     * @return int
     */
    function delete(){
        return Ventas::doDelete($this);
    }

    /**
     * @return int
     */
    function count(){
        return Ventas::doCount($this);
    }

    /**
     * @return VentasQuery
     */
    function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
        if (null !== $type && Ventas::isTemporalType($type)) {
            $value = Ventas::coerceTemporalValue($value, $type);
        }
        if (null === $value && is_array($column) && Model::isTemporalType($type)) {
            $column = Ventas::coerceTemporalValue($column, $type);
        }
        return parent::addAnd($column, $value, $operator, $quote);
    }

    /**
     * @return VentasQuery
     */
    function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
        if (null !== $type && Ventas::isTemporalType($type)) {
            $value = Ventas::coerceTemporalValue($value, $type);
        }
        if (null === $value && is_array($column) && Model::isTemporalType($type)) {
            $column = Ventas::coerceTemporalValue($column, $type);
        }
        return parent::addOr($column, $value, $operator, $quote);
    }

    /**
     * @return VentasQuery
     */
    function andId($integer) {
        return $this->addAnd(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdNot($integer) {
        return $this->andNot(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdLike($integer) {
        return $this->andLike(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdNotLike($integer) {
        return $this->andNotLike(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdGreater($integer) {
        return $this->andGreater(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdGreaterEqual($integer) {
        return $this->andGreaterEqual(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdLess($integer) {
        return $this->andLess(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdLessEqual($integer) {
        return $this->andLessEqual(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdNull() {
        return $this->andNull(Ventas::ID);
    }

    /**
     * @return VentasQuery
     */
    function andIdNotNull() {
        return $this->andNotNull(Ventas::ID);
    }

    /**
     * @return VentasQuery
     */
    function andIdBetween($integer, $from, $to) {
        return $this->andBetween(Ventas::ID, $integer, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andIdBeginsWith($integer) {
        return $this->andBeginsWith(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdEndsWith($integer) {
        return $this->andEndsWith(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andIdContains($integer) {
        return $this->andContains(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orId($integer) {
        return $this->or(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdNot($integer) {
        return $this->orNot(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdLike($integer) {
        return $this->orLike(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdNotLike($integer) {
        return $this->orNotLike(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdGreater($integer) {
        return $this->orGreater(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdGreaterEqual($integer) {
        return $this->orGreaterEqual(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdLess($integer) {
        return $this->orLess(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdLessEqual($integer) {
        return $this->orLessEqual(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdNull() {
        return $this->orNull(Ventas::ID);
    }

    /**
     * @return VentasQuery
     */
    function orIdNotNull() {
        return $this->orNotNull(Ventas::ID);
    }

    /**
     * @return VentasQuery
     */
    function orIdBetween($integer, $from, $to) {
        return $this->orBetween(Ventas::ID, $integer, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orIdBeginsWith($integer) {
        return $this->orBeginsWith(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdEndsWith($integer) {
        return $this->orEndsWith(Ventas::ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orIdContains($integer) {
        return $this->orContains(Ventas::ID, $integer);
    }


    /**
     * @return VentasQuery
     */
    function orderByIdAsc() {
        return $this->orderBy(Ventas::ID, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByIdDesc() {
        return $this->orderBy(Ventas::ID, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupById() {
        return $this->groupBy(Ventas::ID);
    }

    /**
     * @return VentasQuery
     */
    function andEstado($float) {
        return $this->addAnd(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoNot($float) {
        return $this->andNot(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoLike($float) {
        return $this->andLike(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoNotLike($float) {
        return $this->andNotLike(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoGreater($float) {
        return $this->andGreater(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoGreaterEqual($float) {
        return $this->andGreaterEqual(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoLess($float) {
        return $this->andLess(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoLessEqual($float) {
        return $this->andLessEqual(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoNull() {
        return $this->andNull(Ventas::ESTADO);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoNotNull() {
        return $this->andNotNull(Ventas::ESTADO);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoBetween($float, $from, $to) {
        return $this->andBetween(Ventas::ESTADO, $float, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoBeginsWith($float) {
        return $this->andBeginsWith(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoEndsWith($float) {
        return $this->andEndsWith(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function andEstadoContains($float) {
        return $this->andContains(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstado($float) {
        return $this->or(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoNot($float) {
        return $this->orNot(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoLike($float) {
        return $this->orLike(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoNotLike($float) {
        return $this->orNotLike(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoGreater($float) {
        return $this->orGreater(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoGreaterEqual($float) {
        return $this->orGreaterEqual(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoLess($float) {
        return $this->orLess(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoLessEqual($float) {
        return $this->orLessEqual(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoNull() {
        return $this->orNull(Ventas::ESTADO);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoNotNull() {
        return $this->orNotNull(Ventas::ESTADO);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoBetween($float, $from, $to) {
        return $this->orBetween(Ventas::ESTADO, $float, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoBeginsWith($float) {
        return $this->orBeginsWith(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoEndsWith($float) {
        return $this->orEndsWith(Ventas::ESTADO, $float);
    }

    /**
     * @return VentasQuery
     */
    function orEstadoContains($float) {
        return $this->orContains(Ventas::ESTADO, $float);
    }


    /**
     * @return VentasQuery
     */
    function orderByEstadoAsc() {
        return $this->orderBy(Ventas::ESTADO, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByEstadoDesc() {
        return $this->orderBy(Ventas::ESTADO, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByEstado() {
        return $this->groupBy(Ventas::ESTADO);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaId($integer) {
        return $this->addAnd(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdNot($integer) {
        return $this->andNot(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdLike($integer) {
        return $this->andLike(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdNotLike($integer) {
        return $this->andNotLike(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdGreater($integer) {
        return $this->andGreater(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdGreaterEqual($integer) {
        return $this->andGreaterEqual(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdLess($integer) {
        return $this->andLess(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdLessEqual($integer) {
        return $this->andLessEqual(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdNull() {
        return $this->andNull(Ventas::ESTADIA_ID);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdNotNull() {
        return $this->andNotNull(Ventas::ESTADIA_ID);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdBetween($integer, $from, $to) {
        return $this->andBetween(Ventas::ESTADIA_ID, $integer, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdBeginsWith($integer) {
        return $this->andBeginsWith(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdEndsWith($integer) {
        return $this->andEndsWith(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andEstadiaIdContains($integer) {
        return $this->andContains(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaId($integer) {
        return $this->or(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdNot($integer) {
        return $this->orNot(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdLike($integer) {
        return $this->orLike(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdNotLike($integer) {
        return $this->orNotLike(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdGreater($integer) {
        return $this->orGreater(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdGreaterEqual($integer) {
        return $this->orGreaterEqual(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdLess($integer) {
        return $this->orLess(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdLessEqual($integer) {
        return $this->orLessEqual(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdNull() {
        return $this->orNull(Ventas::ESTADIA_ID);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdNotNull() {
        return $this->orNotNull(Ventas::ESTADIA_ID);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdBetween($integer, $from, $to) {
        return $this->orBetween(Ventas::ESTADIA_ID, $integer, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdBeginsWith($integer) {
        return $this->orBeginsWith(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdEndsWith($integer) {
        return $this->orEndsWith(Ventas::ESTADIA_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orEstadiaIdContains($integer) {
        return $this->orContains(Ventas::ESTADIA_ID, $integer);
    }


    /**
     * @return VentasQuery
     */
    function orderByEstadiaIdAsc() {
        return $this->orderBy(Ventas::ESTADIA_ID, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByEstadiaIdDesc() {
        return $this->orderBy(Ventas::ESTADIA_ID, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByEstadiaId() {
        return $this->groupBy(Ventas::ESTADIA_ID);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAt($timestamp) {
        return $this->addAnd(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtNot($timestamp) {
        return $this->andNot(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtLike($timestamp) {
        return $this->andLike(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtNotLike($timestamp) {
        return $this->andNotLike(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtGreater($timestamp) {
        return $this->andGreater(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtGreaterEqual($timestamp) {
        return $this->andGreaterEqual(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtLess($timestamp) {
        return $this->andLess(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtLessEqual($timestamp) {
        return $this->andLessEqual(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtNull() {
        return $this->andNull(Ventas::CREATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtNotNull() {
        return $this->andNotNull(Ventas::CREATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtBetween($timestamp, $from, $to) {
        return $this->andBetween(Ventas::CREATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtBeginsWith($timestamp) {
        return $this->andBeginsWith(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtEndsWith($timestamp) {
        return $this->andEndsWith(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andCreatedAtContains($timestamp) {
        return $this->andContains(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAt($timestamp) {
        return $this->or(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtNot($timestamp) {
        return $this->orNot(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtLike($timestamp) {
        return $this->orLike(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtNotLike($timestamp) {
        return $this->orNotLike(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtGreater($timestamp) {
        return $this->orGreater(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtGreaterEqual($timestamp) {
        return $this->orGreaterEqual(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtLess($timestamp) {
        return $this->orLess(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtLessEqual($timestamp) {
        return $this->orLessEqual(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtNull() {
        return $this->orNull(Ventas::CREATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtNotNull() {
        return $this->orNotNull(Ventas::CREATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtBetween($timestamp, $from, $to) {
        return $this->orBetween(Ventas::CREATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtBeginsWith($timestamp) {
        return $this->orBeginsWith(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtEndsWith($timestamp) {
        return $this->orEndsWith(Ventas::CREATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orCreatedAtContains($timestamp) {
        return $this->orContains(Ventas::CREATED_AT, $timestamp);
    }


    /**
     * @return VentasQuery
     */
    function orderByCreatedAtAsc() {
        return $this->orderBy(Ventas::CREATED_AT, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByCreatedAtDesc() {
        return $this->orderBy(Ventas::CREATED_AT, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByCreatedAt() {
        return $this->groupBy(Ventas::CREATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAt($timestamp) {
        return $this->addAnd(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtNot($timestamp) {
        return $this->andNot(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtLike($timestamp) {
        return $this->andLike(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtNotLike($timestamp) {
        return $this->andNotLike(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtGreater($timestamp) {
        return $this->andGreater(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtGreaterEqual($timestamp) {
        return $this->andGreaterEqual(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtLess($timestamp) {
        return $this->andLess(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtLessEqual($timestamp) {
        return $this->andLessEqual(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtNull() {
        return $this->andNull(Ventas::UPDATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtNotNull() {
        return $this->andNotNull(Ventas::UPDATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtBetween($timestamp, $from, $to) {
        return $this->andBetween(Ventas::UPDATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtBeginsWith($timestamp) {
        return $this->andBeginsWith(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtEndsWith($timestamp) {
        return $this->andEndsWith(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andUpdatedAtContains($timestamp) {
        return $this->andContains(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAt($timestamp) {
        return $this->or(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtNot($timestamp) {
        return $this->orNot(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtLike($timestamp) {
        return $this->orLike(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtNotLike($timestamp) {
        return $this->orNotLike(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtGreater($timestamp) {
        return $this->orGreater(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtGreaterEqual($timestamp) {
        return $this->orGreaterEqual(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtLess($timestamp) {
        return $this->orLess(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtLessEqual($timestamp) {
        return $this->orLessEqual(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtNull() {
        return $this->orNull(Ventas::UPDATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtNotNull() {
        return $this->orNotNull(Ventas::UPDATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtBetween($timestamp, $from, $to) {
        return $this->orBetween(Ventas::UPDATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtBeginsWith($timestamp) {
        return $this->orBeginsWith(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtEndsWith($timestamp) {
        return $this->orEndsWith(Ventas::UPDATED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orUpdatedAtContains($timestamp) {
        return $this->orContains(Ventas::UPDATED_AT, $timestamp);
    }


    /**
     * @return VentasQuery
     */
    function orderByUpdatedAtAsc() {
        return $this->orderBy(Ventas::UPDATED_AT, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByUpdatedAtDesc() {
        return $this->orderBy(Ventas::UPDATED_AT, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByUpdatedAt() {
        return $this->groupBy(Ventas::UPDATED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAt($timestamp) {
        return $this->addAnd(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtNot($timestamp) {
        return $this->andNot(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtLike($timestamp) {
        return $this->andLike(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtNotLike($timestamp) {
        return $this->andNotLike(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtGreater($timestamp) {
        return $this->andGreater(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtGreaterEqual($timestamp) {
        return $this->andGreaterEqual(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtLess($timestamp) {
        return $this->andLess(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtLessEqual($timestamp) {
        return $this->andLessEqual(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtNull() {
        return $this->andNull(Ventas::DELETED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtNotNull() {
        return $this->andNotNull(Ventas::DELETED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtBetween($timestamp, $from, $to) {
        return $this->andBetween(Ventas::DELETED_AT, $timestamp, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtBeginsWith($timestamp) {
        return $this->andBeginsWith(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtEndsWith($timestamp) {
        return $this->andEndsWith(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function andDeletedAtContains($timestamp) {
        return $this->andContains(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAt($timestamp) {
        return $this->or(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtNot($timestamp) {
        return $this->orNot(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtLike($timestamp) {
        return $this->orLike(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtNotLike($timestamp) {
        return $this->orNotLike(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtGreater($timestamp) {
        return $this->orGreater(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtGreaterEqual($timestamp) {
        return $this->orGreaterEqual(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtLess($timestamp) {
        return $this->orLess(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtLessEqual($timestamp) {
        return $this->orLessEqual(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtNull() {
        return $this->orNull(Ventas::DELETED_AT);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtNotNull() {
        return $this->orNotNull(Ventas::DELETED_AT);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtBetween($timestamp, $from, $to) {
        return $this->orBetween(Ventas::DELETED_AT, $timestamp, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtBeginsWith($timestamp) {
        return $this->orBeginsWith(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtEndsWith($timestamp) {
        return $this->orEndsWith(Ventas::DELETED_AT, $timestamp);
    }

    /**
     * @return VentasQuery
     */
    function orDeletedAtContains($timestamp) {
        return $this->orContains(Ventas::DELETED_AT, $timestamp);
    }


    /**
     * @return VentasQuery
     */
    function orderByDeletedAtAsc() {
        return $this->orderBy(Ventas::DELETED_AT, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByDeletedAtDesc() {
        return $this->orderBy(Ventas::DELETED_AT, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByDeletedAt() {
        return $this->groupBy(Ventas::DELETED_AT);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoId($integer) {
        return $this->addAnd(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdNot($integer) {
        return $this->andNot(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdLike($integer) {
        return $this->andLike(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdNotLike($integer) {
        return $this->andNotLike(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdGreater($integer) {
        return $this->andGreater(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdGreaterEqual($integer) {
        return $this->andGreaterEqual(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdLess($integer) {
        return $this->andLess(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdLessEqual($integer) {
        return $this->andLessEqual(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdNull() {
        return $this->andNull(Ventas::MOVIMIENTO_ID);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdNotNull() {
        return $this->andNotNull(Ventas::MOVIMIENTO_ID);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdBetween($integer, $from, $to) {
        return $this->andBetween(Ventas::MOVIMIENTO_ID, $integer, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdBeginsWith($integer) {
        return $this->andBeginsWith(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdEndsWith($integer) {
        return $this->andEndsWith(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function andMovimientoIdContains($integer) {
        return $this->andContains(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoId($integer) {
        return $this->or(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdNot($integer) {
        return $this->orNot(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdLike($integer) {
        return $this->orLike(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdNotLike($integer) {
        return $this->orNotLike(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdGreater($integer) {
        return $this->orGreater(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdGreaterEqual($integer) {
        return $this->orGreaterEqual(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdLess($integer) {
        return $this->orLess(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdLessEqual($integer) {
        return $this->orLessEqual(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdNull() {
        return $this->orNull(Ventas::MOVIMIENTO_ID);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdNotNull() {
        return $this->orNotNull(Ventas::MOVIMIENTO_ID);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdBetween($integer, $from, $to) {
        return $this->orBetween(Ventas::MOVIMIENTO_ID, $integer, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdBeginsWith($integer) {
        return $this->orBeginsWith(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdEndsWith($integer) {
        return $this->orEndsWith(Ventas::MOVIMIENTO_ID, $integer);
    }

    /**
     * @return VentasQuery
     */
    function orMovimientoIdContains($integer) {
        return $this->orContains(Ventas::MOVIMIENTO_ID, $integer);
    }


    /**
     * @return VentasQuery
     */
    function orderByMovimientoIdAsc() {
        return $this->orderBy(Ventas::MOVIMIENTO_ID, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByMovimientoIdDesc() {
        return $this->orderBy(Ventas::MOVIMIENTO_ID, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByMovimientoId() {
        return $this->groupBy(Ventas::MOVIMIENTO_ID);
    }

    /**
     * @return VentasQuery
     */
    function andMonto($decimal) {
        return $this->addAnd(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoNot($decimal) {
        return $this->andNot(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoLike($decimal) {
        return $this->andLike(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoNotLike($decimal) {
        return $this->andNotLike(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoGreater($decimal) {
        return $this->andGreater(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoGreaterEqual($decimal) {
        return $this->andGreaterEqual(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoLess($decimal) {
        return $this->andLess(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoLessEqual($decimal) {
        return $this->andLessEqual(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoNull() {
        return $this->andNull(Ventas::MONTO);
    }

    /**
     * @return VentasQuery
     */
    function andMontoNotNull() {
        return $this->andNotNull(Ventas::MONTO);
    }

    /**
     * @return VentasQuery
     */
    function andMontoBetween($decimal, $from, $to) {
        return $this->andBetween(Ventas::MONTO, $decimal, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andMontoBeginsWith($decimal) {
        return $this->andBeginsWith(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoEndsWith($decimal) {
        return $this->andEndsWith(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andMontoContains($decimal) {
        return $this->andContains(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMonto($decimal) {
        return $this->or(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoNot($decimal) {
        return $this->orNot(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoLike($decimal) {
        return $this->orLike(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoNotLike($decimal) {
        return $this->orNotLike(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoGreater($decimal) {
        return $this->orGreater(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoGreaterEqual($decimal) {
        return $this->orGreaterEqual(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoLess($decimal) {
        return $this->orLess(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoLessEqual($decimal) {
        return $this->orLessEqual(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoNull() {
        return $this->orNull(Ventas::MONTO);
    }

    /**
     * @return VentasQuery
     */
    function orMontoNotNull() {
        return $this->orNotNull(Ventas::MONTO);
    }

    /**
     * @return VentasQuery
     */
    function orMontoBetween($decimal, $from, $to) {
        return $this->orBetween(Ventas::MONTO, $decimal, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orMontoBeginsWith($decimal) {
        return $this->orBeginsWith(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoEndsWith($decimal) {
        return $this->orEndsWith(Ventas::MONTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orMontoContains($decimal) {
        return $this->orContains(Ventas::MONTO, $decimal);
    }


    /**
     * @return VentasQuery
     */
    function orderByMontoAsc() {
        return $this->orderBy(Ventas::MONTO, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByMontoDesc() {
        return $this->orderBy(Ventas::MONTO, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByMonto() {
        return $this->groupBy(Ventas::MONTO);
    }

    /**
     * @return VentasQuery
     */
    function andCuota($decimal) {
        return $this->addAnd(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaNot($decimal) {
        return $this->andNot(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaLike($decimal) {
        return $this->andLike(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaNotLike($decimal) {
        return $this->andNotLike(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaGreater($decimal) {
        return $this->andGreater(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaGreaterEqual($decimal) {
        return $this->andGreaterEqual(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaLess($decimal) {
        return $this->andLess(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaLessEqual($decimal) {
        return $this->andLessEqual(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaNull() {
        return $this->andNull(Ventas::CUOTA);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaNotNull() {
        return $this->andNotNull(Ventas::CUOTA);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaBetween($decimal, $from, $to) {
        return $this->andBetween(Ventas::CUOTA, $decimal, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaBeginsWith($decimal) {
        return $this->andBeginsWith(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaEndsWith($decimal) {
        return $this->andEndsWith(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andCuotaContains($decimal) {
        return $this->andContains(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuota($decimal) {
        return $this->or(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaNot($decimal) {
        return $this->orNot(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaLike($decimal) {
        return $this->orLike(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaNotLike($decimal) {
        return $this->orNotLike(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaGreater($decimal) {
        return $this->orGreater(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaGreaterEqual($decimal) {
        return $this->orGreaterEqual(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaLess($decimal) {
        return $this->orLess(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaLessEqual($decimal) {
        return $this->orLessEqual(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaNull() {
        return $this->orNull(Ventas::CUOTA);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaNotNull() {
        return $this->orNotNull(Ventas::CUOTA);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaBetween($decimal, $from, $to) {
        return $this->orBetween(Ventas::CUOTA, $decimal, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaBeginsWith($decimal) {
        return $this->orBeginsWith(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaEndsWith($decimal) {
        return $this->orEndsWith(Ventas::CUOTA, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orCuotaContains($decimal) {
        return $this->orContains(Ventas::CUOTA, $decimal);
    }


    /**
     * @return VentasQuery
     */
    function orderByCuotaAsc() {
        return $this->orderBy(Ventas::CUOTA, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByCuotaDesc() {
        return $this->orderBy(Ventas::CUOTA, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByCuota() {
        return $this->groupBy(Ventas::CUOTA);
    }

    /**
     * @return VentasQuery
     */
    function andResto($decimal) {
        return $this->addAnd(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoNot($decimal) {
        return $this->andNot(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoLike($decimal) {
        return $this->andLike(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoNotLike($decimal) {
        return $this->andNotLike(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoGreater($decimal) {
        return $this->andGreater(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoGreaterEqual($decimal) {
        return $this->andGreaterEqual(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoLess($decimal) {
        return $this->andLess(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoLessEqual($decimal) {
        return $this->andLessEqual(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoNull() {
        return $this->andNull(Ventas::RESTO);
    }

    /**
     * @return VentasQuery
     */
    function andRestoNotNull() {
        return $this->andNotNull(Ventas::RESTO);
    }

    /**
     * @return VentasQuery
     */
    function andRestoBetween($decimal, $from, $to) {
        return $this->andBetween(Ventas::RESTO, $decimal, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function andRestoBeginsWith($decimal) {
        return $this->andBeginsWith(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoEndsWith($decimal) {
        return $this->andEndsWith(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function andRestoContains($decimal) {
        return $this->andContains(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orResto($decimal) {
        return $this->or(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoNot($decimal) {
        return $this->orNot(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoLike($decimal) {
        return $this->orLike(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoNotLike($decimal) {
        return $this->orNotLike(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoGreater($decimal) {
        return $this->orGreater(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoGreaterEqual($decimal) {
        return $this->orGreaterEqual(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoLess($decimal) {
        return $this->orLess(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoLessEqual($decimal) {
        return $this->orLessEqual(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoNull() {
        return $this->orNull(Ventas::RESTO);
    }

    /**
     * @return VentasQuery
     */
    function orRestoNotNull() {
        return $this->orNotNull(Ventas::RESTO);
    }

    /**
     * @return VentasQuery
     */
    function orRestoBetween($decimal, $from, $to) {
        return $this->orBetween(Ventas::RESTO, $decimal, $from, $to);
    }

    /**
     * @return VentasQuery
     */
    function orRestoBeginsWith($decimal) {
        return $this->orBeginsWith(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoEndsWith($decimal) {
        return $this->orEndsWith(Ventas::RESTO, $decimal);
    }

    /**
     * @return VentasQuery
     */
    function orRestoContains($decimal) {
        return $this->orContains(Ventas::RESTO, $decimal);
    }


    /**
     * @return VentasQuery
     */
    function orderByRestoAsc() {
        return $this->orderBy(Ventas::RESTO, self::ASC);
    }

    /**
     * @return VentasQuery
     */
    function orderByRestoDesc() {
        return $this->orderBy(Ventas::RESTO, self::DESC);
    }

    /**
     * @return VentasQuery
     */
    function groupByResto() {
        return $this->groupBy(Ventas::RESTO);
    }


}