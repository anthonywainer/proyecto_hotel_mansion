<?php

use Dabl\Query\Query;

abstract class baseFormasPagosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = FormasPagos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return FormasPagosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new FormasPagosQuery($table_name, $alias);
	}

	/**
	 * @return FormasPagos[]
	 */
	function select() {
		return FormasPagos::doSelect($this);
	}

	/**
	 * @return FormasPagos
	 */
	function selectOne() {
		return FormasPagos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return FormasPagos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return FormasPagos::doCount($this);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && FormasPagos::isTemporalType($type)) {
			$value = FormasPagos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = FormasPagos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && FormasPagos::isTemporalType($type)) {
			$value = FormasPagos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = FormasPagos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andId($integer) {
		return $this->addAnd(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdNull() {
		return $this->andNull(FormasPagos::ID);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(FormasPagos::ID);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(FormasPagos::ID, $integer, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orId($integer) {
		return $this->or(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdNull() {
		return $this->orNull(FormasPagos::ID);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(FormasPagos::ID);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(FormasPagos::ID, $integer, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(FormasPagos::ID, $integer);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(FormasPagos::ID, $integer);
	}


	/**
	 * @return FormasPagosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(FormasPagos::ID, self::ASC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(FormasPagos::ID, self::DESC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function groupById() {
		return $this->groupBy(FormasPagos::ID);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(FormasPagos::DESCRIPCION);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(FormasPagos::DESCRIPCION);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(FormasPagos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(FormasPagos::DESCRIPCION);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(FormasPagos::DESCRIPCION);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(FormasPagos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(FormasPagos::DESCRIPCION, $varchar);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(FormasPagos::DESCRIPCION, $varchar);
	}


	/**
	 * @return FormasPagosQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(FormasPagos::DESCRIPCION, self::ASC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(FormasPagos::DESCRIPCION, self::DESC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(FormasPagos::DESCRIPCION);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(FormasPagos::CREATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(FormasPagos::CREATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(FormasPagos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(FormasPagos::CREATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(FormasPagos::CREATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(FormasPagos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(FormasPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(FormasPagos::CREATED_AT, $timestamp);
	}


	/**
	 * @return FormasPagosQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(FormasPagos::CREATED_AT, self::ASC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(FormasPagos::CREATED_AT, self::DESC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(FormasPagos::CREATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(FormasPagos::UPDATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(FormasPagos::UPDATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(FormasPagos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(FormasPagos::UPDATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(FormasPagos::UPDATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(FormasPagos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(FormasPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(FormasPagos::UPDATED_AT, $timestamp);
	}


	/**
	 * @return FormasPagosQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(FormasPagos::UPDATED_AT, self::ASC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(FormasPagos::UPDATED_AT, self::DESC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(FormasPagos::UPDATED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(FormasPagos::DELETED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(FormasPagos::DELETED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(FormasPagos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(FormasPagos::DELETED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(FormasPagos::DELETED_AT);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(FormasPagos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(FormasPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(FormasPagos::DELETED_AT, $timestamp);
	}


	/**
	 * @return FormasPagosQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(FormasPagos::DELETED_AT, self::ASC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(FormasPagos::DELETED_AT, self::DESC);
	}

	/**
	 * @return FormasPagosQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(FormasPagos::DELETED_AT);
	}


}