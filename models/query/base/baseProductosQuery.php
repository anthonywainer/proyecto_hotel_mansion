<?php

use Dabl\Query\Query;

abstract class baseProductosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Productos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ProductosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ProductosQuery($table_name, $alias);
	}

	/**
	 * @return Productos[]
	 */
	function select() {
		return Productos::doSelect($this);
	}

	/**
	 * @return Productos
	 */
	function selectOne() {
		return Productos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Productos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Productos::doCount($this);
	}

	/**
	 * @return ProductosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Productos::isTemporalType($type)) {
			$value = Productos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Productos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Productos::isTemporalType($type)) {
			$value = Productos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Productos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNull() {
		return $this->andNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Productos::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orId($integer) {
		return $this->or(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNull() {
		return $this->orNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Productos::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Productos::ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Productos::ID, $integer);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Productos::ID, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Productos::ID, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupById() {
		return $this->groupBy(Productos::ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNull() {
		return $this->andNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(Productos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombre($varchar) {
		return $this->or(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNull() {
		return $this->orNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(Productos::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(Productos::NOMBRE, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(Productos::NOMBRE, $varchar);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(Productos::NOMBRE, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(Productos::NOMBRE, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByNombre() {
		return $this->groupBy(Productos::NOMBRE);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Productos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Productos::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Productos::DESCRIPCION, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Productos::DESCRIPCION, $varchar);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Productos::DESCRIPCION, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Productos::DESCRIPCION, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Productos::DESCRIPCION);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecio($decimal) {
		return $this->addAnd(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNot($decimal) {
		return $this->andNot(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioLike($decimal) {
		return $this->andLike(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNotLike($decimal) {
		return $this->andNotLike(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioGreater($decimal) {
		return $this->andGreater(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioGreaterEqual($decimal) {
		return $this->andGreaterEqual(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioLess($decimal) {
		return $this->andLess(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioLessEqual($decimal) {
		return $this->andLessEqual(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNull() {
		return $this->andNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioBetween($decimal, $from, $to) {
		return $this->andBetween(Productos::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioBeginsWith($decimal) {
		return $this->andBeginsWith(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioEndsWith($decimal) {
		return $this->andEndsWith(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function andPrecioContains($decimal) {
		return $this->andContains(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecio($decimal) {
		return $this->or(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNot($decimal) {
		return $this->orNot(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioLike($decimal) {
		return $this->orLike(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNotLike($decimal) {
		return $this->orNotLike(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioGreater($decimal) {
		return $this->orGreater(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioGreaterEqual($decimal) {
		return $this->orGreaterEqual(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioLess($decimal) {
		return $this->orLess(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioLessEqual($decimal) {
		return $this->orLessEqual(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNull() {
		return $this->orNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioBetween($decimal, $from, $to) {
		return $this->orBetween(Productos::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioBeginsWith($decimal) {
		return $this->orBeginsWith(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioEndsWith($decimal) {
		return $this->orEndsWith(Productos::PRECIO, $decimal);
	}

	/**
	 * @return ProductosQuery
	 */
	function orPrecioContains($decimal) {
		return $this->orContains(Productos::PRECIO, $decimal);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(Productos::PRECIO, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(Productos::PRECIO, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(Productos::PRECIO);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagen($varchar) {
		return $this->addAnd(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenNot($varchar) {
		return $this->andNot(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenLike($varchar) {
		return $this->andLike(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenNotLike($varchar) {
		return $this->andNotLike(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenGreater($varchar) {
		return $this->andGreater(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenGreaterEqual($varchar) {
		return $this->andGreaterEqual(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenLess($varchar) {
		return $this->andLess(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenLessEqual($varchar) {
		return $this->andLessEqual(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenNull() {
		return $this->andNull(Productos::IMAGEN);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenNotNull() {
		return $this->andNotNull(Productos::IMAGEN);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenBetween($varchar, $from, $to) {
		return $this->andBetween(Productos::IMAGEN, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenBeginsWith($varchar) {
		return $this->andBeginsWith(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenEndsWith($varchar) {
		return $this->andEndsWith(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function andImagenContains($varchar) {
		return $this->andContains(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagen($varchar) {
		return $this->or(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenNot($varchar) {
		return $this->orNot(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenLike($varchar) {
		return $this->orLike(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenNotLike($varchar) {
		return $this->orNotLike(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenGreater($varchar) {
		return $this->orGreater(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenGreaterEqual($varchar) {
		return $this->orGreaterEqual(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenLess($varchar) {
		return $this->orLess(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenLessEqual($varchar) {
		return $this->orLessEqual(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenNull() {
		return $this->orNull(Productos::IMAGEN);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenNotNull() {
		return $this->orNotNull(Productos::IMAGEN);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenBetween($varchar, $from, $to) {
		return $this->orBetween(Productos::IMAGEN, $varchar, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenBeginsWith($varchar) {
		return $this->orBeginsWith(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenEndsWith($varchar) {
		return $this->orEndsWith(Productos::IMAGEN, $varchar);
	}

	/**
	 * @return ProductosQuery
	 */
	function orImagenContains($varchar) {
		return $this->orContains(Productos::IMAGEN, $varchar);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByImagenAsc() {
		return $this->orderBy(Productos::IMAGEN, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByImagenDesc() {
		return $this->orderBy(Productos::IMAGEN, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByImagen() {
		return $this->groupBy(Productos::IMAGEN);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStock($integer) {
		return $this->addAnd(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockNot($integer) {
		return $this->andNot(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockLike($integer) {
		return $this->andLike(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockNotLike($integer) {
		return $this->andNotLike(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockGreater($integer) {
		return $this->andGreater(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockGreaterEqual($integer) {
		return $this->andGreaterEqual(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockLess($integer) {
		return $this->andLess(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockLessEqual($integer) {
		return $this->andLessEqual(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockNull() {
		return $this->andNull(Productos::STOCK);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockNotNull() {
		return $this->andNotNull(Productos::STOCK);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockBetween($integer, $from, $to) {
		return $this->andBetween(Productos::STOCK, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockBeginsWith($integer) {
		return $this->andBeginsWith(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockEndsWith($integer) {
		return $this->andEndsWith(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andStockContains($integer) {
		return $this->andContains(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStock($integer) {
		return $this->or(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockNot($integer) {
		return $this->orNot(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockLike($integer) {
		return $this->orLike(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockNotLike($integer) {
		return $this->orNotLike(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockGreater($integer) {
		return $this->orGreater(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockGreaterEqual($integer) {
		return $this->orGreaterEqual(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockLess($integer) {
		return $this->orLess(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockLessEqual($integer) {
		return $this->orLessEqual(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockNull() {
		return $this->orNull(Productos::STOCK);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockNotNull() {
		return $this->orNotNull(Productos::STOCK);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockBetween($integer, $from, $to) {
		return $this->orBetween(Productos::STOCK, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockBeginsWith($integer) {
		return $this->orBeginsWith(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockEndsWith($integer) {
		return $this->orEndsWith(Productos::STOCK, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orStockContains($integer) {
		return $this->orContains(Productos::STOCK, $integer);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByStockAsc() {
		return $this->orderBy(Productos::STOCK, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByStockDesc() {
		return $this->orderBy(Productos::STOCK, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByStock() {
		return $this->groupBy(Productos::STOCK);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaId($integer) {
		return $this->addAnd(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdNot($integer) {
		return $this->andNot(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdLike($integer) {
		return $this->andLike(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdNotLike($integer) {
		return $this->andNotLike(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdGreater($integer) {
		return $this->andGreater(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdLess($integer) {
		return $this->andLess(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdLessEqual($integer) {
		return $this->andLessEqual(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdNull() {
		return $this->andNull(Productos::CATEGORIA_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdNotNull() {
		return $this->andNotNull(Productos::CATEGORIA_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdBetween($integer, $from, $to) {
		return $this->andBetween(Productos::CATEGORIA_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdBeginsWith($integer) {
		return $this->andBeginsWith(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdEndsWith($integer) {
		return $this->andEndsWith(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCategoriaIdContains($integer) {
		return $this->andContains(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaId($integer) {
		return $this->or(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdNot($integer) {
		return $this->orNot(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdLike($integer) {
		return $this->orLike(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdNotLike($integer) {
		return $this->orNotLike(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdGreater($integer) {
		return $this->orGreater(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdLess($integer) {
		return $this->orLess(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdLessEqual($integer) {
		return $this->orLessEqual(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdNull() {
		return $this->orNull(Productos::CATEGORIA_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdNotNull() {
		return $this->orNotNull(Productos::CATEGORIA_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdBetween($integer, $from, $to) {
		return $this->orBetween(Productos::CATEGORIA_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdBeginsWith($integer) {
		return $this->orBeginsWith(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdEndsWith($integer) {
		return $this->orEndsWith(Productos::CATEGORIA_ID, $integer);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCategoriaIdContains($integer) {
		return $this->orContains(Productos::CATEGORIA_ID, $integer);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByCategoriaIdAsc() {
		return $this->orderBy(Productos::CATEGORIA_ID, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByCategoriaIdDesc() {
		return $this->orderBy(Productos::CATEGORIA_ID, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByCategoriaId() {
		return $this->groupBy(Productos::CATEGORIA_ID);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Productos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Productos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Productos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Productos::CREATED_AT, $timestamp);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Productos::CREATED_AT, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Productos::CREATED_AT, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Productos::CREATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Productos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Productos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Productos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Productos::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Productos::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Productos::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Productos::UPDATED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Productos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Productos::DELETED_AT);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Productos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Productos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ProductosQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Productos::DELETED_AT, $timestamp);
	}


	/**
	 * @return ProductosQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Productos::DELETED_AT, self::ASC);
	}

	/**
	 * @return ProductosQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Productos::DELETED_AT, self::DESC);
	}

	/**
	 * @return ProductosQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Productos::DELETED_AT);
	}


}