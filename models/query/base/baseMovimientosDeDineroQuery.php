<?php

use Dabl\Query\Query;

abstract class baseMovimientosDeDineroQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = MovimientosDeDinero::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return MovimientosDeDineroQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new MovimientosDeDineroQuery($table_name, $alias);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	function select() {
		return MovimientosDeDinero::doSelect($this);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function selectOne() {
		return MovimientosDeDinero::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return MovimientosDeDinero::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return MovimientosDeDinero::doCount($this);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MovimientosDeDinero::isTemporalType($type)) {
			$value = MovimientosDeDinero::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MovimientosDeDinero::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MovimientosDeDinero::isTemporalType($type)) {
			$value = MovimientosDeDinero::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MovimientosDeDinero::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andId($integer) {
		return $this->addAnd(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNull() {
		return $this->andNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orId($integer) {
		return $this->or(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNull() {
		return $this->orNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(MovimientosDeDinero::ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(MovimientosDeDinero::ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupById() {
		return $this->groupBy(MovimientosDeDinero::ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoleta($varchar) {
		return $this->addAnd(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaNot($varchar) {
		return $this->andNot(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaLike($varchar) {
		return $this->andLike(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaNotLike($varchar) {
		return $this->andNotLike(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaGreater($varchar) {
		return $this->andGreater(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaGreaterEqual($varchar) {
		return $this->andGreaterEqual(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaLess($varchar) {
		return $this->andLess(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaLessEqual($varchar) {
		return $this->andLessEqual(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaNull() {
		return $this->andNull(MovimientosDeDinero::NUMERO_BOLETA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaNotNull() {
		return $this->andNotNull(MovimientosDeDinero::NUMERO_BOLETA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaBetween($varchar, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::NUMERO_BOLETA, $varchar, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaBeginsWith($varchar) {
		return $this->andBeginsWith(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaEndsWith($varchar) {
		return $this->andEndsWith(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andNumeroBoletaContains($varchar) {
		return $this->andContains(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoleta($varchar) {
		return $this->or(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaNot($varchar) {
		return $this->orNot(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaLike($varchar) {
		return $this->orLike(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaNotLike($varchar) {
		return $this->orNotLike(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaGreater($varchar) {
		return $this->orGreater(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaGreaterEqual($varchar) {
		return $this->orGreaterEqual(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaLess($varchar) {
		return $this->orLess(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaLessEqual($varchar) {
		return $this->orLessEqual(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaNull() {
		return $this->orNull(MovimientosDeDinero::NUMERO_BOLETA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaNotNull() {
		return $this->orNotNull(MovimientosDeDinero::NUMERO_BOLETA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaBetween($varchar, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::NUMERO_BOLETA, $varchar, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaBeginsWith($varchar) {
		return $this->orBeginsWith(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaEndsWith($varchar) {
		return $this->orEndsWith(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orNumeroBoletaContains($varchar) {
		return $this->orContains(MovimientosDeDinero::NUMERO_BOLETA, $varchar);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByNumeroBoletaAsc() {
		return $this->orderBy(MovimientosDeDinero::NUMERO_BOLETA, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByNumeroBoletaDesc() {
		return $this->orderBy(MovimientosDeDinero::NUMERO_BOLETA, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByNumeroBoleta() {
		return $this->groupBy(MovimientosDeDinero::NUMERO_BOLETA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMonto($decimal) {
		return $this->addAnd(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoNot($decimal) {
		return $this->andNot(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoLike($decimal) {
		return $this->andLike(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoNotLike($decimal) {
		return $this->andNotLike(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoGreater($decimal) {
		return $this->andGreater(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoGreaterEqual($decimal) {
		return $this->andGreaterEqual(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoLess($decimal) {
		return $this->andLess(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoLessEqual($decimal) {
		return $this->andLessEqual(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoNull() {
		return $this->andNull(MovimientosDeDinero::MONTO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoNotNull() {
		return $this->andNotNull(MovimientosDeDinero::MONTO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoBetween($decimal, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoBeginsWith($decimal) {
		return $this->andBeginsWith(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoEndsWith($decimal) {
		return $this->andEndsWith(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andMontoContains($decimal) {
		return $this->andContains(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMonto($decimal) {
		return $this->or(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoNot($decimal) {
		return $this->orNot(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoLike($decimal) {
		return $this->orLike(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoNotLike($decimal) {
		return $this->orNotLike(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoGreater($decimal) {
		return $this->orGreater(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoGreaterEqual($decimal) {
		return $this->orGreaterEqual(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoLess($decimal) {
		return $this->orLess(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoLessEqual($decimal) {
		return $this->orLessEqual(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoNull() {
		return $this->orNull(MovimientosDeDinero::MONTO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoNotNull() {
		return $this->orNotNull(MovimientosDeDinero::MONTO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoBetween($decimal, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoBeginsWith($decimal) {
		return $this->orBeginsWith(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoEndsWith($decimal) {
		return $this->orEndsWith(MovimientosDeDinero::MONTO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orMontoContains($decimal) {
		return $this->orContains(MovimientosDeDinero::MONTO, $decimal);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByMontoAsc() {
		return $this->orderBy(MovimientosDeDinero::MONTO, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByMontoDesc() {
		return $this->orderBy(MovimientosDeDinero::MONTO, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByMonto() {
		return $this->groupBy(MovimientosDeDinero::MONTO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFecha($timestamp) {
		return $this->addAnd(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaNull() {
		return $this->andNull(MovimientosDeDinero::FECHA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaNotNull() {
		return $this->andNotNull(MovimientosDeDinero::FECHA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFechaContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFecha($timestamp) {
		return $this->or(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaNull() {
		return $this->orNull(MovimientosDeDinero::FECHA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaNotNull() {
		return $this->orNotNull(MovimientosDeDinero::FECHA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::FECHA, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::FECHA, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFechaContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::FECHA, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByFechaAsc() {
		return $this->orderBy(MovimientosDeDinero::FECHA, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByFechaDesc() {
		return $this->orderBy(MovimientosDeDinero::FECHA, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByFecha() {
		return $this->groupBy(MovimientosDeDinero::FECHA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolso($decimal) {
		return $this->addAnd(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoNot($decimal) {
		return $this->andNot(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoLike($decimal) {
		return $this->andLike(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoNotLike($decimal) {
		return $this->andNotLike(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoGreater($decimal) {
		return $this->andGreater(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoGreaterEqual($decimal) {
		return $this->andGreaterEqual(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoLess($decimal) {
		return $this->andLess(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoLessEqual($decimal) {
		return $this->andLessEqual(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoNull() {
		return $this->andNull(MovimientosDeDinero::REEMBOLSO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoNotNull() {
		return $this->andNotNull(MovimientosDeDinero::REEMBOLSO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoBetween($decimal, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::REEMBOLSO, $decimal, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoBeginsWith($decimal) {
		return $this->andBeginsWith(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoEndsWith($decimal) {
		return $this->andEndsWith(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andReembolsoContains($decimal) {
		return $this->andContains(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolso($decimal) {
		return $this->or(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoNot($decimal) {
		return $this->orNot(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoLike($decimal) {
		return $this->orLike(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoNotLike($decimal) {
		return $this->orNotLike(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoGreater($decimal) {
		return $this->orGreater(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoGreaterEqual($decimal) {
		return $this->orGreaterEqual(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoLess($decimal) {
		return $this->orLess(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoLessEqual($decimal) {
		return $this->orLessEqual(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoNull() {
		return $this->orNull(MovimientosDeDinero::REEMBOLSO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoNotNull() {
		return $this->orNotNull(MovimientosDeDinero::REEMBOLSO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoBetween($decimal, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::REEMBOLSO, $decimal, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoBeginsWith($decimal) {
		return $this->orBeginsWith(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoEndsWith($decimal) {
		return $this->orEndsWith(MovimientosDeDinero::REEMBOLSO, $decimal);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orReembolsoContains($decimal) {
		return $this->orContains(MovimientosDeDinero::REEMBOLSO, $decimal);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByReembolsoAsc() {
		return $this->orderBy(MovimientosDeDinero::REEMBOLSO, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByReembolsoDesc() {
		return $this->orderBy(MovimientosDeDinero::REEMBOLSO, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByReembolso() {
		return $this->groupBy(MovimientosDeDinero::REEMBOLSO);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosa($varchar) {
		return $this->addAnd(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaNot($varchar) {
		return $this->andNot(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaLike($varchar) {
		return $this->andLike(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaNotLike($varchar) {
		return $this->andNotLike(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaGreater($varchar) {
		return $this->andGreater(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaGreaterEqual($varchar) {
		return $this->andGreaterEqual(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaLess($varchar) {
		return $this->andLess(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaLessEqual($varchar) {
		return $this->andLessEqual(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaNull() {
		return $this->andNull(MovimientosDeDinero::GLOSA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaNotNull() {
		return $this->andNotNull(MovimientosDeDinero::GLOSA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaBetween($varchar, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::GLOSA, $varchar, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaBeginsWith($varchar) {
		return $this->andBeginsWith(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaEndsWith($varchar) {
		return $this->andEndsWith(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andGlosaContains($varchar) {
		return $this->andContains(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosa($varchar) {
		return $this->or(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaNot($varchar) {
		return $this->orNot(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaLike($varchar) {
		return $this->orLike(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaNotLike($varchar) {
		return $this->orNotLike(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaGreater($varchar) {
		return $this->orGreater(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaGreaterEqual($varchar) {
		return $this->orGreaterEqual(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaLess($varchar) {
		return $this->orLess(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaLessEqual($varchar) {
		return $this->orLessEqual(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaNull() {
		return $this->orNull(MovimientosDeDinero::GLOSA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaNotNull() {
		return $this->orNotNull(MovimientosDeDinero::GLOSA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaBetween($varchar, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::GLOSA, $varchar, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaBeginsWith($varchar) {
		return $this->orBeginsWith(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaEndsWith($varchar) {
		return $this->orEndsWith(MovimientosDeDinero::GLOSA, $varchar);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orGlosaContains($varchar) {
		return $this->orContains(MovimientosDeDinero::GLOSA, $varchar);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByGlosaAsc() {
		return $this->orderBy(MovimientosDeDinero::GLOSA, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByGlosaDesc() {
		return $this->orderBy(MovimientosDeDinero::GLOSA, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByGlosa() {
		return $this->groupBy(MovimientosDeDinero::GLOSA);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoId($integer) {
		return $this->addAnd(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNull() {
		return $this->andNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::FORMA_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andFormaPagoIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoId($integer) {
		return $this->or(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNull() {
		return $this->orNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::FORMA_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orFormaPagoIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::FORMA_PAGO_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByFormaPagoIdAsc() {
		return $this->orderBy(MovimientosDeDinero::FORMA_PAGO_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByFormaPagoIdDesc() {
		return $this->orderBy(MovimientosDeDinero::FORMA_PAGO_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByFormaPagoId() {
		return $this->groupBy(MovimientosDeDinero::FORMA_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteId($integer) {
		return $this->addAnd(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdNull() {
		return $this->andNull(MovimientosDeDinero::TIPO_COMPROBANTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::TIPO_COMPROBANTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andTipoComprobanteIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteId($integer) {
		return $this->or(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdNull() {
		return $this->orNull(MovimientosDeDinero::TIPO_COMPROBANTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::TIPO_COMPROBANTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orTipoComprobanteIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::TIPO_COMPROBANTE_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByTipoComprobanteIdAsc() {
		return $this->orderBy(MovimientosDeDinero::TIPO_COMPROBANTE_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByTipoComprobanteIdDesc() {
		return $this->orderBy(MovimientosDeDinero::TIPO_COMPROBANTE_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByTipoComprobanteId() {
		return $this->groupBy(MovimientosDeDinero::TIPO_COMPROBANTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaId($integer) {
		return $this->addAnd(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNull() {
		return $this->andNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CAJA_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCajaIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaId($integer) {
		return $this->or(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNull() {
		return $this->orNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CAJA_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::CAJA_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCajaIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::CAJA_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCajaIdAsc() {
		return $this->orderBy(MovimientosDeDinero::CAJA_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCajaIdDesc() {
		return $this->orderBy(MovimientosDeDinero::CAJA_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByCajaId() {
		return $this->groupBy(MovimientosDeDinero::CAJA_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioId($integer) {
		return $this->addAnd(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNull() {
		return $this->andNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUsuarioIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioId($integer) {
		return $this->or(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNull() {
		return $this->orNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::USUARIO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUsuarioIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::USUARIO_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUsuarioIdAsc() {
		return $this->orderBy(MovimientosDeDinero::USUARIO_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUsuarioIdDesc() {
		return $this->orderBy(MovimientosDeDinero::USUARIO_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByUsuarioId() {
		return $this->groupBy(MovimientosDeDinero::USUARIO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoId($integer) {
		return $this->addAnd(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdNull() {
		return $this->andNull(MovimientosDeDinero::CONCEPTO_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CONCEPTO_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andConceptoPagoIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoId($integer) {
		return $this->or(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdNull() {
		return $this->orNull(MovimientosDeDinero::CONCEPTO_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CONCEPTO_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orConceptoPagoIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::CONCEPTO_PAGO_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByConceptoPagoIdAsc() {
		return $this->orderBy(MovimientosDeDinero::CONCEPTO_PAGO_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByConceptoPagoIdDesc() {
		return $this->orderBy(MovimientosDeDinero::CONCEPTO_PAGO_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByConceptoPagoId() {
		return $this->groupBy(MovimientosDeDinero::CONCEPTO_PAGO_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::CREATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::CREATED_AT, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(MovimientosDeDinero::CREATED_AT, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(MovimientosDeDinero::CREATED_AT, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(MovimientosDeDinero::CREATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::UPDATED_AT, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(MovimientosDeDinero::UPDATED_AT, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(MovimientosDeDinero::UPDATED_AT, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(MovimientosDeDinero::UPDATED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(MovimientosDeDinero::DELETED_AT, $timestamp);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(MovimientosDeDinero::DELETED_AT, $timestamp);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(MovimientosDeDinero::DELETED_AT, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(MovimientosDeDinero::DELETED_AT, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(MovimientosDeDinero::DELETED_AT);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(MovimientosDeDinero::CLIENTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(MovimientosDeDinero::CLIENTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(MovimientosDeDinero::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteId($integer) {
		return $this->or(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(MovimientosDeDinero::CLIENTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(MovimientosDeDinero::CLIENTE_ID);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(MovimientosDeDinero::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(MovimientosDeDinero::CLIENTE_ID, $integer);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(MovimientosDeDinero::CLIENTE_ID, $integer);
	}


	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(MovimientosDeDinero::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(MovimientosDeDinero::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return MovimientosDeDineroQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(MovimientosDeDinero::CLIENTE_ID);
	}


}