<?php

use Dabl\Query\Query;

abstract class baseConceptosPagosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ConceptosPagos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ConceptosPagosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ConceptosPagosQuery($table_name, $alias);
	}

	/**
	 * @return ConceptosPagos[]
	 */
	function select() {
		return ConceptosPagos::doSelect($this);
	}

	/**
	 * @return ConceptosPagos
	 */
	function selectOne() {
		return ConceptosPagos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ConceptosPagos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ConceptosPagos::doCount($this);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ConceptosPagos::isTemporalType($type)) {
			$value = ConceptosPagos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ConceptosPagos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ConceptosPagos::isTemporalType($type)) {
			$value = ConceptosPagos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ConceptosPagos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andId($integer) {
		return $this->addAnd(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdNull() {
		return $this->andNull(ConceptosPagos::ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ConceptosPagos::ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ConceptosPagos::ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orId($integer) {
		return $this->or(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdNull() {
		return $this->orNull(ConceptosPagos::ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ConceptosPagos::ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ConceptosPagos::ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ConceptosPagos::ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ConceptosPagos::ID, $integer);
	}


	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ConceptosPagos::ID, self::ASC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ConceptosPagos::ID, self::DESC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function groupById() {
		return $this->groupBy(ConceptosPagos::ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConcepto($varchar) {
		return $this->addAnd(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoNot($varchar) {
		return $this->andNot(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoLike($varchar) {
		return $this->andLike(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoNotLike($varchar) {
		return $this->andNotLike(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoGreater($varchar) {
		return $this->andGreater(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoGreaterEqual($varchar) {
		return $this->andGreaterEqual(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoLess($varchar) {
		return $this->andLess(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoLessEqual($varchar) {
		return $this->andLessEqual(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoNull() {
		return $this->andNull(ConceptosPagos::CONCEPTO);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoNotNull() {
		return $this->andNotNull(ConceptosPagos::CONCEPTO);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoBetween($varchar, $from, $to) {
		return $this->andBetween(ConceptosPagos::CONCEPTO, $varchar, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoBeginsWith($varchar) {
		return $this->andBeginsWith(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoEndsWith($varchar) {
		return $this->andEndsWith(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andConceptoContains($varchar) {
		return $this->andContains(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConcepto($varchar) {
		return $this->or(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoNot($varchar) {
		return $this->orNot(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoLike($varchar) {
		return $this->orLike(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoNotLike($varchar) {
		return $this->orNotLike(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoGreater($varchar) {
		return $this->orGreater(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoGreaterEqual($varchar) {
		return $this->orGreaterEqual(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoLess($varchar) {
		return $this->orLess(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoLessEqual($varchar) {
		return $this->orLessEqual(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoNull() {
		return $this->orNull(ConceptosPagos::CONCEPTO);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoNotNull() {
		return $this->orNotNull(ConceptosPagos::CONCEPTO);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoBetween($varchar, $from, $to) {
		return $this->orBetween(ConceptosPagos::CONCEPTO, $varchar, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoBeginsWith($varchar) {
		return $this->orBeginsWith(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoEndsWith($varchar) {
		return $this->orEndsWith(ConceptosPagos::CONCEPTO, $varchar);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orConceptoContains($varchar) {
		return $this->orContains(ConceptosPagos::CONCEPTO, $varchar);
	}


	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByConceptoAsc() {
		return $this->orderBy(ConceptosPagos::CONCEPTO, self::ASC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByConceptoDesc() {
		return $this->orderBy(ConceptosPagos::CONCEPTO, self::DESC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function groupByConcepto() {
		return $this->groupBy(ConceptosPagos::CONCEPTO);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoId($integer) {
		return $this->addAnd(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdNot($integer) {
		return $this->andNot(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdLike($integer) {
		return $this->andLike(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdNotLike($integer) {
		return $this->andNotLike(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdGreater($integer) {
		return $this->andGreater(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdLess($integer) {
		return $this->andLess(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdLessEqual($integer) {
		return $this->andLessEqual(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdNull() {
		return $this->andNull(ConceptosPagos::TIPO_ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdNotNull() {
		return $this->andNotNull(ConceptosPagos::TIPO_ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdBetween($integer, $from, $to) {
		return $this->andBetween(ConceptosPagos::TIPO_ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdBeginsWith($integer) {
		return $this->andBeginsWith(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdEndsWith($integer) {
		return $this->andEndsWith(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andTipoIdContains($integer) {
		return $this->andContains(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoId($integer) {
		return $this->or(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdNot($integer) {
		return $this->orNot(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdLike($integer) {
		return $this->orLike(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdNotLike($integer) {
		return $this->orNotLike(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdGreater($integer) {
		return $this->orGreater(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdLess($integer) {
		return $this->orLess(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdLessEqual($integer) {
		return $this->orLessEqual(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdNull() {
		return $this->orNull(ConceptosPagos::TIPO_ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdNotNull() {
		return $this->orNotNull(ConceptosPagos::TIPO_ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdBetween($integer, $from, $to) {
		return $this->orBetween(ConceptosPagos::TIPO_ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdBeginsWith($integer) {
		return $this->orBeginsWith(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdEndsWith($integer) {
		return $this->orEndsWith(ConceptosPagos::TIPO_ID, $integer);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orTipoIdContains($integer) {
		return $this->orContains(ConceptosPagos::TIPO_ID, $integer);
	}


	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByTipoIdAsc() {
		return $this->orderBy(ConceptosPagos::TIPO_ID, self::ASC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByTipoIdDesc() {
		return $this->orderBy(ConceptosPagos::TIPO_ID, self::DESC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function groupByTipoId() {
		return $this->groupBy(ConceptosPagos::TIPO_ID);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(ConceptosPagos::CREATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(ConceptosPagos::CREATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptosPagos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(ConceptosPagos::CREATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(ConceptosPagos::CREATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptosPagos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptosPagos::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(ConceptosPagos::CREATED_AT, $timestamp);
	}


	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(ConceptosPagos::CREATED_AT, self::ASC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(ConceptosPagos::CREATED_AT, self::DESC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(ConceptosPagos::CREATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(ConceptosPagos::UPDATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(ConceptosPagos::UPDATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptosPagos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(ConceptosPagos::UPDATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(ConceptosPagos::UPDATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptosPagos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptosPagos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(ConceptosPagos::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(ConceptosPagos::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(ConceptosPagos::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(ConceptosPagos::UPDATED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(ConceptosPagos::DELETED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(ConceptosPagos::DELETED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptosPagos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(ConceptosPagos::DELETED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(ConceptosPagos::DELETED_AT);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptosPagos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptosPagos::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(ConceptosPagos::DELETED_AT, $timestamp);
	}


	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(ConceptosPagos::DELETED_AT, self::ASC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(ConceptosPagos::DELETED_AT, self::DESC);
	}

	/**
	 * @return ConceptosPagosQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(ConceptosPagos::DELETED_AT);
	}


}