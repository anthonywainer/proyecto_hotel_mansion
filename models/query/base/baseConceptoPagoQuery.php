<?php

use Dabl\Query\Query;

abstract class baseConceptoPagoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ConceptoPago::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ConceptoPagoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ConceptoPagoQuery($table_name, $alias);
	}

	/**
	 * @return ConceptoPago[]
	 */
	function select() {
		return ConceptoPago::doSelect($this);
	}

	/**
	 * @return ConceptoPago
	 */
	function selectOne() {
		return ConceptoPago::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ConceptoPago::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ConceptoPago::doCount($this);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ConceptoPago::isTemporalType($type)) {
			$value = ConceptoPago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ConceptoPago::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ConceptoPago::isTemporalType($type)) {
			$value = ConceptoPago::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ConceptoPago::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andId($integer) {
		return $this->addAnd(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdNull() {
		return $this->andNull(ConceptoPago::ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ConceptoPago::ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ConceptoPago::ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orId($integer) {
		return $this->or(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdNull() {
		return $this->orNull(ConceptoPago::ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ConceptoPago::ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ConceptoPago::ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ConceptoPago::ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ConceptoPago::ID, $integer);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ConceptoPago::ID, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ConceptoPago::ID, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupById() {
		return $this->groupBy(ConceptoPago::ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(ConceptoPago::DESCRIPCION);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(ConceptoPago::DESCRIPCION);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(ConceptoPago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(ConceptoPago::DESCRIPCION);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(ConceptoPago::DESCRIPCION);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(ConceptoPago::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(ConceptoPago::DESCRIPCION, $varchar);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(ConceptoPago::DESCRIPCION, $varchar);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(ConceptoPago::DESCRIPCION, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(ConceptoPago::DESCRIPCION, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(ConceptoPago::DESCRIPCION);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoId($integer) {
		return $this->addAnd(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdNot($integer) {
		return $this->andNot(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdLike($integer) {
		return $this->andLike(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdNotLike($integer) {
		return $this->andNotLike(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdGreater($integer) {
		return $this->andGreater(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdLess($integer) {
		return $this->andLess(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdLessEqual($integer) {
		return $this->andLessEqual(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdNull() {
		return $this->andNull(ConceptoPago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdNotNull() {
		return $this->andNotNull(ConceptoPago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdBetween($integer, $from, $to) {
		return $this->andBetween(ConceptoPago::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdBeginsWith($integer) {
		return $this->andBeginsWith(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdEndsWith($integer) {
		return $this->andEndsWith(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMovimientoIdContains($integer) {
		return $this->andContains(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoId($integer) {
		return $this->or(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdNot($integer) {
		return $this->orNot(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdLike($integer) {
		return $this->orLike(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdNotLike($integer) {
		return $this->orNotLike(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdGreater($integer) {
		return $this->orGreater(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdLess($integer) {
		return $this->orLess(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdLessEqual($integer) {
		return $this->orLessEqual(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdNull() {
		return $this->orNull(ConceptoPago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdNotNull() {
		return $this->orNotNull(ConceptoPago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdBetween($integer, $from, $to) {
		return $this->orBetween(ConceptoPago::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdBeginsWith($integer) {
		return $this->orBeginsWith(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdEndsWith($integer) {
		return $this->orEndsWith(ConceptoPago::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMovimientoIdContains($integer) {
		return $this->orContains(ConceptoPago::MOVIMIENTO_ID, $integer);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByMovimientoIdAsc() {
		return $this->orderBy(ConceptoPago::MOVIMIENTO_ID, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByMovimientoIdDesc() {
		return $this->orderBy(ConceptoPago::MOVIMIENTO_ID, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupByMovimientoId() {
		return $this->groupBy(ConceptoPago::MOVIMIENTO_ID);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(ConceptoPago::CREATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(ConceptoPago::CREATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptoPago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(ConceptoPago::CREATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(ConceptoPago::CREATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptoPago::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptoPago::CREATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(ConceptoPago::CREATED_AT, $timestamp);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(ConceptoPago::CREATED_AT, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(ConceptoPago::CREATED_AT, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(ConceptoPago::CREATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(ConceptoPago::UPDATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(ConceptoPago::UPDATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptoPago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(ConceptoPago::UPDATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(ConceptoPago::UPDATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptoPago::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptoPago::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(ConceptoPago::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(ConceptoPago::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(ConceptoPago::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(ConceptoPago::UPDATED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(ConceptoPago::DELETED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(ConceptoPago::DELETED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ConceptoPago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(ConceptoPago::DELETED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(ConceptoPago::DELETED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ConceptoPago::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(ConceptoPago::DELETED_AT, $timestamp);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(ConceptoPago::DELETED_AT, $timestamp);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(ConceptoPago::DELETED_AT, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(ConceptoPago::DELETED_AT, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(ConceptoPago::DELETED_AT);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMonto($decimal) {
		return $this->addAnd(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoNot($decimal) {
		return $this->andNot(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoLike($decimal) {
		return $this->andLike(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoNotLike($decimal) {
		return $this->andNotLike(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoGreater($decimal) {
		return $this->andGreater(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoGreaterEqual($decimal) {
		return $this->andGreaterEqual(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoLess($decimal) {
		return $this->andLess(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoLessEqual($decimal) {
		return $this->andLessEqual(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoNull() {
		return $this->andNull(ConceptoPago::MONTO);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoNotNull() {
		return $this->andNotNull(ConceptoPago::MONTO);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoBetween($decimal, $from, $to) {
		return $this->andBetween(ConceptoPago::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoBeginsWith($decimal) {
		return $this->andBeginsWith(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoEndsWith($decimal) {
		return $this->andEndsWith(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function andMontoContains($decimal) {
		return $this->andContains(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMonto($decimal) {
		return $this->or(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoNot($decimal) {
		return $this->orNot(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoLike($decimal) {
		return $this->orLike(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoNotLike($decimal) {
		return $this->orNotLike(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoGreater($decimal) {
		return $this->orGreater(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoGreaterEqual($decimal) {
		return $this->orGreaterEqual(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoLess($decimal) {
		return $this->orLess(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoLessEqual($decimal) {
		return $this->orLessEqual(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoNull() {
		return $this->orNull(ConceptoPago::MONTO);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoNotNull() {
		return $this->orNotNull(ConceptoPago::MONTO);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoBetween($decimal, $from, $to) {
		return $this->orBetween(ConceptoPago::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoBeginsWith($decimal) {
		return $this->orBeginsWith(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoEndsWith($decimal) {
		return $this->orEndsWith(ConceptoPago::MONTO, $decimal);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orMontoContains($decimal) {
		return $this->orContains(ConceptoPago::MONTO, $decimal);
	}


	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByMontoAsc() {
		return $this->orderBy(ConceptoPago::MONTO, self::ASC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function orderByMontoDesc() {
		return $this->orderBy(ConceptoPago::MONTO, self::DESC);
	}

	/**
	 * @return ConceptoPagoQuery
	 */
	function groupByMonto() {
		return $this->groupBy(ConceptoPago::MONTO);
	}


}