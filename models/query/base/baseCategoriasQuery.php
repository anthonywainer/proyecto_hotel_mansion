<?php

use Dabl\Query\Query;

abstract class baseCategoriasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Categorias::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return CategoriasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new CategoriasQuery($table_name, $alias);
	}

	/**
	 * @return Categorias[]
	 */
	function select() {
		return Categorias::doSelect($this);
	}

	/**
	 * @return Categorias
	 */
	function selectOne() {
		return Categorias::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Categorias::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Categorias::doCount($this);
	}

	/**
	 * @return CategoriasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Categorias::isTemporalType($type)) {
			$value = Categorias::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Categorias::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return CategoriasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Categorias::isTemporalType($type)) {
			$value = Categorias::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Categorias::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andId($integer) {
		return $this->addAnd(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdNull() {
		return $this->andNull(Categorias::ID);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Categorias::ID);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Categorias::ID, $integer, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orId($integer) {
		return $this->or(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdNull() {
		return $this->orNull(Categorias::ID);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Categorias::ID);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Categorias::ID, $integer, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Categorias::ID, $integer);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Categorias::ID, $integer);
	}


	/**
	 * @return CategoriasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Categorias::ID, self::ASC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Categorias::ID, self::DESC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function groupById() {
		return $this->groupBy(Categorias::ID);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Categorias::DESCRIPCION);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Categorias::DESCRIPCION);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Categorias::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Categorias::DESCRIPCION);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Categorias::DESCRIPCION);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Categorias::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Categorias::DESCRIPCION, $varchar);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Categorias::DESCRIPCION, $varchar);
	}


	/**
	 * @return CategoriasQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Categorias::DESCRIPCION, self::ASC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Categorias::DESCRIPCION, self::DESC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Categorias::DESCRIPCION);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Categorias::CREATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Categorias::CREATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Categorias::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Categorias::CREATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Categorias::CREATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Categorias::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Categorias::CREATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Categorias::CREATED_AT, $timestamp);
	}


	/**
	 * @return CategoriasQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Categorias::CREATED_AT, self::ASC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Categorias::CREATED_AT, self::DESC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Categorias::CREATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Categorias::UPDATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Categorias::UPDATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Categorias::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Categorias::UPDATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Categorias::UPDATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Categorias::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Categorias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Categorias::UPDATED_AT, $timestamp);
	}


	/**
	 * @return CategoriasQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Categorias::UPDATED_AT, self::ASC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Categorias::UPDATED_AT, self::DESC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Categorias::UPDATED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Categorias::DELETED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Categorias::DELETED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Categorias::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Categorias::DELETED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Categorias::DELETED_AT);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Categorias::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Categorias::DELETED_AT, $timestamp);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Categorias::DELETED_AT, $timestamp);
	}


	/**
	 * @return CategoriasQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Categorias::DELETED_AT, self::ASC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Categorias::DELETED_AT, self::DESC);
	}

	/**
	 * @return CategoriasQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Categorias::DELETED_AT);
	}


}