<?php

use Dabl\Query\Query;

abstract class baseLicenciaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Licencia::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return LicenciaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new LicenciaQuery($table_name, $alias);
	}

	/**
	 * @return Licencia[]
	 */
	function select() {
		return Licencia::doSelect($this);
	}

	/**
	 * @return Licencia
	 */
	function selectOne() {
		return Licencia::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Licencia::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Licencia::doCount($this);
	}

	/**
	 * @return LicenciaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Licencia::isTemporalType($type)) {
			$value = Licencia::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Licencia::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return LicenciaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Licencia::isTemporalType($type)) {
			$value = Licencia::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Licencia::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaId($integer) {
		return $this->addAnd(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdNot($integer) {
		return $this->andNot(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdLike($integer) {
		return $this->andLike(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdNotLike($integer) {
		return $this->andNotLike(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdGreater($integer) {
		return $this->andGreater(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdLess($integer) {
		return $this->andLess(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdLessEqual($integer) {
		return $this->andLessEqual(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdNull() {
		return $this->andNull(Licencia::LICENCIA_ID);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdNotNull() {
		return $this->andNotNull(Licencia::LICENCIA_ID);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdBetween($integer, $from, $to) {
		return $this->andBetween(Licencia::LICENCIA_ID, $integer, $from, $to);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdBeginsWith($integer) {
		return $this->andBeginsWith(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdEndsWith($integer) {
		return $this->andEndsWith(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andLicenciaIdContains($integer) {
		return $this->andContains(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaId($integer) {
		return $this->or(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdNot($integer) {
		return $this->orNot(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdLike($integer) {
		return $this->orLike(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdNotLike($integer) {
		return $this->orNotLike(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdGreater($integer) {
		return $this->orGreater(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdLess($integer) {
		return $this->orLess(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdLessEqual($integer) {
		return $this->orLessEqual(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdNull() {
		return $this->orNull(Licencia::LICENCIA_ID);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdNotNull() {
		return $this->orNotNull(Licencia::LICENCIA_ID);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdBetween($integer, $from, $to) {
		return $this->orBetween(Licencia::LICENCIA_ID, $integer, $from, $to);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdBeginsWith($integer) {
		return $this->orBeginsWith(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdEndsWith($integer) {
		return $this->orEndsWith(Licencia::LICENCIA_ID, $integer);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orLicenciaIdContains($integer) {
		return $this->orContains(Licencia::LICENCIA_ID, $integer);
	}


	/**
	 * @return LicenciaQuery
	 */
	function orderByLicenciaIdAsc() {
		return $this->orderBy(Licencia::LICENCIA_ID, self::ASC);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orderByLicenciaIdDesc() {
		return $this->orderBy(Licencia::LICENCIA_ID, self::DESC);
	}

	/**
	 * @return LicenciaQuery
	 */
	function groupByLicenciaId() {
		return $this->groupBy(Licencia::LICENCIA_ID);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFin($date) {
		return $this->addAnd(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinNot($date) {
		return $this->andNot(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinLike($date) {
		return $this->andLike(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinNotLike($date) {
		return $this->andNotLike(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinGreater($date) {
		return $this->andGreater(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinGreaterEqual($date) {
		return $this->andGreaterEqual(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinLess($date) {
		return $this->andLess(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinLessEqual($date) {
		return $this->andLessEqual(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinNull() {
		return $this->andNull(Licencia::FECHA_FIN);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinNotNull() {
		return $this->andNotNull(Licencia::FECHA_FIN);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinBetween($date, $from, $to) {
		return $this->andBetween(Licencia::FECHA_FIN, $date, $from, $to);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinBeginsWith($date) {
		return $this->andBeginsWith(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinEndsWith($date) {
		return $this->andEndsWith(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function andFechaFinContains($date) {
		return $this->andContains(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFin($date) {
		return $this->or(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinNot($date) {
		return $this->orNot(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinLike($date) {
		return $this->orLike(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinNotLike($date) {
		return $this->orNotLike(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinGreater($date) {
		return $this->orGreater(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinGreaterEqual($date) {
		return $this->orGreaterEqual(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinLess($date) {
		return $this->orLess(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinLessEqual($date) {
		return $this->orLessEqual(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinNull() {
		return $this->orNull(Licencia::FECHA_FIN);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinNotNull() {
		return $this->orNotNull(Licencia::FECHA_FIN);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinBetween($date, $from, $to) {
		return $this->orBetween(Licencia::FECHA_FIN, $date, $from, $to);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinBeginsWith($date) {
		return $this->orBeginsWith(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinEndsWith($date) {
		return $this->orEndsWith(Licencia::FECHA_FIN, $date);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orFechaFinContains($date) {
		return $this->orContains(Licencia::FECHA_FIN, $date);
	}


	/**
	 * @return LicenciaQuery
	 */
	function orderByFechaFinAsc() {
		return $this->orderBy(Licencia::FECHA_FIN, self::ASC);
	}

	/**
	 * @return LicenciaQuery
	 */
	function orderByFechaFinDesc() {
		return $this->orderBy(Licencia::FECHA_FIN, self::DESC);
	}

	/**
	 * @return LicenciaQuery
	 */
	function groupByFechaFin() {
		return $this->groupBy(Licencia::FECHA_FIN);
	}


}