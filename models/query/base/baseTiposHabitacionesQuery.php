<?php

use Dabl\Query\Query;

abstract class baseTiposHabitacionesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = TiposHabitaciones::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TiposHabitacionesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TiposHabitacionesQuery($table_name, $alias);
	}

	/**
	 * @return TiposHabitaciones[]
	 */
	function select() {
		return TiposHabitaciones::doSelect($this);
	}

	/**
	 * @return TiposHabitaciones
	 */
	function selectOne() {
		return TiposHabitaciones::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return TiposHabitaciones::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return TiposHabitaciones::doCount($this);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposHabitaciones::isTemporalType($type)) {
			$value = TiposHabitaciones::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposHabitaciones::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposHabitaciones::isTemporalType($type)) {
			$value = TiposHabitaciones::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposHabitaciones::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andId($integer) {
		return $this->addAnd(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdNull() {
		return $this->andNull(TiposHabitaciones::ID);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(TiposHabitaciones::ID);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(TiposHabitaciones::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orId($integer) {
		return $this->or(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdNull() {
		return $this->orNull(TiposHabitaciones::ID);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(TiposHabitaciones::ID);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(TiposHabitaciones::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(TiposHabitaciones::ID, $integer);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(TiposHabitaciones::ID, $integer);
	}


	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(TiposHabitaciones::ID, self::ASC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(TiposHabitaciones::ID, self::DESC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function groupById() {
		return $this->groupBy(TiposHabitaciones::ID);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(TiposHabitaciones::DESCRIPCION);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(TiposHabitaciones::DESCRIPCION);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(TiposHabitaciones::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(TiposHabitaciones::DESCRIPCION);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(TiposHabitaciones::DESCRIPCION);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(TiposHabitaciones::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(TiposHabitaciones::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(TiposHabitaciones::DESCRIPCION, $varchar);
	}


	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(TiposHabitaciones::DESCRIPCION, self::ASC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(TiposHabitaciones::DESCRIPCION, self::DESC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(TiposHabitaciones::DESCRIPCION);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecio($decimal) {
		return $this->addAnd(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioNot($decimal) {
		return $this->andNot(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioLike($decimal) {
		return $this->andLike(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioNotLike($decimal) {
		return $this->andNotLike(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioGreater($decimal) {
		return $this->andGreater(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioGreaterEqual($decimal) {
		return $this->andGreaterEqual(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioLess($decimal) {
		return $this->andLess(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioLessEqual($decimal) {
		return $this->andLessEqual(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioNull() {
		return $this->andNull(TiposHabitaciones::PRECIO);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(TiposHabitaciones::PRECIO);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioBetween($decimal, $from, $to) {
		return $this->andBetween(TiposHabitaciones::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioBeginsWith($decimal) {
		return $this->andBeginsWith(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioEndsWith($decimal) {
		return $this->andEndsWith(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andPrecioContains($decimal) {
		return $this->andContains(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecio($decimal) {
		return $this->or(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioNot($decimal) {
		return $this->orNot(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioLike($decimal) {
		return $this->orLike(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioNotLike($decimal) {
		return $this->orNotLike(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioGreater($decimal) {
		return $this->orGreater(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioGreaterEqual($decimal) {
		return $this->orGreaterEqual(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioLess($decimal) {
		return $this->orLess(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioLessEqual($decimal) {
		return $this->orLessEqual(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioNull() {
		return $this->orNull(TiposHabitaciones::PRECIO);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(TiposHabitaciones::PRECIO);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioBetween($decimal, $from, $to) {
		return $this->orBetween(TiposHabitaciones::PRECIO, $decimal, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioBeginsWith($decimal) {
		return $this->orBeginsWith(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioEndsWith($decimal) {
		return $this->orEndsWith(TiposHabitaciones::PRECIO, $decimal);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orPrecioContains($decimal) {
		return $this->orContains(TiposHabitaciones::PRECIO, $decimal);
	}


	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(TiposHabitaciones::PRECIO, self::ASC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(TiposHabitaciones::PRECIO, self::DESC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(TiposHabitaciones::PRECIO);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(TiposHabitaciones::CREATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(TiposHabitaciones::CREATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposHabitaciones::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(TiposHabitaciones::CREATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(TiposHabitaciones::CREATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposHabitaciones::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposHabitaciones::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(TiposHabitaciones::CREATED_AT, $timestamp);
	}


	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(TiposHabitaciones::CREATED_AT, self::ASC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(TiposHabitaciones::CREATED_AT, self::DESC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(TiposHabitaciones::CREATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(TiposHabitaciones::UPDATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(TiposHabitaciones::UPDATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposHabitaciones::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(TiposHabitaciones::UPDATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(TiposHabitaciones::UPDATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposHabitaciones::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposHabitaciones::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(TiposHabitaciones::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(TiposHabitaciones::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(TiposHabitaciones::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(TiposHabitaciones::UPDATED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(TiposHabitaciones::DELETED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(TiposHabitaciones::DELETED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposHabitaciones::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(TiposHabitaciones::DELETED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(TiposHabitaciones::DELETED_AT);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposHabitaciones::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposHabitaciones::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(TiposHabitaciones::DELETED_AT, $timestamp);
	}


	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(TiposHabitaciones::DELETED_AT, self::ASC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(TiposHabitaciones::DELETED_AT, self::DESC);
	}

	/**
	 * @return TiposHabitacionesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(TiposHabitaciones::DELETED_AT);
	}


}