<?php

use Dabl\Query\Query;

abstract class baseProductoConsumidoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ProductoConsumido::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ProductoConsumidoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ProductoConsumidoQuery($table_name, $alias);
	}

	/**
	 * @return ProductoConsumido[]
	 */
	function select() {
		return ProductoConsumido::doSelect($this);
	}

	/**
	 * @return ProductoConsumido
	 */
	function selectOne() {
		return ProductoConsumido::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ProductoConsumido::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ProductoConsumido::doCount($this);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductoConsumido::isTemporalType($type)) {
			$value = ProductoConsumido::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductoConsumido::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductoConsumido::isTemporalType($type)) {
			$value = ProductoConsumido::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductoConsumido::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andId($integer) {
		return $this->addAnd(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdNull() {
		return $this->andNull(ProductoConsumido::ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ProductoConsumido::ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumido::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orId($integer) {
		return $this->or(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdNull() {
		return $this->orNull(ProductoConsumido::ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ProductoConsumido::ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumido::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumido::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ProductoConsumido::ID, $integer);
	}


	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ProductoConsumido::ID, self::ASC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ProductoConsumido::ID, self::DESC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function groupById() {
		return $this->groupBy(ProductoConsumido::ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoId($integer) {
		return $this->addAnd(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdNot($integer) {
		return $this->andNot(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdLike($integer) {
		return $this->andLike(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdNotLike($integer) {
		return $this->andNotLike(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdGreater($integer) {
		return $this->andGreater(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdLess($integer) {
		return $this->andLess(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdNull() {
		return $this->andNull(ProductoConsumido::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdNotNull() {
		return $this->andNotNull(ProductoConsumido::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumido::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andProductoIdContains($integer) {
		return $this->andContains(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoId($integer) {
		return $this->or(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdNot($integer) {
		return $this->orNot(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdLike($integer) {
		return $this->orLike(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdNotLike($integer) {
		return $this->orNotLike(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdGreater($integer) {
		return $this->orGreater(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdLess($integer) {
		return $this->orLess(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdNull() {
		return $this->orNull(ProductoConsumido::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdNotNull() {
		return $this->orNotNull(ProductoConsumido::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumido::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumido::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orProductoIdContains($integer) {
		return $this->orContains(ProductoConsumido::PRODUCTO_ID, $integer);
	}


	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByProductoIdAsc() {
		return $this->orderBy(ProductoConsumido::PRODUCTO_ID, self::ASC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByProductoIdDesc() {
		return $this->orderBy(ProductoConsumido::PRODUCTO_ID, self::DESC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function groupByProductoId() {
		return $this->groupBy(ProductoConsumido::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaId($integer) {
		return $this->addAnd(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdNot($integer) {
		return $this->andNot(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdLike($integer) {
		return $this->andLike(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdNotLike($integer) {
		return $this->andNotLike(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdGreater($integer) {
		return $this->andGreater(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdLess($integer) {
		return $this->andLess(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdNull() {
		return $this->andNull(ProductoConsumido::HABITA_ESTADIA_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdNotNull() {
		return $this->andNotNull(ProductoConsumido::HABITA_ESTADIA_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumido::HABITA_ESTADIA_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andHabitaEstadiaIdContains($integer) {
		return $this->andContains(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaId($integer) {
		return $this->or(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdNot($integer) {
		return $this->orNot(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdLike($integer) {
		return $this->orLike(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdNotLike($integer) {
		return $this->orNotLike(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdGreater($integer) {
		return $this->orGreater(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdLess($integer) {
		return $this->orLess(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdNull() {
		return $this->orNull(ProductoConsumido::HABITA_ESTADIA_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdNotNull() {
		return $this->orNotNull(ProductoConsumido::HABITA_ESTADIA_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumido::HABITA_ESTADIA_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orHabitaEstadiaIdContains($integer) {
		return $this->orContains(ProductoConsumido::HABITA_ESTADIA_ID, $integer);
	}


	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByHabitaEstadiaIdAsc() {
		return $this->orderBy(ProductoConsumido::HABITA_ESTADIA_ID, self::ASC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByHabitaEstadiaIdDesc() {
		return $this->orderBy(ProductoConsumido::HABITA_ESTADIA_ID, self::DESC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function groupByHabitaEstadiaId() {
		return $this->groupBy(ProductoConsumido::HABITA_ESTADIA_ID);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidad($integer) {
		return $this->addAnd(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadNot($integer) {
		return $this->andNot(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadLike($integer) {
		return $this->andLike(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadNotLike($integer) {
		return $this->andNotLike(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadGreater($integer) {
		return $this->andGreater(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadLess($integer) {
		return $this->andLess(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadNull() {
		return $this->andNull(ProductoConsumido::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadNotNull() {
		return $this->andNotNull(ProductoConsumido::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumido::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andCantidadContains($integer) {
		return $this->andContains(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidad($integer) {
		return $this->or(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadNot($integer) {
		return $this->orNot(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadLike($integer) {
		return $this->orLike(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadNotLike($integer) {
		return $this->orNotLike(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadGreater($integer) {
		return $this->orGreater(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadLess($integer) {
		return $this->orLess(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadNull() {
		return $this->orNull(ProductoConsumido::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadNotNull() {
		return $this->orNotNull(ProductoConsumido::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumido::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumido::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orCantidadContains($integer) {
		return $this->orContains(ProductoConsumido::CANTIDAD, $integer);
	}


	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByCantidadAsc() {
		return $this->orderBy(ProductoConsumido::CANTIDAD, self::ASC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByCantidadDesc() {
		return $this->orderBy(ProductoConsumido::CANTIDAD, self::DESC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function groupByCantidad() {
		return $this->groupBy(ProductoConsumido::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMonto($float) {
		return $this->addAnd(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoNot($float) {
		return $this->andNot(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoLike($float) {
		return $this->andLike(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoNotLike($float) {
		return $this->andNotLike(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoGreater($float) {
		return $this->andGreater(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoGreaterEqual($float) {
		return $this->andGreaterEqual(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoLess($float) {
		return $this->andLess(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoLessEqual($float) {
		return $this->andLessEqual(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoNull() {
		return $this->andNull(ProductoConsumido::MONTO);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoNotNull() {
		return $this->andNotNull(ProductoConsumido::MONTO);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoBetween($float, $from, $to) {
		return $this->andBetween(ProductoConsumido::MONTO, $float, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoBeginsWith($float) {
		return $this->andBeginsWith(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoEndsWith($float) {
		return $this->andEndsWith(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function andMontoContains($float) {
		return $this->andContains(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMonto($float) {
		return $this->or(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoNot($float) {
		return $this->orNot(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoLike($float) {
		return $this->orLike(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoNotLike($float) {
		return $this->orNotLike(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoGreater($float) {
		return $this->orGreater(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoGreaterEqual($float) {
		return $this->orGreaterEqual(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoLess($float) {
		return $this->orLess(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoLessEqual($float) {
		return $this->orLessEqual(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoNull() {
		return $this->orNull(ProductoConsumido::MONTO);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoNotNull() {
		return $this->orNotNull(ProductoConsumido::MONTO);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoBetween($float, $from, $to) {
		return $this->orBetween(ProductoConsumido::MONTO, $float, $from, $to);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoBeginsWith($float) {
		return $this->orBeginsWith(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoEndsWith($float) {
		return $this->orEndsWith(ProductoConsumido::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orMontoContains($float) {
		return $this->orContains(ProductoConsumido::MONTO, $float);
	}


	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByMontoAsc() {
		return $this->orderBy(ProductoConsumido::MONTO, self::ASC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function orderByMontoDesc() {
		return $this->orderBy(ProductoConsumido::MONTO, self::DESC);
	}

	/**
	 * @return ProductoConsumidoQuery
	 */
	function groupByMonto() {
		return $this->groupBy(ProductoConsumido::MONTO);
	}


}