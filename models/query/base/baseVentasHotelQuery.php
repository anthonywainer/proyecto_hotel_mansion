<?php

use Dabl\Query\Query;

abstract class baseVentasHotelQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = VentasHotel::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return VentasHotelQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new VentasHotelQuery($table_name, $alias);
	}

	/**
	 * @return VentasHotel[]
	 */
	function select() {
		return VentasHotel::doSelect($this);
	}

	/**
	 * @return VentasHotel
	 */
	function selectOne() {
		return VentasHotel::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return VentasHotel::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return VentasHotel::doCount($this);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasHotel::isTemporalType($type)) {
			$value = VentasHotel::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasHotel::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && VentasHotel::isTemporalType($type)) {
			$value = VentasHotel::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = VentasHotel::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andId($integer) {
		return $this->addAnd(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdNull() {
		return $this->andNull(VentasHotel::ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(VentasHotel::ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasHotel::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orId($integer) {
		return $this->or(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdNull() {
		return $this->orNull(VentasHotel::ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(VentasHotel::ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasHotel::ID, $integer, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(VentasHotel::ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(VentasHotel::ID, $integer);
	}


	/**
	 * @return VentasHotelQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(VentasHotel::ID, self::ASC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(VentasHotel::ID, self::DESC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function groupById() {
		return $this->groupBy(VentasHotel::ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoId($integer) {
		return $this->addAnd(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdNot($integer) {
		return $this->andNot(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdLike($integer) {
		return $this->andLike(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdNotLike($integer) {
		return $this->andNotLike(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdGreater($integer) {
		return $this->andGreater(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdLess($integer) {
		return $this->andLess(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdLessEqual($integer) {
		return $this->andLessEqual(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdNull() {
		return $this->andNull(VentasHotel::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdNotNull() {
		return $this->andNotNull(VentasHotel::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdBetween($integer, $from, $to) {
		return $this->andBetween(VentasHotel::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdBeginsWith($integer) {
		return $this->andBeginsWith(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdEndsWith($integer) {
		return $this->andEndsWith(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMovimientoIdContains($integer) {
		return $this->andContains(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoId($integer) {
		return $this->or(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdNot($integer) {
		return $this->orNot(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdLike($integer) {
		return $this->orLike(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdNotLike($integer) {
		return $this->orNotLike(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdGreater($integer) {
		return $this->orGreater(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdLess($integer) {
		return $this->orLess(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdLessEqual($integer) {
		return $this->orLessEqual(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdNull() {
		return $this->orNull(VentasHotel::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdNotNull() {
		return $this->orNotNull(VentasHotel::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdBetween($integer, $from, $to) {
		return $this->orBetween(VentasHotel::MOVIMIENTO_ID, $integer, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdBeginsWith($integer) {
		return $this->orBeginsWith(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdEndsWith($integer) {
		return $this->orEndsWith(VentasHotel::MOVIMIENTO_ID, $integer);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMovimientoIdContains($integer) {
		return $this->orContains(VentasHotel::MOVIMIENTO_ID, $integer);
	}


	/**
	 * @return VentasHotelQuery
	 */
	function orderByMovimientoIdAsc() {
		return $this->orderBy(VentasHotel::MOVIMIENTO_ID, self::ASC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orderByMovimientoIdDesc() {
		return $this->orderBy(VentasHotel::MOVIMIENTO_ID, self::DESC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function groupByMovimientoId() {
		return $this->groupBy(VentasHotel::MOVIMIENTO_ID);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMonto($decimal) {
		return $this->addAnd(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoNot($decimal) {
		return $this->andNot(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoLike($decimal) {
		return $this->andLike(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoNotLike($decimal) {
		return $this->andNotLike(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoGreater($decimal) {
		return $this->andGreater(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoGreaterEqual($decimal) {
		return $this->andGreaterEqual(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoLess($decimal) {
		return $this->andLess(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoLessEqual($decimal) {
		return $this->andLessEqual(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoNull() {
		return $this->andNull(VentasHotel::MONTO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoNotNull() {
		return $this->andNotNull(VentasHotel::MONTO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoBetween($decimal, $from, $to) {
		return $this->andBetween(VentasHotel::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoBeginsWith($decimal) {
		return $this->andBeginsWith(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoEndsWith($decimal) {
		return $this->andEndsWith(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andMontoContains($decimal) {
		return $this->andContains(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMonto($decimal) {
		return $this->or(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoNot($decimal) {
		return $this->orNot(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoLike($decimal) {
		return $this->orLike(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoNotLike($decimal) {
		return $this->orNotLike(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoGreater($decimal) {
		return $this->orGreater(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoGreaterEqual($decimal) {
		return $this->orGreaterEqual(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoLess($decimal) {
		return $this->orLess(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoLessEqual($decimal) {
		return $this->orLessEqual(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoNull() {
		return $this->orNull(VentasHotel::MONTO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoNotNull() {
		return $this->orNotNull(VentasHotel::MONTO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoBetween($decimal, $from, $to) {
		return $this->orBetween(VentasHotel::MONTO, $decimal, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoBeginsWith($decimal) {
		return $this->orBeginsWith(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoEndsWith($decimal) {
		return $this->orEndsWith(VentasHotel::MONTO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orMontoContains($decimal) {
		return $this->orContains(VentasHotel::MONTO, $decimal);
	}


	/**
	 * @return VentasHotelQuery
	 */
	function orderByMontoAsc() {
		return $this->orderBy(VentasHotel::MONTO, self::ASC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orderByMontoDesc() {
		return $this->orderBy(VentasHotel::MONTO, self::DESC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function groupByMonto() {
		return $this->groupBy(VentasHotel::MONTO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivo($decimal) {
		return $this->addAnd(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoNot($decimal) {
		return $this->andNot(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoLike($decimal) {
		return $this->andLike(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoNotLike($decimal) {
		return $this->andNotLike(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoGreater($decimal) {
		return $this->andGreater(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoGreaterEqual($decimal) {
		return $this->andGreaterEqual(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoLess($decimal) {
		return $this->andLess(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoLessEqual($decimal) {
		return $this->andLessEqual(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoNull() {
		return $this->andNull(VentasHotel::EFECTIVO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoNotNull() {
		return $this->andNotNull(VentasHotel::EFECTIVO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoBetween($decimal, $from, $to) {
		return $this->andBetween(VentasHotel::EFECTIVO, $decimal, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoBeginsWith($decimal) {
		return $this->andBeginsWith(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoEndsWith($decimal) {
		return $this->andEndsWith(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function andEfectivoContains($decimal) {
		return $this->andContains(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivo($decimal) {
		return $this->or(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoNot($decimal) {
		return $this->orNot(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoLike($decimal) {
		return $this->orLike(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoNotLike($decimal) {
		return $this->orNotLike(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoGreater($decimal) {
		return $this->orGreater(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoGreaterEqual($decimal) {
		return $this->orGreaterEqual(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoLess($decimal) {
		return $this->orLess(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoLessEqual($decimal) {
		return $this->orLessEqual(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoNull() {
		return $this->orNull(VentasHotel::EFECTIVO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoNotNull() {
		return $this->orNotNull(VentasHotel::EFECTIVO);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoBetween($decimal, $from, $to) {
		return $this->orBetween(VentasHotel::EFECTIVO, $decimal, $from, $to);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoBeginsWith($decimal) {
		return $this->orBeginsWith(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoEndsWith($decimal) {
		return $this->orEndsWith(VentasHotel::EFECTIVO, $decimal);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orEfectivoContains($decimal) {
		return $this->orContains(VentasHotel::EFECTIVO, $decimal);
	}


	/**
	 * @return VentasHotelQuery
	 */
	function orderByEfectivoAsc() {
		return $this->orderBy(VentasHotel::EFECTIVO, self::ASC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function orderByEfectivoDesc() {
		return $this->orderBy(VentasHotel::EFECTIVO, self::DESC);
	}

	/**
	 * @return VentasHotelQuery
	 */
	function groupByEfectivo() {
		return $this->groupBy(VentasHotel::EFECTIVO);
	}


}