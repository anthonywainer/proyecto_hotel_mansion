<?php

use Dabl\Query\Query;

abstract class baseTelevisoresQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Televisores::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TelevisoresQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TelevisoresQuery($table_name, $alias);
	}

	/**
	 * @return Televisores[]
	 */
	function select() {
		return Televisores::doSelect($this);
	}

	/**
	 * @return Televisores
	 */
	function selectOne() {
		return Televisores::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Televisores::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Televisores::doCount($this);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Televisores::isTemporalType($type)) {
			$value = Televisores::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Televisores::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Televisores::isTemporalType($type)) {
			$value = Televisores::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Televisores::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andId($integer) {
		return $this->addAnd(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdNull() {
		return $this->andNull(Televisores::ID);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Televisores::ID);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Televisores::ID, $integer, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orId($integer) {
		return $this->or(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdNull() {
		return $this->orNull(Televisores::ID);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Televisores::ID);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Televisores::ID, $integer, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Televisores::ID, $integer);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Televisores::ID, $integer);
	}


	/**
	 * @return TelevisoresQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Televisores::ID, self::ASC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Televisores::ID, self::DESC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function groupById() {
		return $this->groupBy(Televisores::ID);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Televisores::DESCRIPCION);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Televisores::DESCRIPCION);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Televisores::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Televisores::DESCRIPCION);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Televisores::DESCRIPCION);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Televisores::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Televisores::DESCRIPCION, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Televisores::DESCRIPCION, $varchar);
	}


	/**
	 * @return TelevisoresQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Televisores::DESCRIPCION, self::ASC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Televisores::DESCRIPCION, self::DESC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Televisores::DESCRIPCION);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstado($varchar) {
		return $this->addAnd(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoNot($varchar) {
		return $this->andNot(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoLike($varchar) {
		return $this->andLike(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoNotLike($varchar) {
		return $this->andNotLike(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoGreater($varchar) {
		return $this->andGreater(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoLess($varchar) {
		return $this->andLess(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoLessEqual($varchar) {
		return $this->andLessEqual(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoNull() {
		return $this->andNull(Televisores::ESTADO);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoNotNull() {
		return $this->andNotNull(Televisores::ESTADO);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoBetween($varchar, $from, $to) {
		return $this->andBetween(Televisores::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoBeginsWith($varchar) {
		return $this->andBeginsWith(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoEndsWith($varchar) {
		return $this->andEndsWith(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andEstadoContains($varchar) {
		return $this->andContains(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstado($varchar) {
		return $this->or(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoNot($varchar) {
		return $this->orNot(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoLike($varchar) {
		return $this->orLike(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoNotLike($varchar) {
		return $this->orNotLike(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoGreater($varchar) {
		return $this->orGreater(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoLess($varchar) {
		return $this->orLess(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoLessEqual($varchar) {
		return $this->orLessEqual(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoNull() {
		return $this->orNull(Televisores::ESTADO);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoNotNull() {
		return $this->orNotNull(Televisores::ESTADO);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoBetween($varchar, $from, $to) {
		return $this->orBetween(Televisores::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoBeginsWith($varchar) {
		return $this->orBeginsWith(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoEndsWith($varchar) {
		return $this->orEndsWith(Televisores::ESTADO, $varchar);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orEstadoContains($varchar) {
		return $this->orContains(Televisores::ESTADO, $varchar);
	}


	/**
	 * @return TelevisoresQuery
	 */
	function orderByEstadoAsc() {
		return $this->orderBy(Televisores::ESTADO, self::ASC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orderByEstadoDesc() {
		return $this->orderBy(Televisores::ESTADO, self::DESC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function groupByEstado() {
		return $this->groupBy(Televisores::ESTADO);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Televisores::CREATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Televisores::CREATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Televisores::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Televisores::CREATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Televisores::CREATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Televisores::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Televisores::CREATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Televisores::CREATED_AT, $timestamp);
	}


	/**
	 * @return TelevisoresQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Televisores::CREATED_AT, self::ASC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Televisores::CREATED_AT, self::DESC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Televisores::CREATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Televisores::UPDATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Televisores::UPDATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Televisores::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Televisores::UPDATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Televisores::UPDATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Televisores::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Televisores::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Televisores::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TelevisoresQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Televisores::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Televisores::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Televisores::UPDATED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Televisores::DELETED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Televisores::DELETED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Televisores::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Televisores::DELETED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Televisores::DELETED_AT);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Televisores::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Televisores::DELETED_AT, $timestamp);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Televisores::DELETED_AT, $timestamp);
	}


	/**
	 * @return TelevisoresQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Televisores::DELETED_AT, self::ASC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Televisores::DELETED_AT, self::DESC);
	}

	/**
	 * @return TelevisoresQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Televisores::DELETED_AT);
	}


}