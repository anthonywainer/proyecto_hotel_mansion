<?php

use Dabl\Query\Query;

abstract class baseClientesEstadiasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ClientesEstadias::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ClientesEstadiasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ClientesEstadiasQuery($table_name, $alias);
	}

	/**
	 * @return ClientesEstadias[]
	 */
	function select() {
		return ClientesEstadias::doSelect($this);
	}

	/**
	 * @return ClientesEstadias
	 */
	function selectOne() {
		return ClientesEstadias::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ClientesEstadias::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ClientesEstadias::doCount($this);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ClientesEstadias::isTemporalType($type)) {
			$value = ClientesEstadias::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ClientesEstadias::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ClientesEstadias::isTemporalType($type)) {
			$value = ClientesEstadias::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ClientesEstadias::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andId($integer) {
		return $this->addAnd(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdNull() {
		return $this->andNull(ClientesEstadias::ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ClientesEstadias::ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ClientesEstadias::ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orId($integer) {
		return $this->or(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdNull() {
		return $this->orNull(ClientesEstadias::ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ClientesEstadias::ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ClientesEstadias::ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ClientesEstadias::ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ClientesEstadias::ID, $integer);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ClientesEstadias::ID, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ClientesEstadias::ID, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupById() {
		return $this->groupBy(ClientesEstadias::ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteId($integer) {
		return $this->addAnd(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdNot($integer) {
		return $this->andNot(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdLike($integer) {
		return $this->andLike(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdNotLike($integer) {
		return $this->andNotLike(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdGreater($integer) {
		return $this->andGreater(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdLess($integer) {
		return $this->andLess(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdLessEqual($integer) {
		return $this->andLessEqual(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdNull() {
		return $this->andNull(ClientesEstadias::CLIENTE_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdNotNull() {
		return $this->andNotNull(ClientesEstadias::CLIENTE_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdBetween($integer, $from, $to) {
		return $this->andBetween(ClientesEstadias::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdBeginsWith($integer) {
		return $this->andBeginsWith(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdEndsWith($integer) {
		return $this->andEndsWith(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andClienteIdContains($integer) {
		return $this->andContains(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteId($integer) {
		return $this->or(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdNot($integer) {
		return $this->orNot(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdLike($integer) {
		return $this->orLike(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdNotLike($integer) {
		return $this->orNotLike(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdGreater($integer) {
		return $this->orGreater(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdLess($integer) {
		return $this->orLess(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdLessEqual($integer) {
		return $this->orLessEqual(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdNull() {
		return $this->orNull(ClientesEstadias::CLIENTE_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdNotNull() {
		return $this->orNotNull(ClientesEstadias::CLIENTE_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdBetween($integer, $from, $to) {
		return $this->orBetween(ClientesEstadias::CLIENTE_ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdBeginsWith($integer) {
		return $this->orBeginsWith(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdEndsWith($integer) {
		return $this->orEndsWith(ClientesEstadias::CLIENTE_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orClienteIdContains($integer) {
		return $this->orContains(ClientesEstadias::CLIENTE_ID, $integer);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByClienteIdAsc() {
		return $this->orderBy(ClientesEstadias::CLIENTE_ID, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByClienteIdDesc() {
		return $this->orderBy(ClientesEstadias::CLIENTE_ID, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupByClienteId() {
		return $this->groupBy(ClientesEstadias::CLIENTE_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaId($integer) {
		return $this->addAnd(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdNot($integer) {
		return $this->andNot(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdLike($integer) {
		return $this->andLike(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdNotLike($integer) {
		return $this->andNotLike(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdGreater($integer) {
		return $this->andGreater(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdLess($integer) {
		return $this->andLess(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdLessEqual($integer) {
		return $this->andLessEqual(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdNull() {
		return $this->andNull(ClientesEstadias::HABITACION_ESTADIA_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdNotNull() {
		return $this->andNotNull(ClientesEstadias::HABITACION_ESTADIA_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdBetween($integer, $from, $to) {
		return $this->andBetween(ClientesEstadias::HABITACION_ESTADIA_ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdBeginsWith($integer) {
		return $this->andBeginsWith(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdEndsWith($integer) {
		return $this->andEndsWith(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andHabitacionEstadiaIdContains($integer) {
		return $this->andContains(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaId($integer) {
		return $this->or(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdNot($integer) {
		return $this->orNot(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdLike($integer) {
		return $this->orLike(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdNotLike($integer) {
		return $this->orNotLike(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdGreater($integer) {
		return $this->orGreater(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdLess($integer) {
		return $this->orLess(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdLessEqual($integer) {
		return $this->orLessEqual(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdNull() {
		return $this->orNull(ClientesEstadias::HABITACION_ESTADIA_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdNotNull() {
		return $this->orNotNull(ClientesEstadias::HABITACION_ESTADIA_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdBetween($integer, $from, $to) {
		return $this->orBetween(ClientesEstadias::HABITACION_ESTADIA_ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdBeginsWith($integer) {
		return $this->orBeginsWith(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdEndsWith($integer) {
		return $this->orEndsWith(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orHabitacionEstadiaIdContains($integer) {
		return $this->orContains(ClientesEstadias::HABITACION_ESTADIA_ID, $integer);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByHabitacionEstadiaIdAsc() {
		return $this->orderBy(ClientesEstadias::HABITACION_ESTADIA_ID, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByHabitacionEstadiaIdDesc() {
		return $this->orderBy(ClientesEstadias::HABITACION_ESTADIA_ID, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupByHabitacionEstadiaId() {
		return $this->groupBy(ClientesEstadias::HABITACION_ESTADIA_ID);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(ClientesEstadias::CREATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(ClientesEstadias::CREATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ClientesEstadias::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(ClientesEstadias::CREATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(ClientesEstadias::CREATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ClientesEstadias::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ClientesEstadias::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(ClientesEstadias::CREATED_AT, $timestamp);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(ClientesEstadias::CREATED_AT, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(ClientesEstadias::CREATED_AT, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(ClientesEstadias::CREATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(ClientesEstadias::UPDATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(ClientesEstadias::UPDATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ClientesEstadias::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(ClientesEstadias::UPDATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(ClientesEstadias::UPDATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ClientesEstadias::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(ClientesEstadias::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(ClientesEstadias::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(ClientesEstadias::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(ClientesEstadias::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(ClientesEstadias::UPDATED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(ClientesEstadias::DELETED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(ClientesEstadias::DELETED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(ClientesEstadias::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(ClientesEstadias::DELETED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(ClientesEstadias::DELETED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(ClientesEstadias::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(ClientesEstadias::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(ClientesEstadias::DELETED_AT, $timestamp);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(ClientesEstadias::DELETED_AT, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(ClientesEstadias::DELETED_AT, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(ClientesEstadias::DELETED_AT);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstado($float) {
		return $this->addAnd(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoNot($float) {
		return $this->andNot(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoLike($float) {
		return $this->andLike(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoNotLike($float) {
		return $this->andNotLike(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoGreater($float) {
		return $this->andGreater(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoGreaterEqual($float) {
		return $this->andGreaterEqual(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoLess($float) {
		return $this->andLess(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoLessEqual($float) {
		return $this->andLessEqual(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoNull() {
		return $this->andNull(ClientesEstadias::ESTADO);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoNotNull() {
		return $this->andNotNull(ClientesEstadias::ESTADO);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoBetween($float, $from, $to) {
		return $this->andBetween(ClientesEstadias::ESTADO, $float, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoBeginsWith($float) {
		return $this->andBeginsWith(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoEndsWith($float) {
		return $this->andEndsWith(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function andEstadoContains($float) {
		return $this->andContains(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstado($float) {
		return $this->or(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoNot($float) {
		return $this->orNot(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoLike($float) {
		return $this->orLike(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoNotLike($float) {
		return $this->orNotLike(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoGreater($float) {
		return $this->orGreater(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoGreaterEqual($float) {
		return $this->orGreaterEqual(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoLess($float) {
		return $this->orLess(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoLessEqual($float) {
		return $this->orLessEqual(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoNull() {
		return $this->orNull(ClientesEstadias::ESTADO);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoNotNull() {
		return $this->orNotNull(ClientesEstadias::ESTADO);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoBetween($float, $from, $to) {
		return $this->orBetween(ClientesEstadias::ESTADO, $float, $from, $to);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoBeginsWith($float) {
		return $this->orBeginsWith(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoEndsWith($float) {
		return $this->orEndsWith(ClientesEstadias::ESTADO, $float);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orEstadoContains($float) {
		return $this->orContains(ClientesEstadias::ESTADO, $float);
	}


	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByEstadoAsc() {
		return $this->orderBy(ClientesEstadias::ESTADO, self::ASC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function orderByEstadoDesc() {
		return $this->orderBy(ClientesEstadias::ESTADO, self::DESC);
	}

	/**
	 * @return ClientesEstadiasQuery
	 */
	function groupByEstado() {
		return $this->groupBy(ClientesEstadias::ESTADO);
	}


}