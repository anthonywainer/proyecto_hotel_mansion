<?php

use Dabl\Query\Query;

abstract class baseEstadiasHabitacionesQuery extends Query {

    function __construct($table_name = null, $alias = null) {
        if (null === $table_name) {
            $table_name = EstadiasHabitaciones::getTableName();
        }
        return parent::__construct($table_name, $alias);
    }

    /**
     * Returns new instance of self by passing arguments directly to constructor.
     * @param string $alias
     * @return EstadiasHabitacionesQuery
     */
    static function create($table_name = null, $alias = null) {
        return new EstadiasHabitacionesQuery($table_name, $alias);
    }

    /**
     * @return EstadiasHabitaciones[]
     */
    function select() {
        return EstadiasHabitaciones::doSelect($this);
    }

    /**
     * @return EstadiasHabitaciones
     */
    function selectOne() {
        return EstadiasHabitaciones::doSelectOne($this);
    }

    /**
     * @return int
     */
    function delete(){
        return EstadiasHabitaciones::doDelete($this);
    }

    /**
     * @return int
     */
    function count(){
        return EstadiasHabitaciones::doCount($this);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
        if (null !== $type && EstadiasHabitaciones::isTemporalType($type)) {
            $value = EstadiasHabitaciones::coerceTemporalValue($value, $type);
        }
        if (null === $value && is_array($column) && Model::isTemporalType($type)) {
            $column = EstadiasHabitaciones::coerceTemporalValue($column, $type);
        }
        return parent::addAnd($column, $value, $operator, $quote);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
        if (null !== $type && EstadiasHabitaciones::isTemporalType($type)) {
            $value = EstadiasHabitaciones::coerceTemporalValue($value, $type);
        }
        if (null === $value && is_array($column) && Model::isTemporalType($type)) {
            $column = EstadiasHabitaciones::coerceTemporalValue($column, $type);
        }
        return parent::addOr($column, $value, $operator, $quote);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andId($integer) {
        return $this->addAnd(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdNot($integer) {
        return $this->andNot(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdLike($integer) {
        return $this->andLike(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdNotLike($integer) {
        return $this->andNotLike(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdGreater($integer) {
        return $this->andGreater(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdGreaterEqual($integer) {
        return $this->andGreaterEqual(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdLess($integer) {
        return $this->andLess(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdLessEqual($integer) {
        return $this->andLessEqual(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdNull() {
        return $this->andNull(EstadiasHabitaciones::ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdBetween($integer, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::ID, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdBeginsWith($integer) {
        return $this->andBeginsWith(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdEndsWith($integer) {
        return $this->andEndsWith(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andIdContains($integer) {
        return $this->andContains(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orId($integer) {
        return $this->or(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdNot($integer) {
        return $this->orNot(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdLike($integer) {
        return $this->orLike(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdNotLike($integer) {
        return $this->orNotLike(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdGreater($integer) {
        return $this->orGreater(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdGreaterEqual($integer) {
        return $this->orGreaterEqual(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdLess($integer) {
        return $this->orLess(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdLessEqual($integer) {
        return $this->orLessEqual(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdNull() {
        return $this->orNull(EstadiasHabitaciones::ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdBetween($integer, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::ID, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdBeginsWith($integer) {
        return $this->orBeginsWith(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdEndsWith($integer) {
        return $this->orEndsWith(EstadiasHabitaciones::ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orIdContains($integer) {
        return $this->orContains(EstadiasHabitaciones::ID, $integer);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByIdAsc() {
        return $this->orderBy(EstadiasHabitaciones::ID, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByIdDesc() {
        return $this->orderBy(EstadiasHabitaciones::ID, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupById() {
        return $this->groupBy(EstadiasHabitaciones::ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuento($decimal) {
        return $this->addAnd(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoNot($decimal) {
        return $this->andNot(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoLike($decimal) {
        return $this->andLike(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoNotLike($decimal) {
        return $this->andNotLike(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoGreater($decimal) {
        return $this->andGreater(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoGreaterEqual($decimal) {
        return $this->andGreaterEqual(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoLess($decimal) {
        return $this->andLess(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoLessEqual($decimal) {
        return $this->andLessEqual(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoNull() {
        return $this->andNull(EstadiasHabitaciones::DESCUENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::DESCUENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoBetween($decimal, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::DESCUENTO, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoBeginsWith($decimal) {
        return $this->andBeginsWith(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoEndsWith($decimal) {
        return $this->andEndsWith(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDescuentoContains($decimal) {
        return $this->andContains(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuento($decimal) {
        return $this->or(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoNot($decimal) {
        return $this->orNot(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoLike($decimal) {
        return $this->orLike(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoNotLike($decimal) {
        return $this->orNotLike(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoGreater($decimal) {
        return $this->orGreater(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoGreaterEqual($decimal) {
        return $this->orGreaterEqual(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoLess($decimal) {
        return $this->orLess(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoLessEqual($decimal) {
        return $this->orLessEqual(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoNull() {
        return $this->orNull(EstadiasHabitaciones::DESCUENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::DESCUENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoBetween($decimal, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::DESCUENTO, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoBeginsWith($decimal) {
        return $this->orBeginsWith(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoEndsWith($decimal) {
        return $this->orEndsWith(EstadiasHabitaciones::DESCUENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDescuentoContains($decimal) {
        return $this->orContains(EstadiasHabitaciones::DESCUENTO, $decimal);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByDescuentoAsc() {
        return $this->orderBy(EstadiasHabitaciones::DESCUENTO, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByDescuentoDesc() {
        return $this->orderBy(EstadiasHabitaciones::DESCUENTO, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByDescuento() {
        return $this->groupBy(EstadiasHabitaciones::DESCUENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistente($varchar) {
        return $this->addAnd(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteNot($varchar) {
        return $this->andNot(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteLike($varchar) {
        return $this->andLike(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteNotLike($varchar) {
        return $this->andNotLike(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteGreater($varchar) {
        return $this->andGreater(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteGreaterEqual($varchar) {
        return $this->andGreaterEqual(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteLess($varchar) {
        return $this->andLess(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteLessEqual($varchar) {
        return $this->andLessEqual(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteNull() {
        return $this->andNull(EstadiasHabitaciones::ASISTENTE);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::ASISTENTE);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteBetween($varchar, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::ASISTENTE, $varchar, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteBeginsWith($varchar) {
        return $this->andBeginsWith(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteEndsWith($varchar) {
        return $this->andEndsWith(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAsistenteContains($varchar) {
        return $this->andContains(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistente($varchar) {
        return $this->or(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteNot($varchar) {
        return $this->orNot(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteLike($varchar) {
        return $this->orLike(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteNotLike($varchar) {
        return $this->orNotLike(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteGreater($varchar) {
        return $this->orGreater(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteGreaterEqual($varchar) {
        return $this->orGreaterEqual(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteLess($varchar) {
        return $this->orLess(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteLessEqual($varchar) {
        return $this->orLessEqual(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteNull() {
        return $this->orNull(EstadiasHabitaciones::ASISTENTE);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::ASISTENTE);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteBetween($varchar, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::ASISTENTE, $varchar, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteBeginsWith($varchar) {
        return $this->orBeginsWith(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteEndsWith($varchar) {
        return $this->orEndsWith(EstadiasHabitaciones::ASISTENTE, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAsistenteContains($varchar) {
        return $this->orContains(EstadiasHabitaciones::ASISTENTE, $varchar);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByAsistenteAsc() {
        return $this->orderBy(EstadiasHabitaciones::ASISTENTE, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByAsistenteDesc() {
        return $this->orderBy(EstadiasHabitaciones::ASISTENTE, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByAsistente() {
        return $this->groupBy(EstadiasHabitaciones::ASISTENTE);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaId($integer) {
        return $this->addAnd(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdNot($integer) {
        return $this->andNot(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdLike($integer) {
        return $this->andLike(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdNotLike($integer) {
        return $this->andNotLike(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdGreater($integer) {
        return $this->andGreater(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdGreaterEqual($integer) {
        return $this->andGreaterEqual(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdLess($integer) {
        return $this->andLess(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdLessEqual($integer) {
        return $this->andLessEqual(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdNull() {
        return $this->andNull(EstadiasHabitaciones::ESTADIA_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::ESTADIA_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdBetween($integer, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::ESTADIA_ID, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdBeginsWith($integer) {
        return $this->andBeginsWith(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdEndsWith($integer) {
        return $this->andEndsWith(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadiaIdContains($integer) {
        return $this->andContains(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaId($integer) {
        return $this->or(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdNot($integer) {
        return $this->orNot(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdLike($integer) {
        return $this->orLike(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdNotLike($integer) {
        return $this->orNotLike(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdGreater($integer) {
        return $this->orGreater(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdGreaterEqual($integer) {
        return $this->orGreaterEqual(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdLess($integer) {
        return $this->orLess(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdLessEqual($integer) {
        return $this->orLessEqual(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdNull() {
        return $this->orNull(EstadiasHabitaciones::ESTADIA_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::ESTADIA_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdBetween($integer, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::ESTADIA_ID, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdBeginsWith($integer) {
        return $this->orBeginsWith(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdEndsWith($integer) {
        return $this->orEndsWith(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadiaIdContains($integer) {
        return $this->orContains(EstadiasHabitaciones::ESTADIA_ID, $integer);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByEstadiaIdAsc() {
        return $this->orderBy(EstadiasHabitaciones::ESTADIA_ID, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByEstadiaIdDesc() {
        return $this->orderBy(EstadiasHabitaciones::ESTADIA_ID, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByEstadiaId() {
        return $this->groupBy(EstadiasHabitaciones::ESTADIA_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionId($integer) {
        return $this->addAnd(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdNot($integer) {
        return $this->andNot(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdLike($integer) {
        return $this->andLike(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdNotLike($integer) {
        return $this->andNotLike(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdGreater($integer) {
        return $this->andGreater(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdGreaterEqual($integer) {
        return $this->andGreaterEqual(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdLess($integer) {
        return $this->andLess(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdLessEqual($integer) {
        return $this->andLessEqual(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdNull() {
        return $this->andNull(EstadiasHabitaciones::HABITACION_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::HABITACION_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdBetween($integer, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::HABITACION_ID, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdBeginsWith($integer) {
        return $this->andBeginsWith(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdEndsWith($integer) {
        return $this->andEndsWith(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andHabitacionIdContains($integer) {
        return $this->andContains(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionId($integer) {
        return $this->or(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdNot($integer) {
        return $this->orNot(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdLike($integer) {
        return $this->orLike(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdNotLike($integer) {
        return $this->orNotLike(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdGreater($integer) {
        return $this->orGreater(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdGreaterEqual($integer) {
        return $this->orGreaterEqual(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdLess($integer) {
        return $this->orLess(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdLessEqual($integer) {
        return $this->orLessEqual(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdNull() {
        return $this->orNull(EstadiasHabitaciones::HABITACION_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::HABITACION_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdBetween($integer, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::HABITACION_ID, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdBeginsWith($integer) {
        return $this->orBeginsWith(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdEndsWith($integer) {
        return $this->orEndsWith(EstadiasHabitaciones::HABITACION_ID, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orHabitacionIdContains($integer) {
        return $this->orContains(EstadiasHabitaciones::HABITACION_ID, $integer);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByHabitacionIdAsc() {
        return $this->orderBy(EstadiasHabitaciones::HABITACION_ID, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByHabitacionIdDesc() {
        return $this->orderBy(EstadiasHabitaciones::HABITACION_ID, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByHabitacionId() {
        return $this->groupBy(EstadiasHabitaciones::HABITACION_ID);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAt($timestamp) {
        return $this->addAnd(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtNot($timestamp) {
        return $this->andNot(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtLike($timestamp) {
        return $this->andLike(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtNotLike($timestamp) {
        return $this->andNotLike(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtGreater($timestamp) {
        return $this->andGreater(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtGreaterEqual($timestamp) {
        return $this->andGreaterEqual(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtLess($timestamp) {
        return $this->andLess(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtLessEqual($timestamp) {
        return $this->andLessEqual(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtNull() {
        return $this->andNull(EstadiasHabitaciones::CREATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::CREATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtBetween($timestamp, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::CREATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtBeginsWith($timestamp) {
        return $this->andBeginsWith(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtEndsWith($timestamp) {
        return $this->andEndsWith(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andCreatedAtContains($timestamp) {
        return $this->andContains(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAt($timestamp) {
        return $this->or(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtNot($timestamp) {
        return $this->orNot(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtLike($timestamp) {
        return $this->orLike(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtNotLike($timestamp) {
        return $this->orNotLike(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtGreater($timestamp) {
        return $this->orGreater(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtGreaterEqual($timestamp) {
        return $this->orGreaterEqual(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtLess($timestamp) {
        return $this->orLess(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtLessEqual($timestamp) {
        return $this->orLessEqual(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtNull() {
        return $this->orNull(EstadiasHabitaciones::CREATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::CREATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtBetween($timestamp, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::CREATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtBeginsWith($timestamp) {
        return $this->orBeginsWith(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtEndsWith($timestamp) {
        return $this->orEndsWith(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orCreatedAtContains($timestamp) {
        return $this->orContains(EstadiasHabitaciones::CREATED_AT, $timestamp);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByCreatedAtAsc() {
        return $this->orderBy(EstadiasHabitaciones::CREATED_AT, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByCreatedAtDesc() {
        return $this->orderBy(EstadiasHabitaciones::CREATED_AT, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByCreatedAt() {
        return $this->groupBy(EstadiasHabitaciones::CREATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAt($timestamp) {
        return $this->addAnd(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtNot($timestamp) {
        return $this->andNot(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtLike($timestamp) {
        return $this->andLike(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtNotLike($timestamp) {
        return $this->andNotLike(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtGreater($timestamp) {
        return $this->andGreater(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtGreaterEqual($timestamp) {
        return $this->andGreaterEqual(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtLess($timestamp) {
        return $this->andLess(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtLessEqual($timestamp) {
        return $this->andLessEqual(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtNull() {
        return $this->andNull(EstadiasHabitaciones::UPDATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::UPDATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtBetween($timestamp, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::UPDATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtBeginsWith($timestamp) {
        return $this->andBeginsWith(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtEndsWith($timestamp) {
        return $this->andEndsWith(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andUpdatedAtContains($timestamp) {
        return $this->andContains(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAt($timestamp) {
        return $this->or(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtNot($timestamp) {
        return $this->orNot(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtLike($timestamp) {
        return $this->orLike(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtNotLike($timestamp) {
        return $this->orNotLike(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtGreater($timestamp) {
        return $this->orGreater(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtGreaterEqual($timestamp) {
        return $this->orGreaterEqual(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtLess($timestamp) {
        return $this->orLess(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtLessEqual($timestamp) {
        return $this->orLessEqual(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtNull() {
        return $this->orNull(EstadiasHabitaciones::UPDATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::UPDATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtBetween($timestamp, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::UPDATED_AT, $timestamp, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtBeginsWith($timestamp) {
        return $this->orBeginsWith(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtEndsWith($timestamp) {
        return $this->orEndsWith(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orUpdatedAtContains($timestamp) {
        return $this->orContains(EstadiasHabitaciones::UPDATED_AT, $timestamp);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByUpdatedAtAsc() {
        return $this->orderBy(EstadiasHabitaciones::UPDATED_AT, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByUpdatedAtDesc() {
        return $this->orderBy(EstadiasHabitaciones::UPDATED_AT, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByUpdatedAt() {
        return $this->groupBy(EstadiasHabitaciones::UPDATED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAt($timestamp) {
        return $this->addAnd(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtNot($timestamp) {
        return $this->andNot(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtLike($timestamp) {
        return $this->andLike(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtNotLike($timestamp) {
        return $this->andNotLike(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtGreater($timestamp) {
        return $this->andGreater(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtGreaterEqual($timestamp) {
        return $this->andGreaterEqual(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtLess($timestamp) {
        return $this->andLess(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtLessEqual($timestamp) {
        return $this->andLessEqual(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtNull() {
        return $this->andNull(EstadiasHabitaciones::DELETED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::DELETED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtBetween($timestamp, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::DELETED_AT, $timestamp, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtBeginsWith($timestamp) {
        return $this->andBeginsWith(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtEndsWith($timestamp) {
        return $this->andEndsWith(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDeletedAtContains($timestamp) {
        return $this->andContains(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAt($timestamp) {
        return $this->or(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtNot($timestamp) {
        return $this->orNot(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtLike($timestamp) {
        return $this->orLike(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtNotLike($timestamp) {
        return $this->orNotLike(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtGreater($timestamp) {
        return $this->orGreater(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtGreaterEqual($timestamp) {
        return $this->orGreaterEqual(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtLess($timestamp) {
        return $this->orLess(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtLessEqual($timestamp) {
        return $this->orLessEqual(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtNull() {
        return $this->orNull(EstadiasHabitaciones::DELETED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::DELETED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtBetween($timestamp, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::DELETED_AT, $timestamp, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtBeginsWith($timestamp) {
        return $this->orBeginsWith(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtEndsWith($timestamp) {
        return $this->orEndsWith(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDeletedAtContains($timestamp) {
        return $this->orContains(EstadiasHabitaciones::DELETED_AT, $timestamp);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByDeletedAtAsc() {
        return $this->orderBy(EstadiasHabitaciones::DELETED_AT, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByDeletedAtDesc() {
        return $this->orderBy(EstadiasHabitaciones::DELETED_AT, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByDeletedAt() {
        return $this->groupBy(EstadiasHabitaciones::DELETED_AT);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReserva($date) {
        return $this->addAnd(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaNot($date) {
        return $this->andNot(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaLike($date) {
        return $this->andLike(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaNotLike($date) {
        return $this->andNotLike(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaGreater($date) {
        return $this->andGreater(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaGreaterEqual($date) {
        return $this->andGreaterEqual(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaLess($date) {
        return $this->andLess(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaLessEqual($date) {
        return $this->andLessEqual(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaNull() {
        return $this->andNull(EstadiasHabitaciones::FECHA_RESERVA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::FECHA_RESERVA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaBetween($date, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::FECHA_RESERVA, $date, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaBeginsWith($date) {
        return $this->andBeginsWith(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaEndsWith($date) {
        return $this->andEndsWith(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaReservaContains($date) {
        return $this->andContains(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReserva($date) {
        return $this->or(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaNot($date) {
        return $this->orNot(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaLike($date) {
        return $this->orLike(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaNotLike($date) {
        return $this->orNotLike(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaGreater($date) {
        return $this->orGreater(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaGreaterEqual($date) {
        return $this->orGreaterEqual(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaLess($date) {
        return $this->orLess(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaLessEqual($date) {
        return $this->orLessEqual(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaNull() {
        return $this->orNull(EstadiasHabitaciones::FECHA_RESERVA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::FECHA_RESERVA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaBetween($date, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::FECHA_RESERVA, $date, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaBeginsWith($date) {
        return $this->orBeginsWith(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaEndsWith($date) {
        return $this->orEndsWith(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaReservaContains($date) {
        return $this->orContains(EstadiasHabitaciones::FECHA_RESERVA, $date);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByFechaReservaAsc() {
        return $this->orderBy(EstadiasHabitaciones::FECHA_RESERVA, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByFechaReservaDesc() {
        return $this->orderBy(EstadiasHabitaciones::FECHA_RESERVA, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByFechaReserva() {
        return $this->groupBy(EstadiasHabitaciones::FECHA_RESERVA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngreso($date) {
        return $this->addAnd(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoNot($date) {
        return $this->andNot(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoLike($date) {
        return $this->andLike(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoNotLike($date) {
        return $this->andNotLike(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoGreater($date) {
        return $this->andGreater(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoGreaterEqual($date) {
        return $this->andGreaterEqual(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoLess($date) {
        return $this->andLess(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoLessEqual($date) {
        return $this->andLessEqual(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoNull() {
        return $this->andNull(EstadiasHabitaciones::FECHA_INGRESO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::FECHA_INGRESO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoBetween($date, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::FECHA_INGRESO, $date, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoBeginsWith($date) {
        return $this->andBeginsWith(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoEndsWith($date) {
        return $this->andEndsWith(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaIngresoContains($date) {
        return $this->andContains(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngreso($date) {
        return $this->or(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoNot($date) {
        return $this->orNot(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoLike($date) {
        return $this->orLike(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoNotLike($date) {
        return $this->orNotLike(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoGreater($date) {
        return $this->orGreater(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoGreaterEqual($date) {
        return $this->orGreaterEqual(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoLess($date) {
        return $this->orLess(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoLessEqual($date) {
        return $this->orLessEqual(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoNull() {
        return $this->orNull(EstadiasHabitaciones::FECHA_INGRESO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::FECHA_INGRESO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoBetween($date, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::FECHA_INGRESO, $date, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoBeginsWith($date) {
        return $this->orBeginsWith(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoEndsWith($date) {
        return $this->orEndsWith(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaIngresoContains($date) {
        return $this->orContains(EstadiasHabitaciones::FECHA_INGRESO, $date);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByFechaIngresoAsc() {
        return $this->orderBy(EstadiasHabitaciones::FECHA_INGRESO, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByFechaIngresoDesc() {
        return $this->orderBy(EstadiasHabitaciones::FECHA_INGRESO, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByFechaIngreso() {
        return $this->groupBy(EstadiasHabitaciones::FECHA_INGRESO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalida($date) {
        return $this->addAnd(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaNot($date) {
        return $this->andNot(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaLike($date) {
        return $this->andLike(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaNotLike($date) {
        return $this->andNotLike(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaGreater($date) {
        return $this->andGreater(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaGreaterEqual($date) {
        return $this->andGreaterEqual(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaLess($date) {
        return $this->andLess(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaLessEqual($date) {
        return $this->andLessEqual(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaNull() {
        return $this->andNull(EstadiasHabitaciones::FECHA_SALIDA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::FECHA_SALIDA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaBetween($date, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::FECHA_SALIDA, $date, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaBeginsWith($date) {
        return $this->andBeginsWith(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaEndsWith($date) {
        return $this->andEndsWith(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andFechaSalidaContains($date) {
        return $this->andContains(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalida($date) {
        return $this->or(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaNot($date) {
        return $this->orNot(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaLike($date) {
        return $this->orLike(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaNotLike($date) {
        return $this->orNotLike(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaGreater($date) {
        return $this->orGreater(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaGreaterEqual($date) {
        return $this->orGreaterEqual(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaLess($date) {
        return $this->orLess(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaLessEqual($date) {
        return $this->orLessEqual(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaNull() {
        return $this->orNull(EstadiasHabitaciones::FECHA_SALIDA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::FECHA_SALIDA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaBetween($date, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::FECHA_SALIDA, $date, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaBeginsWith($date) {
        return $this->orBeginsWith(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaEndsWith($date) {
        return $this->orEndsWith(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orFechaSalidaContains($date) {
        return $this->orContains(EstadiasHabitaciones::FECHA_SALIDA, $date);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByFechaSalidaAsc() {
        return $this->orderBy(EstadiasHabitaciones::FECHA_SALIDA, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByFechaSalidaDesc() {
        return $this->orderBy(EstadiasHabitaciones::FECHA_SALIDA, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByFechaSalida() {
        return $this->groupBy(EstadiasHabitaciones::FECHA_SALIDA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacion($varchar) {
        return $this->addAnd(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionNot($varchar) {
        return $this->andNot(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionLike($varchar) {
        return $this->andLike(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionNotLike($varchar) {
        return $this->andNotLike(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionGreater($varchar) {
        return $this->andGreater(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionGreaterEqual($varchar) {
        return $this->andGreaterEqual(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionLess($varchar) {
        return $this->andLess(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionLessEqual($varchar) {
        return $this->andLessEqual(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionNull() {
        return $this->andNull(EstadiasHabitaciones::OBSERVACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::OBSERVACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionBetween($varchar, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::OBSERVACION, $varchar, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionBeginsWith($varchar) {
        return $this->andBeginsWith(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionEndsWith($varchar) {
        return $this->andEndsWith(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andObservacionContains($varchar) {
        return $this->andContains(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacion($varchar) {
        return $this->or(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionNot($varchar) {
        return $this->orNot(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionLike($varchar) {
        return $this->orLike(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionNotLike($varchar) {
        return $this->orNotLike(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionGreater($varchar) {
        return $this->orGreater(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionGreaterEqual($varchar) {
        return $this->orGreaterEqual(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionLess($varchar) {
        return $this->orLess(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionLessEqual($varchar) {
        return $this->orLessEqual(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionNull() {
        return $this->orNull(EstadiasHabitaciones::OBSERVACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::OBSERVACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionBetween($varchar, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::OBSERVACION, $varchar, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionBeginsWith($varchar) {
        return $this->orBeginsWith(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionEndsWith($varchar) {
        return $this->orEndsWith(EstadiasHabitaciones::OBSERVACION, $varchar);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orObservacionContains($varchar) {
        return $this->orContains(EstadiasHabitaciones::OBSERVACION, $varchar);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByObservacionAsc() {
        return $this->orderBy(EstadiasHabitaciones::OBSERVACION, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByObservacionDesc() {
        return $this->orderBy(EstadiasHabitaciones::OBSERVACION, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByObservacion() {
        return $this->groupBy(EstadiasHabitaciones::OBSERVACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotal($decimal) {
        return $this->addAnd(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalNot($decimal) {
        return $this->andNot(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalLike($decimal) {
        return $this->andLike(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalNotLike($decimal) {
        return $this->andNotLike(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalGreater($decimal) {
        return $this->andGreater(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalGreaterEqual($decimal) {
        return $this->andGreaterEqual(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalLess($decimal) {
        return $this->andLess(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalLessEqual($decimal) {
        return $this->andLessEqual(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalNull() {
        return $this->andNull(EstadiasHabitaciones::PRECIO_TOTAL);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::PRECIO_TOTAL);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalBetween($decimal, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::PRECIO_TOTAL, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalBeginsWith($decimal) {
        return $this->andBeginsWith(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalEndsWith($decimal) {
        return $this->andEndsWith(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioTotalContains($decimal) {
        return $this->andContains(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotal($decimal) {
        return $this->or(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalNot($decimal) {
        return $this->orNot(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalLike($decimal) {
        return $this->orLike(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalNotLike($decimal) {
        return $this->orNotLike(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalGreater($decimal) {
        return $this->orGreater(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalGreaterEqual($decimal) {
        return $this->orGreaterEqual(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalLess($decimal) {
        return $this->orLess(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalLessEqual($decimal) {
        return $this->orLessEqual(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalNull() {
        return $this->orNull(EstadiasHabitaciones::PRECIO_TOTAL);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::PRECIO_TOTAL);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalBetween($decimal, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::PRECIO_TOTAL, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalBeginsWith($decimal) {
        return $this->orBeginsWith(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalEndsWith($decimal) {
        return $this->orEndsWith(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioTotalContains($decimal) {
        return $this->orContains(EstadiasHabitaciones::PRECIO_TOTAL, $decimal);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByPrecioTotalAsc() {
        return $this->orderBy(EstadiasHabitaciones::PRECIO_TOTAL, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByPrecioTotalDesc() {
        return $this->orderBy(EstadiasHabitaciones::PRECIO_TOTAL, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByPrecioTotal() {
        return $this->groupBy(EstadiasHabitaciones::PRECIO_TOTAL);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDias($integer) {
        return $this->addAnd(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasNot($integer) {
        return $this->andNot(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasLike($integer) {
        return $this->andLike(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasNotLike($integer) {
        return $this->andNotLike(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasGreater($integer) {
        return $this->andGreater(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasGreaterEqual($integer) {
        return $this->andGreaterEqual(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasLess($integer) {
        return $this->andLess(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasLessEqual($integer) {
        return $this->andLessEqual(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasNull() {
        return $this->andNull(EstadiasHabitaciones::DIAS);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::DIAS);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasBetween($integer, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::DIAS, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasBeginsWith($integer) {
        return $this->andBeginsWith(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasEndsWith($integer) {
        return $this->andEndsWith(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andDiasContains($integer) {
        return $this->andContains(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDias($integer) {
        return $this->or(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasNot($integer) {
        return $this->orNot(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasLike($integer) {
        return $this->orLike(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasNotLike($integer) {
        return $this->orNotLike(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasGreater($integer) {
        return $this->orGreater(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasGreaterEqual($integer) {
        return $this->orGreaterEqual(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasLess($integer) {
        return $this->orLess(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasLessEqual($integer) {
        return $this->orLessEqual(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasNull() {
        return $this->orNull(EstadiasHabitaciones::DIAS);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::DIAS);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasBetween($integer, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::DIAS, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasBeginsWith($integer) {
        return $this->orBeginsWith(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasEndsWith($integer) {
        return $this->orEndsWith(EstadiasHabitaciones::DIAS, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orDiasContains($integer) {
        return $this->orContains(EstadiasHabitaciones::DIAS, $integer);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByDiasAsc() {
        return $this->orderBy(EstadiasHabitaciones::DIAS, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByDiasDesc() {
        return $this->orderBy(EstadiasHabitaciones::DIAS, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByDias() {
        return $this->groupBy(EstadiasHabitaciones::DIAS);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacion($integer) {
        return $this->addAnd(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionNot($integer) {
        return $this->andNot(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionLike($integer) {
        return $this->andLike(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionNotLike($integer) {
        return $this->andNotLike(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionGreater($integer) {
        return $this->andGreater(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionGreaterEqual($integer) {
        return $this->andGreaterEqual(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionLess($integer) {
        return $this->andLess(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionLessEqual($integer) {
        return $this->andLessEqual(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionNull() {
        return $this->andNull(EstadiasHabitaciones::TIPO_HABITACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::TIPO_HABITACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionBetween($integer, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::TIPO_HABITACION, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionBeginsWith($integer) {
        return $this->andBeginsWith(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionEndsWith($integer) {
        return $this->andEndsWith(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoHabitacionContains($integer) {
        return $this->andContains(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacion($integer) {
        return $this->or(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionNot($integer) {
        return $this->orNot(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionLike($integer) {
        return $this->orLike(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionNotLike($integer) {
        return $this->orNotLike(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionGreater($integer) {
        return $this->orGreater(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionGreaterEqual($integer) {
        return $this->orGreaterEqual(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionLess($integer) {
        return $this->orLess(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionLessEqual($integer) {
        return $this->orLessEqual(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionNull() {
        return $this->orNull(EstadiasHabitaciones::TIPO_HABITACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::TIPO_HABITACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionBetween($integer, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::TIPO_HABITACION, $integer, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionBeginsWith($integer) {
        return $this->orBeginsWith(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionEndsWith($integer) {
        return $this->orEndsWith(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoHabitacionContains($integer) {
        return $this->orContains(EstadiasHabitaciones::TIPO_HABITACION, $integer);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByTipoHabitacionAsc() {
        return $this->orderBy(EstadiasHabitaciones::TIPO_HABITACION, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByTipoHabitacionDesc() {
        return $this->orderBy(EstadiasHabitaciones::TIPO_HABITACION, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByTipoHabitacion() {
        return $this->groupBy(EstadiasHabitaciones::TIPO_HABITACION);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHa($boolean) {
        return $this->addAnd(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaNot($boolean) {
        return $this->andNot(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaLike($boolean) {
        return $this->andLike(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaNotLike($boolean) {
        return $this->andNotLike(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaGreater($boolean) {
        return $this->andGreater(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaGreaterEqual($boolean) {
        return $this->andGreaterEqual(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaLess($boolean) {
        return $this->andLess(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaLessEqual($boolean) {
        return $this->andLessEqual(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaNull() {
        return $this->andNull(EstadiasHabitaciones::ESTADO_ES_HA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::ESTADO_ES_HA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaBetween($boolean, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::ESTADO_ES_HA, $boolean, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaBeginsWith($boolean) {
        return $this->andBeginsWith(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaEndsWith($boolean) {
        return $this->andEndsWith(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andEstadoEsHaContains($boolean) {
        return $this->andContains(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHa($boolean) {
        return $this->or(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaNot($boolean) {
        return $this->orNot(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaLike($boolean) {
        return $this->orLike(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaNotLike($boolean) {
        return $this->orNotLike(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaGreater($boolean) {
        return $this->orGreater(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaGreaterEqual($boolean) {
        return $this->orGreaterEqual(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaLess($boolean) {
        return $this->orLess(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaLessEqual($boolean) {
        return $this->orLessEqual(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaNull() {
        return $this->orNull(EstadiasHabitaciones::ESTADO_ES_HA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::ESTADO_ES_HA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaBetween($boolean, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::ESTADO_ES_HA, $boolean, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaBeginsWith($boolean) {
        return $this->orBeginsWith(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaEndsWith($boolean) {
        return $this->orEndsWith(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orEstadoEsHaContains($boolean) {
        return $this->orContains(EstadiasHabitaciones::ESTADO_ES_HA, $boolean);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByEstadoEsHaAsc() {
        return $this->orderBy(EstadiasHabitaciones::ESTADO_ES_HA, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByEstadoEsHaDesc() {
        return $this->orderBy(EstadiasHabitaciones::ESTADO_ES_HA, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByEstadoEsHa() {
        return $this->groupBy(EstadiasHabitaciones::ESTADO_ES_HA);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumento($decimal) {
        return $this->addAnd(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoNot($decimal) {
        return $this->andNot(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoLike($decimal) {
        return $this->andLike(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoNotLike($decimal) {
        return $this->andNotLike(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoGreater($decimal) {
        return $this->andGreater(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoGreaterEqual($decimal) {
        return $this->andGreaterEqual(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoLess($decimal) {
        return $this->andLess(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoLessEqual($decimal) {
        return $this->andLessEqual(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoNull() {
        return $this->andNull(EstadiasHabitaciones::AUMENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::AUMENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoBetween($decimal, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::AUMENTO, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoBeginsWith($decimal) {
        return $this->andBeginsWith(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoEndsWith($decimal) {
        return $this->andEndsWith(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andAumentoContains($decimal) {
        return $this->andContains(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumento($decimal) {
        return $this->or(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoNot($decimal) {
        return $this->orNot(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoLike($decimal) {
        return $this->orLike(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoNotLike($decimal) {
        return $this->orNotLike(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoGreater($decimal) {
        return $this->orGreater(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoGreaterEqual($decimal) {
        return $this->orGreaterEqual(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoLess($decimal) {
        return $this->orLess(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoLessEqual($decimal) {
        return $this->orLessEqual(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoNull() {
        return $this->orNull(EstadiasHabitaciones::AUMENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::AUMENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoBetween($decimal, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::AUMENTO, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoBeginsWith($decimal) {
        return $this->orBeginsWith(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoEndsWith($decimal) {
        return $this->orEndsWith(EstadiasHabitaciones::AUMENTO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orAumentoContains($decimal) {
        return $this->orContains(EstadiasHabitaciones::AUMENTO, $decimal);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByAumentoAsc() {
        return $this->orderBy(EstadiasHabitaciones::AUMENTO, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByAumentoDesc() {
        return $this->orderBy(EstadiasHabitaciones::AUMENTO, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByAumento() {
        return $this->groupBy(EstadiasHabitaciones::AUMENTO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecio($boolean) {
        return $this->addAnd(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioNot($boolean) {
        return $this->andNot(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioLike($boolean) {
        return $this->andLike(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioNotLike($boolean) {
        return $this->andNotLike(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioGreater($boolean) {
        return $this->andGreater(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioGreaterEqual($boolean) {
        return $this->andGreaterEqual(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioLess($boolean) {
        return $this->andLess(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioLessEqual($boolean) {
        return $this->andLessEqual(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioNull() {
        return $this->andNull(EstadiasHabitaciones::TIPO_PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::TIPO_PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioBetween($boolean, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::TIPO_PRECIO, $boolean, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioBeginsWith($boolean) {
        return $this->andBeginsWith(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioEndsWith($boolean) {
        return $this->andEndsWith(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andTipoPrecioContains($boolean) {
        return $this->andContains(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecio($boolean) {
        return $this->or(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioNot($boolean) {
        return $this->orNot(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioLike($boolean) {
        return $this->orLike(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioNotLike($boolean) {
        return $this->orNotLike(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioGreater($boolean) {
        return $this->orGreater(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioGreaterEqual($boolean) {
        return $this->orGreaterEqual(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioLess($boolean) {
        return $this->orLess(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioLessEqual($boolean) {
        return $this->orLessEqual(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioNull() {
        return $this->orNull(EstadiasHabitaciones::TIPO_PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::TIPO_PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioBetween($boolean, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::TIPO_PRECIO, $boolean, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioBeginsWith($boolean) {
        return $this->orBeginsWith(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioEndsWith($boolean) {
        return $this->orEndsWith(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orTipoPrecioContains($boolean) {
        return $this->orContains(EstadiasHabitaciones::TIPO_PRECIO, $boolean);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByTipoPrecioAsc() {
        return $this->orderBy(EstadiasHabitaciones::TIPO_PRECIO, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByTipoPrecioDesc() {
        return $this->orderBy(EstadiasHabitaciones::TIPO_PRECIO, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByTipoPrecio() {
        return $this->groupBy(EstadiasHabitaciones::TIPO_PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecio($decimal) {
        return $this->addAnd(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioNot($decimal) {
        return $this->andNot(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioLike($decimal) {
        return $this->andLike(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioNotLike($decimal) {
        return $this->andNotLike(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioGreater($decimal) {
        return $this->andGreater(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioGreaterEqual($decimal) {
        return $this->andGreaterEqual(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioLess($decimal) {
        return $this->andLess(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioLessEqual($decimal) {
        return $this->andLessEqual(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioNull() {
        return $this->andNull(EstadiasHabitaciones::PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioNotNull() {
        return $this->andNotNull(EstadiasHabitaciones::PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioBetween($decimal, $from, $to) {
        return $this->andBetween(EstadiasHabitaciones::PRECIO, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioBeginsWith($decimal) {
        return $this->andBeginsWith(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioEndsWith($decimal) {
        return $this->andEndsWith(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function andPrecioContains($decimal) {
        return $this->andContains(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecio($decimal) {
        return $this->or(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioNot($decimal) {
        return $this->orNot(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioLike($decimal) {
        return $this->orLike(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioNotLike($decimal) {
        return $this->orNotLike(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioGreater($decimal) {
        return $this->orGreater(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioGreaterEqual($decimal) {
        return $this->orGreaterEqual(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioLess($decimal) {
        return $this->orLess(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioLessEqual($decimal) {
        return $this->orLessEqual(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioNull() {
        return $this->orNull(EstadiasHabitaciones::PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioNotNull() {
        return $this->orNotNull(EstadiasHabitaciones::PRECIO);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioBetween($decimal, $from, $to) {
        return $this->orBetween(EstadiasHabitaciones::PRECIO, $decimal, $from, $to);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioBeginsWith($decimal) {
        return $this->orBeginsWith(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioEndsWith($decimal) {
        return $this->orEndsWith(EstadiasHabitaciones::PRECIO, $decimal);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orPrecioContains($decimal) {
        return $this->orContains(EstadiasHabitaciones::PRECIO, $decimal);
    }


    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByPrecioAsc() {
        return $this->orderBy(EstadiasHabitaciones::PRECIO, self::ASC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function orderByPrecioDesc() {
        return $this->orderBy(EstadiasHabitaciones::PRECIO, self::DESC);
    }

    /**
     * @return EstadiasHabitacionesQuery
     */
    function groupByPrecio() {
        return $this->groupBy(EstadiasHabitaciones::PRECIO);
    }


}