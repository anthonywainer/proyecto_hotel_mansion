<?php

use Dabl\Query\Query;

abstract class baseMediosDeEnvioQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = MediosDeEnvio::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return MediosDeEnvioQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new MediosDeEnvioQuery($table_name, $alias);
	}

	/**
	 * @return MediosDeEnvio[]
	 */
	function select() {
		return MediosDeEnvio::doSelect($this);
	}

	/**
	 * @return MediosDeEnvio
	 */
	function selectOne() {
		return MediosDeEnvio::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return MediosDeEnvio::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return MediosDeEnvio::doCount($this);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MediosDeEnvio::isTemporalType($type)) {
			$value = MediosDeEnvio::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MediosDeEnvio::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MediosDeEnvio::isTemporalType($type)) {
			$value = MediosDeEnvio::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MediosDeEnvio::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andId($integer) {
		return $this->addAnd(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdNull() {
		return $this->andNull(MediosDeEnvio::ID);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(MediosDeEnvio::ID);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(MediosDeEnvio::ID, $integer, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orId($integer) {
		return $this->or(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdNull() {
		return $this->orNull(MediosDeEnvio::ID);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(MediosDeEnvio::ID);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(MediosDeEnvio::ID, $integer, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(MediosDeEnvio::ID, $integer);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(MediosDeEnvio::ID, $integer);
	}


	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(MediosDeEnvio::ID, self::ASC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(MediosDeEnvio::ID, self::DESC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function groupById() {
		return $this->groupBy(MediosDeEnvio::ID);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(MediosDeEnvio::DESCRIPCION);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(MediosDeEnvio::DESCRIPCION);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(MediosDeEnvio::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(MediosDeEnvio::DESCRIPCION);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(MediosDeEnvio::DESCRIPCION);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(MediosDeEnvio::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(MediosDeEnvio::DESCRIPCION, $varchar);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(MediosDeEnvio::DESCRIPCION, $varchar);
	}


	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(MediosDeEnvio::DESCRIPCION, self::ASC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(MediosDeEnvio::DESCRIPCION, self::DESC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(MediosDeEnvio::DESCRIPCION);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(MediosDeEnvio::CREATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(MediosDeEnvio::CREATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MediosDeEnvio::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(MediosDeEnvio::CREATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(MediosDeEnvio::CREATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MediosDeEnvio::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MediosDeEnvio::CREATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(MediosDeEnvio::CREATED_AT, $timestamp);
	}


	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(MediosDeEnvio::CREATED_AT, self::ASC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(MediosDeEnvio::CREATED_AT, self::DESC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(MediosDeEnvio::CREATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(MediosDeEnvio::UPDATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(MediosDeEnvio::UPDATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MediosDeEnvio::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(MediosDeEnvio::UPDATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(MediosDeEnvio::UPDATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MediosDeEnvio::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MediosDeEnvio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(MediosDeEnvio::UPDATED_AT, $timestamp);
	}


	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(MediosDeEnvio::UPDATED_AT, self::ASC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(MediosDeEnvio::UPDATED_AT, self::DESC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(MediosDeEnvio::UPDATED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(MediosDeEnvio::DELETED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(MediosDeEnvio::DELETED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MediosDeEnvio::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(MediosDeEnvio::DELETED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(MediosDeEnvio::DELETED_AT);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MediosDeEnvio::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(MediosDeEnvio::DELETED_AT, $timestamp);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(MediosDeEnvio::DELETED_AT, $timestamp);
	}


	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(MediosDeEnvio::DELETED_AT, self::ASC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(MediosDeEnvio::DELETED_AT, self::DESC);
	}

	/**
	 * @return MediosDeEnvioQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(MediosDeEnvio::DELETED_AT);
	}


}