<?php

use Dabl\Query\Query;

abstract class baseProductosConsumidosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ProductosConsumidos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ProductosConsumidosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ProductosConsumidosQuery($table_name, $alias);
	}

	/**
	 * @return ProductosConsumidos[]
	 */
	function select() {
		return ProductosConsumidos::doSelect($this);
	}

	/**
	 * @return ProductosConsumidos
	 */
	function selectOne() {
		return ProductosConsumidos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ProductosConsumidos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ProductosConsumidos::doCount($this);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductosConsumidos::isTemporalType($type)) {
			$value = ProductosConsumidos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductosConsumidos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductosConsumidos::isTemporalType($type)) {
			$value = ProductosConsumidos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductosConsumidos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andId($integer) {
		return $this->addAnd(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdNull() {
		return $this->andNull(ProductosConsumidos::ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ProductosConsumidos::ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductosConsumidos::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orId($integer) {
		return $this->or(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdNull() {
		return $this->orNull(ProductosConsumidos::ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ProductosConsumidos::ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductosConsumidos::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ProductosConsumidos::ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ProductosConsumidos::ID, $integer);
	}


	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ProductosConsumidos::ID, self::ASC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ProductosConsumidos::ID, self::DESC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function groupById() {
		return $this->groupBy(ProductosConsumidos::ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoId($integer) {
		return $this->addAnd(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdNot($integer) {
		return $this->andNot(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdLike($integer) {
		return $this->andLike(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdNotLike($integer) {
		return $this->andNotLike(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdGreater($integer) {
		return $this->andGreater(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdLess($integer) {
		return $this->andLess(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdLessEqual($integer) {
		return $this->andLessEqual(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdNull() {
		return $this->andNull(ProductosConsumidos::PRODUCTO_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdNotNull() {
		return $this->andNotNull(ProductosConsumidos::PRODUCTO_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductosConsumidos::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdEndsWith($integer) {
		return $this->andEndsWith(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andProductoIdContains($integer) {
		return $this->andContains(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoId($integer) {
		return $this->or(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdNot($integer) {
		return $this->orNot(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdLike($integer) {
		return $this->orLike(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdNotLike($integer) {
		return $this->orNotLike(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdGreater($integer) {
		return $this->orGreater(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdLess($integer) {
		return $this->orLess(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdLessEqual($integer) {
		return $this->orLessEqual(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdNull() {
		return $this->orNull(ProductosConsumidos::PRODUCTO_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdNotNull() {
		return $this->orNotNull(ProductosConsumidos::PRODUCTO_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductosConsumidos::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdEndsWith($integer) {
		return $this->orEndsWith(ProductosConsumidos::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orProductoIdContains($integer) {
		return $this->orContains(ProductosConsumidos::PRODUCTO_ID, $integer);
	}


	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByProductoIdAsc() {
		return $this->orderBy(ProductosConsumidos::PRODUCTO_ID, self::ASC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByProductoIdDesc() {
		return $this->orderBy(ProductosConsumidos::PRODUCTO_ID, self::DESC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function groupByProductoId() {
		return $this->groupBy(ProductosConsumidos::PRODUCTO_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionId($integer) {
		return $this->addAnd(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdNot($integer) {
		return $this->andNot(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdLike($integer) {
		return $this->andLike(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdNotLike($integer) {
		return $this->andNotLike(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdGreater($integer) {
		return $this->andGreater(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdLess($integer) {
		return $this->andLess(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdLessEqual($integer) {
		return $this->andLessEqual(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdNull() {
		return $this->andNull(ProductosConsumidos::HABITACION_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdNotNull() {
		return $this->andNotNull(ProductosConsumidos::HABITACION_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductosConsumidos::HABITACION_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdEndsWith($integer) {
		return $this->andEndsWith(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andHabitacionIdContains($integer) {
		return $this->andContains(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionId($integer) {
		return $this->or(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdNot($integer) {
		return $this->orNot(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdLike($integer) {
		return $this->orLike(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdNotLike($integer) {
		return $this->orNotLike(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdGreater($integer) {
		return $this->orGreater(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdLess($integer) {
		return $this->orLess(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdLessEqual($integer) {
		return $this->orLessEqual(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdNull() {
		return $this->orNull(ProductosConsumidos::HABITACION_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdNotNull() {
		return $this->orNotNull(ProductosConsumidos::HABITACION_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductosConsumidos::HABITACION_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdEndsWith($integer) {
		return $this->orEndsWith(ProductosConsumidos::HABITACION_ID, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orHabitacionIdContains($integer) {
		return $this->orContains(ProductosConsumidos::HABITACION_ID, $integer);
	}


	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByHabitacionIdAsc() {
		return $this->orderBy(ProductosConsumidos::HABITACION_ID, self::ASC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByHabitacionIdDesc() {
		return $this->orderBy(ProductosConsumidos::HABITACION_ID, self::DESC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function groupByHabitacionId() {
		return $this->groupBy(ProductosConsumidos::HABITACION_ID);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidad($integer) {
		return $this->addAnd(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadNot($integer) {
		return $this->andNot(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadLike($integer) {
		return $this->andLike(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadNotLike($integer) {
		return $this->andNotLike(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadGreater($integer) {
		return $this->andGreater(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadLess($integer) {
		return $this->andLess(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadLessEqual($integer) {
		return $this->andLessEqual(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadNull() {
		return $this->andNull(ProductosConsumidos::CANTIDAD);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadNotNull() {
		return $this->andNotNull(ProductosConsumidos::CANTIDAD);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadBetween($integer, $from, $to) {
		return $this->andBetween(ProductosConsumidos::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadBeginsWith($integer) {
		return $this->andBeginsWith(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadEndsWith($integer) {
		return $this->andEndsWith(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andCantidadContains($integer) {
		return $this->andContains(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidad($integer) {
		return $this->or(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadNot($integer) {
		return $this->orNot(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadLike($integer) {
		return $this->orLike(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadNotLike($integer) {
		return $this->orNotLike(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadGreater($integer) {
		return $this->orGreater(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadLess($integer) {
		return $this->orLess(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadLessEqual($integer) {
		return $this->orLessEqual(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadNull() {
		return $this->orNull(ProductosConsumidos::CANTIDAD);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadNotNull() {
		return $this->orNotNull(ProductosConsumidos::CANTIDAD);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadBetween($integer, $from, $to) {
		return $this->orBetween(ProductosConsumidos::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadBeginsWith($integer) {
		return $this->orBeginsWith(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadEndsWith($integer) {
		return $this->orEndsWith(ProductosConsumidos::CANTIDAD, $integer);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orCantidadContains($integer) {
		return $this->orContains(ProductosConsumidos::CANTIDAD, $integer);
	}


	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByCantidadAsc() {
		return $this->orderBy(ProductosConsumidos::CANTIDAD, self::ASC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByCantidadDesc() {
		return $this->orderBy(ProductosConsumidos::CANTIDAD, self::DESC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function groupByCantidad() {
		return $this->groupBy(ProductosConsumidos::CANTIDAD);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecio($float) {
		return $this->addAnd(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioNot($float) {
		return $this->andNot(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioLike($float) {
		return $this->andLike(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioNotLike($float) {
		return $this->andNotLike(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioGreater($float) {
		return $this->andGreater(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioGreaterEqual($float) {
		return $this->andGreaterEqual(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioLess($float) {
		return $this->andLess(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioLessEqual($float) {
		return $this->andLessEqual(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioNull() {
		return $this->andNull(ProductosConsumidos::PRECIO);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioNotNull() {
		return $this->andNotNull(ProductosConsumidos::PRECIO);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioBetween($float, $from, $to) {
		return $this->andBetween(ProductosConsumidos::PRECIO, $float, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioBeginsWith($float) {
		return $this->andBeginsWith(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioEndsWith($float) {
		return $this->andEndsWith(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function andPrecioContains($float) {
		return $this->andContains(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecio($float) {
		return $this->or(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioNot($float) {
		return $this->orNot(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioLike($float) {
		return $this->orLike(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioNotLike($float) {
		return $this->orNotLike(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioGreater($float) {
		return $this->orGreater(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioGreaterEqual($float) {
		return $this->orGreaterEqual(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioLess($float) {
		return $this->orLess(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioLessEqual($float) {
		return $this->orLessEqual(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioNull() {
		return $this->orNull(ProductosConsumidos::PRECIO);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioNotNull() {
		return $this->orNotNull(ProductosConsumidos::PRECIO);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioBetween($float, $from, $to) {
		return $this->orBetween(ProductosConsumidos::PRECIO, $float, $from, $to);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioBeginsWith($float) {
		return $this->orBeginsWith(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioEndsWith($float) {
		return $this->orEndsWith(ProductosConsumidos::PRECIO, $float);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orPrecioContains($float) {
		return $this->orContains(ProductosConsumidos::PRECIO, $float);
	}


	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByPrecioAsc() {
		return $this->orderBy(ProductosConsumidos::PRECIO, self::ASC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function orderByPrecioDesc() {
		return $this->orderBy(ProductosConsumidos::PRECIO, self::DESC);
	}

	/**
	 * @return ProductosConsumidosQuery
	 */
	function groupByPrecio() {
		return $this->groupBy(ProductosConsumidos::PRECIO);
	}


}