<?php

use Dabl\Query\Query;

abstract class baseHotelesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Hoteles::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return HotelesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new HotelesQuery($table_name, $alias);
	}

	/**
	 * @return Hoteles[]
	 */
	function select() {
		return Hoteles::doSelect($this);
	}

	/**
	 * @return Hoteles
	 */
	function selectOne() {
		return Hoteles::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Hoteles::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Hoteles::doCount($this);
	}

	/**
	 * @return HotelesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Hoteles::isTemporalType($type)) {
			$value = Hoteles::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Hoteles::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return HotelesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Hoteles::isTemporalType($type)) {
			$value = Hoteles::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Hoteles::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return HotelesQuery
	 */
	function andId($integer) {
		return $this->addAnd(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdNull() {
		return $this->andNull(Hoteles::ID);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Hoteles::ID);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Hoteles::ID, $integer, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orId($integer) {
		return $this->or(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdNull() {
		return $this->orNull(Hoteles::ID);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Hoteles::ID);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Hoteles::ID, $integer, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Hoteles::ID, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Hoteles::ID, $integer);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Hoteles::ID, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Hoteles::ID, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupById() {
		return $this->groupBy(Hoteles::ID);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreNull() {
		return $this->andNull(Hoteles::NOMBRE);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(Hoteles::NOMBRE);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(Hoteles::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombre($varchar) {
		return $this->or(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreNull() {
		return $this->orNull(Hoteles::NOMBRE);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(Hoteles::NOMBRE);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(Hoteles::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(Hoteles::NOMBRE, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(Hoteles::NOMBRE, $varchar);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(Hoteles::NOMBRE, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(Hoteles::NOMBRE, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByNombre() {
		return $this->groupBy(Hoteles::NOMBRE);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Hoteles::DESCRIPCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Hoteles::DESCRIPCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Hoteles::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Hoteles::DESCRIPCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Hoteles::DESCRIPCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Hoteles::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Hoteles::DESCRIPCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Hoteles::DESCRIPCION, $varchar);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Hoteles::DESCRIPCION, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Hoteles::DESCRIPCION, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Hoteles::DESCRIPCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccion($varchar) {
		return $this->addAnd(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionNot($varchar) {
		return $this->andNot(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionLike($varchar) {
		return $this->andLike(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionNotLike($varchar) {
		return $this->andNotLike(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionGreater($varchar) {
		return $this->andGreater(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionLess($varchar) {
		return $this->andLess(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionLessEqual($varchar) {
		return $this->andLessEqual(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionNull() {
		return $this->andNull(Hoteles::DIRECCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionNotNull() {
		return $this->andNotNull(Hoteles::DIRECCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionBetween($varchar, $from, $to) {
		return $this->andBetween(Hoteles::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionBeginsWith($varchar) {
		return $this->andBeginsWith(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionEndsWith($varchar) {
		return $this->andEndsWith(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDireccionContains($varchar) {
		return $this->andContains(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccion($varchar) {
		return $this->or(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionNot($varchar) {
		return $this->orNot(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionLike($varchar) {
		return $this->orLike(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionNotLike($varchar) {
		return $this->orNotLike(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionGreater($varchar) {
		return $this->orGreater(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionLess($varchar) {
		return $this->orLess(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionLessEqual($varchar) {
		return $this->orLessEqual(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionNull() {
		return $this->orNull(Hoteles::DIRECCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionNotNull() {
		return $this->orNotNull(Hoteles::DIRECCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionBetween($varchar, $from, $to) {
		return $this->orBetween(Hoteles::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionBeginsWith($varchar) {
		return $this->orBeginsWith(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionEndsWith($varchar) {
		return $this->orEndsWith(Hoteles::DIRECCION, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDireccionContains($varchar) {
		return $this->orContains(Hoteles::DIRECCION, $varchar);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByDireccionAsc() {
		return $this->orderBy(Hoteles::DIRECCION, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByDireccionDesc() {
		return $this->orderBy(Hoteles::DIRECCION, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByDireccion() {
		return $this->groupBy(Hoteles::DIRECCION);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRuc($integer) {
		return $this->addAnd(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucNot($integer) {
		return $this->andNot(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucLike($integer) {
		return $this->andLike(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucNotLike($integer) {
		return $this->andNotLike(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucGreater($integer) {
		return $this->andGreater(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucGreaterEqual($integer) {
		return $this->andGreaterEqual(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucLess($integer) {
		return $this->andLess(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucLessEqual($integer) {
		return $this->andLessEqual(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucNull() {
		return $this->andNull(Hoteles::RUC);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucNotNull() {
		return $this->andNotNull(Hoteles::RUC);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucBetween($integer, $from, $to) {
		return $this->andBetween(Hoteles::RUC, $integer, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucBeginsWith($integer) {
		return $this->andBeginsWith(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucEndsWith($integer) {
		return $this->andEndsWith(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andRucContains($integer) {
		return $this->andContains(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRuc($integer) {
		return $this->or(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucNot($integer) {
		return $this->orNot(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucLike($integer) {
		return $this->orLike(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucNotLike($integer) {
		return $this->orNotLike(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucGreater($integer) {
		return $this->orGreater(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucGreaterEqual($integer) {
		return $this->orGreaterEqual(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucLess($integer) {
		return $this->orLess(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucLessEqual($integer) {
		return $this->orLessEqual(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucNull() {
		return $this->orNull(Hoteles::RUC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucNotNull() {
		return $this->orNotNull(Hoteles::RUC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucBetween($integer, $from, $to) {
		return $this->orBetween(Hoteles::RUC, $integer, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucBeginsWith($integer) {
		return $this->orBeginsWith(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucEndsWith($integer) {
		return $this->orEndsWith(Hoteles::RUC, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orRucContains($integer) {
		return $this->orContains(Hoteles::RUC, $integer);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByRucAsc() {
		return $this->orderBy(Hoteles::RUC, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByRucDesc() {
		return $this->orderBy(Hoteles::RUC, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByRuc() {
		return $this->groupBy(Hoteles::RUC);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefono($integer) {
		return $this->addAnd(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoNot($integer) {
		return $this->andNot(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoLike($integer) {
		return $this->andLike(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoNotLike($integer) {
		return $this->andNotLike(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoGreater($integer) {
		return $this->andGreater(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoGreaterEqual($integer) {
		return $this->andGreaterEqual(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoLess($integer) {
		return $this->andLess(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoLessEqual($integer) {
		return $this->andLessEqual(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoNull() {
		return $this->andNull(Hoteles::TELEFONO);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoNotNull() {
		return $this->andNotNull(Hoteles::TELEFONO);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoBetween($integer, $from, $to) {
		return $this->andBetween(Hoteles::TELEFONO, $integer, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoBeginsWith($integer) {
		return $this->andBeginsWith(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoEndsWith($integer) {
		return $this->andEndsWith(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function andTelefonoContains($integer) {
		return $this->andContains(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefono($integer) {
		return $this->or(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoNot($integer) {
		return $this->orNot(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoLike($integer) {
		return $this->orLike(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoNotLike($integer) {
		return $this->orNotLike(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoGreater($integer) {
		return $this->orGreater(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoGreaterEqual($integer) {
		return $this->orGreaterEqual(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoLess($integer) {
		return $this->orLess(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoLessEqual($integer) {
		return $this->orLessEqual(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoNull() {
		return $this->orNull(Hoteles::TELEFONO);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoNotNull() {
		return $this->orNotNull(Hoteles::TELEFONO);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoBetween($integer, $from, $to) {
		return $this->orBetween(Hoteles::TELEFONO, $integer, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoBeginsWith($integer) {
		return $this->orBeginsWith(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoEndsWith($integer) {
		return $this->orEndsWith(Hoteles::TELEFONO, $integer);
	}

	/**
	 * @return HotelesQuery
	 */
	function orTelefonoContains($integer) {
		return $this->orContains(Hoteles::TELEFONO, $integer);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByTelefonoAsc() {
		return $this->orderBy(Hoteles::TELEFONO, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByTelefonoDesc() {
		return $this->orderBy(Hoteles::TELEFONO, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByTelefono() {
		return $this->groupBy(Hoteles::TELEFONO);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColores($varchar) {
		return $this->addAnd(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresNot($varchar) {
		return $this->andNot(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresLike($varchar) {
		return $this->andLike(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresNotLike($varchar) {
		return $this->andNotLike(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresGreater($varchar) {
		return $this->andGreater(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresGreaterEqual($varchar) {
		return $this->andGreaterEqual(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresLess($varchar) {
		return $this->andLess(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresLessEqual($varchar) {
		return $this->andLessEqual(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresNull() {
		return $this->andNull(Hoteles::COLORES);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresNotNull() {
		return $this->andNotNull(Hoteles::COLORES);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresBetween($varchar, $from, $to) {
		return $this->andBetween(Hoteles::COLORES, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresBeginsWith($varchar) {
		return $this->andBeginsWith(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresEndsWith($varchar) {
		return $this->andEndsWith(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function andColoresContains($varchar) {
		return $this->andContains(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColores($varchar) {
		return $this->or(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresNot($varchar) {
		return $this->orNot(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresLike($varchar) {
		return $this->orLike(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresNotLike($varchar) {
		return $this->orNotLike(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresGreater($varchar) {
		return $this->orGreater(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresGreaterEqual($varchar) {
		return $this->orGreaterEqual(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresLess($varchar) {
		return $this->orLess(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresLessEqual($varchar) {
		return $this->orLessEqual(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresNull() {
		return $this->orNull(Hoteles::COLORES);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresNotNull() {
		return $this->orNotNull(Hoteles::COLORES);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresBetween($varchar, $from, $to) {
		return $this->orBetween(Hoteles::COLORES, $varchar, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresBeginsWith($varchar) {
		return $this->orBeginsWith(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresEndsWith($varchar) {
		return $this->orEndsWith(Hoteles::COLORES, $varchar);
	}

	/**
	 * @return HotelesQuery
	 */
	function orColoresContains($varchar) {
		return $this->orContains(Hoteles::COLORES, $varchar);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByColoresAsc() {
		return $this->orderBy(Hoteles::COLORES, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByColoresDesc() {
		return $this->orderBy(Hoteles::COLORES, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByColores() {
		return $this->groupBy(Hoteles::COLORES);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Hoteles::CREATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Hoteles::CREATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Hoteles::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Hoteles::CREATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Hoteles::CREATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Hoteles::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Hoteles::CREATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Hoteles::CREATED_AT, $timestamp);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Hoteles::CREATED_AT, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Hoteles::CREATED_AT, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Hoteles::CREATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Hoteles::UPDATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Hoteles::UPDATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Hoteles::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Hoteles::UPDATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Hoteles::UPDATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Hoteles::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Hoteles::UPDATED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Hoteles::UPDATED_AT, $timestamp);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Hoteles::UPDATED_AT, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Hoteles::UPDATED_AT, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Hoteles::UPDATED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Hoteles::DELETED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Hoteles::DELETED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Hoteles::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Hoteles::DELETED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Hoteles::DELETED_AT);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Hoteles::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Hoteles::DELETED_AT, $timestamp);
	}

	/**
	 * @return HotelesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Hoteles::DELETED_AT, $timestamp);
	}


	/**
	 * @return HotelesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Hoteles::DELETED_AT, self::ASC);
	}

	/**
	 * @return HotelesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Hoteles::DELETED_AT, self::DESC);
	}

	/**
	 * @return HotelesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Hoteles::DELETED_AT);
	}


}