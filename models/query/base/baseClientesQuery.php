<?php

use Dabl\Query\Query;

abstract class baseClientesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Clientes::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ClientesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ClientesQuery($table_name, $alias);
	}

	/**
	 * @return Clientes[]
	 */
	function select() {
		return Clientes::doSelect($this);
	}

	/**
	 * @return Clientes
	 */
	function selectOne() {
		return Clientes::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Clientes::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Clientes::doCount($this);
	}

	/**
	 * @return ClientesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Clientes::isTemporalType($type)) {
			$value = Clientes::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Clientes::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ClientesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Clientes::isTemporalType($type)) {
			$value = Clientes::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Clientes::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ClientesQuery
	 */
	function andId($integer) {
		return $this->addAnd(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdNull() {
		return $this->andNull(Clientes::ID);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Clientes::ID);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Clientes::ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orId($integer) {
		return $this->or(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdNull() {
		return $this->orNull(Clientes::ID);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Clientes::ID);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Clientes::ID, $integer, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Clientes::ID, $integer);
	}

	/**
	 * @return ClientesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Clientes::ID, $integer);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Clientes::ID, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Clientes::ID, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupById() {
		return $this->groupBy(Clientes::ID);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRuc($varchar) {
		return $this->addAnd(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucNot($varchar) {
		return $this->andNot(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucLike($varchar) {
		return $this->andLike(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucNotLike($varchar) {
		return $this->andNotLike(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucGreater($varchar) {
		return $this->andGreater(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucLess($varchar) {
		return $this->andLess(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucLessEqual($varchar) {
		return $this->andLessEqual(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucNull() {
		return $this->andNull(Clientes::RUC);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucNotNull() {
		return $this->andNotNull(Clientes::RUC);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::RUC, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucEndsWith($varchar) {
		return $this->andEndsWith(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andRucContains($varchar) {
		return $this->andContains(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRuc($varchar) {
		return $this->or(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucNot($varchar) {
		return $this->orNot(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucLike($varchar) {
		return $this->orLike(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucNotLike($varchar) {
		return $this->orNotLike(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucGreater($varchar) {
		return $this->orGreater(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucLess($varchar) {
		return $this->orLess(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucLessEqual($varchar) {
		return $this->orLessEqual(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucNull() {
		return $this->orNull(Clientes::RUC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucNotNull() {
		return $this->orNotNull(Clientes::RUC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::RUC, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucEndsWith($varchar) {
		return $this->orEndsWith(Clientes::RUC, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orRucContains($varchar) {
		return $this->orContains(Clientes::RUC, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByRucAsc() {
		return $this->orderBy(Clientes::RUC, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByRucDesc() {
		return $this->orderBy(Clientes::RUC, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByRuc() {
		return $this->groupBy(Clientes::RUC);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombres($varchar) {
		return $this->addAnd(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresNot($varchar) {
		return $this->andNot(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresLike($varchar) {
		return $this->andLike(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresNotLike($varchar) {
		return $this->andNotLike(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresGreater($varchar) {
		return $this->andGreater(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresLess($varchar) {
		return $this->andLess(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresLessEqual($varchar) {
		return $this->andLessEqual(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresNull() {
		return $this->andNull(Clientes::NOMBRES);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresNotNull() {
		return $this->andNotNull(Clientes::NOMBRES);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresEndsWith($varchar) {
		return $this->andEndsWith(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andNombresContains($varchar) {
		return $this->andContains(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombres($varchar) {
		return $this->or(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresNot($varchar) {
		return $this->orNot(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresLike($varchar) {
		return $this->orLike(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresNotLike($varchar) {
		return $this->orNotLike(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresGreater($varchar) {
		return $this->orGreater(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresLess($varchar) {
		return $this->orLess(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresLessEqual($varchar) {
		return $this->orLessEqual(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresNull() {
		return $this->orNull(Clientes::NOMBRES);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresNotNull() {
		return $this->orNotNull(Clientes::NOMBRES);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresEndsWith($varchar) {
		return $this->orEndsWith(Clientes::NOMBRES, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orNombresContains($varchar) {
		return $this->orContains(Clientes::NOMBRES, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByNombresAsc() {
		return $this->orderBy(Clientes::NOMBRES, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByNombresDesc() {
		return $this->orderBy(Clientes::NOMBRES, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByNombres() {
		return $this->groupBy(Clientes::NOMBRES);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidos($varchar) {
		return $this->addAnd(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosNot($varchar) {
		return $this->andNot(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosLike($varchar) {
		return $this->andLike(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosNotLike($varchar) {
		return $this->andNotLike(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosGreater($varchar) {
		return $this->andGreater(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosLess($varchar) {
		return $this->andLess(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosLessEqual($varchar) {
		return $this->andLessEqual(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosNull() {
		return $this->andNull(Clientes::APELLIDOS);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosNotNull() {
		return $this->andNotNull(Clientes::APELLIDOS);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosEndsWith($varchar) {
		return $this->andEndsWith(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andApellidosContains($varchar) {
		return $this->andContains(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidos($varchar) {
		return $this->or(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosNot($varchar) {
		return $this->orNot(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosLike($varchar) {
		return $this->orLike(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosNotLike($varchar) {
		return $this->orNotLike(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosGreater($varchar) {
		return $this->orGreater(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosLess($varchar) {
		return $this->orLess(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosLessEqual($varchar) {
		return $this->orLessEqual(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosNull() {
		return $this->orNull(Clientes::APELLIDOS);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosNotNull() {
		return $this->orNotNull(Clientes::APELLIDOS);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::APELLIDOS, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosEndsWith($varchar) {
		return $this->orEndsWith(Clientes::APELLIDOS, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orApellidosContains($varchar) {
		return $this->orContains(Clientes::APELLIDOS, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByApellidosAsc() {
		return $this->orderBy(Clientes::APELLIDOS, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByApellidosDesc() {
		return $this->orderBy(Clientes::APELLIDOS, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByApellidos() {
		return $this->groupBy(Clientes::APELLIDOS);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDni($varchar) {
		return $this->addAnd(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniNot($varchar) {
		return $this->andNot(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniLike($varchar) {
		return $this->andLike(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniNotLike($varchar) {
		return $this->andNotLike(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniGreater($varchar) {
		return $this->andGreater(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniLess($varchar) {
		return $this->andLess(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniLessEqual($varchar) {
		return $this->andLessEqual(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniNull() {
		return $this->andNull(Clientes::DNI);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniNotNull() {
		return $this->andNotNull(Clientes::DNI);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::DNI, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniEndsWith($varchar) {
		return $this->andEndsWith(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDniContains($varchar) {
		return $this->andContains(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDni($varchar) {
		return $this->or(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniNot($varchar) {
		return $this->orNot(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniLike($varchar) {
		return $this->orLike(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniNotLike($varchar) {
		return $this->orNotLike(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniGreater($varchar) {
		return $this->orGreater(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniLess($varchar) {
		return $this->orLess(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniLessEqual($varchar) {
		return $this->orLessEqual(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniNull() {
		return $this->orNull(Clientes::DNI);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniNotNull() {
		return $this->orNotNull(Clientes::DNI);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::DNI, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniEndsWith($varchar) {
		return $this->orEndsWith(Clientes::DNI, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDniContains($varchar) {
		return $this->orContains(Clientes::DNI, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByDniAsc() {
		return $this->orderBy(Clientes::DNI, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByDniDesc() {
		return $this->orderBy(Clientes::DNI, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByDni() {
		return $this->groupBy(Clientes::DNI);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccion($varchar) {
		return $this->addAnd(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionNot($varchar) {
		return $this->andNot(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionLike($varchar) {
		return $this->andLike(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionNotLike($varchar) {
		return $this->andNotLike(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionGreater($varchar) {
		return $this->andGreater(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionLess($varchar) {
		return $this->andLess(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionLessEqual($varchar) {
		return $this->andLessEqual(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionNull() {
		return $this->andNull(Clientes::DIRECCION);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionNotNull() {
		return $this->andNotNull(Clientes::DIRECCION);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionEndsWith($varchar) {
		return $this->andEndsWith(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDireccionContains($varchar) {
		return $this->andContains(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccion($varchar) {
		return $this->or(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionNot($varchar) {
		return $this->orNot(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionLike($varchar) {
		return $this->orLike(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionNotLike($varchar) {
		return $this->orNotLike(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionGreater($varchar) {
		return $this->orGreater(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionLess($varchar) {
		return $this->orLess(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionLessEqual($varchar) {
		return $this->orLessEqual(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionNull() {
		return $this->orNull(Clientes::DIRECCION);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionNotNull() {
		return $this->orNotNull(Clientes::DIRECCION);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionEndsWith($varchar) {
		return $this->orEndsWith(Clientes::DIRECCION, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDireccionContains($varchar) {
		return $this->orContains(Clientes::DIRECCION, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByDireccionAsc() {
		return $this->orderBy(Clientes::DIRECCION, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByDireccionDesc() {
		return $this->orderBy(Clientes::DIRECCION, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByDireccion() {
		return $this->groupBy(Clientes::DIRECCION);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefono($varchar) {
		return $this->addAnd(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoNot($varchar) {
		return $this->andNot(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoLike($varchar) {
		return $this->andLike(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoNotLike($varchar) {
		return $this->andNotLike(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoGreater($varchar) {
		return $this->andGreater(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoLess($varchar) {
		return $this->andLess(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoLessEqual($varchar) {
		return $this->andLessEqual(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoNull() {
		return $this->andNull(Clientes::TELEFONO);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoNotNull() {
		return $this->andNotNull(Clientes::TELEFONO);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::TELEFONO, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoEndsWith($varchar) {
		return $this->andEndsWith(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andTelefonoContains($varchar) {
		return $this->andContains(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefono($varchar) {
		return $this->or(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoNot($varchar) {
		return $this->orNot(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoLike($varchar) {
		return $this->orLike(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoNotLike($varchar) {
		return $this->orNotLike(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoGreater($varchar) {
		return $this->orGreater(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoLess($varchar) {
		return $this->orLess(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoLessEqual($varchar) {
		return $this->orLessEqual(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoNull() {
		return $this->orNull(Clientes::TELEFONO);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoNotNull() {
		return $this->orNotNull(Clientes::TELEFONO);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::TELEFONO, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoEndsWith($varchar) {
		return $this->orEndsWith(Clientes::TELEFONO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orTelefonoContains($varchar) {
		return $this->orContains(Clientes::TELEFONO, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByTelefonoAsc() {
		return $this->orderBy(Clientes::TELEFONO, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByTelefonoDesc() {
		return $this->orderBy(Clientes::TELEFONO, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByTelefono() {
		return $this->groupBy(Clientes::TELEFONO);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedencia($varchar) {
		return $this->addAnd(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaNot($varchar) {
		return $this->andNot(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaLike($varchar) {
		return $this->andLike(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaNotLike($varchar) {
		return $this->andNotLike(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaGreater($varchar) {
		return $this->andGreater(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaLess($varchar) {
		return $this->andLess(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaLessEqual($varchar) {
		return $this->andLessEqual(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaNull() {
		return $this->andNull(Clientes::PROCEDENCIA);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaNotNull() {
		return $this->andNotNull(Clientes::PROCEDENCIA);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::PROCEDENCIA, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaEndsWith($varchar) {
		return $this->andEndsWith(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andProcedenciaContains($varchar) {
		return $this->andContains(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedencia($varchar) {
		return $this->or(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaNot($varchar) {
		return $this->orNot(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaLike($varchar) {
		return $this->orLike(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaNotLike($varchar) {
		return $this->orNotLike(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaGreater($varchar) {
		return $this->orGreater(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaLess($varchar) {
		return $this->orLess(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaLessEqual($varchar) {
		return $this->orLessEqual(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaNull() {
		return $this->orNull(Clientes::PROCEDENCIA);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaNotNull() {
		return $this->orNotNull(Clientes::PROCEDENCIA);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::PROCEDENCIA, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaEndsWith($varchar) {
		return $this->orEndsWith(Clientes::PROCEDENCIA, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orProcedenciaContains($varchar) {
		return $this->orContains(Clientes::PROCEDENCIA, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByProcedenciaAsc() {
		return $this->orderBy(Clientes::PROCEDENCIA, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByProcedenciaDesc() {
		return $this->orderBy(Clientes::PROCEDENCIA, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByProcedencia() {
		return $this->groupBy(Clientes::PROCEDENCIA);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexo($varchar) {
		return $this->addAnd(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoNot($varchar) {
		return $this->andNot(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoLike($varchar) {
		return $this->andLike(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoNotLike($varchar) {
		return $this->andNotLike(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoGreater($varchar) {
		return $this->andGreater(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoLess($varchar) {
		return $this->andLess(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoLessEqual($varchar) {
		return $this->andLessEqual(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoNull() {
		return $this->andNull(Clientes::SEXO);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoNotNull() {
		return $this->andNotNull(Clientes::SEXO);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoBetween($varchar, $from, $to) {
		return $this->andBetween(Clientes::SEXO, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoBeginsWith($varchar) {
		return $this->andBeginsWith(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoEndsWith($varchar) {
		return $this->andEndsWith(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function andSexoContains($varchar) {
		return $this->andContains(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexo($varchar) {
		return $this->or(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoNot($varchar) {
		return $this->orNot(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoLike($varchar) {
		return $this->orLike(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoNotLike($varchar) {
		return $this->orNotLike(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoGreater($varchar) {
		return $this->orGreater(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoLess($varchar) {
		return $this->orLess(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoLessEqual($varchar) {
		return $this->orLessEqual(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoNull() {
		return $this->orNull(Clientes::SEXO);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoNotNull() {
		return $this->orNotNull(Clientes::SEXO);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoBetween($varchar, $from, $to) {
		return $this->orBetween(Clientes::SEXO, $varchar, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoBeginsWith($varchar) {
		return $this->orBeginsWith(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoEndsWith($varchar) {
		return $this->orEndsWith(Clientes::SEXO, $varchar);
	}

	/**
	 * @return ClientesQuery
	 */
	function orSexoContains($varchar) {
		return $this->orContains(Clientes::SEXO, $varchar);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderBySexoAsc() {
		return $this->orderBy(Clientes::SEXO, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderBySexoDesc() {
		return $this->orderBy(Clientes::SEXO, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupBySexo() {
		return $this->groupBy(Clientes::SEXO);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Clientes::CREATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Clientes::CREATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Clientes::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Clientes::CREATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Clientes::CREATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Clientes::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Clientes::CREATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Clientes::CREATED_AT, $timestamp);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Clientes::CREATED_AT, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Clientes::CREATED_AT, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Clientes::CREATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Clientes::UPDATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Clientes::UPDATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Clientes::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Clientes::UPDATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Clientes::UPDATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Clientes::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Clientes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Clientes::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Clientes::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Clientes::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Clientes::UPDATED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Clientes::DELETED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Clientes::DELETED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Clientes::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Clientes::DELETED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Clientes::DELETED_AT);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Clientes::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Clientes::DELETED_AT, $timestamp);
	}

	/**
	 * @return ClientesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Clientes::DELETED_AT, $timestamp);
	}


	/**
	 * @return ClientesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Clientes::DELETED_AT, self::ASC);
	}

	/**
	 * @return ClientesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Clientes::DELETED_AT, self::DESC);
	}

	/**
	 * @return ClientesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Clientes::DELETED_AT);
	}


}