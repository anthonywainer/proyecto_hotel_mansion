<?php

use Dabl\Query\Query;

abstract class baseTiposComprobantesQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = TiposComprobantes::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TiposComprobantesQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TiposComprobantesQuery($table_name, $alias);
	}

	/**
	 * @return TiposComprobantes[]
	 */
	function select() {
		return TiposComprobantes::doSelect($this);
	}

	/**
	 * @return TiposComprobantes
	 */
	function selectOne() {
		return TiposComprobantes::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return TiposComprobantes::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return TiposComprobantes::doCount($this);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposComprobantes::isTemporalType($type)) {
			$value = TiposComprobantes::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposComprobantes::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TiposComprobantes::isTemporalType($type)) {
			$value = TiposComprobantes::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TiposComprobantes::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andId($integer) {
		return $this->addAnd(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdNull() {
		return $this->andNull(TiposComprobantes::ID);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(TiposComprobantes::ID);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(TiposComprobantes::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orId($integer) {
		return $this->or(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdNull() {
		return $this->orNull(TiposComprobantes::ID);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(TiposComprobantes::ID);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(TiposComprobantes::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(TiposComprobantes::ID, $integer);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(TiposComprobantes::ID, $integer);
	}


	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(TiposComprobantes::ID, self::ASC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(TiposComprobantes::ID, self::DESC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function groupById() {
		return $this->groupBy(TiposComprobantes::ID);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(TiposComprobantes::DESCRIPCION);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(TiposComprobantes::DESCRIPCION);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(TiposComprobantes::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(TiposComprobantes::DESCRIPCION);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(TiposComprobantes::DESCRIPCION);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(TiposComprobantes::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(TiposComprobantes::DESCRIPCION, $varchar);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(TiposComprobantes::DESCRIPCION, $varchar);
	}


	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(TiposComprobantes::DESCRIPCION, self::ASC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(TiposComprobantes::DESCRIPCION, self::DESC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(TiposComprobantes::DESCRIPCION);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(TiposComprobantes::CREATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(TiposComprobantes::CREATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposComprobantes::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(TiposComprobantes::CREATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(TiposComprobantes::CREATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposComprobantes::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposComprobantes::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(TiposComprobantes::CREATED_AT, $timestamp);
	}


	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(TiposComprobantes::CREATED_AT, self::ASC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(TiposComprobantes::CREATED_AT, self::DESC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(TiposComprobantes::CREATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(TiposComprobantes::UPDATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(TiposComprobantes::UPDATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposComprobantes::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(TiposComprobantes::UPDATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(TiposComprobantes::UPDATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposComprobantes::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposComprobantes::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(TiposComprobantes::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(TiposComprobantes::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(TiposComprobantes::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(TiposComprobantes::UPDATED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(TiposComprobantes::DELETED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(TiposComprobantes::DELETED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TiposComprobantes::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(TiposComprobantes::DELETED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(TiposComprobantes::DELETED_AT);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TiposComprobantes::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(TiposComprobantes::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(TiposComprobantes::DELETED_AT, $timestamp);
	}


	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(TiposComprobantes::DELETED_AT, self::ASC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(TiposComprobantes::DELETED_AT, self::DESC);
	}

	/**
	 * @return TiposComprobantesQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(TiposComprobantes::DELETED_AT);
	}


}