<?php

use Dabl\Query\Query;

abstract class baseTiposQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Tipos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TiposQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TiposQuery($table_name, $alias);
	}

	/**
	 * @return Tipos[]
	 */
	function select() {
		return Tipos::doSelect($this);
	}

	/**
	 * @return Tipos
	 */
	function selectOne() {
		return Tipos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Tipos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Tipos::doCount($this);
	}

	/**
	 * @return TiposQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Tipos::isTemporalType($type)) {
			$value = Tipos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Tipos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Tipos::isTemporalType($type)) {
			$value = Tipos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Tipos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TiposQuery
	 */
	function andId($integer) {
		return $this->addAnd(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdNull() {
		return $this->andNull(Tipos::ID);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Tipos::ID);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Tipos::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orId($integer) {
		return $this->or(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdNull() {
		return $this->orNull(Tipos::ID);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Tipos::ID);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Tipos::ID, $integer, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Tipos::ID, $integer);
	}

	/**
	 * @return TiposQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Tipos::ID, $integer);
	}


	/**
	 * @return TiposQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Tipos::ID, self::ASC);
	}

	/**
	 * @return TiposQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Tipos::ID, self::DESC);
	}

	/**
	 * @return TiposQuery
	 */
	function groupById() {
		return $this->groupBy(Tipos::ID);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipo($varchar) {
		return $this->addAnd(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoNot($varchar) {
		return $this->andNot(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoLike($varchar) {
		return $this->andLike(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoNotLike($varchar) {
		return $this->andNotLike(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoGreater($varchar) {
		return $this->andGreater(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoLess($varchar) {
		return $this->andLess(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoLessEqual($varchar) {
		return $this->andLessEqual(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoNull() {
		return $this->andNull(Tipos::TIPO);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoNotNull() {
		return $this->andNotNull(Tipos::TIPO);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoBetween($varchar, $from, $to) {
		return $this->andBetween(Tipos::TIPO, $varchar, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoBeginsWith($varchar) {
		return $this->andBeginsWith(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoEndsWith($varchar) {
		return $this->andEndsWith(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function andTipoContains($varchar) {
		return $this->andContains(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipo($varchar) {
		return $this->or(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoNot($varchar) {
		return $this->orNot(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoLike($varchar) {
		return $this->orLike(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoNotLike($varchar) {
		return $this->orNotLike(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoGreater($varchar) {
		return $this->orGreater(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoLess($varchar) {
		return $this->orLess(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoLessEqual($varchar) {
		return $this->orLessEqual(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoNull() {
		return $this->orNull(Tipos::TIPO);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoNotNull() {
		return $this->orNotNull(Tipos::TIPO);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoBetween($varchar, $from, $to) {
		return $this->orBetween(Tipos::TIPO, $varchar, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoBeginsWith($varchar) {
		return $this->orBeginsWith(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoEndsWith($varchar) {
		return $this->orEndsWith(Tipos::TIPO, $varchar);
	}

	/**
	 * @return TiposQuery
	 */
	function orTipoContains($varchar) {
		return $this->orContains(Tipos::TIPO, $varchar);
	}


	/**
	 * @return TiposQuery
	 */
	function orderByTipoAsc() {
		return $this->orderBy(Tipos::TIPO, self::ASC);
	}

	/**
	 * @return TiposQuery
	 */
	function orderByTipoDesc() {
		return $this->orderBy(Tipos::TIPO, self::DESC);
	}

	/**
	 * @return TiposQuery
	 */
	function groupByTipo() {
		return $this->groupBy(Tipos::TIPO);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Tipos::CREATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Tipos::CREATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Tipos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Tipos::CREATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Tipos::CREATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Tipos::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Tipos::CREATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Tipos::CREATED_AT, $timestamp);
	}


	/**
	 * @return TiposQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Tipos::CREATED_AT, self::ASC);
	}

	/**
	 * @return TiposQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Tipos::CREATED_AT, self::DESC);
	}

	/**
	 * @return TiposQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Tipos::CREATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Tipos::UPDATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Tipos::UPDATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Tipos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Tipos::UPDATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Tipos::UPDATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Tipos::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Tipos::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Tipos::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TiposQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Tipos::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TiposQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Tipos::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TiposQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Tipos::UPDATED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Tipos::DELETED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Tipos::DELETED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Tipos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Tipos::DELETED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Tipos::DELETED_AT);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Tipos::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Tipos::DELETED_AT, $timestamp);
	}

	/**
	 * @return TiposQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Tipos::DELETED_AT, $timestamp);
	}


	/**
	 * @return TiposQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Tipos::DELETED_AT, self::ASC);
	}

	/**
	 * @return TiposQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Tipos::DELETED_AT, self::DESC);
	}

	/**
	 * @return TiposQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Tipos::DELETED_AT);
	}


}