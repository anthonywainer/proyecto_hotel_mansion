<?php

use Dabl\Query\Query;

abstract class baseCajasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Cajas::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return CajasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new CajasQuery($table_name, $alias);
	}

	/**
	 * @return Cajas[]
	 */
	function select() {
		return Cajas::doSelect($this);
	}

	/**
	 * @return Cajas
	 */
	function selectOne() {
		return Cajas::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Cajas::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Cajas::doCount($this);
	}

	/**
	 * @return CajasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Cajas::isTemporalType($type)) {
			$value = Cajas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Cajas::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return CajasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Cajas::isTemporalType($type)) {
			$value = Cajas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Cajas::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return CajasQuery
	 */
	function andId($integer) {
		return $this->addAnd(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdNull() {
		return $this->andNull(Cajas::ID);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Cajas::ID);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Cajas::ID, $integer, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orId($integer) {
		return $this->or(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdNull() {
		return $this->orNull(Cajas::ID);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Cajas::ID);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Cajas::ID, $integer, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Cajas::ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Cajas::ID, $integer);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Cajas::ID, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Cajas::ID, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupById() {
		return $this->groupBy(Cajas::ID);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbierto($float) {
		return $this->addAnd(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoNot($float) {
		return $this->andNot(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoLike($float) {
		return $this->andLike(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoNotLike($float) {
		return $this->andNotLike(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoGreater($float) {
		return $this->andGreater(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoGreaterEqual($float) {
		return $this->andGreaterEqual(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoLess($float) {
		return $this->andLess(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoLessEqual($float) {
		return $this->andLessEqual(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoNull() {
		return $this->andNull(Cajas::MONTO_ABIERTO);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoNotNull() {
		return $this->andNotNull(Cajas::MONTO_ABIERTO);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoBetween($float, $from, $to) {
		return $this->andBetween(Cajas::MONTO_ABIERTO, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoBeginsWith($float) {
		return $this->andBeginsWith(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoEndsWith($float) {
		return $this->andEndsWith(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoAbiertoContains($float) {
		return $this->andContains(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbierto($float) {
		return $this->or(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoNot($float) {
		return $this->orNot(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoLike($float) {
		return $this->orLike(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoNotLike($float) {
		return $this->orNotLike(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoGreater($float) {
		return $this->orGreater(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoGreaterEqual($float) {
		return $this->orGreaterEqual(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoLess($float) {
		return $this->orLess(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoLessEqual($float) {
		return $this->orLessEqual(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoNull() {
		return $this->orNull(Cajas::MONTO_ABIERTO);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoNotNull() {
		return $this->orNotNull(Cajas::MONTO_ABIERTO);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoBetween($float, $from, $to) {
		return $this->orBetween(Cajas::MONTO_ABIERTO, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoBeginsWith($float) {
		return $this->orBeginsWith(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoEndsWith($float) {
		return $this->orEndsWith(Cajas::MONTO_ABIERTO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoAbiertoContains($float) {
		return $this->orContains(Cajas::MONTO_ABIERTO, $float);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByMontoAbiertoAsc() {
		return $this->orderBy(Cajas::MONTO_ABIERTO, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByMontoAbiertoDesc() {
		return $this->orderBy(Cajas::MONTO_ABIERTO, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByMontoAbierto() {
		return $this->groupBy(Cajas::MONTO_ABIERTO);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerrado($float) {
		return $this->addAnd(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoNot($float) {
		return $this->andNot(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoLike($float) {
		return $this->andLike(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoNotLike($float) {
		return $this->andNotLike(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoGreater($float) {
		return $this->andGreater(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoGreaterEqual($float) {
		return $this->andGreaterEqual(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoLess($float) {
		return $this->andLess(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoLessEqual($float) {
		return $this->andLessEqual(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoNull() {
		return $this->andNull(Cajas::MONTO_CERRADO);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoNotNull() {
		return $this->andNotNull(Cajas::MONTO_CERRADO);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoBetween($float, $from, $to) {
		return $this->andBetween(Cajas::MONTO_CERRADO, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoBeginsWith($float) {
		return $this->andBeginsWith(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoEndsWith($float) {
		return $this->andEndsWith(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andMontoCerradoContains($float) {
		return $this->andContains(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerrado($float) {
		return $this->or(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoNot($float) {
		return $this->orNot(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoLike($float) {
		return $this->orLike(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoNotLike($float) {
		return $this->orNotLike(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoGreater($float) {
		return $this->orGreater(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoGreaterEqual($float) {
		return $this->orGreaterEqual(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoLess($float) {
		return $this->orLess(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoLessEqual($float) {
		return $this->orLessEqual(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoNull() {
		return $this->orNull(Cajas::MONTO_CERRADO);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoNotNull() {
		return $this->orNotNull(Cajas::MONTO_CERRADO);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoBetween($float, $from, $to) {
		return $this->orBetween(Cajas::MONTO_CERRADO, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoBeginsWith($float) {
		return $this->orBeginsWith(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoEndsWith($float) {
		return $this->orEndsWith(Cajas::MONTO_CERRADO, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orMontoCerradoContains($float) {
		return $this->orContains(Cajas::MONTO_CERRADO, $float);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByMontoCerradoAsc() {
		return $this->orderBy(Cajas::MONTO_CERRADO, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByMontoCerradoDesc() {
		return $this->orderBy(Cajas::MONTO_CERRADO, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByMontoCerrado() {
		return $this->groupBy(Cajas::MONTO_CERRADO);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerrada($float) {
		return $this->addAnd(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaNot($float) {
		return $this->andNot(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaLike($float) {
		return $this->andLike(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaNotLike($float) {
		return $this->andNotLike(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaGreater($float) {
		return $this->andGreater(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaGreaterEqual($float) {
		return $this->andGreaterEqual(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaLess($float) {
		return $this->andLess(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaLessEqual($float) {
		return $this->andLessEqual(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaNull() {
		return $this->andNull(Cajas::FECHA_CERRADA);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaNotNull() {
		return $this->andNotNull(Cajas::FECHA_CERRADA);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaBetween($float, $from, $to) {
		return $this->andBetween(Cajas::FECHA_CERRADA, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaBeginsWith($float) {
		return $this->andBeginsWith(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaEndsWith($float) {
		return $this->andEndsWith(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaCerradaContains($float) {
		return $this->andContains(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerrada($float) {
		return $this->or(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaNot($float) {
		return $this->orNot(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaLike($float) {
		return $this->orLike(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaNotLike($float) {
		return $this->orNotLike(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaGreater($float) {
		return $this->orGreater(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaGreaterEqual($float) {
		return $this->orGreaterEqual(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaLess($float) {
		return $this->orLess(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaLessEqual($float) {
		return $this->orLessEqual(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaNull() {
		return $this->orNull(Cajas::FECHA_CERRADA);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaNotNull() {
		return $this->orNotNull(Cajas::FECHA_CERRADA);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaBetween($float, $from, $to) {
		return $this->orBetween(Cajas::FECHA_CERRADA, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaBeginsWith($float) {
		return $this->orBeginsWith(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaEndsWith($float) {
		return $this->orEndsWith(Cajas::FECHA_CERRADA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaCerradaContains($float) {
		return $this->orContains(Cajas::FECHA_CERRADA, $float);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByFechaCerradaAsc() {
		return $this->orderBy(Cajas::FECHA_CERRADA, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByFechaCerradaDesc() {
		return $this->orderBy(Cajas::FECHA_CERRADA, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByFechaCerrada() {
		return $this->groupBy(Cajas::FECHA_CERRADA);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbierta($float) {
		return $this->addAnd(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaNot($float) {
		return $this->andNot(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaLike($float) {
		return $this->andLike(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaNotLike($float) {
		return $this->andNotLike(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaGreater($float) {
		return $this->andGreater(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaGreaterEqual($float) {
		return $this->andGreaterEqual(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaLess($float) {
		return $this->andLess(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaLessEqual($float) {
		return $this->andLessEqual(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaNull() {
		return $this->andNull(Cajas::FECHA_ABIERTA);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaNotNull() {
		return $this->andNotNull(Cajas::FECHA_ABIERTA);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaBetween($float, $from, $to) {
		return $this->andBetween(Cajas::FECHA_ABIERTA, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaBeginsWith($float) {
		return $this->andBeginsWith(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaEndsWith($float) {
		return $this->andEndsWith(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function andFechaAbiertaContains($float) {
		return $this->andContains(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbierta($float) {
		return $this->or(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaNot($float) {
		return $this->orNot(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaLike($float) {
		return $this->orLike(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaNotLike($float) {
		return $this->orNotLike(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaGreater($float) {
		return $this->orGreater(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaGreaterEqual($float) {
		return $this->orGreaterEqual(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaLess($float) {
		return $this->orLess(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaLessEqual($float) {
		return $this->orLessEqual(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaNull() {
		return $this->orNull(Cajas::FECHA_ABIERTA);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaNotNull() {
		return $this->orNotNull(Cajas::FECHA_ABIERTA);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaBetween($float, $from, $to) {
		return $this->orBetween(Cajas::FECHA_ABIERTA, $float, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaBeginsWith($float) {
		return $this->orBeginsWith(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaEndsWith($float) {
		return $this->orEndsWith(Cajas::FECHA_ABIERTA, $float);
	}

	/**
	 * @return CajasQuery
	 */
	function orFechaAbiertaContains($float) {
		return $this->orContains(Cajas::FECHA_ABIERTA, $float);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByFechaAbiertaAsc() {
		return $this->orderBy(Cajas::FECHA_ABIERTA, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByFechaAbiertaDesc() {
		return $this->orderBy(Cajas::FECHA_ABIERTA, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByFechaAbierta() {
		return $this->groupBy(Cajas::FECHA_ABIERTA);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioId($integer) {
		return $this->addAnd(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdNot($integer) {
		return $this->andNot(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdLike($integer) {
		return $this->andLike(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdNotLike($integer) {
		return $this->andNotLike(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdGreater($integer) {
		return $this->andGreater(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdLess($integer) {
		return $this->andLess(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdLessEqual($integer) {
		return $this->andLessEqual(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdNull() {
		return $this->andNull(Cajas::USUARIO_ID);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdNotNull() {
		return $this->andNotNull(Cajas::USUARIO_ID);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdBetween($integer, $from, $to) {
		return $this->andBetween(Cajas::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdBeginsWith($integer) {
		return $this->andBeginsWith(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdEndsWith($integer) {
		return $this->andEndsWith(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function andUsuarioIdContains($integer) {
		return $this->andContains(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioId($integer) {
		return $this->or(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdNot($integer) {
		return $this->orNot(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdLike($integer) {
		return $this->orLike(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdNotLike($integer) {
		return $this->orNotLike(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdGreater($integer) {
		return $this->orGreater(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdLess($integer) {
		return $this->orLess(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdLessEqual($integer) {
		return $this->orLessEqual(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdNull() {
		return $this->orNull(Cajas::USUARIO_ID);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdNotNull() {
		return $this->orNotNull(Cajas::USUARIO_ID);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdBetween($integer, $from, $to) {
		return $this->orBetween(Cajas::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdBeginsWith($integer) {
		return $this->orBeginsWith(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdEndsWith($integer) {
		return $this->orEndsWith(Cajas::USUARIO_ID, $integer);
	}

	/**
	 * @return CajasQuery
	 */
	function orUsuarioIdContains($integer) {
		return $this->orContains(Cajas::USUARIO_ID, $integer);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByUsuarioIdAsc() {
		return $this->orderBy(Cajas::USUARIO_ID, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByUsuarioIdDesc() {
		return $this->orderBy(Cajas::USUARIO_ID, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByUsuarioId() {
		return $this->groupBy(Cajas::USUARIO_ID);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Cajas::CREATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Cajas::CREATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Cajas::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Cajas::CREATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Cajas::CREATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Cajas::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Cajas::CREATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Cajas::CREATED_AT, $timestamp);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Cajas::CREATED_AT, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Cajas::CREATED_AT, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Cajas::CREATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Cajas::UPDATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Cajas::UPDATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Cajas::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Cajas::UPDATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Cajas::UPDATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Cajas::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Cajas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Cajas::UPDATED_AT, $timestamp);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Cajas::UPDATED_AT, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Cajas::UPDATED_AT, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Cajas::UPDATED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Cajas::DELETED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Cajas::DELETED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Cajas::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Cajas::DELETED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Cajas::DELETED_AT);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Cajas::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Cajas::DELETED_AT, $timestamp);
	}

	/**
	 * @return CajasQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Cajas::DELETED_AT, $timestamp);
	}


	/**
	 * @return CajasQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Cajas::DELETED_AT, self::ASC);
	}

	/**
	 * @return CajasQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Cajas::DELETED_AT, self::DESC);
	}

	/**
	 * @return CajasQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Cajas::DELETED_AT);
	}


}