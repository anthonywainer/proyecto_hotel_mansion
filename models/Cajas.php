<?php

class Cajas extends baseCajas {

    function getIngresos($c) {
        $q = "
        SELECT
        sum(CASE WHEN ventas.monto >= ventas.cuota 
          THEN ventas.cuota
          ELSE ventas.monto END )
          AS monto,
            count(ventas.cuota) AS cant
        FROM
            movimientos_de_dinero
        INNER JOIN ventas ON ventas.movimiento_id = movimientos_de_dinero.id
        WHERE
            movimientos_de_dinero.caja_id = " . $c[0]->id . "
        AND movimientos_de_dinero.concepto_pago_id = 1
        AND movimientos_de_dinero.forma_pago_id = 1
        AND ventas.estado=0                   
                                ";
        return Cajas::getConnection()->query($q)->fetch();
    }

    function getIngresosN($c) {
        $q = "
SELECT
	sum(concepto_pago.monto) AS monto,
	count(concepto_pago.monto) AS cant
FROM
	movimientos_de_dinero
INNER JOIN concepto_pago ON concepto_pago.movimiento_id = movimientos_de_dinero.id
WHERE
	movimientos_de_dinero.caja_id = " . $c[0]->id . "
AND concepto_pago_id =  ";
        return Cajas::getConnection()->query($q.'1')->fetch();
    }

    function getIngresosH($c) {
        $q = "
SELECT
	sum(ventas_hotel.efectivo) AS monto,
	count(ventas_hotel.efectivo) AS cant
FROM
	movimientos_de_dinero
INNER JOIN ventas_hotel ON ventas_hotel.movimiento_id = movimientos_de_dinero.id
WHERE
	movimientos_de_dinero.caja_id = " . $c[0]->id . "
AND concepto_pago_id =  ";
        return Cajas::getConnection()->query($q.'1')->fetch();
    }

    function getEgresos($c) {
        $q = "
SELECT
	sum(concepto_pago.monto) AS monto,
	count(concepto_pago.monto) AS cant
FROM
	movimientos_de_dinero
INNER JOIN concepto_pago ON concepto_pago.movimiento_id = movimientos_de_dinero.id
WHERE
	movimientos_de_dinero.caja_id = " . $c[0]->id . "
AND concepto_pago_id =  ";
        return Cajas::getConnection()->query($q.'2')->fetch();
    }
}