<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseLicencia extends ApplicationModel {

	const LICENCIA_ID = 'licencia.licencia_id';
	const FECHA_FIN = 'licencia.fecha_fin';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'licencia';

	/**
	 * Cache of objects retrieved from the database
	 * @var Licencia[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'licencia_id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'licencia_id' => Model::COLUMN_TYPE_INTEGER,
		'fecha_fin' => Model::COLUMN_TYPE_DATE,
	);

	/**
	 * `licencia_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $licencia_id;

	/**
	 * `fecha_fin` DATE NOT NULL
	 * @var string
	 */
	protected $fecha_fin;

	/**
	 * Gets the value of the licencia_id field
	 */
	function getLicenciaId() {
		return $this->licencia_id;
	}

	/**
	 * Sets the value of the licencia_id field
	 * @return Licencia
	 */
	function setLicenciaId($value) {
		return $this->setColumnValue('licencia_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Licencia::getLicenciaId
	 * final because getLicenciaId should be extended instead
	 * to ensure consistent behavior
	 * @see Licencia::getLicenciaId
	 */
	final function getLicencia_id() {
		return $this->getLicenciaId();
	}

	/**
	 * Convenience function for Licencia::setLicenciaId
	 * final because setLicenciaId should be extended instead
	 * to ensure consistent behavior
	 * @see Licencia::setLicenciaId
	 * @return Licencia
	 */
	final function setLicencia_id($value) {
		return $this->setLicenciaId($value);
	}

	/**
	 * Gets the value of the fecha_fin field
	 */
	function getFechaFin($format = null) {
		if (null === $this->fecha_fin || null === $format) {
			return $this->fecha_fin;
		}
		if (0 === strpos($this->fecha_fin, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_fin));
	}

	/**
	 * Sets the value of the fecha_fin field
	 * @return Licencia
	 */
	function setFechaFin($value) {
		return $this->setColumnValue('fecha_fin', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for Licencia::getFechaFin
	 * final because getFechaFin should be extended instead
	 * to ensure consistent behavior
	 * @see Licencia::getFechaFin
	 */
	final function getFecha_fin($format = null) {
		return $this->getFechaFin($format);
	}

	/**
	 * Convenience function for Licencia::setFechaFin
	 * final because setFechaFin should be extended instead
	 * to ensure consistent behavior
	 * @see Licencia::setFechaFin
	 * @return Licencia
	 */
	final function setFecha_fin($value) {
		return $this->setFechaFin($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Licencia
	 */
	static function retrieveByPK($licencia_id) {
		return static::retrieveByPKs($licencia_id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Licencia
	 */
	static function retrieveByPKs($licencia_id) {
		if (null === $licencia_id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($licencia_id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('licencia_id', $licencia_id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a licencia_id
	 * value that matches the one provided
	 * @return Licencia
	 */
	static function retrieveByLicenciaId($value) {
		return Licencia::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a fecha_fin
	 * value that matches the one provided
	 * @return Licencia
	 */
	static function retrieveByFechaFin($value) {
		return static::retrieveByColumn('fecha_fin', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Licencia
	 */
	function castInts() {
		$this->licencia_id = (null === $this->licencia_id) ? null : (int) $this->licencia_id;
		return $this;
	}

	/**
	 * @return Licencia[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getfecha_fin()) {
			$this->_validationErrors[] = 'fecha_fin must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}