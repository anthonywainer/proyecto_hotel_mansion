<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseMovimientosDeDinero extends ApplicationModel {

	const ID = 'movimientos_de_dinero.id';
	const NUMERO_BOLETA = 'movimientos_de_dinero.numero_boleta';
	const MONTO = 'movimientos_de_dinero.monto';
	const FECHA = 'movimientos_de_dinero.fecha';
	const REEMBOLSO = 'movimientos_de_dinero.reembolso';
	const GLOSA = 'movimientos_de_dinero.glosa';
	const FORMA_PAGO_ID = 'movimientos_de_dinero.forma_pago_id';
	const TIPO_COMPROBANTE_ID = 'movimientos_de_dinero.tipo_comprobante_id';
	const CAJA_ID = 'movimientos_de_dinero.caja_id';
	const USUARIO_ID = 'movimientos_de_dinero.usuario_id';
	const CONCEPTO_PAGO_ID = 'movimientos_de_dinero.concepto_pago_id';
	const CREATED_AT = 'movimientos_de_dinero.created_at';
	const UPDATED_AT = 'movimientos_de_dinero.updated_at';
	const DELETED_AT = 'movimientos_de_dinero.deleted_at';
	const CLIENTE_ID = 'movimientos_de_dinero.cliente_id';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'movimientos_de_dinero';

	/**
	 * Cache of objects retrieved from the database
	 * @var MovimientosDeDinero[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'numero_boleta' => Model::COLUMN_TYPE_VARCHAR,
		'monto' => Model::COLUMN_TYPE_DECIMAL,
		'fecha' => Model::COLUMN_TYPE_TIMESTAMP,
		'reembolso' => Model::COLUMN_TYPE_DECIMAL,
		'glosa' => Model::COLUMN_TYPE_VARCHAR,
		'forma_pago_id' => Model::COLUMN_TYPE_INTEGER,
		'tipo_comprobante_id' => Model::COLUMN_TYPE_INTEGER,
		'caja_id' => Model::COLUMN_TYPE_INTEGER,
		'usuario_id' => Model::COLUMN_TYPE_INTEGER,
		'concepto_pago_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `numero_boleta` VARCHAR
	 * @var string
	 */
	protected $numero_boleta;

	/**
	 * `monto` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $monto;

	/**
	 * `fecha` TIMESTAMP
	 * @var string
	 */
	protected $fecha;

	/**
	 * `reembolso` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $reembolso;

	/**
	 * `glosa` VARCHAR
	 * @var string
	 */
	protected $glosa;

	/**
	 * `forma_pago_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $forma_pago_id;

	/**
	 * `tipo_comprobante_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $tipo_comprobante_id;

	/**
	 * `caja_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $caja_id;

	/**
	 * `usuario_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $usuario_id;

	/**
	 * `concepto_pago_id` INTEGER NOT NULL DEFAULT 1
	 * @var int
	 */
	protected $concepto_pago_id = 1;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `cliente_id` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return MovimientosDeDinero
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the numero_boleta field
	 */
	function getNumeroBoleta() {
		return $this->numero_boleta;
	}

	/**
	 * Sets the value of the numero_boleta field
	 * @return MovimientosDeDinero
	 */
	function setNumeroBoleta($value) {
		return $this->setColumnValue('numero_boleta', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getNumeroBoleta
	 * final because getNumeroBoleta should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getNumeroBoleta
	 */
	final function getNumero_boleta() {
		return $this->getNumeroBoleta();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setNumeroBoleta
	 * final because setNumeroBoleta should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setNumeroBoleta
	 * @return MovimientosDeDinero
	 */
	final function setNumero_boleta($value) {
		return $this->setNumeroBoleta($value);
	}

	/**
	 * Gets the value of the monto field
	 */
	function getMonto() {
		return $this->monto;
	}

	/**
	 * Sets the value of the monto field
	 * @return MovimientosDeDinero
	 */
	function setMonto($value) {
		return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->fecha || null === $format) {
			return $this->fecha;
		}
		if (0 === strpos($this->fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha));
	}

	/**
	 * Sets the value of the fecha field
	 * @return MovimientosDeDinero
	 */
	function setFecha($value) {
		return $this->setColumnValue('fecha', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Gets the value of the reembolso field
	 */
	function getReembolso() {
		return $this->reembolso;
	}

	/**
	 * Sets the value of the reembolso field
	 * @return MovimientosDeDinero
	 */
	function setReembolso($value) {
		return $this->setColumnValue('reembolso', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the glosa field
	 */
	function getGlosa() {
		return $this->glosa;
	}

	/**
	 * Sets the value of the glosa field
	 * @return MovimientosDeDinero
	 */
	function setGlosa($value) {
		return $this->setColumnValue('glosa', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the forma_pago_id field
	 */
	function getFormaPagoId() {
		return $this->forma_pago_id;
	}

	/**
	 * Sets the value of the forma_pago_id field
	 * @return MovimientosDeDinero
	 */
	function setFormaPagoId($value) {
		return $this->setColumnValue('forma_pago_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getFormaPagoId
	 * final because getFormaPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getFormaPagoId
	 */
	final function getForma_pago_id() {
		return $this->getFormaPagoId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setFormaPagoId
	 * final because setFormaPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setFormaPagoId
	 * @return MovimientosDeDinero
	 */
	final function setForma_pago_id($value) {
		return $this->setFormaPagoId($value);
	}

	/**
	 * Gets the value of the tipo_comprobante_id field
	 */
	function getTipoComprobanteId() {
		return $this->tipo_comprobante_id;
	}

	/**
	 * Sets the value of the tipo_comprobante_id field
	 * @return MovimientosDeDinero
	 */
	function setTipoComprobanteId($value) {
		return $this->setColumnValue('tipo_comprobante_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getTipoComprobanteId
	 * final because getTipoComprobanteId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getTipoComprobanteId
	 */
	final function getTipo_comprobante_id() {
		return $this->getTipoComprobanteId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setTipoComprobanteId
	 * final because setTipoComprobanteId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setTipoComprobanteId
	 * @return MovimientosDeDinero
	 */
	final function setTipo_comprobante_id($value) {
		return $this->setTipoComprobanteId($value);
	}

	/**
	 * Gets the value of the caja_id field
	 */
	function getCajaId() {
		return $this->caja_id;
	}

	/**
	 * Sets the value of the caja_id field
	 * @return MovimientosDeDinero
	 */
	function setCajaId($value) {
		return $this->setColumnValue('caja_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getCajaId
	 * final because getCajaId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getCajaId
	 */
	final function getCaja_id() {
		return $this->getCajaId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setCajaId
	 * final because setCajaId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setCajaId
	 * @return MovimientosDeDinero
	 */
	final function setCaja_id($value) {
		return $this->setCajaId($value);
	}

	/**
	 * Gets the value of the usuario_id field
	 */
	function getUsuarioId() {
		return $this->usuario_id;
	}

	/**
	 * Sets the value of the usuario_id field
	 * @return MovimientosDeDinero
	 */
	function setUsuarioId($value) {
		return $this->setColumnValue('usuario_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getUsuarioId
	 * final because getUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getUsuarioId
	 */
	final function getUsuario_id() {
		return $this->getUsuarioId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setUsuarioId
	 * final because setUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setUsuarioId
	 * @return MovimientosDeDinero
	 */
	final function setUsuario_id($value) {
		return $this->setUsuarioId($value);
	}

	/**
	 * Gets the value of the concepto_pago_id field
	 */
	function getConceptoPagoId() {
		return $this->concepto_pago_id;
	}

	/**
	 * Sets the value of the concepto_pago_id field
	 * @return MovimientosDeDinero
	 */
	function setConceptoPagoId($value) {
		return $this->setColumnValue('concepto_pago_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getConceptoPagoId
	 * final because getConceptoPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getConceptoPagoId
	 */
	final function getConcepto_pago_id() {
		return $this->getConceptoPagoId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setConceptoPagoId
	 * final because setConceptoPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setConceptoPagoId
	 * @return MovimientosDeDinero
	 */
	final function setConcepto_pago_id($value) {
		return $this->setConceptoPagoId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return MovimientosDeDinero
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for MovimientosDeDinero::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setCreatedAt
	 * @return MovimientosDeDinero
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return MovimientosDeDinero
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for MovimientosDeDinero::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setUpdatedAt
	 * @return MovimientosDeDinero
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return MovimientosDeDinero
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for MovimientosDeDinero::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setDeletedAt
	 * @return MovimientosDeDinero
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return MovimientosDeDinero
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for MovimientosDeDinero::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see MovimientosDeDinero::setClienteId
	 * @return MovimientosDeDinero
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return MovimientosDeDinero
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return MovimientosDeDinero
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveById($value) {
		return MovimientosDeDinero::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a numero_boleta
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByNumeroBoleta($value) {
		return static::retrieveByColumn('numero_boleta', $value);
	}

	/**
	 * Searches the database for a row with a monto
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByMonto($value) {
		return static::retrieveByColumn('monto', $value);
	}

	/**
	 * Searches the database for a row with a fecha
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('fecha', $value);
	}

	/**
	 * Searches the database for a row with a reembolso
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByReembolso($value) {
		return static::retrieveByColumn('reembolso', $value);
	}

	/**
	 * Searches the database for a row with a glosa
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByGlosa($value) {
		return static::retrieveByColumn('glosa', $value);
	}

	/**
	 * Searches the database for a row with a forma_pago_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByFormaPagoId($value) {
		return static::retrieveByColumn('forma_pago_id', $value);
	}

	/**
	 * Searches the database for a row with a tipo_comprobante_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByTipoComprobanteId($value) {
		return static::retrieveByColumn('tipo_comprobante_id', $value);
	}

	/**
	 * Searches the database for a row with a caja_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByCajaId($value) {
		return static::retrieveByColumn('caja_id', $value);
	}

	/**
	 * Searches the database for a row with a usuario_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByUsuarioId($value) {
		return static::retrieveByColumn('usuario_id', $value);
	}

	/**
	 * Searches the database for a row with a concepto_pago_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByConceptoPagoId($value) {
		return static::retrieveByColumn('concepto_pago_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return MovimientosDeDinero
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return MovimientosDeDinero
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->forma_pago_id = (null === $this->forma_pago_id) ? null : (int) $this->forma_pago_id;
		$this->tipo_comprobante_id = (null === $this->tipo_comprobante_id) ? null : (int) $this->tipo_comprobante_id;
		$this->caja_id = (null === $this->caja_id) ? null : (int) $this->caja_id;
		$this->usuario_id = (null === $this->usuario_id) ? null : (int) $this->usuario_id;
		$this->concepto_pago_id = (null === $this->concepto_pago_id) ? null : (int) $this->concepto_pago_id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		return $this;
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCaja(Cajas $cajas = null) {
		return $this->setCajasRelatedByCajaId($cajas);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCajasRelatedByCajaId(Cajas $cajas = null) {
		if (null === $cajas) {
			$this->setcaja_id(null);
		} else {
			if (!$cajas->getid()) {
				throw new Exception('Cannot connect a Cajas without a id');
			}
			$this->setcaja_id($cajas->getid());
		}
		return $this;
	}

	/**
	 * Returns a cajas object with a id
	 * that matches $this->caja_id.
	 * @return Cajas
	 */
	function getCaja() {
		return $this->getCajasRelatedByCajaId();
	}

	/**
	 * Returns a cajas object with a id
	 * that matches $this->caja_id.
	 * @return Cajas
	 */
	function getCajasRelatedByCajaId() {
		$fk_value = $this->getcaja_id();
		if (null === $fk_value) {
			return null;
		}
		return Cajas::retrieveByPK($fk_value);
	}

	static function doSelectJoinCaja(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinCajasRelatedByCajaId($q, $join_type);
	}

	/**
	 * Returns a cajas object with a id
	 * that matches $this->caja_id.
	 * @return Cajas
	 */
	function getCajas() {
		return $this->getCajasRelatedByCajaId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCajas(Cajas $cajas = null) {
		return $this->setCajasRelatedByCajaId($cajas);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinCajasRelatedByCajaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Cajas::getTableName();
		$q->join($to_table, $this_table . '.caja_id = ' . $to_table . '.id', $join_type);
		foreach (Cajas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Cajas'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setFormaPago(FormasPagos $formaspagos = null) {
		return $this->setFormasPagosRelatedByFormaPagoId($formaspagos);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setFormasPagosRelatedByFormaPagoId(FormasPagos $formaspagos = null) {
		if (null === $formaspagos) {
			$this->setforma_pago_id(null);
		} else {
			if (!$formaspagos->getid()) {
				throw new Exception('Cannot connect a FormasPagos without a id');
			}
			$this->setforma_pago_id($formaspagos->getid());
		}
		return $this;
	}

	/**
	 * Returns a formas_pagos object with a id
	 * that matches $this->forma_pago_id.
	 * @return FormasPagos
	 */
	function getFormaPago() {
		return $this->getFormasPagosRelatedByFormaPagoId();
	}

	/**
	 * Returns a formas_pagos object with a id
	 * that matches $this->forma_pago_id.
	 * @return FormasPagos
	 */
	function getFormasPagosRelatedByFormaPagoId() {
		$fk_value = $this->getforma_pago_id();
		if (null === $fk_value) {
			return null;
		}
		return FormasPagos::retrieveByPK($fk_value);
	}

	static function doSelectJoinFormaPago(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinFormasPagosRelatedByFormaPagoId($q, $join_type);
	}

	/**
	 * Returns a formas_pagos object with a id
	 * that matches $this->forma_pago_id.
	 * @return FormasPagos
	 */
	function getFormasPagos() {
		return $this->getFormasPagosRelatedByFormaPagoId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setFormasPagos(FormasPagos $formaspagos = null) {
		return $this->setFormasPagosRelatedByFormaPagoId($formaspagos);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinFormasPagosRelatedByFormaPagoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = FormasPagos::getTableName();
		$q->join($to_table, $this_table . '.forma_pago_id = ' . $to_table . '.id', $join_type);
		foreach (FormasPagos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('FormasPagos'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setTipoComprobante(TiposComprobantes $tiposcomprobantes = null) {
		return $this->setTiposComprobantesRelatedByTipoComprobanteId($tiposcomprobantes);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setTiposComprobantesRelatedByTipoComprobanteId(TiposComprobantes $tiposcomprobantes = null) {
		if (null === $tiposcomprobantes) {
			$this->settipo_comprobante_id(null);
		} else {
			if (!$tiposcomprobantes->getid()) {
				throw new Exception('Cannot connect a TiposComprobantes without a id');
			}
			$this->settipo_comprobante_id($tiposcomprobantes->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipos_comprobantes object with a id
	 * that matches $this->tipo_comprobante_id.
	 * @return TiposComprobantes
	 */
	function getTipoComprobante() {
		return $this->getTiposComprobantesRelatedByTipoComprobanteId();
	}

	/**
	 * Returns a tipos_comprobantes object with a id
	 * that matches $this->tipo_comprobante_id.
	 * @return TiposComprobantes
	 */
	function getTiposComprobantesRelatedByTipoComprobanteId() {
		$fk_value = $this->gettipo_comprobante_id();
		if (null === $fk_value) {
			return null;
		}
		return TiposComprobantes::retrieveByPK($fk_value);
	}

	static function doSelectJoinTipoComprobante(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinTiposComprobantesRelatedByTipoComprobanteId($q, $join_type);
	}

	/**
	 * Returns a tipos_comprobantes object with a id
	 * that matches $this->tipo_comprobante_id.
	 * @return TiposComprobantes
	 */
	function getTiposComprobantes() {
		return $this->getTiposComprobantesRelatedByTipoComprobanteId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setTiposComprobantes(TiposComprobantes $tiposcomprobantes = null) {
		return $this->setTiposComprobantesRelatedByTipoComprobanteId($tiposcomprobantes);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinTiposComprobantesRelatedByTipoComprobanteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TiposComprobantes::getTableName();
		$q->join($to_table, $this_table . '.tipo_comprobante_id = ' . $to_table . '.id', $join_type);
		foreach (TiposComprobantes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('TiposComprobantes'));
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return MovimientosDeDinero
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return MovimientosDeDinero[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Cajas::getTableName();
		$q->join($to_table, $this_table . '.caja_id = ' . $to_table . '.id', $join_type);
		foreach (Cajas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Cajas';
	
		$to_table = FormasPagos::getTableName();
		$q->join($to_table, $this_table . '.forma_pago_id = ' . $to_table . '.id', $join_type);
		foreach (FormasPagos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'FormasPagos';
	
		$to_table = TiposComprobantes::getTableName();
		$q->join($to_table, $this_table . '.tipo_comprobante_id = ' . $to_table . '.id', $join_type);
		foreach (TiposComprobantes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'TiposComprobantes';
	
		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting amortizaciones Objects(rows) from the amortizaciones table
	 * with a movimiento_id that matches $this->id.
	 * @return Query
	 */
	function getAmortizacionessRelatedByMovimientoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('amortizaciones', 'movimiento_id', 'id', $q);
	}

	/**
	 * Returns the count of Amortizaciones Objects(rows) from the amortizaciones table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function countAmortizacionessRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Amortizaciones::doCount($this->getAmortizacionessRelatedByMovimientoIdQuery($q));
	}

	/**
	 * Deletes the amortizaciones Objects(rows) from the amortizaciones table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function deleteAmortizacionessRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AmortizacionessRelatedByMovimientoId_c = array();
		return Amortizaciones::doDelete($this->getAmortizacionessRelatedByMovimientoIdQuery($q));
	}

	protected $AmortizacionessRelatedByMovimientoId_c = array();

	/**
	 * Returns an array of Amortizaciones objects with a movimiento_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Amortizaciones[]
	 */
	function getAmortizacionessRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AmortizacionessRelatedByMovimientoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AmortizacionessRelatedByMovimientoId_c;
		}

		$result = Amortizaciones::doSelect($this->getAmortizacionessRelatedByMovimientoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AmortizacionessRelatedByMovimientoId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas Objects(rows) from the ventas table
	 * with a movimiento_id that matches $this->id.
	 * @return Query
	 */
	function getVentassRelatedByMovimientoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'movimiento_id', 'id', $q);
	}

	/**
	 * Returns the count of Ventas Objects(rows) from the ventas table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function countVentassRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Ventas::doCount($this->getVentassRelatedByMovimientoIdQuery($q));
	}

	/**
	 * Deletes the ventas Objects(rows) from the ventas table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function deleteVentassRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentassRelatedByMovimientoId_c = array();
		return Ventas::doDelete($this->getVentassRelatedByMovimientoIdQuery($q));
	}

	protected $VentassRelatedByMovimientoId_c = array();

	/**
	 * Returns an array of Ventas objects with a movimiento_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Ventas[]
	 */
	function getVentassRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentassRelatedByMovimientoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentassRelatedByMovimientoId_c;
		}

		$result = Ventas::doSelect($this->getVentassRelatedByMovimientoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentassRelatedByMovimientoId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting ventas_hotel Objects(rows) from the ventas_hotel table
	 * with a movimiento_id that matches $this->id.
	 * @return Query
	 */
	function getVentasHotelsRelatedByMovimientoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_hotel', 'movimiento_id', 'id', $q);
	}

	/**
	 * Returns the count of VentasHotel Objects(rows) from the ventas_hotel table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function countVentasHotelsRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return VentasHotel::doCount($this->getVentasHotelsRelatedByMovimientoIdQuery($q));
	}

	/**
	 * Deletes the ventas_hotel Objects(rows) from the ventas_hotel table
	 * with a movimiento_id that matches $this->id.
	 * @return int
	 */
	function deleteVentasHotelsRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentasHotelsRelatedByMovimientoId_c = array();
		return VentasHotel::doDelete($this->getVentasHotelsRelatedByMovimientoIdQuery($q));
	}

	protected $VentasHotelsRelatedByMovimientoId_c = array();

	/**
	 * Returns an array of VentasHotel objects with a movimiento_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return VentasHotel[]
	 */
	function getVentasHotelsRelatedByMovimientoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentasHotelsRelatedByMovimientoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentasHotelsRelatedByMovimientoId_c;
		}

		$result = VentasHotel::doSelect($this->getVentasHotelsRelatedByMovimientoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentasHotelsRelatedByMovimientoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for MovimientosDeDinero::getAmortizacionessRelatedBymovimiento_id
	 * @return Amortizaciones[]
	 * @see MovimientosDeDinero::getAmortizacionessRelatedByMovimientoId
	 */
	function getAmortizacioness($extra = null) {
		return $this->getAmortizacionessRelatedByMovimientoId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getAmortizacionessRelatedBymovimiento_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getAmortizacionessRelatedBymovimiento_idQuery
	  */
	function getAmortizacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('amortizaciones', 'movimiento_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteAmortizacionessRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::deleteAmortizacionessRelatedBymovimiento_id
	  */
	function deleteAmortizacioness(Query $q = null) {
		return $this->deleteAmortizacionessRelatedByMovimientoId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countAmortizacionessRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::countAmortizacionessRelatedByMovimientoId
	  */
	function countAmortizacioness(Query $q = null) {
		return $this->countAmortizacionessRelatedByMovimientoId($q);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getVentassRelatedBymovimiento_id
	 * @return Ventas[]
	 * @see MovimientosDeDinero::getVentassRelatedByMovimientoId
	 */
	function getVentass($extra = null) {
		return $this->getVentassRelatedByMovimientoId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getVentassRelatedBymovimiento_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getVentassRelatedBymovimiento_idQuery
	  */
	function getVentassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'movimiento_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteVentassRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::deleteVentassRelatedBymovimiento_id
	  */
	function deleteVentass(Query $q = null) {
		return $this->deleteVentassRelatedByMovimientoId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countVentassRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::countVentassRelatedByMovimientoId
	  */
	function countVentass(Query $q = null) {
		return $this->countVentassRelatedByMovimientoId($q);
	}

	/**
	 * Convenience function for MovimientosDeDinero::getVentasHotelsRelatedBymovimiento_id
	 * @return VentasHotel[]
	 * @see MovimientosDeDinero::getVentasHotelsRelatedByMovimientoId
	 */
	function getVentasHotels($extra = null) {
		return $this->getVentasHotelsRelatedByMovimientoId($extra);
	}

	/**
	  * Convenience function for MovimientosDeDinero::getVentasHotelsRelatedBymovimiento_idQuery
	  * @return Query
	  * @see MovimientosDeDinero::getVentasHotelsRelatedBymovimiento_idQuery
	  */
	function getVentasHotelsQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas_hotel', 'movimiento_id','id', $q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::deleteVentasHotelsRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::deleteVentasHotelsRelatedBymovimiento_id
	  */
	function deleteVentasHotels(Query $q = null) {
		return $this->deleteVentasHotelsRelatedByMovimientoId($q);
	}

	/**
	  * Convenience function for MovimientosDeDinero::countVentasHotelsRelatedBymovimiento_id
	  * @return int
	  * @see MovimientosDeDinero::countVentasHotelsRelatedByMovimientoId
	  */
	function countVentasHotels(Query $q = null) {
		return $this->countVentasHotelsRelatedByMovimientoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getforma_pago_id()) {
			$this->_validationErrors[] = 'forma_pago_id must not be null';
		}
		if (null === $this->getcaja_id()) {
			$this->_validationErrors[] = 'caja_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}