<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseTiposComprobantes extends ApplicationModel {

	const ID = 'tipos_comprobantes.id';
	const DESCRIPCION = 'tipos_comprobantes.descripcion';
	const CREATED_AT = 'tipos_comprobantes.created_at';
	const UPDATED_AT = 'tipos_comprobantes.updated_at';
	const DELETED_AT = 'tipos_comprobantes.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'tipos_comprobantes';

	/**
	 * Cache of objects retrieved from the database
	 * @var TiposComprobantes[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return TiposComprobantes
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return TiposComprobantes
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return TiposComprobantes
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for TiposComprobantes::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TiposComprobantes::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for TiposComprobantes::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TiposComprobantes::setCreatedAt
	 * @return TiposComprobantes
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return TiposComprobantes
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for TiposComprobantes::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TiposComprobantes::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for TiposComprobantes::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TiposComprobantes::setUpdatedAt
	 * @return TiposComprobantes
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return TiposComprobantes
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for TiposComprobantes::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TiposComprobantes::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for TiposComprobantes::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TiposComprobantes::setDeletedAt
	 * @return TiposComprobantes
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return TiposComprobantes
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return TiposComprobantes
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return TiposComprobantes
	 */
	static function retrieveById($value) {
		return TiposComprobantes::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return TiposComprobantes
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return TiposComprobantes
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return TiposComprobantes
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return TiposComprobantes
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return TiposComprobantes
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return TiposComprobantes[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a tipo_comprobante_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientosDeDinerosRelatedByTipoComprobanteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'tipo_comprobante_id', 'id', $q);
	}

	/**
	 * Returns the count of MovimientosDeDinero Objects(rows) from the movimientos_de_dinero table
	 * with a tipo_comprobante_id that matches $this->id.
	 * @return int
	 */
	function countMovimientosDeDinerosRelatedByTipoComprobanteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MovimientosDeDinero::doCount($this->getMovimientosDeDinerosRelatedByTipoComprobanteIdQuery($q));
	}

	/**
	 * Deletes the movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a tipo_comprobante_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientosDeDinerosRelatedByTipoComprobanteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientosDeDinerosRelatedByTipoComprobanteId_c = array();
		return MovimientosDeDinero::doDelete($this->getMovimientosDeDinerosRelatedByTipoComprobanteIdQuery($q));
	}

	protected $MovimientosDeDinerosRelatedByTipoComprobanteId_c = array();

	/**
	 * Returns an array of MovimientosDeDinero objects with a tipo_comprobante_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MovimientosDeDinero[]
	 */
	function getMovimientosDeDinerosRelatedByTipoComprobanteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientosDeDinerosRelatedByTipoComprobanteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientosDeDinerosRelatedByTipoComprobanteId_c;
		}

		$result = MovimientosDeDinero::doSelect($this->getMovimientosDeDinerosRelatedByTipoComprobanteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientosDeDinerosRelatedByTipoComprobanteId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting series Objects(rows) from the series table
	 * with a tipo_comprobante_id that matches $this->id.
	 * @return Query
	 */
	function getSeriessRelatedByTipoComprobanteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('series', 'tipo_comprobante_id', 'id', $q);
	}

	/**
	 * Returns the count of Series Objects(rows) from the series table
	 * with a tipo_comprobante_id that matches $this->id.
	 * @return int
	 */
	function countSeriessRelatedByTipoComprobanteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Series::doCount($this->getSeriessRelatedByTipoComprobanteIdQuery($q));
	}

	/**
	 * Deletes the series Objects(rows) from the series table
	 * with a tipo_comprobante_id that matches $this->id.
	 * @return int
	 */
	function deleteSeriessRelatedByTipoComprobanteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->SeriessRelatedByTipoComprobanteId_c = array();
		return Series::doDelete($this->getSeriessRelatedByTipoComprobanteIdQuery($q));
	}

	protected $SeriessRelatedByTipoComprobanteId_c = array();

	/**
	 * Returns an array of Series objects with a tipo_comprobante_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Series[]
	 */
	function getSeriessRelatedByTipoComprobanteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->SeriessRelatedByTipoComprobanteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->SeriessRelatedByTipoComprobanteId_c;
		}

		$result = Series::doSelect($this->getSeriessRelatedByTipoComprobanteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->SeriessRelatedByTipoComprobanteId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for TiposComprobantes::getMovimientosDeDinerosRelatedBytipo_comprobante_id
	 * @return MovimientosDeDinero[]
	 * @see TiposComprobantes::getMovimientosDeDinerosRelatedByTipoComprobanteId
	 */
	function getMovimientosDeDineros($extra = null) {
		return $this->getMovimientosDeDinerosRelatedByTipoComprobanteId($extra);
	}

	/**
	  * Convenience function for TiposComprobantes::getMovimientosDeDinerosRelatedBytipo_comprobante_idQuery
	  * @return Query
	  * @see TiposComprobantes::getMovimientosDeDinerosRelatedBytipo_comprobante_idQuery
	  */
	function getMovimientosDeDinerosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'tipo_comprobante_id','id', $q);
	}

	/**
	  * Convenience function for TiposComprobantes::deleteMovimientosDeDinerosRelatedBytipo_comprobante_id
	  * @return int
	  * @see TiposComprobantes::deleteMovimientosDeDinerosRelatedBytipo_comprobante_id
	  */
	function deleteMovimientosDeDineros(Query $q = null) {
		return $this->deleteMovimientosDeDinerosRelatedByTipoComprobanteId($q);
	}

	/**
	  * Convenience function for TiposComprobantes::countMovimientosDeDinerosRelatedBytipo_comprobante_id
	  * @return int
	  * @see TiposComprobantes::countMovimientosDeDinerosRelatedByTipoComprobanteId
	  */
	function countMovimientosDeDineros(Query $q = null) {
		return $this->countMovimientosDeDinerosRelatedByTipoComprobanteId($q);
	}

	/**
	 * Convenience function for TiposComprobantes::getSeriessRelatedBytipo_comprobante_id
	 * @return Series[]
	 * @see TiposComprobantes::getSeriessRelatedByTipoComprobanteId
	 */
	function getSeriess($extra = null) {
		return $this->getSeriessRelatedByTipoComprobanteId($extra);
	}

	/**
	  * Convenience function for TiposComprobantes::getSeriessRelatedBytipo_comprobante_idQuery
	  * @return Query
	  * @see TiposComprobantes::getSeriessRelatedBytipo_comprobante_idQuery
	  */
	function getSeriessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('series', 'tipo_comprobante_id','id', $q);
	}

	/**
	  * Convenience function for TiposComprobantes::deleteSeriessRelatedBytipo_comprobante_id
	  * @return int
	  * @see TiposComprobantes::deleteSeriessRelatedBytipo_comprobante_id
	  */
	function deleteSeriess(Query $q = null) {
		return $this->deleteSeriessRelatedByTipoComprobanteId($q);
	}

	/**
	  * Convenience function for TiposComprobantes::countSeriessRelatedBytipo_comprobante_id
	  * @return int
	  * @see TiposComprobantes::countSeriessRelatedByTipoComprobanteId
	  */
	function countSeriess(Query $q = null) {
		return $this->countSeriessRelatedByTipoComprobanteId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}