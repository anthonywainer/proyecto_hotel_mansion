<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseEstadias extends ApplicationModel {

	const ID = 'estadias.id';
	const FECHA_RESERVA = 'estadias.fecha_reserva';
	const FECHA_INGRESO = 'estadias.fecha_ingreso';
	const FECHA_SALIDA = 'estadias.fecha_salida';
	const ESTADO = 'estadias.estado';
	const CLIENTE_ID = 'estadias.cliente_id';
	const CREATED_AT = 'estadias.created_at';
	const UPDATED_AT = 'estadias.updated_at';
	const DELETED_AT = 'estadias.deleted_at';
	const USUARIO_ID = 'estadias.usuario_id';
	const PRECIO = 'estadias.precio';
	const DESCUENTO = 'estadias.descuento';
	const OBSERVACION = 'estadias.observacion';
	const MONTO_PAGAR = 'estadias.monto_pagar';
	const CUOTA = 'estadias.cuota';
	const RESTO = 'estadias.resto';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'estadias';

	/**
	 * Cache of objects retrieved from the database
	 * @var Estadias[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'fecha_reserva' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_ingreso' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_salida' => Model::COLUMN_TYPE_TIMESTAMP,
		'estado' => Model::COLUMN_TYPE_VARCHAR,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'usuario_id' => Model::COLUMN_TYPE_VARCHAR,
		'precio' => Model::COLUMN_TYPE_DECIMAL,
		'descuento' => Model::COLUMN_TYPE_DECIMAL,
		'observacion' => Model::COLUMN_TYPE_VARCHAR,
		'monto_pagar' => Model::COLUMN_TYPE_DECIMAL,
		'cuota' => Model::COLUMN_TYPE_DECIMAL,
		'resto' => Model::COLUMN_TYPE_DECIMAL,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `fecha_reserva` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_reserva;

	/**
	 * `fecha_ingreso` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_ingreso;

	/**
	 * `fecha_salida` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_salida;

	/**
	 * `estado` VARCHAR NOT NULL
	 * @var string
	 */
	protected $estado;

	/**
	 * `cliente_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `usuario_id` VARCHAR NOT NULL
	 * @var string
	 */
	protected $usuario_id;

	/**
	 * `precio` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $precio;

	/**
	 * `descuento` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $descuento;

	/**
	 * `observacion` VARCHAR
	 * @var string
	 */
	protected $observacion;

	/**
	 * `monto_pagar` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $monto_pagar;

	/**
	 * `cuota` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $cuota;

	/**
	 * `resto` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $resto;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Estadias
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the fecha_reserva field
	 */
	function getFechaReserva($format = null) {
		if (null === $this->fecha_reserva || null === $format) {
			return $this->fecha_reserva;
		}
		if (0 === strpos($this->fecha_reserva, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_reserva));
	}

	/**
	 * Sets the value of the fecha_reserva field
	 * @return Estadias
	 */
	function setFechaReserva($value) {
		return $this->setColumnValue('fecha_reserva', $value,Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Estadias::getFechaReserva
	 * final because getFechaReserva should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getFechaReserva
	 */
	final function getFecha_reserva($format = null) {
		return $this->getFechaReserva($format);
	}

	/**
	 * Convenience function for Estadias::setFechaReserva
	 * final because setFechaReserva should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setFechaReserva
	 * @return Estadias
	 */
	final function setFecha_reserva($value) {
        if (strpos($value, '/')){
            $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }

		return $this->setFechaReserva($value);
	}

	/**
	 * Gets the value of the fecha_ingreso field
	 */
	function getFechaIngreso($format = null) {
		if (null === $this->fecha_ingreso || null === $format) {
			return $this->fecha_ingreso;
		}
		if (0 === strpos($this->fecha_ingreso, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_ingreso));
	}

	/**
	 * Sets the value of the fecha_ingreso field
	 * @return Estadias
	 */
	function setFechaIngreso($value) {
		return $this->setColumnValue('fecha_ingreso', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Estadias::getFechaIngreso
	 * final because getFechaIngreso should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getFechaIngreso
	 */
	final function getFecha_ingreso($format = null) {
		return $this->getFechaIngreso($format);
	}

	/**
	 * Convenience function for Estadias::setFechaIngreso
	 * final because setFechaIngreso should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setFechaIngreso
	 * @return Estadias
	 */
	final function setFecha_ingreso($value) {
        if (strpos($value, '/')){
           $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
		return $this->setFechaIngreso($value);
	}

	/**
	 * Gets the value of the fecha_salida field
	 */
	function getFechaSalida($format = null) {
		if (null === $this->fecha_salida || null === $format) {
			return $this->fecha_salida;
		}
		if (0 === strpos($this->fecha_salida, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_salida));
	}

	/**
	 * Sets the value of the fecha_salida field
	 * @return Estadias
	 */
	function setFechaSalida($value) {
		return $this->setColumnValue('fecha_salida', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Estadias::getFechaSalida
	 * final because getFechaSalida should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getFechaSalida
	 */
	final function getFecha_salida($format = null) {
		return $this->getFechaSalida($format);
	}

	/**
	 * Convenience function for Estadias::setFechaSalida
	 * final because setFechaSalida should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setFechaSalida
	 * @return Estadias
	 */
	final function setFecha_salida($value) {
        if (strpos($value, '/')){
            $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
		return $this->setFechaSalida($value);
	}

	/**
	 * Gets the value of the estado field
	 */
	function getEstado() {
		return $this->estado;
	}

	/**
	 * Sets the value of the estado field
	 * @return Estadias
	 */
	function setEstado($value) {
		return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return Estadias
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Estadias::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for Estadias::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setClienteId
	 * @return Estadias
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Estadias
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Estadias::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Estadias::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setCreatedAt
	 * @return Estadias
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Estadias
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Estadias::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Estadias::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setUpdatedAt
	 * @return Estadias
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Estadias
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Estadias::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Estadias::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setDeletedAt
	 * @return Estadias
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the usuario_id field
	 */
	function getUsuarioId() {
		return $this->usuario_id;
	}

	/**
	 * Sets the value of the usuario_id field
	 * @return Estadias
	 */
	function setUsuarioId($value) {
		return $this->setColumnValue('usuario_id', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Estadias::getUsuarioId
	 * final because getUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getUsuarioId
	 */
	final function getUsuario_id() {
		return $this->getUsuarioId();
	}

	/**
	 * Convenience function for Estadias::setUsuarioId
	 * final because setUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setUsuarioId
	 * @return Estadias
	 */
	final function setUsuario_id($value) {
		return $this->setUsuarioId($value);
	}

	/**
	 * Gets the value of the precio field
	 */
	function getPrecio() {
		return $this->precio;
	}

	/**
	 * Sets the value of the precio field
	 * @return Estadias
	 */
	function setPrecio($value) {
		return $this->setColumnValue('precio', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the descuento field
	 */
	function getDescuento() {
		return $this->descuento;
	}

	/**
	 * Sets the value of the descuento field
	 * @return Estadias
	 */
	function setDescuento($value) {
		return $this->setColumnValue('descuento', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the observacion field
	 */
	function getObservacion() {
		return $this->observacion;
	}

	/**
	 * Sets the value of the observacion field
	 * @return Estadias
	 */
	function setObservacion($value) {
		return $this->setColumnValue('observacion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the monto_pagar field
	 */
	function getMontoPagar() {
		return $this->monto_pagar;
	}

	/**
	 * Sets the value of the monto_pagar field
	 * @return Estadias
	 */
	function setMontoPagar($value) {
		return $this->setColumnValue('monto_pagar', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Convenience function for Estadias::getMontoPagar
	 * final because getMontoPagar should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::getMontoPagar
	 */
	final function getMonto_pagar() {
		return $this->getMontoPagar();
	}

	/**
	 * Convenience function for Estadias::setMontoPagar
	 * final because setMontoPagar should be extended instead
	 * to ensure consistent behavior
	 * @see Estadias::setMontoPagar
	 * @return Estadias
	 */
	final function setMonto_pagar($value) {
		return $this->setMontoPagar($value);
	}

	/**
	 * Gets the value of the cuota field
	 */
	function getCuota() {
		return $this->cuota;
	}

	/**
	 * Sets the value of the cuota field
	 * @return Estadias
	 */
	function setCuota($value) {
		return $this->setColumnValue('cuota', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the resto field
	 */
	function getResto() {
		return $this->resto;
	}

	/**
	 * Sets the value of the resto field
	 * @return Estadias
	 */
	function setResto($value) {
		return $this->setColumnValue('resto', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Estadias
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Estadias
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveById($value) {
		return Estadias::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a fecha_reserva
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByFechaReserva($value) {
		return static::retrieveByColumn('fecha_reserva', $value);
	}

	/**
	 * Searches the database for a row with a fecha_ingreso
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByFechaIngreso($value) {
		return static::retrieveByColumn('fecha_ingreso', $value);
	}

	/**
	 * Searches the database for a row with a fecha_salida
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByFechaSalida($value) {
		return static::retrieveByColumn('fecha_salida', $value);
	}

	/**
	 * Searches the database for a row with a estado
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByEstado($value) {
		return static::retrieveByColumn('estado', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a usuario_id
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByUsuarioId($value) {
		return static::retrieveByColumn('usuario_id', $value);
	}

	/**
	 * Searches the database for a row with a precio
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByPrecio($value) {
		return static::retrieveByColumn('precio', $value);
	}

	/**
	 * Searches the database for a row with a descuento
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByDescuento($value) {
		return static::retrieveByColumn('descuento', $value);
	}

	/**
	 * Searches the database for a row with a observacion
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByObservacion($value) {
		return static::retrieveByColumn('observacion', $value);
	}

	/**
	 * Searches the database for a row with a monto_pagar
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByMontoPagar($value) {
		return static::retrieveByColumn('monto_pagar', $value);
	}

	/**
	 * Searches the database for a row with a cuota
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByCuota($value) {
		return static::retrieveByColumn('cuota', $value);
	}

	/**
	 * Searches the database for a row with a resto
	 * value that matches the one provided
	 * @return Estadias
	 */
	static function retrieveByResto($value) {
		return static::retrieveByColumn('resto', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Estadias
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		return $this;
	}

	/**
	 * @return Estadias
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return Estadias
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return Estadias
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return Estadias[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return Estadias[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting cronogramas_pagos Objects(rows) from the cronogramas_pagos table
	 * with a estadia_id that matches $this->id.
	 * @return Query
	 */
	function getCronogramasPagossRelatedByEstadiaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cronogramas_pagos', 'estadia_id', 'id', $q);
	}

	/**
	 * Returns the count of CronogramasPagos Objects(rows) from the cronogramas_pagos table
	 * with a estadia_id that matches $this->id.
	 * @return int
	 */
	function countCronogramasPagossRelatedByEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return CronogramasPagos::doCount($this->getCronogramasPagossRelatedByEstadiaIdQuery($q));
	}

	/**
	 * Deletes the cronogramas_pagos Objects(rows) from the cronogramas_pagos table
	 * with a estadia_id that matches $this->id.
	 * @return int
	 */
	function deleteCronogramasPagossRelatedByEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->CronogramasPagossRelatedByEstadiaId_c = array();
		return CronogramasPagos::doDelete($this->getCronogramasPagossRelatedByEstadiaIdQuery($q));
	}

	protected $CronogramasPagossRelatedByEstadiaId_c = array();

	/**
	 * Returns an array of CronogramasPagos objects with a estadia_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return CronogramasPagos[]
	 */
	function getCronogramasPagossRelatedByEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->CronogramasPagossRelatedByEstadiaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->CronogramasPagossRelatedByEstadiaId_c;
		}

		$result = CronogramasPagos::doSelect($this->getCronogramasPagossRelatedByEstadiaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->CronogramasPagossRelatedByEstadiaId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting estadias_habitaciones Objects(rows) from the estadias_habitaciones table
	 * with a estadia_id that matches $this->id.
	 * @return Query
	 */
	function getEstadiasHabitacionessRelatedByEstadiaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('estadias_habitaciones', 'estadia_id', 'id', $q);
	}

	/**
	 * Returns the count of EstadiasHabitaciones Objects(rows) from the estadias_habitaciones table
	 * with a estadia_id that matches $this->id.
	 * @return int
	 */
	function countEstadiasHabitacionessRelatedByEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return EstadiasHabitaciones::doCount($this->getEstadiasHabitacionessRelatedByEstadiaIdQuery($q));
	}

	/**
	 * Deletes the estadias_habitaciones Objects(rows) from the estadias_habitaciones table
	 * with a estadia_id that matches $this->id.
	 * @return int
	 */
	function deleteEstadiasHabitacionessRelatedByEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->EstadiasHabitacionessRelatedByEstadiaId_c = array();
		return EstadiasHabitaciones::doDelete($this->getEstadiasHabitacionessRelatedByEstadiaIdQuery($q));
	}

	protected $EstadiasHabitacionessRelatedByEstadiaId_c = array();

	/**
	 * Returns an array of EstadiasHabitaciones objects with a estadia_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return EstadiasHabitaciones[]
	 */
	function getEstadiasHabitacionessRelatedByEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->EstadiasHabitacionessRelatedByEstadiaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->EstadiasHabitacionessRelatedByEstadiaId_c;
		}

		$result = EstadiasHabitaciones::doSelect($this->getEstadiasHabitacionessRelatedByEstadiaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->EstadiasHabitacionessRelatedByEstadiaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Estadias::getCronogramasPagossRelatedByestadia_id
	 * @return CronogramasPagos[]
	 * @see Estadias::getCronogramasPagossRelatedByEstadiaId
	 */
	function getCronogramasPagoss($extra = null) {
		return $this->getCronogramasPagossRelatedByEstadiaId($extra);
	}

	/**
	  * Convenience function for Estadias::getCronogramasPagossRelatedByestadia_idQuery
	  * @return Query
	  * @see Estadias::getCronogramasPagossRelatedByestadia_idQuery
	  */
	function getCronogramasPagossQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cronogramas_pagos', 'estadia_id','id', $q);
	}

	/**
	  * Convenience function for Estadias::deleteCronogramasPagossRelatedByestadia_id
	  * @return int
	  * @see Estadias::deleteCronogramasPagossRelatedByestadia_id
	  */
	function deleteCronogramasPagoss(Query $q = null) {
		return $this->deleteCronogramasPagossRelatedByEstadiaId($q);
	}

	/**
	  * Convenience function for Estadias::countCronogramasPagossRelatedByestadia_id
	  * @return int
	  * @see Estadias::countCronogramasPagossRelatedByEstadiaId
	  */
	function countCronogramasPagoss(Query $q = null) {
		return $this->countCronogramasPagossRelatedByEstadiaId($q);
	}

	/**
	 * Convenience function for Estadias::getEstadiasHabitacionessRelatedByestadia_id
	 * @return EstadiasHabitaciones[]
	 * @see Estadias::getEstadiasHabitacionessRelatedByEstadiaId
	 */
	function getEstadiasHabitacioness($extra = null) {
		return $this->getEstadiasHabitacionessRelatedByEstadiaId($extra);
	}

	/**
	  * Convenience function for Estadias::getEstadiasHabitacionessRelatedByestadia_idQuery
	  * @return Query
	  * @see Estadias::getEstadiasHabitacionessRelatedByestadia_idQuery
	  */
	function getEstadiasHabitacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('estadias_habitaciones', 'estadia_id','id', $q);
	}

	/**
	  * Convenience function for Estadias::deleteEstadiasHabitacionessRelatedByestadia_id
	  * @return int
	  * @see Estadias::deleteEstadiasHabitacionessRelatedByestadia_id
	  */
	function deleteEstadiasHabitacioness(Query $q = null) {
		return $this->deleteEstadiasHabitacionessRelatedByEstadiaId($q);
	}

	/**
	  * Convenience function for Estadias::countEstadiasHabitacionessRelatedByestadia_id
	  * @return int
	  * @see Estadias::countEstadiasHabitacionessRelatedByEstadiaId
	  */
	function countEstadiasHabitacioness(Query $q = null) {
		return $this->countEstadiasHabitacionessRelatedByEstadiaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();

		if (null === $this->getestado()) {
			$this->_validationErrors[] = 'estado must not be null';
		}

		if (null === $this->getusuario_id()) {
			$this->_validationErrors[] = 'usuario_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}