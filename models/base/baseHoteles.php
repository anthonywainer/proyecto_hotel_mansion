<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseHoteles extends ApplicationModel {

	const ID = 'hoteles.id';
	const NOMBRE = 'hoteles.nombre';
	const DESCRIPCION = 'hoteles.descripcion';
	const DIRECCION = 'hoteles.direccion';
	const RUC = 'hoteles.ruc';
	const TELEFONO = 'hoteles.telefono';
	const COLORES = 'hoteles.colores';
	const CREATED_AT = 'hoteles.created_at';
	const UPDATED_AT = 'hoteles.updated_at';
	const DELETED_AT = 'hoteles.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'hoteles';

	/**
	 * Cache of objects retrieved from the database
	 * @var Hoteles[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'ruc' => Model::COLUMN_TYPE_INTEGER,
		'telefono' => Model::COLUMN_TYPE_INTEGER,
		'colores' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `direccion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $direccion;

	/**
	 * `ruc` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $ruc;

	/**
	 * `telefono` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $telefono;

	/**
	 * `colores` VARCHAR NOT NULL
	 * @var string
	 */
	protected $colores;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Hoteles
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Hoteles
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Hoteles
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Hoteles
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the ruc field
	 */
	function getRuc() {
		return $this->ruc;
	}

	/**
	 * Sets the value of the ruc field
	 * @return Hoteles
	 */
	function setRuc($value) {
		return $this->setColumnValue('ruc', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Hoteles
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the colores field
	 */
	function getColores() {
		return $this->colores;
	}

	/**
	 * Sets the value of the colores field
	 * @return Hoteles
	 */
	function setColores($value) {
		return $this->setColumnValue('colores', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Hoteles
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Hoteles::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Hoteles::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Hoteles::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Hoteles::setCreatedAt
	 * @return Hoteles
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Hoteles
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Hoteles::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Hoteles::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Hoteles::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Hoteles::setUpdatedAt
	 * @return Hoteles
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Hoteles
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Hoteles::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Hoteles::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Hoteles::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Hoteles::setDeletedAt
	 * @return Hoteles
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Hoteles
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Hoteles
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveById($value) {
		return Hoteles::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a ruc
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByRuc($value) {
		return static::retrieveByColumn('ruc', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a colores
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByColores($value) {
		return static::retrieveByColumn('colores', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Hoteles
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Hoteles
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->ruc = (null === $this->ruc) ? null : (int) $this->ruc;
		$this->telefono = (null === $this->telefono) ? null : (int) $this->telefono;
		return $this;
	}

	/**
	 * @return Hoteles[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		if (null === $this->getdireccion()) {
			$this->_validationErrors[] = 'direccion must not be null';
		}
		if (null === $this->getruc()) {
			$this->_validationErrors[] = 'ruc must not be null';
		}
		if (null === $this->gettelefono()) {
			$this->_validationErrors[] = 'telefono must not be null';
		}
		if (null === $this->getcolores()) {
			$this->_validationErrors[] = 'colores must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}