<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseHabitaciones extends ApplicationModel {

	const ID = 'habitaciones.id';
	const NUMERO = 'habitaciones.numero';
	const DESCRIPCION = 'habitaciones.descripcion';
	const ESTADO = 'habitaciones.estado';
	const PISO = 'habitaciones.piso';
	const TELEVISOR_ID = 'habitaciones.televisor_id';
	const TIPO_HABITACION_ID = 'habitaciones.tipo_habitacion_id';
	const CREATED_AT = 'habitaciones.created_at';
	const UPDATED_AT = 'habitaciones.updated_at';
	const DELETED_AT = 'habitaciones.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'habitaciones';

	/**
	 * Cache of objects retrieved from the database
	 * @var Habitaciones[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'numero' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'estado' => Model::COLUMN_TYPE_VARCHAR,
		'piso' => Model::COLUMN_TYPE_INTEGER,
		'televisor_id' => Model::COLUMN_TYPE_INTEGER,
		'tipo_habitacion_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `numero` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $numero;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `estado` VARCHAR NOT NULL
	 * @var string
	 */
	protected $estado;

	/**
	 * `piso` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $piso;

	/**
	 * `televisor_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $televisor_id;

	/**
	 * `tipo_habitacion_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $tipo_habitacion_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Habitaciones
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the numero field
	 */
	function getNumero() {
		return $this->numero;
	}

	/**
	 * Sets the value of the numero field
	 * @return Habitaciones
	 */
	function setNumero($value) {
		return $this->setColumnValue('numero', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Habitaciones
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the estado field
	 */
	function getEstado() {
		return $this->estado;
	}

	/**
	 * Sets the value of the estado field
	 * @return Habitaciones
	 */
	function setEstado($value) {
		return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the piso field
	 */
	function getPiso() {
		return $this->piso;
	}

	/**
	 * Sets the value of the piso field
	 * @return Habitaciones
	 */
	function setPiso($value) {
		return $this->setColumnValue('piso', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the televisor_id field
	 */
	function getTelevisorId() {
		return $this->televisor_id;
	}

	/**
	 * Sets the value of the televisor_id field
	 * @return Habitaciones
	 */
	function setTelevisorId($value) {
		return $this->setColumnValue('televisor_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Habitaciones::getTelevisorId
	 * final because getTelevisorId should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::getTelevisorId
	 */
	final function getTelevisor_id() {
		return $this->getTelevisorId();
	}

	/**
	 * Convenience function for Habitaciones::setTelevisorId
	 * final because setTelevisorId should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::setTelevisorId
	 * @return Habitaciones
	 */
	final function setTelevisor_id($value) {
		return $this->setTelevisorId($value);
	}

	/**
	 * Gets the value of the tipo_habitacion_id field
	 */
	function getTipoHabitacionId() {
		return $this->tipo_habitacion_id;
	}

	/**
	 * Sets the value of the tipo_habitacion_id field
	 * @return Habitaciones
	 */
	function setTipoHabitacionId($value) {
		return $this->setColumnValue('tipo_habitacion_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Habitaciones::getTipoHabitacionId
	 * final because getTipoHabitacionId should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::getTipoHabitacionId
	 */
	final function getTipo_habitacion_id() {
		return $this->getTipoHabitacionId();
	}

	/**
	 * Convenience function for Habitaciones::setTipoHabitacionId
	 * final because setTipoHabitacionId should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::setTipoHabitacionId
	 * @return Habitaciones
	 */
	final function setTipo_habitacion_id($value) {
		return $this->setTipoHabitacionId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Habitaciones
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Habitaciones::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Habitaciones::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::setCreatedAt
	 * @return Habitaciones
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Habitaciones
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Habitaciones::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Habitaciones::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::setUpdatedAt
	 * @return Habitaciones
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Habitaciones
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Habitaciones::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Habitaciones::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Habitaciones::setDeletedAt
	 * @return Habitaciones
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Habitaciones
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Habitaciones
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveById($value) {
		return Habitaciones::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a numero
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByNumero($value) {
		return static::retrieveByColumn('numero', $value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a estado
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByEstado($value) {
		return static::retrieveByColumn('estado', $value);
	}

	/**
	 * Searches the database for a row with a piso
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByPiso($value) {
		return static::retrieveByColumn('piso', $value);
	}

	/**
	 * Searches the database for a row with a televisor_id
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByTelevisorId($value) {
		return static::retrieveByColumn('televisor_id', $value);
	}

	/**
	 * Searches the database for a row with a tipo_habitacion_id
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByTipoHabitacionId($value) {
		return static::retrieveByColumn('tipo_habitacion_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Habitaciones
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Habitaciones
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->numero = (null === $this->numero) ? null : (int) $this->numero;
		$this->piso = (null === $this->piso) ? null : (int) $this->piso;
		$this->televisor_id = (null === $this->televisor_id) ? null : (int) $this->televisor_id;
		$this->tipo_habitacion_id = (null === $this->tipo_habitacion_id) ? null : (int) $this->tipo_habitacion_id;
		return $this;
	}

	/**
	 * @return Habitaciones
	 */
	function setTelevisor(Televisores $televisores = null) {
		return $this->setTelevisoresRelatedByTelevisorId($televisores);
	}

	/**
	 * @return Habitaciones
	 */
	function setTelevisoresRelatedByTelevisorId(Televisores $televisores = null) {
		if (null === $televisores) {
			$this->settelevisor_id(null);
		} else {
			if (!$televisores->getid()) {
				throw new Exception('Cannot connect a Televisores without a id');
			}
			$this->settelevisor_id($televisores->getid());
		}
		return $this;
	}

	/**
	 * Returns a televisores object with a id
	 * that matches $this->televisor_id.
	 * @return Televisores
	 */
	function getTelevisor() {
		return $this->getTelevisoresRelatedByTelevisorId();
	}

	/**
	 * Returns a televisores object with a id
	 * that matches $this->televisor_id.
	 * @return Televisores
	 */
	function getTelevisoresRelatedByTelevisorId() {
		$fk_value = $this->gettelevisor_id();
		if (null === $fk_value) {
			return null;
		}
		return Televisores::retrieveByPK($fk_value);
	}

	static function doSelectJoinTelevisor(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinTelevisoresRelatedByTelevisorId($q, $join_type);
	}

	/**
	 * Returns a televisores object with a id
	 * that matches $this->televisor_id.
	 * @return Televisores
	 */
	function getTelevisores() {
		return $this->getTelevisoresRelatedByTelevisorId();
	}

	/**
	 * @return Habitaciones
	 */
	function setTelevisores(Televisores $televisores = null) {
		return $this->setTelevisoresRelatedByTelevisorId($televisores);
	}

	/**
	 * @return Habitaciones[]
	 */
	static function doSelectJoinTelevisoresRelatedByTelevisorId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Televisores::getTableName();
		$q->join($to_table, $this_table . '.televisor_id = ' . $to_table . '.id', $join_type);
		foreach (Televisores::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Televisores'));
	}

	/**
	 * @return Habitaciones
	 */
	function setTipoHabitacion(TiposHabitaciones $tiposhabitaciones = null) {
		return $this->setTiposHabitacionesRelatedByTipoHabitacionId($tiposhabitaciones);
	}

	/**
	 * @return Habitaciones
	 */
	function setTiposHabitacionesRelatedByTipoHabitacionId(TiposHabitaciones $tiposhabitaciones = null) {
		if (null === $tiposhabitaciones) {
			$this->settipo_habitacion_id(null);
		} else {
			if (!$tiposhabitaciones->getid()) {
				throw new Exception('Cannot connect a TiposHabitaciones without a id');
			}
			$this->settipo_habitacion_id($tiposhabitaciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipos_habitaciones object with a id
	 * that matches $this->tipo_habitacion_id.
	 * @return TiposHabitaciones
	 */
	function getTipoHabitacion() {
		return $this->getTiposHabitacionesRelatedByTipoHabitacionId();
	}

	/**
	 * Returns a tipos_habitaciones object with a id
	 * that matches $this->tipo_habitacion_id.
	 * @return TiposHabitaciones
	 */
	function getTiposHabitacionesRelatedByTipoHabitacionId() {
		$fk_value = $this->gettipo_habitacion_id();
		if (null === $fk_value) {
			return null;
		}
		return TiposHabitaciones::retrieveByPK($fk_value);
	}

	static function doSelectJoinTipoHabitacion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinTiposHabitacionesRelatedByTipoHabitacionId($q, $join_type);
	}

	/**
	 * Returns a tipos_habitaciones object with a id
	 * that matches $this->tipo_habitacion_id.
	 * @return TiposHabitaciones
	 */
	function getTiposHabitaciones() {
		return $this->getTiposHabitacionesRelatedByTipoHabitacionId();
	}

	/**
	 * @return Habitaciones
	 */
	function setTiposHabitaciones(TiposHabitaciones $tiposhabitaciones = null) {
		return $this->setTiposHabitacionesRelatedByTipoHabitacionId($tiposhabitaciones);
	}

	/**
	 * @return Habitaciones[]
	 */
	static function doSelectJoinTiposHabitacionesRelatedByTipoHabitacionId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TiposHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.tipo_habitacion_id = ' . $to_table . '.id', $join_type);
		foreach (TiposHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('TiposHabitaciones'));
	}

	/**
	 * @return Habitaciones[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Televisores::getTableName();
		$q->join($to_table, $this_table . '.televisor_id = ' . $to_table . '.id', $join_type);
		foreach (Televisores::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Televisores';
	
		$to_table = TiposHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.tipo_habitacion_id = ' . $to_table . '.id', $join_type);
		foreach (TiposHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'TiposHabitaciones';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting clientes_habitaciones Objects(rows) from the clientes_habitaciones table
	 * with a habitacion_id that matches $this->id.
	 * @return Query
	 */
	function getClientesHabitacionessRelatedByHabitacionIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_habitaciones', 'habitacion_id', 'id', $q);
	}

	/**
	 * Returns the count of ClientesHabitaciones Objects(rows) from the clientes_habitaciones table
	 * with a habitacion_id that matches $this->id.
	 * @return int
	 */
	function countClientesHabitacionessRelatedByHabitacionId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ClientesHabitaciones::doCount($this->getClientesHabitacionessRelatedByHabitacionIdQuery($q));
	}

	/**
	 * Deletes the clientes_habitaciones Objects(rows) from the clientes_habitaciones table
	 * with a habitacion_id that matches $this->id.
	 * @return int
	 */
	function deleteClientesHabitacionessRelatedByHabitacionId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ClientesHabitacionessRelatedByHabitacionId_c = array();
		return ClientesHabitaciones::doDelete($this->getClientesHabitacionessRelatedByHabitacionIdQuery($q));
	}

	protected $ClientesHabitacionessRelatedByHabitacionId_c = array();

	/**
	 * Returns an array of ClientesHabitaciones objects with a habitacion_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ClientesHabitaciones[]
	 */
	function getClientesHabitacionessRelatedByHabitacionId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ClientesHabitacionessRelatedByHabitacionId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ClientesHabitacionessRelatedByHabitacionId_c;
		}

		$result = ClientesHabitaciones::doSelect($this->getClientesHabitacionessRelatedByHabitacionIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ClientesHabitacionessRelatedByHabitacionId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting estadias_habitaciones Objects(rows) from the estadias_habitaciones table
	 * with a habitacion_id that matches $this->id.
	 * @return Query
	 */
	function getEstadiasHabitacionessRelatedByHabitacionIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('estadias_habitaciones', 'habitacion_id', 'id', $q);
	}

	/**
	 * Returns the count of EstadiasHabitaciones Objects(rows) from the estadias_habitaciones table
	 * with a habitacion_id that matches $this->id.
	 * @return int
	 */
	function countEstadiasHabitacionessRelatedByHabitacionId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return EstadiasHabitaciones::doCount($this->getEstadiasHabitacionessRelatedByHabitacionIdQuery($q));
	}

	/**
	 * Deletes the estadias_habitaciones Objects(rows) from the estadias_habitaciones table
	 * with a habitacion_id that matches $this->id.
	 * @return int
	 */
	function deleteEstadiasHabitacionessRelatedByHabitacionId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->EstadiasHabitacionessRelatedByHabitacionId_c = array();
		return EstadiasHabitaciones::doDelete($this->getEstadiasHabitacionessRelatedByHabitacionIdQuery($q));
	}

	protected $EstadiasHabitacionessRelatedByHabitacionId_c = array();

	/**
	 * Returns an array of EstadiasHabitaciones objects with a habitacion_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return EstadiasHabitaciones[]
	 */
	function getEstadiasHabitacionessRelatedByHabitacionId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->EstadiasHabitacionessRelatedByHabitacionId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->EstadiasHabitacionessRelatedByHabitacionId_c;
		}

		$result = EstadiasHabitaciones::doSelect($this->getEstadiasHabitacionessRelatedByHabitacionIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->EstadiasHabitacionessRelatedByHabitacionId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Habitaciones::getClientesHabitacionessRelatedByhabitacion_id
	 * @return ClientesHabitaciones[]
	 * @see Habitaciones::getClientesHabitacionessRelatedByHabitacionId
	 */
	function getClientesHabitacioness($extra = null) {
		return $this->getClientesHabitacionessRelatedByHabitacionId($extra);
	}

	/**
	  * Convenience function for Habitaciones::getClientesHabitacionessRelatedByhabitacion_idQuery
	  * @return Query
	  * @see Habitaciones::getClientesHabitacionessRelatedByhabitacion_idQuery
	  */
	function getClientesHabitacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_habitaciones', 'habitacion_id','id', $q);
	}

	/**
	  * Convenience function for Habitaciones::deleteClientesHabitacionessRelatedByhabitacion_id
	  * @return int
	  * @see Habitaciones::deleteClientesHabitacionessRelatedByhabitacion_id
	  */
	function deleteClientesHabitacioness(Query $q = null) {
		return $this->deleteClientesHabitacionessRelatedByHabitacionId($q);
	}

	/**
	  * Convenience function for Habitaciones::countClientesHabitacionessRelatedByhabitacion_id
	  * @return int
	  * @see Habitaciones::countClientesHabitacionessRelatedByHabitacionId
	  */
	function countClientesHabitacioness(Query $q = null) {
		return $this->countClientesHabitacionessRelatedByHabitacionId($q);
	}

	/**
	 * Convenience function for Habitaciones::getEstadiasHabitacionessRelatedByhabitacion_id
	 * @return EstadiasHabitaciones[]
	 * @see Habitaciones::getEstadiasHabitacionessRelatedByHabitacionId
	 */
	function getEstadiasHabitacioness($extra = null) {
		return $this->getEstadiasHabitacionessRelatedByHabitacionId($extra);
	}

	/**
	  * Convenience function for Habitaciones::getEstadiasHabitacionessRelatedByhabitacion_idQuery
	  * @return Query
	  * @see Habitaciones::getEstadiasHabitacionessRelatedByhabitacion_idQuery
	  */
	function getEstadiasHabitacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('estadias_habitaciones', 'habitacion_id','id', $q);
	}

	/**
	  * Convenience function for Habitaciones::deleteEstadiasHabitacionessRelatedByhabitacion_id
	  * @return int
	  * @see Habitaciones::deleteEstadiasHabitacionessRelatedByhabitacion_id
	  */
	function deleteEstadiasHabitacioness(Query $q = null) {
		return $this->deleteEstadiasHabitacionessRelatedByHabitacionId($q);
	}

	/**
	  * Convenience function for Habitaciones::countEstadiasHabitacionessRelatedByhabitacion_id
	  * @return int
	  * @see Habitaciones::countEstadiasHabitacionessRelatedByHabitacionId
	  */
	function countEstadiasHabitacioness(Query $q = null) {
		return $this->countEstadiasHabitacionessRelatedByHabitacionId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnumero()) {
			$this->_validationErrors[] = 'numero must not be null';
		}
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		if (null === $this->getestado()) {
			$this->_validationErrors[] = 'estado must not be null';
		}
		if (null === $this->getpiso()) {
			$this->_validationErrors[] = 'piso must not be null';
		}
		if (null === $this->gettelevisor_id()) {
			$this->_validationErrors[] = 'televisor_id must not be null';
		}
		if (null === $this->gettipo_habitacion_id()) {
			$this->_validationErrors[] = 'tipo_habitacion_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}