<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseFormasPago extends ApplicationModel {

	const ID = 'formas_pago.id';
	const DESCRIPCION = 'formas_pago.descripcion';
	const CREATED_AT = 'formas_pago.created_at';
	const UPDATED_AT = 'formas_pago.updated_at';
	const DELETED_AT = 'formas_pago.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'formas_pago';

	/**
	 * Cache of objects retrieved from the database
	 * @var FormasPago[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return FormasPago
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return FormasPago
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return FormasPago
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for FormasPago::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see FormasPago::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for FormasPago::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see FormasPago::setCreatedAt
	 * @return FormasPago
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return FormasPago
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for FormasPago::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see FormasPago::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for FormasPago::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see FormasPago::setUpdatedAt
	 * @return FormasPago
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return FormasPago
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for FormasPago::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see FormasPago::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for FormasPago::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see FormasPago::setDeletedAt
	 * @return FormasPago
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return FormasPago
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return FormasPago
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return FormasPago
	 */
	static function retrieveById($value) {
		return FormasPago::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return FormasPago
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return FormasPago
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return FormasPago
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return FormasPago
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return FormasPago
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return FormasPago[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a forma_pago_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientosDeDinerosRelatedByFormaPagoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'forma_pago_id', 'id', $q);
	}

	/**
	 * Returns the count of MovimientosDeDinero Objects(rows) from the movimientos_de_dinero table
	 * with a forma_pago_id that matches $this->id.
	 * @return int
	 */
	function countMovimientosDeDinerosRelatedByFormaPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MovimientosDeDinero::doCount($this->getMovimientosDeDinerosRelatedByFormaPagoIdQuery($q));
	}

	/**
	 * Deletes the movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a forma_pago_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientosDeDinerosRelatedByFormaPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientosDeDinerosRelatedByFormaPagoId_c = array();
		return MovimientosDeDinero::doDelete($this->getMovimientosDeDinerosRelatedByFormaPagoIdQuery($q));
	}

	protected $MovimientosDeDinerosRelatedByFormaPagoId_c = array();

	/**
	 * Returns an array of MovimientosDeDinero objects with a forma_pago_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MovimientosDeDinero[]
	 */
	function getMovimientosDeDinerosRelatedByFormaPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientosDeDinerosRelatedByFormaPagoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientosDeDinerosRelatedByFormaPagoId_c;
		}

		$result = MovimientosDeDinero::doSelect($this->getMovimientosDeDinerosRelatedByFormaPagoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientosDeDinerosRelatedByFormaPagoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for FormasPago::getMovimientosDeDinerosRelatedByforma_pago_id
	 * @return MovimientosDeDinero[]
	 * @see FormasPago::getMovimientosDeDinerosRelatedByFormaPagoId
	 */
	function getMovimientosDeDineros($extra = null) {
		return $this->getMovimientosDeDinerosRelatedByFormaPagoId($extra);
	}

	/**
	  * Convenience function for FormasPago::getMovimientosDeDinerosRelatedByforma_pago_idQuery
	  * @return Query
	  * @see FormasPago::getMovimientosDeDinerosRelatedByforma_pago_idQuery
	  */
	function getMovimientosDeDinerosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'forma_pago_id','id', $q);
	}

	/**
	  * Convenience function for FormasPago::deleteMovimientosDeDinerosRelatedByforma_pago_id
	  * @return int
	  * @see FormasPago::deleteMovimientosDeDinerosRelatedByforma_pago_id
	  */
	function deleteMovimientosDeDineros(Query $q = null) {
		return $this->deleteMovimientosDeDinerosRelatedByFormaPagoId($q);
	}

	/**
	  * Convenience function for FormasPago::countMovimientosDeDinerosRelatedByforma_pago_id
	  * @return int
	  * @see FormasPago::countMovimientosDeDinerosRelatedByFormaPagoId
	  */
	function countMovimientosDeDineros(Query $q = null) {
		return $this->countMovimientosDeDinerosRelatedByFormaPagoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}