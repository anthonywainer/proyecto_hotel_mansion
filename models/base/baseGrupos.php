<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseGrupos extends ApplicationModel {

	const ID = 'grupos.id';
	const DESCRIPCION = 'grupos.descripcion';
	const CREATED_AT = 'grupos.created_at';
	const UPDATED_AT = 'grupos.updated_at';
	const DELETED_AT = 'grupos.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'grupos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Grupos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Grupos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Grupos
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Grupos
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Grupos::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Grupos::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Grupos::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Grupos::setCreatedAt
	 * @return Grupos
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Grupos
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Grupos::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Grupos::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Grupos::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Grupos::setUpdatedAt
	 * @return Grupos
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Grupos
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Grupos::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Grupos::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Grupos::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Grupos::setDeletedAt
	 * @return Grupos
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Grupos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Grupos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Grupos
	 */
	static function retrieveById($value) {
		return Grupos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Grupos
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Grupos
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Grupos
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Grupos
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Grupos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Grupos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting permisos_grupo Objects(rows) from the permisos_grupo table
	 * with a idgrupo that matches $this->id.
	 * @return Query
	 */
	function getPermisosGruposRelatedByIdgrupoQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_grupo', 'idgrupo', 'id', $q);
	}

	/**
	 * Returns the count of PermisosGrupo Objects(rows) from the permisos_grupo table
	 * with a idgrupo that matches $this->id.
	 * @return int
	 */
	function countPermisosGruposRelatedByIdgrupo(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PermisosGrupo::doCount($this->getPermisosGruposRelatedByIdgrupoQuery($q));
	}

	/**
	 * Deletes the permisos_grupo Objects(rows) from the permisos_grupo table
	 * with a idgrupo that matches $this->id.
	 * @return int
	 */
	function deletePermisosGruposRelatedByIdgrupo(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PermisosGruposRelatedByIdgrupo_c = array();
		return PermisosGrupo::doDelete($this->getPermisosGruposRelatedByIdgrupoQuery($q));
	}

	protected $PermisosGruposRelatedByIdgrupo_c = array();

	/**
	 * Returns an array of PermisosGrupo objects with a idgrupo
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PermisosGrupo[]
	 */
	function getPermisosGruposRelatedByIdgrupo(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PermisosGruposRelatedByIdgrupo_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PermisosGruposRelatedByIdgrupo_c;
		}

		$result = PermisosGrupo::doSelect($this->getPermisosGruposRelatedByIdgrupoQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PermisosGruposRelatedByIdgrupo_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting usuarios_grupo Objects(rows) from the usuarios_grupo table
	 * with a grupo_id that matches $this->id.
	 * @return Query
	 */
	function getUsuariosGruposRelatedByGrupoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios_grupo', 'grupo_id', 'id', $q);
	}

	/**
	 * Returns the count of UsuariosGrupo Objects(rows) from the usuarios_grupo table
	 * with a grupo_id that matches $this->id.
	 * @return int
	 */
	function countUsuariosGruposRelatedByGrupoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return UsuariosGrupo::doCount($this->getUsuariosGruposRelatedByGrupoIdQuery($q));
	}

	/**
	 * Deletes the usuarios_grupo Objects(rows) from the usuarios_grupo table
	 * with a grupo_id that matches $this->id.
	 * @return int
	 */
	function deleteUsuariosGruposRelatedByGrupoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->UsuariosGruposRelatedByGrupoId_c = array();
		return UsuariosGrupo::doDelete($this->getUsuariosGruposRelatedByGrupoIdQuery($q));
	}

	protected $UsuariosGruposRelatedByGrupoId_c = array();

	/**
	 * Returns an array of UsuariosGrupo objects with a grupo_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return UsuariosGrupo[]
	 */
	function getUsuariosGruposRelatedByGrupoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->UsuariosGruposRelatedByGrupoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->UsuariosGruposRelatedByGrupoId_c;
		}

		$result = UsuariosGrupo::doSelect($this->getUsuariosGruposRelatedByGrupoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->UsuariosGruposRelatedByGrupoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Grupos::getPermisosGruposRelatedByidgrupo
	 * @return PermisosGrupo[]
	 * @see Grupos::getPermisosGruposRelatedByIdgrupo
	 */
	function getPermisosGrupos($extra = null) {
		return $this->getPermisosGruposRelatedByIdgrupo($extra);
	}

	/**
	  * Convenience function for Grupos::getPermisosGruposRelatedByidgrupoQuery
	  * @return Query
	  * @see Grupos::getPermisosGruposRelatedByidgrupoQuery
	  */
	function getPermisosGruposQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_grupo', 'idgrupo','id', $q);
	}

	/**
	  * Convenience function for Grupos::deletePermisosGruposRelatedByidgrupo
	  * @return int
	  * @see Grupos::deletePermisosGruposRelatedByidgrupo
	  */
	function deletePermisosGrupos(Query $q = null) {
		return $this->deletePermisosGruposRelatedByIdgrupo($q);
	}

	/**
	  * Convenience function for Grupos::countPermisosGruposRelatedByidgrupo
	  * @return int
	  * @see Grupos::countPermisosGruposRelatedByIdgrupo
	  */
	function countPermisosGrupos(Query $q = null) {
		return $this->countPermisosGruposRelatedByIdgrupo($q);
	}

	/**
	 * Convenience function for Grupos::getUsuariosGruposRelatedBygrupo_id
	 * @return UsuariosGrupo[]
	 * @see Grupos::getUsuariosGruposRelatedByGrupoId
	 */
	function getUsuariosGrupos($extra = null) {
		return $this->getUsuariosGruposRelatedByGrupoId($extra);
	}

	/**
	  * Convenience function for Grupos::getUsuariosGruposRelatedBygrupo_idQuery
	  * @return Query
	  * @see Grupos::getUsuariosGruposRelatedBygrupo_idQuery
	  */
	function getUsuariosGruposQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios_grupo', 'grupo_id','id', $q);
	}

	/**
	  * Convenience function for Grupos::deleteUsuariosGruposRelatedBygrupo_id
	  * @return int
	  * @see Grupos::deleteUsuariosGruposRelatedBygrupo_id
	  */
	function deleteUsuariosGrupos(Query $q = null) {
		return $this->deleteUsuariosGruposRelatedByGrupoId($q);
	}

	/**
	  * Convenience function for Grupos::countUsuariosGruposRelatedBygrupo_id
	  * @return int
	  * @see Grupos::countUsuariosGruposRelatedByGrupoId
	  */
	function countUsuariosGrupos(Query $q = null) {
		return $this->countUsuariosGruposRelatedByGrupoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}