<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseClientesEstadias extends ApplicationModel {

	const ID = 'clientes_estadias.id';
	const CLIENTE_ID = 'clientes_estadias.cliente_id';
	const HABITACION_ESTADIA_ID = 'clientes_estadias.habitacion_estadia_id';
	const CREATED_AT = 'clientes_estadias.created_at';
	const UPDATED_AT = 'clientes_estadias.updated_at';
	const DELETED_AT = 'clientes_estadias.deleted_at';
	const ESTADO = 'clientes_estadias.estado';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'clientes_estadias';

	/**
	 * Cache of objects retrieved from the database
	 * @var ClientesEstadias[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
		'habitacion_estadia_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'estado' => Model::COLUMN_TYPE_FLOAT,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `cliente_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * `habitacion_estadia_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $habitacion_estadia_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `estado` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $estado;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ClientesEstadias
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return ClientesEstadias
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ClientesEstadias::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for ClientesEstadias::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::setClienteId
	 * @return ClientesEstadias
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * Gets the value of the habitacion_estadia_id field
	 */
	function getHabitacionEstadiaId() {
		return $this->habitacion_estadia_id;
	}

	/**
	 * Sets the value of the habitacion_estadia_id field
	 * @return ClientesEstadias
	 */
	function setHabitacionEstadiaId($value) {
		return $this->setColumnValue('habitacion_estadia_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ClientesEstadias::getHabitacionEstadiaId
	 * final because getHabitacionEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::getHabitacionEstadiaId
	 */
	final function getHabitacion_estadia_id() {
		return $this->getHabitacionEstadiaId();
	}

	/**
	 * Convenience function for ClientesEstadias::setHabitacionEstadiaId
	 * final because setHabitacionEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::setHabitacionEstadiaId
	 * @return ClientesEstadias
	 */
	final function setHabitacion_estadia_id($value) {
		return $this->setHabitacionEstadiaId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return ClientesEstadias
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ClientesEstadias::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for ClientesEstadias::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::setCreatedAt
	 * @return ClientesEstadias
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return ClientesEstadias
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ClientesEstadias::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for ClientesEstadias::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::setUpdatedAt
	 * @return ClientesEstadias
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return ClientesEstadias
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ClientesEstadias::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for ClientesEstadias::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesEstadias::setDeletedAt
	 * @return ClientesEstadias
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the estado field
	 */
	function getEstado() {
		return $this->estado;
	}

	/**
	 * Sets the value of the estado field
	 * @return ClientesEstadias
	 */
	function setEstado($value) {
		return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ClientesEstadias
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ClientesEstadias
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveById($value) {
		return ClientesEstadias::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}

	/**
	 * Searches the database for a row with a habitacion_estadia_id
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveByHabitacionEstadiaId($value) {
		return static::retrieveByColumn('habitacion_estadia_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a estado
	 * value that matches the one provided
	 * @return ClientesEstadias
	 */
	static function retrieveByEstado($value) {
		return static::retrieveByColumn('estado', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ClientesEstadias
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		$this->habitacion_estadia_id = (null === $this->habitacion_estadia_id) ? null : (int) $this->habitacion_estadia_id;
		return $this;
	}

	/**
	 * @return ClientesEstadias
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return ClientesEstadias
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return ClientesEstadias
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return ClientesEstadias[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return ClientesEstadias
	 */
	function setHabitacionEstadia(EstadiasHabitaciones $estadiashabitaciones = null) {
		return $this->setEstadiasHabitacionesRelatedByHabitacionEstadiaId($estadiashabitaciones);
	}

	/**
	 * @return ClientesEstadias
	 */
	function setEstadiasHabitacionesRelatedByHabitacionEstadiaId(EstadiasHabitaciones $estadiashabitaciones = null) {
		if (null === $estadiashabitaciones) {
			$this->sethabitacion_estadia_id(null);
		} else {
			if (!$estadiashabitaciones->getid()) {
				throw new Exception('Cannot connect a EstadiasHabitaciones without a id');
			}
			$this->sethabitacion_estadia_id($estadiashabitaciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a estadias_habitaciones object with a id
	 * that matches $this->habitacion_estadia_id.
	 * @return EstadiasHabitaciones
	 */
	function getHabitacionEstadia() {
		return $this->getEstadiasHabitacionesRelatedByHabitacionEstadiaId();
	}

	/**
	 * Returns a estadias_habitaciones object with a id
	 * that matches $this->habitacion_estadia_id.
	 * @return EstadiasHabitaciones
	 */
	function getEstadiasHabitacionesRelatedByHabitacionEstadiaId() {
		$fk_value = $this->gethabitacion_estadia_id();
		if (null === $fk_value) {
			return null;
		}
		return EstadiasHabitaciones::retrieveByPK($fk_value);
	}

	static function doSelectJoinHabitacionEstadia(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinEstadiasHabitacionesRelatedByHabitacionEstadiaId($q, $join_type);
	}

	/**
	 * Returns a estadias_habitaciones object with a id
	 * that matches $this->habitacion_estadia_id.
	 * @return EstadiasHabitaciones
	 */
	function getEstadiasHabitaciones() {
		return $this->getEstadiasHabitacionesRelatedByHabitacionEstadiaId();
	}

	/**
	 * @return ClientesEstadias
	 */
	function setEstadiasHabitaciones(EstadiasHabitaciones $estadiashabitaciones = null) {
		return $this->setEstadiasHabitacionesRelatedByHabitacionEstadiaId($estadiashabitaciones);
	}

	/**
	 * @return ClientesEstadias[]
	 */
	static function doSelectJoinEstadiasHabitacionesRelatedByHabitacionEstadiaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = EstadiasHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.habitacion_estadia_id = ' . $to_table . '.id', $join_type);
		foreach (EstadiasHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('EstadiasHabitaciones'));
	}

	/**
	 * @return ClientesEstadias[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$to_table = EstadiasHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.habitacion_estadia_id = ' . $to_table . '.id', $join_type);
		foreach (EstadiasHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'EstadiasHabitaciones';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getcliente_id()) {
			$this->_validationErrors[] = 'cliente_id must not be null';
		}
		if (null === $this->gethabitacion_estadia_id()) {
			$this->_validationErrors[] = 'habitacion_estadia_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}