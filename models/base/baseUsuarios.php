<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseUsuarios extends ApplicationModel {

	const ID = 'usuarios.id';
	const NOMBRES = 'usuarios.nombres';
	const APELLIDOS = 'usuarios.apellidos';
	const CLAVE = 'usuarios.clave';
	const FECHA_INGRESO = 'usuarios.fecha_ingreso';
	const FECHA_SALIDA = 'usuarios.fecha_salida';
	const ESTA_ACTIVO = 'usuarios.esta_activo';
	const CORREO = 'usuarios.correo';
	const TELEFONO = 'usuarios.telefono';
	const DIRECCION = 'usuarios.direccion';
	const CREATED_AT = 'usuarios.created_at';
	const UPDATED_AT = 'usuarios.updated_at';
	const DELETED_AT = 'usuarios.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'usuarios';

	/**
	 * Cache of objects retrieved from the database
	 * @var Usuarios[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'apellidos' => Model::COLUMN_TYPE_VARCHAR,
		'clave' => Model::COLUMN_TYPE_VARCHAR,
		'fecha_ingreso' => Model::COLUMN_TYPE_DATE,
		'fecha_salida' => Model::COLUMN_TYPE_DATE,
		'esta_activo' => Model::COLUMN_TYPE_VARCHAR,
		'correo' => Model::COLUMN_TYPE_VARCHAR,
		'telefono' => Model::COLUMN_TYPE_INTEGER,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombres` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombres;

	/**
	 * `apellidos` VARCHAR NOT NULL
	 * @var string
	 */
	protected $apellidos;

	/**
	 * `clave` VARCHAR NOT NULL
	 * @var string
	 */
	protected $clave;

	/**
	 * `fecha_ingreso` DATE NOT NULL
	 * @var string
	 */
	protected $fecha_ingreso;

	/**
	 * `fecha_salida` DATE NOT NULL
	 * @var string
	 */
	protected $fecha_salida;

	/**
	 * `esta_activo` VARCHAR NOT NULL
	 * @var string
	 */
	protected $esta_activo;

	/**
	 * `correo` VARCHAR NOT NULL
	 * @var string
	 */
	protected $correo;

	/**
	 * `telefono` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $telefono;

	/**
	 * `direccion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $direccion;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Usuarios
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Usuarios
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the apellidos field
	 */
	function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * Sets the value of the apellidos field
	 * @return Usuarios
	 */
	function setApellidos($value) {
		return $this->setColumnValue('apellidos', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the clave field
	 */
	function getClave() {
		return $this->clave;
	}

	/**
	 * Sets the value of the clave field
	 * @return Usuarios
	 */
	function setClave($value) {
		return $this->setColumnValue('clave', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the fecha_ingreso field
	 */
	function getFechaIngreso($format = null) {
		if (null === $this->fecha_ingreso || null === $format) {
			return $this->fecha_ingreso;
		}
		if (0 === strpos($this->fecha_ingreso, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_ingreso));
	}

	/**
	 * Sets the value of the fecha_ingreso field
	 * @return Usuarios
	 */
	function setFechaIngreso($value) {
		return $this->setColumnValue('fecha_ingreso', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for Usuarios::getFechaIngreso
	 * final because getFechaIngreso should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getFechaIngreso
	 */
	final function getFecha_ingreso($format = null) {
		return $this->getFechaIngreso($format);
	}

	/**
	 * Convenience function for Usuarios::setFechaIngreso
	 * final because setFechaIngreso should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setFechaIngreso
	 * @return Usuarios
	 */
	final function setFecha_ingreso($value) {
		return $this->setFechaIngreso($value);
	}

	/**
	 * Gets the value of the fecha_salida field
	 */
	function getFechaSalida($format = null) {
		if (null === $this->fecha_salida || null === $format) {
			return $this->fecha_salida;
		}
		if (0 === strpos($this->fecha_salida, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_salida));
	}

	/**
	 * Sets the value of the fecha_salida field
	 * @return Usuarios
	 */
	function setFechaSalida($value) {
		return $this->setColumnValue('fecha_salida', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for Usuarios::getFechaSalida
	 * final because getFechaSalida should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getFechaSalida
	 */
	final function getFecha_salida($format = null) {
		return $this->getFechaSalida($format);
	}

	/**
	 * Convenience function for Usuarios::setFechaSalida
	 * final because setFechaSalida should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setFechaSalida
	 * @return Usuarios
	 */
	final function setFecha_salida($value) {
		return $this->setFechaSalida($value);
	}

	/**
	 * Gets the value of the esta_activo field
	 */
	function getEstaActivo() {
		return $this->esta_activo;
	}

	/**
	 * Sets the value of the esta_activo field
	 * @return Usuarios
	 */
	function setEstaActivo($value) {
		return $this->setColumnValue('esta_activo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Usuarios::getEstaActivo
	 * final because getEstaActivo should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getEstaActivo
	 */
	final function getEsta_activo() {
		return $this->getEstaActivo();
	}

	/**
	 * Convenience function for Usuarios::setEstaActivo
	 * final because setEstaActivo should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setEstaActivo
	 * @return Usuarios
	 */
	final function setEsta_activo($value) {
		return $this->setEstaActivo($value);
	}

	/**
	 * Gets the value of the correo field
	 */
	function getCorreo() {
		return $this->correo;
	}

	/**
	 * Sets the value of the correo field
	 * @return Usuarios
	 */
	function setCorreo($value) {
		return $this->setColumnValue('correo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Usuarios
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Usuarios
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Usuarios
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Usuarios::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Usuarios::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setCreatedAt
	 * @return Usuarios
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Usuarios
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Usuarios::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Usuarios::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setUpdatedAt
	 * @return Usuarios
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Usuarios
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Usuarios::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Usuarios::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setDeletedAt
	 * @return Usuarios
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Usuarios
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Usuarios
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveById($value) {
		return Usuarios::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a apellidos
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByApellidos($value) {
		return static::retrieveByColumn('apellidos', $value);
	}

	/**
	 * Searches the database for a row with a clave
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByClave($value) {
		return static::retrieveByColumn('clave', $value);
	}

	/**
	 * Searches the database for a row with a fecha_ingreso
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByFechaIngreso($value) {
		return static::retrieveByColumn('fecha_ingreso', $value);
	}

	/**
	 * Searches the database for a row with a fecha_salida
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByFechaSalida($value) {
		return static::retrieveByColumn('fecha_salida', $value);
	}

	/**
	 * Searches the database for a row with a esta_activo
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByEstaActivo($value) {
		return static::retrieveByColumn('esta_activo', $value);
	}

	/**
	 * Searches the database for a row with a correo
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByCorreo($value) {
		return static::retrieveByColumn('correo', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Usuarios
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->telefono = (null === $this->telefono) ? null : (int) $this->telefono;
		return $this;
	}

	/**
	 * @return Usuarios[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting permisos_usuario Objects(rows) from the permisos_usuario table
	 * with a idusuario that matches $this->id.
	 * @return Query
	 */
	function getPermisosUsuariosRelatedByIdusuarioQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_usuario', 'idusuario', 'id', $q);
	}

	/**
	 * Returns the count of PermisosUsuario Objects(rows) from the permisos_usuario table
	 * with a idusuario that matches $this->id.
	 * @return int
	 */
	function countPermisosUsuariosRelatedByIdusuario(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PermisosUsuario::doCount($this->getPermisosUsuariosRelatedByIdusuarioQuery($q));
	}

	/**
	 * Deletes the permisos_usuario Objects(rows) from the permisos_usuario table
	 * with a idusuario that matches $this->id.
	 * @return int
	 */
	function deletePermisosUsuariosRelatedByIdusuario(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PermisosUsuariosRelatedByIdusuario_c = array();
		return PermisosUsuario::doDelete($this->getPermisosUsuariosRelatedByIdusuarioQuery($q));
	}

	protected $PermisosUsuariosRelatedByIdusuario_c = array();

	/**
	 * Returns an array of PermisosUsuario objects with a idusuario
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PermisosUsuario[]
	 */
	function getPermisosUsuariosRelatedByIdusuario(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PermisosUsuariosRelatedByIdusuario_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PermisosUsuariosRelatedByIdusuario_c;
		}

		$result = PermisosUsuario::doSelect($this->getPermisosUsuariosRelatedByIdusuarioQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PermisosUsuariosRelatedByIdusuario_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting usuarios_grupo Objects(rows) from the usuarios_grupo table
	 * with a usuario_id that matches $this->id.
	 * @return Query
	 */
	function getUsuariosGruposRelatedByUsuarioIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios_grupo', 'usuario_id', 'id', $q);
	}

	/**
	 * Returns the count of UsuariosGrupo Objects(rows) from the usuarios_grupo table
	 * with a usuario_id that matches $this->id.
	 * @return int
	 */
	function countUsuariosGruposRelatedByUsuarioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return UsuariosGrupo::doCount($this->getUsuariosGruposRelatedByUsuarioIdQuery($q));
	}

	/**
	 * Deletes the usuarios_grupo Objects(rows) from the usuarios_grupo table
	 * with a usuario_id that matches $this->id.
	 * @return int
	 */
	function deleteUsuariosGruposRelatedByUsuarioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->UsuariosGruposRelatedByUsuarioId_c = array();
		return UsuariosGrupo::doDelete($this->getUsuariosGruposRelatedByUsuarioIdQuery($q));
	}

	protected $UsuariosGruposRelatedByUsuarioId_c = array();

	/**
	 * Returns an array of UsuariosGrupo objects with a usuario_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return UsuariosGrupo[]
	 */
	function getUsuariosGruposRelatedByUsuarioId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->UsuariosGruposRelatedByUsuarioId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->UsuariosGruposRelatedByUsuarioId_c;
		}

		$result = UsuariosGrupo::doSelect($this->getUsuariosGruposRelatedByUsuarioIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->UsuariosGruposRelatedByUsuarioId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Usuarios::getPermisosUsuariosRelatedByidusuario
	 * @return PermisosUsuario[]
	 * @see Usuarios::getPermisosUsuariosRelatedByIdusuario
	 */
	function getPermisosUsuarios($extra = null) {
		return $this->getPermisosUsuariosRelatedByIdusuario($extra);
	}

	/**
	  * Convenience function for Usuarios::getPermisosUsuariosRelatedByidusuarioQuery
	  * @return Query
	  * @see Usuarios::getPermisosUsuariosRelatedByidusuarioQuery
	  */
	function getPermisosUsuariosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_usuario', 'idusuario','id', $q);
	}

	/**
	  * Convenience function for Usuarios::deletePermisosUsuariosRelatedByidusuario
	  * @return int
	  * @see Usuarios::deletePermisosUsuariosRelatedByidusuario
	  */
	function deletePermisosUsuarios(Query $q = null) {
		return $this->deletePermisosUsuariosRelatedByIdusuario($q);
	}

	/**
	  * Convenience function for Usuarios::countPermisosUsuariosRelatedByidusuario
	  * @return int
	  * @see Usuarios::countPermisosUsuariosRelatedByIdusuario
	  */
	function countPermisosUsuarios(Query $q = null) {
		return $this->countPermisosUsuariosRelatedByIdusuario($q);
	}

	/**
	 * Convenience function for Usuarios::getUsuariosGruposRelatedByusuario_id
	 * @return UsuariosGrupo[]
	 * @see Usuarios::getUsuariosGruposRelatedByUsuarioId
	 */
	function getUsuariosGrupos($extra = null) {
		return $this->getUsuariosGruposRelatedByUsuarioId($extra);
	}

	/**
	  * Convenience function for Usuarios::getUsuariosGruposRelatedByusuario_idQuery
	  * @return Query
	  * @see Usuarios::getUsuariosGruposRelatedByusuario_idQuery
	  */
	function getUsuariosGruposQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios_grupo', 'usuario_id','id', $q);
	}

	/**
	  * Convenience function for Usuarios::deleteUsuariosGruposRelatedByusuario_id
	  * @return int
	  * @see Usuarios::deleteUsuariosGruposRelatedByusuario_id
	  */
	function deleteUsuariosGrupos(Query $q = null) {
		return $this->deleteUsuariosGruposRelatedByUsuarioId($q);
	}

	/**
	  * Convenience function for Usuarios::countUsuariosGruposRelatedByusuario_id
	  * @return int
	  * @see Usuarios::countUsuariosGruposRelatedByUsuarioId
	  */
	function countUsuariosGrupos(Query $q = null) {
		return $this->countUsuariosGruposRelatedByUsuarioId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombres()) {
			$this->_validationErrors[] = 'nombres must not be null';
		}
		if (null === $this->getapellidos()) {
			$this->_validationErrors[] = 'apellidos must not be null';
		}
		if (null === $this->getclave()) {
			$this->_validationErrors[] = 'clave must not be null';
		}
		if (null === $this->getcorreo()) {
			$this->_validationErrors[] = 'correo must not be null';
		}
		if (null === $this->gettelefono()) {
			$this->_validationErrors[] = 'telefono must not be null';
		}
		if (null === $this->getdireccion()) {
			$this->_validationErrors[] = 'direccion must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}