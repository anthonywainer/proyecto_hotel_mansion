<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseProductoConsumido extends ApplicationModel {

	const ID = 'producto_consumido.id';
	const PRODUCTO_ID = 'producto_consumido.producto_id';
	const HABITA_ESTADIA_ID = 'producto_consumido.habita_estadia_id';
	const CANTIDAD = 'producto_consumido.cantidad';
	const MONTO = 'producto_consumido.monto';
	const DESCUENTO='producto_consumido.descuento';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'producto_consumido';

	/**
	 * Cache of objects retrieved from the database
	 * @var ProductoConsumido[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'producto_id' => Model::COLUMN_TYPE_INTEGER,
		'habita_estadia_id' => Model::COLUMN_TYPE_INTEGER,
		'cantidad' => Model::COLUMN_TYPE_INTEGER,
		'monto' => Model::COLUMN_TYPE_FLOAT,
        'descuento'=> Model::COLUMN_TYPE_FLOAT,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `producto_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $producto_id;

	/**
	 * `habita_estadia_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $habita_estadia_id;

	/**
	 * `cantidad` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cantidad;

	/**
	 * `monto` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $monto;

    protected $descuento;
	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ProductoConsumido
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the producto_id field
	 */
	function getProductoId() {
		return $this->producto_id;
	}

	/**
	 * Sets the value of the producto_id field
	 * @return ProductoConsumido
	 */
	function setProductoId($value) {
		return $this->setColumnValue('producto_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductoConsumido::getProductoId
	 * final because getProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoConsumido::getProductoId
	 */
	final function getProducto_id() {
		return $this->getProductoId();
	}

	/**
	 * Convenience function for ProductoConsumido::setProductoId
	 * final because setProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoConsumido::setProductoId
	 * @return ProductoConsumido
	 */
	final function setProducto_id($value) {
		return $this->setProductoId($value);
	}

	/**
	 * Gets the value of the habita_estadia_id field
	 */
	function getHabitaEstadiaId() {
		return $this->habita_estadia_id;
	}

	/**
	 * Sets the value of the habita_estadia_id field
	 * @return ProductoConsumido
	 */
	function setHabitaEstadiaId($value) {
		return $this->setColumnValue('habita_estadia_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductoConsumido::getHabitaEstadiaId
	 * final because getHabitaEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoConsumido::getHabitaEstadiaId
	 */
	final function getHabita_estadia_id() {
		return $this->getHabitaEstadiaId();
	}

	/**
	 * Convenience function for ProductoConsumido::setHabitaEstadiaId
	 * final because setHabitaEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoConsumido::setHabitaEstadiaId
	 * @return ProductoConsumido
	 */
	final function setHabita_estadia_id($value) {
		return $this->setHabitaEstadiaId($value);
	}

	/**
	 * Gets the value of the cantidad field
	 */
	function getCantidad() {
		return $this->cantidad;
	}

	/**
	 * Sets the value of the cantidad field
	 * @return ProductoConsumido
	 */
	function setCantidad($value) {
		return $this->setColumnValue('cantidad', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the monto field
	 */
	function getMonto() {
		return $this->monto;
	}

	/**
	 * Sets the value of the monto field
	 * @return ProductoConsumido
	 */
	function setMonto($value) {
		return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_FLOAT);
	}

	function getDescuento(){
        return $this->descuento;
    }

    function setDescuento($value) {
        return $this->setColumnValue('descuento', $value, Model::COLUMN_TYPE_FLOAT);
    }

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ProductoConsumido
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ProductoConsumido
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ProductoConsumido
	 */
	static function retrieveById($value) {
		return ProductoConsumido::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a producto_id
	 * value that matches the one provided
	 * @return ProductoConsumido
	 */
	static function retrieveByProductoId($value) {
		return static::retrieveByColumn('producto_id', $value);
	}

	/**
	 * Searches the database for a row with a habita_estadia_id
	 * value that matches the one provided
	 * @return ProductoConsumido
	 */
	static function retrieveByHabitaEstadiaId($value) {
		return static::retrieveByColumn('habita_estadia_id', $value);
	}

	/**
	 * Searches the database for a row with a cantidad
	 * value that matches the one provided
	 * @return ProductoConsumido
	 */
	static function retrieveByCantidad($value) {
		return static::retrieveByColumn('cantidad', $value);
	}

	/**
	 * Searches the database for a row with a monto
	 * value that matches the one provided
	 * @return ProductoConsumido
	 */
	static function retrieveByMonto($value) {
		return static::retrieveByColumn('monto', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ProductoConsumido
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->producto_id = (null === $this->producto_id) ? null : (int) $this->producto_id;
		$this->habita_estadia_id = (null === $this->habita_estadia_id) ? null : (int) $this->habita_estadia_id;
		$this->cantidad = (null === $this->cantidad) ? null : (int) $this->cantidad;
		return $this;
	}

	/**
	 * @return ProductoConsumido
	 */
	function setHabitaEstadia(EstadiasHabitaciones $estadiashabitaciones = null) {
		return $this->setEstadiasHabitacionesRelatedByHabitaEstadiaId($estadiashabitaciones);
	}

	/**
	 * @return ProductoConsumido
	 */
	function setEstadiasHabitacionesRelatedByHabitaEstadiaId(EstadiasHabitaciones $estadiashabitaciones = null) {
		if (null === $estadiashabitaciones) {
			$this->sethabita_estadia_id(null);
		} else {
			if (!$estadiashabitaciones->getid()) {
				throw new Exception('Cannot connect a EstadiasHabitaciones without a id');
			}
			$this->sethabita_estadia_id($estadiashabitaciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a estadias_habitaciones object with a id
	 * that matches $this->habita_estadia_id.
	 * @return EstadiasHabitaciones
	 */
	function getHabitaEstadia() {
		return $this->getEstadiasHabitacionesRelatedByHabitaEstadiaId();
	}

	/**
	 * Returns a estadias_habitaciones object with a id
	 * that matches $this->habita_estadia_id.
	 * @return EstadiasHabitaciones
	 */
	function getEstadiasHabitacionesRelatedByHabitaEstadiaId() {
		$fk_value = $this->gethabita_estadia_id();
		if (null === $fk_value) {
			return null;
		}
		return EstadiasHabitaciones::retrieveByPK($fk_value);
	}

	static function doSelectJoinHabitaEstadia(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinEstadiasHabitacionesRelatedByHabitaEstadiaId($q, $join_type);
	}

	/**
	 * Returns a estadias_habitaciones object with a id
	 * that matches $this->habita_estadia_id.
	 * @return EstadiasHabitaciones
	 */
	function getEstadiasHabitaciones() {
		return $this->getEstadiasHabitacionesRelatedByHabitaEstadiaId();
	}

	/**
	 * @return ProductoConsumido
	 */
	function setEstadiasHabitaciones(EstadiasHabitaciones $estadiashabitaciones = null) {
		return $this->setEstadiasHabitacionesRelatedByHabitaEstadiaId($estadiashabitaciones);
	}

	/**
	 * @return ProductoConsumido[]
	 */
	static function doSelectJoinEstadiasHabitacionesRelatedByHabitaEstadiaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = EstadiasHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.habita_estadia_id = ' . $to_table . '.id', $join_type);
		foreach (EstadiasHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('EstadiasHabitaciones'));
	}

	/**
	 * @return ProductoConsumido
	 */
	function setProducto(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductoConsumido
	 */
	function setProductosRelatedByProductoId(Productos $productos = null) {
		if (null === $productos) {
			$this->setproducto_id(null);
		} else {
			if (!$productos->getid()) {
				throw new Exception('Cannot connect a Productos without a id');
			}
			$this->setproducto_id($productos->getid());
		}
		return $this;
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProducto() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductosRelatedByProductoId() {
		$fk_value = $this->getproducto_id();
		if (null === $fk_value) {
			return null;
		}
		return Productos::retrieveByPK($fk_value);
	}

	static function doSelectJoinProducto(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinProductosRelatedByProductoId($q, $join_type);
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductos() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * @return ProductoConsumido
	 */
	function setProductos(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductoConsumido[]
	 */
	static function doSelectJoinProductosRelatedByProductoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Productos'));
	}

	/**
	 * @return ProductoConsumido[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = EstadiasHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.habita_estadia_id = ' . $to_table . '.id', $join_type);
		foreach (EstadiasHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'EstadiasHabitaciones';
	
		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Productos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getproducto_id()) {
			$this->_validationErrors[] = 'producto_id must not be null';
		}
		if (null === $this->gethabita_estadia_id()) {
			$this->_validationErrors[] = 'habita_estadia_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}