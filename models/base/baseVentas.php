<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVentas extends ApplicationModel {

    const ID = 'ventas.id';
    const ESTADO = 'ventas.estado';
    const ESTADIA_ID = 'ventas.estadia_id';
    const CREATED_AT = 'ventas.created_at';
    const UPDATED_AT = 'ventas.updated_at';
    const DELETED_AT = 'ventas.deleted_at';
    const MOVIMIENTO_ID = 'ventas.movimiento_id';
    const MONTO = 'ventas.monto';
    const CUOTA = 'ventas.cuota';
    const RESTO = 'ventas.resto';

    /**
     * Name of the table
     * @var string
     */
    protected static $_tableName = 'ventas';

    /**
     * Cache of objects retrieved from the database
     * @var Ventas[]
     */
    protected static $_instancePool = array();

    protected static $_instancePoolCount = 0;

    protected static $_poolEnabled = true;

    /**
     * Array of objects to batch insert
     */
    protected static $_insertBatch = array();

    /**
     * Maximum size of the insert batch
     */
    protected static $_insertBatchSize = 500;

    /**
     * Array of all primary keys
     * @var string[]
     */
    protected static $_primaryKeys = array(
        'id',
    );

    /**
     * true if primary key is an auto-increment column
     * @var bool
     */
    protected static $_isAutoIncrement = true;

    /**
     * array of all column types
     * @var string[]
     */
    protected static $_columns = array(
        'id' => Model::COLUMN_TYPE_INTEGER,
        'estado' => Model::COLUMN_TYPE_FLOAT,
        'estadia_id' => Model::COLUMN_TYPE_INTEGER,
        'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
        'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
        'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
        'movimiento_id' => Model::COLUMN_TYPE_INTEGER,
        'monto' => Model::COLUMN_TYPE_DECIMAL,
        'cuota' => Model::COLUMN_TYPE_DECIMAL,
        'resto' => Model::COLUMN_TYPE_DECIMAL,
    );

    /**
     * `id` INTEGER NOT NULL DEFAULT ''
     * @var int
     */
    protected $id;

    /**
     * `estado` FLOAT NOT NULL DEFAULT 1
     * @var double
     */
    protected $estado = 1;

    /**
     * `estadia_id` INTEGER DEFAULT ''
     * @var int
     */
    protected $estadia_id;

    /**
     * `created_at` TIMESTAMP
     * @var string
     */
    protected $created_at;

    /**
     * `updated_at` TIMESTAMP
     * @var string
     */
    protected $updated_at;

    /**
     * `deleted_at` TIMESTAMP
     * @var string
     */
    protected $deleted_at;

    /**
     * `movimiento_id` INTEGER DEFAULT ''
     * @var int
     */
    protected $movimiento_id;

    /**
     * `monto` DECIMAL DEFAULT 0
     * @var string
     */
    protected $monto = 0;

    /**
     * `cuota` DECIMAL DEFAULT '0.00'
     * @var string
     */
    protected $cuota = 0.00;

    /**
     * `resto` DECIMAL DEFAULT '0.00'
     * @var string
     */
    protected $resto = 0.00;

    /**
     * Gets the value of the id field
     */
    function getId() {
        return $this->id;
    }

    /**
     * Sets the value of the id field
     * @return Ventas
     */
    function setId($value) {
        return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
    }

    /**
     * Gets the value of the estado field
     */
    function getEstado() {
        return $this->estado;
    }

    /**
     * Sets the value of the estado field
     * @return Ventas
     */
    function setEstado($value) {
        return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_FLOAT);
    }

    /**
     * Gets the value of the estadia_id field
     */
    function getEstadiaId() {
        return $this->estadia_id;
    }

    /**
     * Sets the value of the estadia_id field
     * @return Ventas
     */
    function setEstadiaId($value) {
        return $this->setColumnValue('estadia_id', $value, Model::COLUMN_TYPE_INTEGER);
    }

    /**
     * Convenience function for Ventas::getEstadiaId
     * final because getEstadiaId should be extended instead
     * to ensure consistent behavior
     * @see Ventas::getEstadiaId
     */
    final function getEstadia_id() {
        return $this->getEstadiaId();
    }

    /**
     * Convenience function for Ventas::setEstadiaId
     * final because setEstadiaId should be extended instead
     * to ensure consistent behavior
     * @see Ventas::setEstadiaId
     * @return Ventas
     */
    final function setEstadia_id($value) {
        return $this->setEstadiaId($value);
    }

    /**
     * Gets the value of the created_at field
     */
    function getCreatedAt($format = null) {
        if (null === $this->created_at || null === $format) {
            return $this->created_at;
        }
        if (0 === strpos($this->created_at, '0000-00-00')) {
            return null;
        }
        return date($format, strtotime($this->created_at));
    }

    /**
     * Sets the value of the created_at field
     * @return Ventas
     */
    function setCreatedAt($value) {
        return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
    }

    /**
     * Convenience function for Ventas::getCreatedAt
     * final because getCreatedAt should be extended instead
     * to ensure consistent behavior
     * @see Ventas::getCreatedAt
     */
    final function getCreated_at($format = null) {
        return $this->getCreatedAt($format);
    }

    /**
     * Convenience function for Ventas::setCreatedAt
     * final because setCreatedAt should be extended instead
     * to ensure consistent behavior
     * @see Ventas::setCreatedAt
     * @return Ventas
     */
    final function setCreated_at($value) {
        return $this->setCreatedAt($value);
    }

    /**
     * Gets the value of the updated_at field
     */
    function getUpdatedAt($format = null) {
        if (null === $this->updated_at || null === $format) {
            return $this->updated_at;
        }
        if (0 === strpos($this->updated_at, '0000-00-00')) {
            return null;
        }
        return date($format, strtotime($this->updated_at));
    }

    /**
     * Sets the value of the updated_at field
     * @return Ventas
     */
    function setUpdatedAt($value) {
        return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
    }

    /**
     * Convenience function for Ventas::getUpdatedAt
     * final because getUpdatedAt should be extended instead
     * to ensure consistent behavior
     * @see Ventas::getUpdatedAt
     */
    final function getUpdated_at($format = null) {
        return $this->getUpdatedAt($format);
    }

    /**
     * Convenience function for Ventas::setUpdatedAt
     * final because setUpdatedAt should be extended instead
     * to ensure consistent behavior
     * @see Ventas::setUpdatedAt
     * @return Ventas
     */
    final function setUpdated_at($value) {
        return $this->setUpdatedAt($value);
    }

    /**
     * Gets the value of the deleted_at field
     */
    function getDeletedAt($format = null) {
        if (null === $this->deleted_at || null === $format) {
            return $this->deleted_at;
        }
        if (0 === strpos($this->deleted_at, '0000-00-00')) {
            return null;
        }
        return date($format, strtotime($this->deleted_at));
    }

    /**
     * Sets the value of the deleted_at field
     * @return Ventas
     */
    function setDeletedAt($value) {
        return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
    }

    /**
     * Convenience function for Ventas::getDeletedAt
     * final because getDeletedAt should be extended instead
     * to ensure consistent behavior
     * @see Ventas::getDeletedAt
     */
    final function getDeleted_at($format = null) {
        return $this->getDeletedAt($format);
    }

    /**
     * Convenience function for Ventas::setDeletedAt
     * final because setDeletedAt should be extended instead
     * to ensure consistent behavior
     * @see Ventas::setDeletedAt
     * @return Ventas
     */
    final function setDeleted_at($value) {
        return $this->setDeletedAt($value);
    }

    /**
     * Gets the value of the movimiento_id field
     */
    function getMovimientoId() {
        return $this->movimiento_id;
    }

    /**
     * Sets the value of the movimiento_id field
     * @return Ventas
     */
    function setMovimientoId($value) {
        return $this->setColumnValue('movimiento_id', $value, Model::COLUMN_TYPE_INTEGER);
    }

    /**
     * Convenience function for Ventas::getMovimientoId
     * final because getMovimientoId should be extended instead
     * to ensure consistent behavior
     * @see Ventas::getMovimientoId
     */
    final function getMovimiento_id() {
        return $this->getMovimientoId();
    }

    /**
     * Convenience function for Ventas::setMovimientoId
     * final because setMovimientoId should be extended instead
     * to ensure consistent behavior
     * @see Ventas::setMovimientoId
     * @return Ventas
     */
    final function setMovimiento_id($value) {
        return $this->setMovimientoId($value);
    }

    /**
     * Gets the value of the monto field
     */
    function getMonto() {
        return $this->monto;
    }

    /**
     * Sets the value of the monto field
     * @return Ventas
     */
    function setMonto($value) {
        return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_DECIMAL);
    }

    /**
     * Gets the value of the cuota field
     */
    function getCuota() {
        return $this->cuota;
    }

    /**
     * Sets the value of the cuota field
     * @return Ventas
     */
    function setCuota($value) {
        return $this->setColumnValue('cuota', $value, Model::COLUMN_TYPE_DECIMAL);
    }

    /**
     * Gets the value of the resto field
     */
    function getResto() {
        return $this->resto;
    }

    /**
     * Sets the value of the resto field
     * @return Ventas
     */
    function setResto($value) {
        return $this->setColumnValue('resto', $value, Model::COLUMN_TYPE_DECIMAL);
    }

    /**
     * @return DABLPDO
     */
    static function getConnection() {
        return DBManager::getConnection('default_connection');
    }

    /**
     * Searches the database for a row with the ID(primary key) that matches
     * the one input.
     * @return Ventas
     */
    static function retrieveByPK($id) {
        return static::retrieveByPKs($id);
    }

    /**
     * Searches the database for a row with the primary keys that match
     * the ones input.
     * @return Ventas
     */
    static function retrieveByPKs($id) {
        if (null === $id) {
            return null;
        }
        if (static::$_poolEnabled) {
            $pool_instance = static::retrieveFromPool($id);
            if (null !== $pool_instance) {
                return $pool_instance;
            }
        }
        $q = new Query;
        $q->add('id', $id);
        return static::doSelectOne($q);
    }

    /**
     * Searches the database for a row with a id
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveById($value) {
        return Ventas::retrieveByPK($value);
    }

    /**
     * Searches the database for a row with a estado
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByEstado($value) {
        return static::retrieveByColumn('estado', $value);
    }

    /**
     * Searches the database for a row with a estadia_id
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByEstadiaId($value) {
        return static::retrieveByColumn('estadia_id', $value);
    }

    /**
     * Searches the database for a row with a created_at
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByCreatedAt($value) {
        return static::retrieveByColumn('created_at', $value);
    }

    /**
     * Searches the database for a row with a updated_at
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByUpdatedAt($value) {
        return static::retrieveByColumn('updated_at', $value);
    }

    /**
     * Searches the database for a row with a deleted_at
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByDeletedAt($value) {
        return static::retrieveByColumn('deleted_at', $value);
    }

    /**
     * Searches the database for a row with a movimiento_id
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByMovimientoId($value) {
        return static::retrieveByColumn('movimiento_id', $value);
    }

    /**
     * Searches the database for a row with a monto
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByMonto($value) {
        return static::retrieveByColumn('monto', $value);
    }

    /**
     * Searches the database for a row with a cuota
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByCuota($value) {
        return static::retrieveByColumn('cuota', $value);
    }

    /**
     * Searches the database for a row with a resto
     * value that matches the one provided
     * @return Ventas
     */
    static function retrieveByResto($value) {
        return static::retrieveByColumn('resto', $value);
    }


    /**
     * Casts values of int fields to (int)
     * @return Ventas
     */
    function castInts() {
        $this->id = (null === $this->id) ? null : (int) $this->id;
        $this->estadia_id = (null === $this->estadia_id) ? null : (int) $this->estadia_id;
        $this->movimiento_id = (null === $this->movimiento_id) ? null : (int) $this->movimiento_id;
        return $this;
    }

    /**
     * @return Ventas
     */
    function setMovimiento(MovimientosDeDinero $movimientosdedinero = null) {
        return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
    }

    /**
     * @return Ventas
     */
    function setMovimientosDeDineroRelatedByMovimientoId(MovimientosDeDinero $movimientosdedinero = null) {
        if (null === $movimientosdedinero) {
            $this->setmovimiento_id(null);
        } else {
            if (!$movimientosdedinero->getid()) {
                throw new Exception('Cannot connect a MovimientosDeDinero without a id');
            }
            $this->setmovimiento_id($movimientosdedinero->getid());
        }
        return $this;
    }

    /**
     * Returns a movimientos_de_dinero object with a id
     * that matches $this->movimiento_id.
     * @return MovimientosDeDinero
     */
    function getMovimiento() {
        return $this->getMovimientosDeDineroRelatedByMovimientoId();
    }

    /**
     * Returns a movimientos_de_dinero object with a id
     * that matches $this->movimiento_id.
     * @return MovimientosDeDinero
     */
    function getMovimientosDeDineroRelatedByMovimientoId() {
        $fk_value = $this->getmovimiento_id();
        if (null === $fk_value) {
            return null;
        }
        return MovimientosDeDinero::retrieveByPK($fk_value);
    }

    static function doSelectJoinMovimiento(Query $q = null, $join_type = Query::LEFT_JOIN) {
        return static::doSelectJoinMovimientosDeDineroRelatedByMovimientoId($q, $join_type);
    }

    /**
     * Returns a movimientos_de_dinero object with a id
     * that matches $this->movimiento_id.
     * @return MovimientosDeDinero
     */
    function getMovimientosDeDinero() {
        return $this->getMovimientosDeDineroRelatedByMovimientoId();
    }

    /**
     * @return Ventas
     */
    function setMovimientosDeDinero(MovimientosDeDinero $movimientosdedinero = null) {
        return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
    }

    /**
     * @return Ventas[]
     */
    static function doSelectJoinMovimientosDeDineroRelatedByMovimientoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
        $q = $q ? clone $q : new Query;
        $columns = $q->getColumns();
        $alias = $q->getAlias();
        $this_table = $alias ? $alias : static::getTableName();
        if (!$columns) {
            if ($alias) {
                foreach (static::getColumns() as $column_name) {
                    $columns[] = $alias . '.' . $column_name;
                }
            } else {
                $columns = static::getColumns();
            }
        }

        $to_table = MovimientosDeDinero::getTableName();
        $q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
        foreach (MovimientosDeDinero::getColumns() as $column) {
            $columns[] = $column;
        }
        $q->setColumns($columns);

        return static::doSelect($q, array('MovimientosDeDinero'));
    }

    /**
     * @return Ventas
     */
    function setEstadia(Estadias $estadias = null) {
        return $this->setEstadiasRelatedByEstadiaId($estadias);
    }

    /**
     * @return Ventas
     */
    function setEstadiasRelatedByEstadiaId(Estadias $estadias = null) {
        if (null === $estadias) {
            $this->setestadia_id(null);
        } else {
            if (!$estadias->getid()) {
                throw new Exception('Cannot connect a Estadias without a id');
            }
            $this->setestadia_id($estadias->getid());
        }
        return $this;
    }

    /**
     * Returns a estadias object with a id
     * that matches $this->estadia_id.
     * @return Estadias
     */
    function getEstadia() {
        return $this->getEstadiasRelatedByEstadiaId();
    }

    /**
     * Returns a estadias object with a id
     * that matches $this->estadia_id.
     * @return Estadias
     */
    function getEstadiasRelatedByEstadiaId() {
        $fk_value = $this->getestadia_id();
        if (null === $fk_value) {
            return null;
        }
        return Estadias::retrieveByPK($fk_value);
    }

    static function doSelectJoinEstadia(Query $q = null, $join_type = Query::LEFT_JOIN) {
        return static::doSelectJoinEstadiasRelatedByEstadiaId($q, $join_type);
    }

    /**
     * Returns a estadias object with a id
     * that matches $this->estadia_id.
     * @return Estadias
     */
    function getEstadias() {
        return $this->getEstadiasRelatedByEstadiaId();
    }

    /**
     * @return Ventas
     */
    function setEstadias(Estadias $estadias = null) {
        return $this->setEstadiasRelatedByEstadiaId($estadias);
    }

    /**
     * @return Ventas[]
     */
    static function doSelectJoinEstadiasRelatedByEstadiaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
        $q = $q ? clone $q : new Query;
        $columns = $q->getColumns();
        $alias = $q->getAlias();
        $this_table = $alias ? $alias : static::getTableName();
        if (!$columns) {
            if ($alias) {
                foreach (static::getColumns() as $column_name) {
                    $columns[] = $alias . '.' . $column_name;
                }
            } else {
                $columns = static::getColumns();
            }
        }

        $to_table = Estadias::getTableName();
        $q->join($to_table, $this_table . '.estadia_id = ' . $to_table . '.id', $join_type);
        foreach (Estadias::getColumns() as $column) {
            $columns[] = $column;
        }
        $q->setColumns($columns);

        return static::doSelect($q, array('Estadias'));
    }

    /**
     * @return Ventas[]
     */
    static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
        $q = $q ? clone $q : new Query;
        $columns = $q->getColumns();
        $classes = array();
        $alias = $q->getAlias();
        $this_table = $alias ? $alias : static::getTableName();
        if (!$columns) {
            if ($alias) {
                foreach (static::getColumns() as $column_name) {
                    $columns[] = $alias . '.' . $column_name;
                }
            } else {
                $columns = static::getColumns();
            }
        }

        $to_table = MovimientosDeDinero::getTableName();
        $q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
        foreach (MovimientosDeDinero::getColumns() as $column) {
            $columns[] = $column;
        }
        $classes[] = 'MovimientosDeDinero';

        $to_table = Estadias::getTableName();
        $q->join($to_table, $this_table . '.estadia_id = ' . $to_table . '.id', $join_type);
        foreach (Estadias::getColumns() as $column) {
            $columns[] = $column;
        }
        $classes[] = 'Estadias';

        $q->setColumns($columns);
        return static::doSelect($q, $classes);
    }

    /**
     * Returns true if the column values validate.
     * @return bool
     */
    function validate() {
        $this->_validationErrors = array();
        return 0 === count($this->_validationErrors);
    }

}