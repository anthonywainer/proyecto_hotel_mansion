<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseClientes extends ApplicationModel {

	const ID = 'clientes.id';
	const RUC = 'clientes.ruc';
	const NOMBRES = 'clientes.nombres';
	const APELLIDOS = 'clientes.apellidos';
	const DNI = 'clientes.dni';
	const DIRECCION = 'clientes.direccion';
	const TELEFONO = 'clientes.telefono';
	const PROCEDENCIA = 'clientes.procedencia';
	const SEXO = 'clientes.sexo';
	const CREATED_AT = 'clientes.created_at';
	const UPDATED_AT = 'clientes.updated_at';
	const DELETED_AT = 'clientes.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'clientes';

	/**
	 * Cache of objects retrieved from the database
	 * @var Clientes[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'ruc' => Model::COLUMN_TYPE_VARCHAR,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'apellidos' => Model::COLUMN_TYPE_VARCHAR,
		'dni' => Model::COLUMN_TYPE_VARCHAR,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'telefono' => Model::COLUMN_TYPE_VARCHAR,
		'procedencia' => Model::COLUMN_TYPE_VARCHAR,
		'sexo' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `ruc` VARCHAR
	 * @var string
	 */
	protected $ruc;

	/**
	 * `nombres` VARCHAR
	 * @var string
	 */
	protected $nombres;

	/**
	 * `apellidos` VARCHAR
	 * @var string
	 */
	protected $apellidos;

	/**
	 * `dni` VARCHAR
	 * @var string
	 */
	protected $dni;

	/**
	 * `direccion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $direccion;

	/**
	 * `telefono` VARCHAR
	 * @var string
	 */
	protected $telefono;

	/**
	 * `procedencia` VARCHAR
	 * @var string
	 */
	protected $procedencia;

	/**
	 * `sexo` VARCHAR
	 * @var string
	 */
	protected $sexo;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Clientes
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the ruc field
	 */
	function getRuc() {
		return $this->ruc;
	}

	/**
	 * Sets the value of the ruc field
	 * @return Clientes
	 */
	function setRuc($value) {
		return $this->setColumnValue('ruc', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Clientes
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the apellidos field
	 */
	function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * Sets the value of the apellidos field
	 * @return Clientes
	 */
	function setApellidos($value) {
		return $this->setColumnValue('apellidos', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the dni field
	 */
	function getDni() {
		return $this->dni;
	}

	/**
	 * Sets the value of the dni field
	 * @return Clientes
	 */
	function setDni($value) {
		return $this->setColumnValue('dni', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Clientes
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Clientes
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the procedencia field
	 */
	function getProcedencia() {
		return $this->procedencia;
	}

	/**
	 * Sets the value of the procedencia field
	 * @return Clientes
	 */
	function setProcedencia($value) {
		return $this->setColumnValue('procedencia', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the sexo field
	 */
	function getSexo() {
		return $this->sexo;
	}

	/**
	 * Sets the value of the sexo field
	 * @return Clientes
	 */
	function setSexo($value) {
		return $this->setColumnValue('sexo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Clientes
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Clientes::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Clientes::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::setCreatedAt
	 * @return Clientes
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Clientes
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Clientes::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Clientes::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::setUpdatedAt
	 * @return Clientes
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Clientes
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Clientes::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Clientes::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Clientes::setDeletedAt
	 * @return Clientes
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Clientes
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Clientes
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveById($value) {
		return Clientes::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a ruc
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByRuc($value) {
		return static::retrieveByColumn('ruc', $value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a apellidos
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByApellidos($value) {
		return static::retrieveByColumn('apellidos', $value);
	}

	/**
	 * Searches the database for a row with a dni
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByDni($value) {
		return static::retrieveByColumn('dni', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a procedencia
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByProcedencia($value) {
		return static::retrieveByColumn('procedencia', $value);
	}

	/**
	 * Searches the database for a row with a sexo
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveBySexo($value) {
		return static::retrieveByColumn('sexo', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Clientes
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Clientes
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Clientes[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting clientes_estadias Objects(rows) from the clientes_estadias table
	 * with a cliente_id that matches $this->id.
	 * @return Query
	 */
	function getClientesEstadiassRelatedByClienteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_estadias', 'cliente_id', 'id', $q);
	}

	/**
	 * Returns the count of ClientesEstadias Objects(rows) from the clientes_estadias table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function countClientesEstadiassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ClientesEstadias::doCount($this->getClientesEstadiassRelatedByClienteIdQuery($q));
	}

	/**
	 * Deletes the clientes_estadias Objects(rows) from the clientes_estadias table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function deleteClientesEstadiassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ClientesEstadiassRelatedByClienteId_c = array();
		return ClientesEstadias::doDelete($this->getClientesEstadiassRelatedByClienteIdQuery($q));
	}

	protected $ClientesEstadiassRelatedByClienteId_c = array();

	/**
	 * Returns an array of ClientesEstadias objects with a cliente_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ClientesEstadias[]
	 */
	function getClientesEstadiassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ClientesEstadiassRelatedByClienteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ClientesEstadiassRelatedByClienteId_c;
		}

		$result = ClientesEstadias::doSelect($this->getClientesEstadiassRelatedByClienteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ClientesEstadiassRelatedByClienteId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting clientes_habitaciones Objects(rows) from the clientes_habitaciones table
	 * with a cliente_id that matches $this->id.
	 * @return Query
	 */
	function getClientesHabitacionessRelatedByClienteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_habitaciones', 'cliente_id', 'id', $q);
	}

	/**
	 * Returns the count of ClientesHabitaciones Objects(rows) from the clientes_habitaciones table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function countClientesHabitacionessRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ClientesHabitaciones::doCount($this->getClientesHabitacionessRelatedByClienteIdQuery($q));
	}

	/**
	 * Deletes the clientes_habitaciones Objects(rows) from the clientes_habitaciones table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function deleteClientesHabitacionessRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ClientesHabitacionessRelatedByClienteId_c = array();
		return ClientesHabitaciones::doDelete($this->getClientesHabitacionessRelatedByClienteIdQuery($q));
	}

	protected $ClientesHabitacionessRelatedByClienteId_c = array();

	/**
	 * Returns an array of ClientesHabitaciones objects with a cliente_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ClientesHabitaciones[]
	 */
	function getClientesHabitacionessRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ClientesHabitacionessRelatedByClienteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ClientesHabitacionessRelatedByClienteId_c;
		}

		$result = ClientesHabitaciones::doSelect($this->getClientesHabitacionessRelatedByClienteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ClientesHabitacionessRelatedByClienteId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting estadias Objects(rows) from the estadias table
	 * with a cliente_id that matches $this->id.
	 * @return Query
	 */
	function getEstadiassRelatedByClienteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('estadias', 'cliente_id', 'id', $q);
	}

	/**
	 * Returns the count of Estadias Objects(rows) from the estadias table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function countEstadiassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Estadias::doCount($this->getEstadiassRelatedByClienteIdQuery($q));
	}

	/**
	 * Deletes the estadias Objects(rows) from the estadias table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function deleteEstadiassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->EstadiassRelatedByClienteId_c = array();
		return Estadias::doDelete($this->getEstadiassRelatedByClienteIdQuery($q));
	}

	protected $EstadiassRelatedByClienteId_c = array();

	/**
	 * Returns an array of Estadias objects with a cliente_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Estadias[]
	 */
	function getEstadiassRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->EstadiassRelatedByClienteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->EstadiassRelatedByClienteId_c;
		}

		$result = Estadias::doSelect($this->getEstadiassRelatedByClienteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->EstadiassRelatedByClienteId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a cliente_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientosDeDinerosRelatedByClienteIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'cliente_id', 'id', $q);
	}

	/**
	 * Returns the count of MovimientosDeDinero Objects(rows) from the movimientos_de_dinero table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function countMovimientosDeDinerosRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MovimientosDeDinero::doCount($this->getMovimientosDeDinerosRelatedByClienteIdQuery($q));
	}

	/**
	 * Deletes the movimientos_de_dinero Objects(rows) from the movimientos_de_dinero table
	 * with a cliente_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientosDeDinerosRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientosDeDinerosRelatedByClienteId_c = array();
		return MovimientosDeDinero::doDelete($this->getMovimientosDeDinerosRelatedByClienteIdQuery($q));
	}

	protected $MovimientosDeDinerosRelatedByClienteId_c = array();

	/**
	 * Returns an array of MovimientosDeDinero objects with a cliente_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MovimientosDeDinero[]
	 */
	function getMovimientosDeDinerosRelatedByClienteId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientosDeDinerosRelatedByClienteId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientosDeDinerosRelatedByClienteId_c;
		}

		$result = MovimientosDeDinero::doSelect($this->getMovimientosDeDinerosRelatedByClienteIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientosDeDinerosRelatedByClienteId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Clientes::getClientesEstadiassRelatedBycliente_id
	 * @return ClientesEstadias[]
	 * @see Clientes::getClientesEstadiassRelatedByClienteId
	 */
	function getClientesEstadiass($extra = null) {
		return $this->getClientesEstadiassRelatedByClienteId($extra);
	}

	/**
	  * Convenience function for Clientes::getClientesEstadiassRelatedBycliente_idQuery
	  * @return Query
	  * @see Clientes::getClientesEstadiassRelatedBycliente_idQuery
	  */
	function getClientesEstadiassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_estadias', 'cliente_id','id', $q);
	}

	/**
	  * Convenience function for Clientes::deleteClientesEstadiassRelatedBycliente_id
	  * @return int
	  * @see Clientes::deleteClientesEstadiassRelatedBycliente_id
	  */
	function deleteClientesEstadiass(Query $q = null) {
		return $this->deleteClientesEstadiassRelatedByClienteId($q);
	}

	/**
	  * Convenience function for Clientes::countClientesEstadiassRelatedBycliente_id
	  * @return int
	  * @see Clientes::countClientesEstadiassRelatedByClienteId
	  */
	function countClientesEstadiass(Query $q = null) {
		return $this->countClientesEstadiassRelatedByClienteId($q);
	}

	/**
	 * Convenience function for Clientes::getClientesHabitacionessRelatedBycliente_id
	 * @return ClientesHabitaciones[]
	 * @see Clientes::getClientesHabitacionessRelatedByClienteId
	 */
	function getClientesHabitacioness($extra = null) {
		return $this->getClientesHabitacionessRelatedByClienteId($extra);
	}

	/**
	  * Convenience function for Clientes::getClientesHabitacionessRelatedBycliente_idQuery
	  * @return Query
	  * @see Clientes::getClientesHabitacionessRelatedBycliente_idQuery
	  */
	function getClientesHabitacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_habitaciones', 'cliente_id','id', $q);
	}

	/**
	  * Convenience function for Clientes::deleteClientesHabitacionessRelatedBycliente_id
	  * @return int
	  * @see Clientes::deleteClientesHabitacionessRelatedBycliente_id
	  */
	function deleteClientesHabitacioness(Query $q = null) {
		return $this->deleteClientesHabitacionessRelatedByClienteId($q);
	}

	/**
	  * Convenience function for Clientes::countClientesHabitacionessRelatedBycliente_id
	  * @return int
	  * @see Clientes::countClientesHabitacionessRelatedByClienteId
	  */
	function countClientesHabitacioness(Query $q = null) {
		return $this->countClientesHabitacionessRelatedByClienteId($q);
	}

	/**
	 * Convenience function for Clientes::getEstadiassRelatedBycliente_id
	 * @return Estadias[]
	 * @see Clientes::getEstadiassRelatedByClienteId
	 */
	function getEstadiass($extra = null) {
		return $this->getEstadiassRelatedByClienteId($extra);
	}

	/**
	  * Convenience function for Clientes::getEstadiassRelatedBycliente_idQuery
	  * @return Query
	  * @see Clientes::getEstadiassRelatedBycliente_idQuery
	  */
	function getEstadiassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('estadias', 'cliente_id','id', $q);
	}

	/**
	  * Convenience function for Clientes::deleteEstadiassRelatedBycliente_id
	  * @return int
	  * @see Clientes::deleteEstadiassRelatedBycliente_id
	  */
	function deleteEstadiass(Query $q = null) {
		return $this->deleteEstadiassRelatedByClienteId($q);
	}

	/**
	  * Convenience function for Clientes::countEstadiassRelatedBycliente_id
	  * @return int
	  * @see Clientes::countEstadiassRelatedByClienteId
	  */
	function countEstadiass(Query $q = null) {
		return $this->countEstadiassRelatedByClienteId($q);
	}

	/**
	 * Convenience function for Clientes::getMovimientosDeDinerosRelatedBycliente_id
	 * @return MovimientosDeDinero[]
	 * @see Clientes::getMovimientosDeDinerosRelatedByClienteId
	 */
	function getMovimientosDeDineros($extra = null) {
		return $this->getMovimientosDeDinerosRelatedByClienteId($extra);
	}

	/**
	  * Convenience function for Clientes::getMovimientosDeDinerosRelatedBycliente_idQuery
	  * @return Query
	  * @see Clientes::getMovimientosDeDinerosRelatedBycliente_idQuery
	  */
	function getMovimientosDeDinerosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos_de_dinero', 'cliente_id','id', $q);
	}

	/**
	  * Convenience function for Clientes::deleteMovimientosDeDinerosRelatedBycliente_id
	  * @return int
	  * @see Clientes::deleteMovimientosDeDinerosRelatedBycliente_id
	  */
	function deleteMovimientosDeDineros(Query $q = null) {
		return $this->deleteMovimientosDeDinerosRelatedByClienteId($q);
	}

	/**
	  * Convenience function for Clientes::countMovimientosDeDinerosRelatedBycliente_id
	  * @return int
	  * @see Clientes::countMovimientosDeDinerosRelatedByClienteId
	  */
	function countMovimientosDeDineros(Query $q = null) {
		return $this->countMovimientosDeDinerosRelatedByClienteId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdireccion()) {
			$this->_validationErrors[] = 'direccion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}