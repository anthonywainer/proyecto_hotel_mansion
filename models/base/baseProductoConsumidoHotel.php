<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseProductoConsumidoHotel extends ApplicationModel {

	const ID = 'producto_consumido_hotel.id';
	const PRODUCTO_ID = 'producto_consumido_hotel.producto_id';
	const HOTEL = 'producto_consumido_hotel.hotel';
	const CANTIDAD = 'producto_consumido_hotel.cantidad';
	const MONTO = 'producto_consumido_hotel.monto';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'producto_consumido_hotel';

	/**
	 * Cache of objects retrieved from the database
	 * @var ProductoConsumidoHotel[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'producto_id' => Model::COLUMN_TYPE_INTEGER,
		'hotel' => Model::COLUMN_TYPE_INTEGER,
		'cantidad' => Model::COLUMN_TYPE_INTEGER,
		'monto' => Model::COLUMN_TYPE_FLOAT,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `producto_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $producto_id;

	/**
	 * `hotel` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $hotel;

	/**
	 * `cantidad` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cantidad;

	/**
	 * `monto` FLOAT DEFAULT ''
	 * @var double
	 */
	protected $monto;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ProductoConsumidoHotel
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the producto_id field
	 */
	function getProductoId() {
		return $this->producto_id;
	}

	/**
	 * Sets the value of the producto_id field
	 * @return ProductoConsumidoHotel
	 */
	function setProductoId($value) {
		return $this->setColumnValue('producto_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductoConsumidoHotel::getProductoId
	 * final because getProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoConsumidoHotel::getProductoId
	 */
	final function getProducto_id() {
		return $this->getProductoId();
	}

	/**
	 * Convenience function for ProductoConsumidoHotel::setProductoId
	 * final because setProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductoConsumidoHotel::setProductoId
	 * @return ProductoConsumidoHotel
	 */
	final function setProducto_id($value) {
		return $this->setProductoId($value);
	}

	/**
	 * Gets the value of the hotel field
	 */
	function getHotel() {
		return $this->hotel;
	}

	/**
	 * Sets the value of the hotel field
	 * @return ProductoConsumidoHotel
	 */
	function setHotel($value) {
		return $this->setColumnValue('hotel', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the cantidad field
	 */
	function getCantidad() {
		return $this->cantidad;
	}

	/**
	 * Sets the value of the cantidad field
	 * @return ProductoConsumidoHotel
	 */
	function setCantidad($value) {
		return $this->setColumnValue('cantidad', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the monto field
	 */
	function getMonto() {
		return $this->monto;
	}

	/**
	 * Sets the value of the monto field
	 * @return ProductoConsumidoHotel
	 */
	function setMonto($value) {
		return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_FLOAT);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveById($value) {
		return ProductoConsumidoHotel::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a producto_id
	 * value that matches the one provided
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveByProductoId($value) {
		return static::retrieveByColumn('producto_id', $value);
	}

	/**
	 * Searches the database for a row with a hotel
	 * value that matches the one provided
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveByHotel($value) {
		return static::retrieveByColumn('hotel', $value);
	}

	/**
	 * Searches the database for a row with a cantidad
	 * value that matches the one provided
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveByCantidad($value) {
		return static::retrieveByColumn('cantidad', $value);
	}

	/**
	 * Searches the database for a row with a monto
	 * value that matches the one provided
	 * @return ProductoConsumidoHotel
	 */
	static function retrieveByMonto($value) {
		return static::retrieveByColumn('monto', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ProductoConsumidoHotel
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->producto_id = (null === $this->producto_id) ? null : (int) $this->producto_id;
		$this->hotel = (null === $this->hotel) ? null : (int) $this->hotel;
		$this->cantidad = (null === $this->cantidad) ? null : (int) $this->cantidad;
		return $this;
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	function setProducto(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	function setProductosRelatedByProductoId(Productos $productos = null) {
		if (null === $productos) {
			$this->setproducto_id(null);
		} else {
			if (!$productos->getid()) {
				throw new Exception('Cannot connect a Productos without a id');
			}
			$this->setproducto_id($productos->getid());
		}
		return $this;
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProducto() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductosRelatedByProductoId() {
		$fk_value = $this->getproducto_id();
		if (null === $fk_value) {
			return null;
		}
		return Productos::retrieveByPK($fk_value);
	}

	static function doSelectJoinProducto(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinProductosRelatedByProductoId($q, $join_type);
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductos() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	function setProductos(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductoConsumidoHotel[]
	 */
	static function doSelectJoinProductosRelatedByProductoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Productos'));
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	function setVentasHotelRelatedByHotel(VentasHotel $ventashotel = null) {
		if (null === $ventashotel) {
			$this->sethotel(null);
		} else {
			if (!$ventashotel->getid()) {
				throw new Exception('Cannot connect a VentasHotel without a id');
			}
			$this->sethotel($ventashotel->getid());
		}
		return $this;
	}

	/**
	 * Returns a ventas_hotel object with a id
	 * that matches $this->hotel.
	 * @return VentasHotel
	 */
	function getVentasHotelRelatedByHotel() {
		$fk_value = $this->gethotel();
		if (null === $fk_value) {
			return null;
		}
		return VentasHotel::retrieveByPK($fk_value);
	}

	/**
	 * Returns a ventas_hotel object with a id
	 * that matches $this->hotel.
	 * @return VentasHotel
	 */
	function getVentasHotel() {
		return $this->getVentasHotelRelatedByHotel();
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	function setVentasHotel(VentasHotel $ventashotel = null) {
		return $this->setVentasHotelRelatedByHotel($ventashotel);
	}

	/**
	 * @return ProductoConsumidoHotel[]
	 */
	static function doSelectJoinVentasHotelRelatedByHotel(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = VentasHotel::getTableName();
		$q->join($to_table, $this_table . '.hotel = ' . $to_table . '.id', $join_type);
		foreach (VentasHotel::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('VentasHotel'));
	}

	/**
	 * @return ProductoConsumidoHotel[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Productos';
	
		$to_table = VentasHotel::getTableName();
		$q->join($to_table, $this_table . '.hotel = ' . $to_table . '.id', $join_type);
		foreach (VentasHotel::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'VentasHotel';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getproducto_id()) {
			$this->_validationErrors[] = 'producto_id must not be null';
		}
		if (null === $this->gethotel()) {
			$this->_validationErrors[] = 'hotel must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}