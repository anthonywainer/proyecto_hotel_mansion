<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseConceptosPagos extends ApplicationModel {

	const ID = 'conceptos_pagos.id';
	const CONCEPTO = 'conceptos_pagos.concepto';
	const TIPO_ID = 'conceptos_pagos.tipo_id';
	const CREATED_AT = 'conceptos_pagos.created_at';
	const UPDATED_AT = 'conceptos_pagos.updated_at';
	const DELETED_AT = 'conceptos_pagos.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'conceptos_pagos';

	/**
	 * Cache of objects retrieved from the database
	 * @var ConceptosPagos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'concepto' => Model::COLUMN_TYPE_VARCHAR,
		'tipo_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `concepto` VARCHAR NOT NULL
	 * @var string
	 */
	protected $concepto;

	/**
	 * `tipo_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $tipo_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ConceptosPagos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the concepto field
	 */
	function getConcepto() {
		return $this->concepto;
	}

	/**
	 * Sets the value of the concepto field
	 * @return ConceptosPagos
	 */
	function setConcepto($value) {
		return $this->setColumnValue('concepto', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the tipo_id field
	 */
	function getTipoId() {
		return $this->tipo_id;
	}

	/**
	 * Sets the value of the tipo_id field
	 * @return ConceptosPagos
	 */
	function setTipoId($value) {
		return $this->setColumnValue('tipo_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ConceptosPagos::getTipoId
	 * final because getTipoId should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::getTipoId
	 */
	final function getTipo_id() {
		return $this->getTipoId();
	}

	/**
	 * Convenience function for ConceptosPagos::setTipoId
	 * final because setTipoId should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::setTipoId
	 * @return ConceptosPagos
	 */
	final function setTipo_id($value) {
		return $this->setTipoId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return ConceptosPagos
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ConceptosPagos::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for ConceptosPagos::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::setCreatedAt
	 * @return ConceptosPagos
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return ConceptosPagos
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ConceptosPagos::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for ConceptosPagos::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::setUpdatedAt
	 * @return ConceptosPagos
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return ConceptosPagos
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ConceptosPagos::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for ConceptosPagos::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptosPagos::setDeletedAt
	 * @return ConceptosPagos
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ConceptosPagos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ConceptosPagos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ConceptosPagos
	 */
	static function retrieveById($value) {
		return ConceptosPagos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a concepto
	 * value that matches the one provided
	 * @return ConceptosPagos
	 */
	static function retrieveByConcepto($value) {
		return static::retrieveByColumn('concepto', $value);
	}

	/**
	 * Searches the database for a row with a tipo_id
	 * value that matches the one provided
	 * @return ConceptosPagos
	 */
	static function retrieveByTipoId($value) {
		return static::retrieveByColumn('tipo_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return ConceptosPagos
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return ConceptosPagos
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return ConceptosPagos
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ConceptosPagos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->tipo_id = (null === $this->tipo_id) ? null : (int) $this->tipo_id;
		return $this;
	}

	/**
	 * @return ConceptosPagos
	 */
	function setTipo(Tipos $tipos = null) {
		return $this->setTiposRelatedByTipoId($tipos);
	}

	/**
	 * @return ConceptosPagos
	 */
	function setTiposRelatedByTipoId(Tipos $tipos = null) {
		if (null === $tipos) {
			$this->settipo_id(null);
		} else {
			if (!$tipos->getid()) {
				throw new Exception('Cannot connect a Tipos without a id');
			}
			$this->settipo_id($tipos->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipos object with a id
	 * that matches $this->tipo_id.
	 * @return Tipos
	 */
	function getTipo() {
		return $this->getTiposRelatedByTipoId();
	}

	/**
	 * Returns a tipos object with a id
	 * that matches $this->tipo_id.
	 * @return Tipos
	 */
	function getTiposRelatedByTipoId() {
		$fk_value = $this->gettipo_id();
		if (null === $fk_value) {
			return null;
		}
		return Tipos::retrieveByPK($fk_value);
	}

	static function doSelectJoinTipo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinTiposRelatedByTipoId($q, $join_type);
	}

	/**
	 * Returns a tipos object with a id
	 * that matches $this->tipo_id.
	 * @return Tipos
	 */
	function getTipos() {
		return $this->getTiposRelatedByTipoId();
	}

	/**
	 * @return ConceptosPagos
	 */
	function setTipos(Tipos $tipos = null) {
		return $this->setTiposRelatedByTipoId($tipos);
	}

	/**
	 * @return ConceptosPagos[]
	 */
	static function doSelectJoinTiposRelatedByTipoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Tipos::getTableName();
		$q->join($to_table, $this_table . '.tipo_id = ' . $to_table . '.id', $join_type);
		foreach (Tipos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Tipos'));
	}

	/**
	 * @return ConceptosPagos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Tipos::getTableName();
		$q->join($to_table, $this_table . '.tipo_id = ' . $to_table . '.id', $join_type);
		foreach (Tipos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Tipos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting movimientos Objects(rows) from the movimientos table
	 * with a concepto_pago_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientossRelatedByConceptoPagoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos', 'concepto_pago_id', 'id', $q);
	}

	/**
	 * Returns the count of Movimientos Objects(rows) from the movimientos table
	 * with a concepto_pago_id that matches $this->id.
	 * @return int
	 */
	function countMovimientossRelatedByConceptoPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Movimientos::doCount($this->getMovimientossRelatedByConceptoPagoIdQuery($q));
	}

	/**
	 * Deletes the movimientos Objects(rows) from the movimientos table
	 * with a concepto_pago_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientossRelatedByConceptoPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientossRelatedByConceptoPagoId_c = array();
		return Movimientos::doDelete($this->getMovimientossRelatedByConceptoPagoIdQuery($q));
	}

	protected $MovimientossRelatedByConceptoPagoId_c = array();

	/**
	 * Returns an array of Movimientos objects with a concepto_pago_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Movimientos[]
	 */
	function getMovimientossRelatedByConceptoPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientossRelatedByConceptoPagoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientossRelatedByConceptoPagoId_c;
		}

		$result = Movimientos::doSelect($this->getMovimientossRelatedByConceptoPagoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientossRelatedByConceptoPagoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for ConceptosPagos::getMovimientossRelatedByconcepto_pago_id
	 * @return Movimientos[]
	 * @see ConceptosPagos::getMovimientossRelatedByConceptoPagoId
	 */
	function getMovimientoss($extra = null) {
		return $this->getMovimientossRelatedByConceptoPagoId($extra);
	}

	/**
	  * Convenience function for ConceptosPagos::getMovimientossRelatedByconcepto_pago_idQuery
	  * @return Query
	  * @see ConceptosPagos::getMovimientossRelatedByconcepto_pago_idQuery
	  */
	function getMovimientossQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos', 'concepto_pago_id','id', $q);
	}

	/**
	  * Convenience function for ConceptosPagos::deleteMovimientossRelatedByconcepto_pago_id
	  * @return int
	  * @see ConceptosPagos::deleteMovimientossRelatedByconcepto_pago_id
	  */
	function deleteMovimientoss(Query $q = null) {
		return $this->deleteMovimientossRelatedByConceptoPagoId($q);
	}

	/**
	  * Convenience function for ConceptosPagos::countMovimientossRelatedByconcepto_pago_id
	  * @return int
	  * @see ConceptosPagos::countMovimientossRelatedByConceptoPagoId
	  */
	function countMovimientoss(Query $q = null) {
		return $this->countMovimientossRelatedByConceptoPagoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getconcepto()) {
			$this->_validationErrors[] = 'concepto must not be null';
		}
		if (null === $this->gettipo_id()) {
			$this->_validationErrors[] = 'tipo_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}