<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseAcciones extends ApplicationModel {

	const ID = 'acciones.id';
	const ACCION = 'acciones.accion';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'acciones';

	/**
	 * Cache of objects retrieved from the database
	 * @var Acciones[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'accion' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `accion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $accion;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Acciones
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the accion field
	 */
	function getAccion() {
		return $this->accion;
	}

	/**
	 * Sets the value of the accion field
	 * @return Acciones
	 */
	function setAccion($value) {
		return $this->setColumnValue('accion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Acciones
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Acciones
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Acciones
	 */
	static function retrieveById($value) {
		return Acciones::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a accion
	 * value that matches the one provided
	 * @return Acciones
	 */
	static function retrieveByAccion($value) {
		return static::retrieveByColumn('accion', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Acciones
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Acciones[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting permisos Objects(rows) from the permisos table
	 * with a idaccion that matches $this->id.
	 * @return Query
	 */
	function getPermisossRelatedByIdaccionQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos', 'idaccion', 'id', $q);
	}

	/**
	 * Returns the count of Permisos Objects(rows) from the permisos table
	 * with a idaccion that matches $this->id.
	 * @return int
	 */
	function countPermisossRelatedByIdaccion(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Permisos::doCount($this->getPermisossRelatedByIdaccionQuery($q));
	}

	/**
	 * Deletes the permisos Objects(rows) from the permisos table
	 * with a idaccion that matches $this->id.
	 * @return int
	 */
	function deletePermisossRelatedByIdaccion(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PermisossRelatedByIdaccion_c = array();
		return Permisos::doDelete($this->getPermisossRelatedByIdaccionQuery($q));
	}

	protected $PermisossRelatedByIdaccion_c = array();

	/**
	 * Returns an array of Permisos objects with a idaccion
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Permisos[]
	 */
	function getPermisossRelatedByIdaccion(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PermisossRelatedByIdaccion_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PermisossRelatedByIdaccion_c;
		}

		$result = Permisos::doSelect($this->getPermisossRelatedByIdaccionQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PermisossRelatedByIdaccion_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Acciones::getPermisossRelatedByidaccion
	 * @return Permisos[]
	 * @see Acciones::getPermisossRelatedByIdaccion
	 */
	function getPermisoss($extra = null) {
		return $this->getPermisossRelatedByIdaccion($extra);
	}

	/**
	  * Convenience function for Acciones::getPermisossRelatedByidaccionQuery
	  * @return Query
	  * @see Acciones::getPermisossRelatedByidaccionQuery
	  */
	function getPermisossQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos', 'idaccion','id', $q);
	}

	/**
	  * Convenience function for Acciones::deletePermisossRelatedByidaccion
	  * @return int
	  * @see Acciones::deletePermisossRelatedByidaccion
	  */
	function deletePermisoss(Query $q = null) {
		return $this->deletePermisossRelatedByIdaccion($q);
	}

	/**
	  * Convenience function for Acciones::countPermisossRelatedByidaccion
	  * @return int
	  * @see Acciones::countPermisossRelatedByIdaccion
	  */
	function countPermisoss(Query $q = null) {
		return $this->countPermisossRelatedByIdaccion($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getaccion()) {
			$this->_validationErrors[] = 'accion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}