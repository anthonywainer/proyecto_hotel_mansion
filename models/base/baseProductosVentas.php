<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseProductosVentas extends ApplicationModel {

	const ID = 'productos_ventas.id';
	const CANTIDAD = 'productos_ventas.cantidad';
	const DESCUENTO = 'productos_ventas.descuento';
	const TOTAL = 'productos_ventas.total';
	const PRODUCTO_ID = 'productos_ventas.producto_id';
	const VENTA_ID = 'productos_ventas.venta_id';
	const CREATED_AT = 'productos_ventas.created_at';
	const UPDATED_AT = 'productos_ventas.updated_at';
	const DELETED_AT = 'productos_ventas.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'productos_ventas';

	/**
	 * Cache of objects retrieved from the database
	 * @var ProductosVentas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'cantidad' => Model::COLUMN_TYPE_INTEGER,
		'descuento' => Model::COLUMN_TYPE_DECIMAL,
		'total' => Model::COLUMN_TYPE_DECIMAL,
		'producto_id' => Model::COLUMN_TYPE_INTEGER,
		'venta_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `cantidad` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cantidad;

	/**
	 * `descuento` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $descuento;

	/**
	 * `total` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $total;

	/**
	 * `producto_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $producto_id;

	/**
	 * `venta_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $venta_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ProductosVentas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the cantidad field
	 */
	function getCantidad() {
		return $this->cantidad;
	}

	/**
	 * Sets the value of the cantidad field
	 * @return ProductosVentas
	 */
	function setCantidad($value) {
		return $this->setColumnValue('cantidad', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descuento field
	 */
	function getDescuento() {
		return $this->descuento;
	}

	/**
	 * Sets the value of the descuento field
	 * @return ProductosVentas
	 */
	function setDescuento($value) {
		return $this->setColumnValue('descuento', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the total field
	 */
	function getTotal() {
		return $this->total;
	}

	/**
	 * Sets the value of the total field
	 * @return ProductosVentas
	 */
	function setTotal($value) {
		return $this->setColumnValue('total', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the producto_id field
	 */
	function getProductoId() {
		return $this->producto_id;
	}

	/**
	 * Sets the value of the producto_id field
	 * @return ProductosVentas
	 */
	function setProductoId($value) {
		return $this->setColumnValue('producto_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductosVentas::getProductoId
	 * final because getProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::getProductoId
	 */
	final function getProducto_id() {
		return $this->getProductoId();
	}

	/**
	 * Convenience function for ProductosVentas::setProductoId
	 * final because setProductoId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::setProductoId
	 * @return ProductosVentas
	 */
	final function setProducto_id($value) {
		return $this->setProductoId($value);
	}

	/**
	 * Gets the value of the venta_id field
	 */
	function getVentaId() {
		return $this->venta_id;
	}

	/**
	 * Sets the value of the venta_id field
	 * @return ProductosVentas
	 */
	function setVentaId($value) {
		return $this->setColumnValue('venta_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ProductosVentas::getVentaId
	 * final because getVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::getVentaId
	 */
	final function getVenta_id() {
		return $this->getVentaId();
	}

	/**
	 * Convenience function for ProductosVentas::setVentaId
	 * final because setVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::setVentaId
	 * @return ProductosVentas
	 */
	final function setVenta_id($value) {
		return $this->setVentaId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return ProductosVentas
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ProductosVentas::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for ProductosVentas::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::setCreatedAt
	 * @return ProductosVentas
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return ProductosVentas
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ProductosVentas::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for ProductosVentas::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::setUpdatedAt
	 * @return ProductosVentas
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return ProductosVentas
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ProductosVentas::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for ProductosVentas::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ProductosVentas::setDeletedAt
	 * @return ProductosVentas
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ProductosVentas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ProductosVentas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveById($value) {
		return ProductosVentas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a cantidad
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByCantidad($value) {
		return static::retrieveByColumn('cantidad', $value);
	}

	/**
	 * Searches the database for a row with a descuento
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByDescuento($value) {
		return static::retrieveByColumn('descuento', $value);
	}

	/**
	 * Searches the database for a row with a total
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByTotal($value) {
		return static::retrieveByColumn('total', $value);
	}

	/**
	 * Searches the database for a row with a producto_id
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByProductoId($value) {
		return static::retrieveByColumn('producto_id', $value);
	}

	/**
	 * Searches the database for a row with a venta_id
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByVentaId($value) {
		return static::retrieveByColumn('venta_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return ProductosVentas
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ProductosVentas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->cantidad = (null === $this->cantidad) ? null : (int) $this->cantidad;
		$this->producto_id = (null === $this->producto_id) ? null : (int) $this->producto_id;
		$this->venta_id = (null === $this->venta_id) ? null : (int) $this->venta_id;
		return $this;
	}

	/**
	 * @return ProductosVentas
	 */
	function setProducto(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductosVentas
	 */
	function setProductosRelatedByProductoId(Productos $productos = null) {
		if (null === $productos) {
			$this->setproducto_id(null);
		} else {
			if (!$productos->getid()) {
				throw new Exception('Cannot connect a Productos without a id');
			}
			$this->setproducto_id($productos->getid());
		}
		return $this;
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProducto() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductosRelatedByProductoId() {
		$fk_value = $this->getproducto_id();
		if (null === $fk_value) {
			return null;
		}
		return Productos::retrieveByPK($fk_value);
	}

	static function doSelectJoinProducto(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinProductosRelatedByProductoId($q, $join_type);
	}

	/**
	 * Returns a productos object with a id
	 * that matches $this->producto_id.
	 * @return Productos
	 */
	function getProductos() {
		return $this->getProductosRelatedByProductoId();
	}

	/**
	 * @return ProductosVentas
	 */
	function setProductos(Productos $productos = null) {
		return $this->setProductosRelatedByProductoId($productos);
	}

	/**
	 * @return ProductosVentas[]
	 */
	static function doSelectJoinProductosRelatedByProductoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Productos'));
	}

	/**
	 * @return ProductosVentas
	 */
	function setVenta(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return ProductosVentas
	 */
	function setVentasRelatedByVentaId(Ventas $ventas = null) {
		if (null === $ventas) {
			$this->setventa_id(null);
		} else {
			if (!$ventas->getid()) {
				throw new Exception('Cannot connect a Ventas without a id');
			}
			$this->setventa_id($ventas->getid());
		}
		return $this;
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVenta() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentasRelatedByVentaId() {
		$fk_value = $this->getventa_id();
		if (null === $fk_value) {
			return null;
		}
		return Ventas::retrieveByPK($fk_value);
	}

	static function doSelectJoinVenta(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVentasRelatedByVentaId($q, $join_type);
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentas() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * @return ProductosVentas
	 */
	function setVentas(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return ProductosVentas[]
	 */
	static function doSelectJoinVentasRelatedByVentaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Ventas'));
	}

	/**
	 * @return ProductosVentas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Productos::getTableName();
		$q->join($to_table, $this_table . '.producto_id = ' . $to_table . '.id', $join_type);
		foreach (Productos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Productos';
	
		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Ventas';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getcantidad()) {
			$this->_validationErrors[] = 'cantidad must not be null';
		}
		if (null === $this->getdescuento()) {
			$this->_validationErrors[] = 'descuento must not be null';
		}
		if (null === $this->gettotal()) {
			$this->_validationErrors[] = 'total must not be null';
		}
		if (null === $this->getproducto_id()) {
			$this->_validationErrors[] = 'producto_id must not be null';
		}
		if (null === $this->getventa_id()) {
			$this->_validationErrors[] = 'venta_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}