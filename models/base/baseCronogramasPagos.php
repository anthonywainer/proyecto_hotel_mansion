<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseCronogramasPagos extends ApplicationModel {

	const ID = 'cronogramas_pagos.id';
	const FECHA = 'cronogramas_pagos.fecha';
	const MONTO_CARGADO = 'cronogramas_pagos.monto_cargado';
	const CUOTA = 'cronogramas_pagos.cuota';
	const ESTADIA_ID = 'cronogramas_pagos.estadia_id';
	const VENTA_ID = 'cronogramas_pagos.venta_id';
	const CREATED_AT = 'cronogramas_pagos.created_at';
	const UPDATED_AT = 'cronogramas_pagos.updated_at';
	const DELETED_AT = 'cronogramas_pagos.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'cronogramas_pagos';

	/**
	 * Cache of objects retrieved from the database
	 * @var CronogramasPagos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'fecha' => Model::COLUMN_TYPE_TIMESTAMP,
		'monto_cargado' => Model::COLUMN_TYPE_DECIMAL,
		'cuota' => Model::COLUMN_TYPE_DECIMAL,
		'estadia_id' => Model::COLUMN_TYPE_INTEGER,
		'venta_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `fecha` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha;

	/**
	 * `monto_cargado` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $monto_cargado;

	/**
	 * `cuota` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $cuota;

	/**
	 * `estadia_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $estadia_id;

	/**
	 * `venta_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $venta_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return CronogramasPagos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the fecha field
	 */
	function getFecha($format = null) {
		if (null === $this->fecha || null === $format) {
			return $this->fecha;
		}
		if (0 === strpos($this->fecha, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha));
	}

	/**
	 * Sets the value of the fecha field
	 * @return CronogramasPagos
	 */
	function setFecha($value) {
		return $this->setColumnValue('fecha', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Gets the value of the monto_cargado field
	 */
	function getMontoCargado() {
		return $this->monto_cargado;
	}

	/**
	 * Sets the value of the monto_cargado field
	 * @return CronogramasPagos
	 */
	function setMontoCargado($value) {
		return $this->setColumnValue('monto_cargado', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Convenience function for CronogramasPagos::getMontoCargado
	 * final because getMontoCargado should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::getMontoCargado
	 */
	final function getMonto_cargado() {
		return $this->getMontoCargado();
	}

	/**
	 * Convenience function for CronogramasPagos::setMontoCargado
	 * final because setMontoCargado should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::setMontoCargado
	 * @return CronogramasPagos
	 */
	final function setMonto_cargado($value) {
		return $this->setMontoCargado($value);
	}

	/**
	 * Gets the value of the cuota field
	 */
	function getCuota() {
		return $this->cuota;
	}

	/**
	 * Sets the value of the cuota field
	 * @return CronogramasPagos
	 */
	function setCuota($value) {
		return $this->setColumnValue('cuota', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the estadia_id field
	 */
	function getEstadiaId() {
		return $this->estadia_id;
	}

	/**
	 * Sets the value of the estadia_id field
	 * @return CronogramasPagos
	 */
	function setEstadiaId($value) {
		return $this->setColumnValue('estadia_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for CronogramasPagos::getEstadiaId
	 * final because getEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::getEstadiaId
	 */
	final function getEstadia_id() {
		return $this->getEstadiaId();
	}

	/**
	 * Convenience function for CronogramasPagos::setEstadiaId
	 * final because setEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::setEstadiaId
	 * @return CronogramasPagos
	 */
	final function setEstadia_id($value) {
		return $this->setEstadiaId($value);
	}

	/**
	 * Gets the value of the venta_id field
	 */
	function getVentaId() {
		return $this->venta_id;
	}

	/**
	 * Sets the value of the venta_id field
	 * @return CronogramasPagos
	 */
	function setVentaId($value) {
		return $this->setColumnValue('venta_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for CronogramasPagos::getVentaId
	 * final because getVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::getVentaId
	 */
	final function getVenta_id() {
		return $this->getVentaId();
	}

	/**
	 * Convenience function for CronogramasPagos::setVentaId
	 * final because setVentaId should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::setVentaId
	 * @return CronogramasPagos
	 */
	final function setVenta_id($value) {
		return $this->setVentaId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return CronogramasPagos
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for CronogramasPagos::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for CronogramasPagos::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::setCreatedAt
	 * @return CronogramasPagos
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return CronogramasPagos
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for CronogramasPagos::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for CronogramasPagos::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::setUpdatedAt
	 * @return CronogramasPagos
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return CronogramasPagos
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for CronogramasPagos::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for CronogramasPagos::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CronogramasPagos::setDeletedAt
	 * @return CronogramasPagos
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return CronogramasPagos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return CronogramasPagos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveById($value) {
		return CronogramasPagos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a fecha
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByFecha($value) {
		return static::retrieveByColumn('fecha', $value);
	}

	/**
	 * Searches the database for a row with a monto_cargado
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByMontoCargado($value) {
		return static::retrieveByColumn('monto_cargado', $value);
	}

	/**
	 * Searches the database for a row with a cuota
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByCuota($value) {
		return static::retrieveByColumn('cuota', $value);
	}

	/**
	 * Searches the database for a row with a estadia_id
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByEstadiaId($value) {
		return static::retrieveByColumn('estadia_id', $value);
	}

	/**
	 * Searches the database for a row with a venta_id
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByVentaId($value) {
		return static::retrieveByColumn('venta_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return CronogramasPagos
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return CronogramasPagos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->estadia_id = (null === $this->estadia_id) ? null : (int) $this->estadia_id;
		$this->venta_id = (null === $this->venta_id) ? null : (int) $this->venta_id;
		return $this;
	}

	/**
	 * @return CronogramasPagos
	 */
	function setEstadia(Estadias $estadias = null) {
		return $this->setEstadiasRelatedByEstadiaId($estadias);
	}

	/**
	 * @return CronogramasPagos
	 */
	function setEstadiasRelatedByEstadiaId(Estadias $estadias = null) {
		if (null === $estadias) {
			$this->setestadia_id(null);
		} else {
			if (!$estadias->getid()) {
				throw new Exception('Cannot connect a Estadias without a id');
			}
			$this->setestadia_id($estadias->getid());
		}
		return $this;
	}

	/**
	 * Returns a estadias object with a id
	 * that matches $this->estadia_id.
	 * @return Estadias
	 */
	function getEstadia() {
		return $this->getEstadiasRelatedByEstadiaId();
	}

	/**
	 * Returns a estadias object with a id
	 * that matches $this->estadia_id.
	 * @return Estadias
	 */
	function getEstadiasRelatedByEstadiaId() {
		$fk_value = $this->getestadia_id();
		if (null === $fk_value) {
			return null;
		}
		return Estadias::retrieveByPK($fk_value);
	}

	static function doSelectJoinEstadia(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinEstadiasRelatedByEstadiaId($q, $join_type);
	}

	/**
	 * Returns a estadias object with a id
	 * that matches $this->estadia_id.
	 * @return Estadias
	 */
	function getEstadias() {
		return $this->getEstadiasRelatedByEstadiaId();
	}

	/**
	 * @return CronogramasPagos
	 */
	function setEstadias(Estadias $estadias = null) {
		return $this->setEstadiasRelatedByEstadiaId($estadias);
	}

	/**
	 * @return CronogramasPagos[]
	 */
	static function doSelectJoinEstadiasRelatedByEstadiaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Estadias::getTableName();
		$q->join($to_table, $this_table . '.estadia_id = ' . $to_table . '.id', $join_type);
		foreach (Estadias::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Estadias'));
	}

	/**
	 * @return CronogramasPagos
	 */
	function setVenta(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return CronogramasPagos
	 */
	function setVentasRelatedByVentaId(Ventas $ventas = null) {
		if (null === $ventas) {
			$this->setventa_id(null);
		} else {
			if (!$ventas->getid()) {
				throw new Exception('Cannot connect a Ventas without a id');
			}
			$this->setventa_id($ventas->getid());
		}
		return $this;
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVenta() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentasRelatedByVentaId() {
		$fk_value = $this->getventa_id();
		if (null === $fk_value) {
			return null;
		}
		return Ventas::retrieveByPK($fk_value);
	}

	static function doSelectJoinVenta(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinVentasRelatedByVentaId($q, $join_type);
	}

	/**
	 * Returns a ventas object with a id
	 * that matches $this->venta_id.
	 * @return Ventas
	 */
	function getVentas() {
		return $this->getVentasRelatedByVentaId();
	}

	/**
	 * @return CronogramasPagos
	 */
	function setVentas(Ventas $ventas = null) {
		return $this->setVentasRelatedByVentaId($ventas);
	}

	/**
	 * @return CronogramasPagos[]
	 */
	static function doSelectJoinVentasRelatedByVentaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Ventas'));
	}

	/**
	 * @return CronogramasPagos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Estadias::getTableName();
		$q->join($to_table, $this_table . '.estadia_id = ' . $to_table . '.id', $join_type);
		foreach (Estadias::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Estadias';
	
		$to_table = Ventas::getTableName();
		$q->join($to_table, $this_table . '.venta_id = ' . $to_table . '.id', $join_type);
		foreach (Ventas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Ventas';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting amortizaciones Objects(rows) from the amortizaciones table
	 * with a cronograma_pago_id that matches $this->id.
	 * @return Query
	 */
	function getAmortizacionessRelatedByCronogramaPagoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('amortizaciones', 'cronograma_pago_id', 'id', $q);
	}

	/**
	 * Returns the count of Amortizaciones Objects(rows) from the amortizaciones table
	 * with a cronograma_pago_id that matches $this->id.
	 * @return int
	 */
	function countAmortizacionessRelatedByCronogramaPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Amortizaciones::doCount($this->getAmortizacionessRelatedByCronogramaPagoIdQuery($q));
	}

	/**
	 * Deletes the amortizaciones Objects(rows) from the amortizaciones table
	 * with a cronograma_pago_id that matches $this->id.
	 * @return int
	 */
	function deleteAmortizacionessRelatedByCronogramaPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AmortizacionessRelatedByCronogramaPagoId_c = array();
		return Amortizaciones::doDelete($this->getAmortizacionessRelatedByCronogramaPagoIdQuery($q));
	}

	protected $AmortizacionessRelatedByCronogramaPagoId_c = array();

	/**
	 * Returns an array of Amortizaciones objects with a cronograma_pago_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Amortizaciones[]
	 */
	function getAmortizacionessRelatedByCronogramaPagoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AmortizacionessRelatedByCronogramaPagoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AmortizacionessRelatedByCronogramaPagoId_c;
		}

		$result = Amortizaciones::doSelect($this->getAmortizacionessRelatedByCronogramaPagoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AmortizacionessRelatedByCronogramaPagoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for CronogramasPagos::getAmortizacionessRelatedBycronograma_pago_id
	 * @return Amortizaciones[]
	 * @see CronogramasPagos::getAmortizacionessRelatedByCronogramaPagoId
	 */
	function getAmortizacioness($extra = null) {
		return $this->getAmortizacionessRelatedByCronogramaPagoId($extra);
	}

	/**
	  * Convenience function for CronogramasPagos::getAmortizacionessRelatedBycronograma_pago_idQuery
	  * @return Query
	  * @see CronogramasPagos::getAmortizacionessRelatedBycronograma_pago_idQuery
	  */
	function getAmortizacionessQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('amortizaciones', 'cronograma_pago_id','id', $q);
	}

	/**
	  * Convenience function for CronogramasPagos::deleteAmortizacionessRelatedBycronograma_pago_id
	  * @return int
	  * @see CronogramasPagos::deleteAmortizacionessRelatedBycronograma_pago_id
	  */
	function deleteAmortizacioness(Query $q = null) {
		return $this->deleteAmortizacionessRelatedByCronogramaPagoId($q);
	}

	/**
	  * Convenience function for CronogramasPagos::countAmortizacionessRelatedBycronograma_pago_id
	  * @return int
	  * @see CronogramasPagos::countAmortizacionessRelatedByCronogramaPagoId
	  */
	function countAmortizacioness(Query $q = null) {
		return $this->countAmortizacionessRelatedByCronogramaPagoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getfecha()) {
			$this->_validationErrors[] = 'fecha must not be null';
		}
		if (null === $this->getmonto_cargado()) {
			$this->_validationErrors[] = 'monto_cargado must not be null';
		}
		if (null === $this->getcuota()) {
			$this->_validationErrors[] = 'cuota must not be null';
		}
		if (null === $this->getestadia_id()) {
			$this->_validationErrors[] = 'estadia_id must not be null';
		}
		if (null === $this->getventa_id()) {
			$this->_validationErrors[] = 'venta_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}