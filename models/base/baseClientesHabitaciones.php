<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseClientesHabitaciones extends ApplicationModel {

	const ID = 'clientes_habitaciones.id';
	const HABITACION_ID = 'clientes_habitaciones.habitacion_id';
	const CLIENTE_ID = 'clientes_habitaciones.cliente_id';
	const CREATED_AT = 'clientes_habitaciones.created_at';
	const UPDATED_AT = 'clientes_habitaciones.updated_at';
	const DELETED_AT = 'clientes_habitaciones.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'clientes_habitaciones';

	/**
	 * Cache of objects retrieved from the database
	 * @var ClientesHabitaciones[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'habitacion_id' => Model::COLUMN_TYPE_INTEGER,
		'cliente_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `habitacion_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $habitacion_id;

	/**
	 * `cliente_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cliente_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ClientesHabitaciones
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the habitacion_id field
	 */
	function getHabitacionId() {
		return $this->habitacion_id;
	}

	/**
	 * Sets the value of the habitacion_id field
	 * @return ClientesHabitaciones
	 */
	function setHabitacionId($value) {
		return $this->setColumnValue('habitacion_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ClientesHabitaciones::getHabitacionId
	 * final because getHabitacionId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::getHabitacionId
	 */
	final function getHabitacion_id() {
		return $this->getHabitacionId();
	}

	/**
	 * Convenience function for ClientesHabitaciones::setHabitacionId
	 * final because setHabitacionId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::setHabitacionId
	 * @return ClientesHabitaciones
	 */
	final function setHabitacion_id($value) {
		return $this->setHabitacionId($value);
	}

	/**
	 * Gets the value of the cliente_id field
	 */
	function getClienteId() {
		return $this->cliente_id;
	}

	/**
	 * Sets the value of the cliente_id field
	 * @return ClientesHabitaciones
	 */
	function setClienteId($value) {
		return $this->setColumnValue('cliente_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ClientesHabitaciones::getClienteId
	 * final because getClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::getClienteId
	 */
	final function getCliente_id() {
		return $this->getClienteId();
	}

	/**
	 * Convenience function for ClientesHabitaciones::setClienteId
	 * final because setClienteId should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::setClienteId
	 * @return ClientesHabitaciones
	 */
	final function setCliente_id($value) {
		return $this->setClienteId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return ClientesHabitaciones
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ClientesHabitaciones::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for ClientesHabitaciones::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::setCreatedAt
	 * @return ClientesHabitaciones
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return ClientesHabitaciones
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ClientesHabitaciones::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for ClientesHabitaciones::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::setUpdatedAt
	 * @return ClientesHabitaciones
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return ClientesHabitaciones
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ClientesHabitaciones::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for ClientesHabitaciones::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ClientesHabitaciones::setDeletedAt
	 * @return ClientesHabitaciones
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ClientesHabitaciones
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ClientesHabitaciones
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ClientesHabitaciones
	 */
	static function retrieveById($value) {
		return ClientesHabitaciones::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a habitacion_id
	 * value that matches the one provided
	 * @return ClientesHabitaciones
	 */
	static function retrieveByHabitacionId($value) {
		return static::retrieveByColumn('habitacion_id', $value);
	}

	/**
	 * Searches the database for a row with a cliente_id
	 * value that matches the one provided
	 * @return ClientesHabitaciones
	 */
	static function retrieveByClienteId($value) {
		return static::retrieveByColumn('cliente_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return ClientesHabitaciones
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return ClientesHabitaciones
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return ClientesHabitaciones
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ClientesHabitaciones
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->habitacion_id = (null === $this->habitacion_id) ? null : (int) $this->habitacion_id;
		$this->cliente_id = (null === $this->cliente_id) ? null : (int) $this->cliente_id;
		return $this;
	}

	/**
	 * @return ClientesHabitaciones
	 */
	function setCliente(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return ClientesHabitaciones
	 */
	function setClientesRelatedByClienteId(Clientes $clientes = null) {
		if (null === $clientes) {
			$this->setcliente_id(null);
		} else {
			if (!$clientes->getid()) {
				throw new Exception('Cannot connect a Clientes without a id');
			}
			$this->setcliente_id($clientes->getid());
		}
		return $this;
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getCliente() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientesRelatedByClienteId() {
		$fk_value = $this->getcliente_id();
		if (null === $fk_value) {
			return null;
		}
		return Clientes::retrieveByPK($fk_value);
	}

	static function doSelectJoinCliente(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinClientesRelatedByClienteId($q, $join_type);
	}

	/**
	 * Returns a clientes object with a id
	 * that matches $this->cliente_id.
	 * @return Clientes
	 */
	function getClientes() {
		return $this->getClientesRelatedByClienteId();
	}

	/**
	 * @return ClientesHabitaciones
	 */
	function setClientes(Clientes $clientes = null) {
		return $this->setClientesRelatedByClienteId($clientes);
	}

	/**
	 * @return ClientesHabitaciones[]
	 */
	static function doSelectJoinClientesRelatedByClienteId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Clientes'));
	}

	/**
	 * @return ClientesHabitaciones
	 */
	function setHabitacion(Habitaciones $habitaciones = null) {
		return $this->setHabitacionesRelatedByHabitacionId($habitaciones);
	}

	/**
	 * @return ClientesHabitaciones
	 */
	function setHabitacionesRelatedByHabitacionId(Habitaciones $habitaciones = null) {
		if (null === $habitaciones) {
			$this->sethabitacion_id(null);
		} else {
			if (!$habitaciones->getid()) {
				throw new Exception('Cannot connect a Habitaciones without a id');
			}
			$this->sethabitacion_id($habitaciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a habitaciones object with a id
	 * that matches $this->habitacion_id.
	 * @return Habitaciones
	 */
	function getHabitacion() {
		return $this->getHabitacionesRelatedByHabitacionId();
	}

	/**
	 * Returns a habitaciones object with a id
	 * that matches $this->habitacion_id.
	 * @return Habitaciones
	 */
	function getHabitacionesRelatedByHabitacionId() {
		$fk_value = $this->gethabitacion_id();
		if (null === $fk_value) {
			return null;
		}
		return Habitaciones::retrieveByPK($fk_value);
	}

	static function doSelectJoinHabitacion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinHabitacionesRelatedByHabitacionId($q, $join_type);
	}

	/**
	 * Returns a habitaciones object with a id
	 * that matches $this->habitacion_id.
	 * @return Habitaciones
	 */
	function getHabitaciones() {
		return $this->getHabitacionesRelatedByHabitacionId();
	}

	/**
	 * @return ClientesHabitaciones
	 */
	function setHabitaciones(Habitaciones $habitaciones = null) {
		return $this->setHabitacionesRelatedByHabitacionId($habitaciones);
	}

	/**
	 * @return ClientesHabitaciones[]
	 */
	static function doSelectJoinHabitacionesRelatedByHabitacionId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Habitaciones::getTableName();
		$q->join($to_table, $this_table . '.habitacion_id = ' . $to_table . '.id', $join_type);
		foreach (Habitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Habitaciones'));
	}

	/**
	 * @return ClientesHabitaciones[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Clientes::getTableName();
		$q->join($to_table, $this_table . '.cliente_id = ' . $to_table . '.id', $join_type);
		foreach (Clientes::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Clientes';
	
		$to_table = Habitaciones::getTableName();
		$q->join($to_table, $this_table . '.habitacion_id = ' . $to_table . '.id', $join_type);
		foreach (Habitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Habitaciones';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->gethabitacion_id()) {
			$this->_validationErrors[] = 'habitacion_id must not be null';
		}
		if (null === $this->getcliente_id()) {
			$this->_validationErrors[] = 'cliente_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}