<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseProductos extends ApplicationModel {

	const ID = 'productos.id';
	const NOMBRE = 'productos.nombre';
	const DESCRIPCION = 'productos.descripcion';
	const PRECIO = 'productos.precio';
	const IMAGEN = 'productos.imagen';
	const STOCK = 'productos.stock';
	const CATEGORIA_ID = 'productos.categoria_id';
	const CREATED_AT = 'productos.created_at';
	const UPDATED_AT = 'productos.updated_at';
	const DELETED_AT = 'productos.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'productos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Productos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'precio' => Model::COLUMN_TYPE_DECIMAL,
		'imagen' => Model::COLUMN_TYPE_VARCHAR,
		'stock' => Model::COLUMN_TYPE_INTEGER,
		'categoria_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `precio` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $precio;

	/**
	 * `imagen` VARCHAR NOT NULL
	 * @var string
	 */
	protected $imagen;

	/**
	 * `stock` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $stock;

	/**
	 * `categoria_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $categoria_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Productos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Productos
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Productos
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the precio field
	 */
	function getPrecio() {
		return $this->precio;
	}

	/**
	 * Sets the value of the precio field
	 * @return Productos
	 */
	function setPrecio($value) {
		return $this->setColumnValue('precio', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the imagen field
	 */
	function getImagen() {
		return $this->imagen;
	}

	/**
	 * Sets the value of the imagen field
	 * @return Productos
	 */
	function setImagen($value) {
		return $this->setColumnValue('imagen', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the stock field
	 */
	function getStock() {
		return $this->stock;
	}

	/**
	 * Sets the value of the stock field
	 * @return Productos
	 */
	function setStock($value) {
		return $this->setColumnValue('stock', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the categoria_id field
	 */
	function getCategoriaId() {
		return $this->categoria_id;
	}

	/**
	 * Sets the value of the categoria_id field
	 * @return Productos
	 */
	function setCategoriaId($value) {
		return $this->setColumnValue('categoria_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Productos::getCategoriaId
	 * final because getCategoriaId should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getCategoriaId
	 */
	final function getCategoria_id() {
		return $this->getCategoriaId();
	}

	/**
	 * Convenience function for Productos::setCategoriaId
	 * final because setCategoriaId should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setCategoriaId
	 * @return Productos
	 */
	final function setCategoria_id($value) {
		return $this->setCategoriaId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Productos
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Productos::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Productos::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setCreatedAt
	 * @return Productos
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Productos
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Productos::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Productos::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setUpdatedAt
	 * @return Productos
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Productos
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Productos::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Productos::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Productos::setDeletedAt
	 * @return Productos
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Productos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Productos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveById($value) {
		return Productos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a precio
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByPrecio($value) {
		return static::retrieveByColumn('precio', $value);
	}

	/**
	 * Searches the database for a row with a imagen
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByImagen($value) {
		return static::retrieveByColumn('imagen', $value);
	}

	/**
	 * Searches the database for a row with a stock
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByStock($value) {
		return static::retrieveByColumn('stock', $value);
	}

	/**
	 * Searches the database for a row with a categoria_id
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByCategoriaId($value) {
		return static::retrieveByColumn('categoria_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Productos
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Productos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->stock = (null === $this->stock) ? null : (int) $this->stock;
		$this->categoria_id = (null === $this->categoria_id) ? null : (int) $this->categoria_id;
		return $this;
	}

	/**
	 * @return Productos
	 */
	function setCategoria(Categorias $categorias = null) {
		return $this->setCategoriasRelatedByCategoriaId($categorias);
	}

	/**
	 * @return Productos
	 */
	function setCategoriasRelatedByCategoriaId(Categorias $categorias = null) {
		if (null === $categorias) {
			$this->setcategoria_id(null);
		} else {
			if (!$categorias->getid()) {
				throw new Exception('Cannot connect a Categorias without a id');
			}
			$this->setcategoria_id($categorias->getid());
		}
		return $this;
	}

	/**
	 * Returns a categorias object with a id
	 * that matches $this->categoria_id.
	 * @return Categorias
	 */
	function getCategoria() {
		return $this->getCategoriasRelatedByCategoriaId();
	}

	/**
	 * Returns a categorias object with a id
	 * that matches $this->categoria_id.
	 * @return Categorias
	 */
	function getCategoriasRelatedByCategoriaId() {
		$fk_value = $this->getcategoria_id();
		if (null === $fk_value) {
			return null;
		}
		return Categorias::retrieveByPK($fk_value);
	}

	static function doSelectJoinCategoria(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinCategoriasRelatedByCategoriaId($q, $join_type);
	}

	/**
	 * Returns a categorias object with a id
	 * that matches $this->categoria_id.
	 * @return Categorias
	 */
	function getCategorias() {
		return $this->getCategoriasRelatedByCategoriaId();
	}

	/**
	 * @return Productos
	 */
	function setCategorias(Categorias $categorias = null) {
		return $this->setCategoriasRelatedByCategoriaId($categorias);
	}

	/**
	 * @return Productos[]
	 */
	static function doSelectJoinCategoriasRelatedByCategoriaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Categorias::getTableName();
		$q->join($to_table, $this_table . '.categoria_id = ' . $to_table . '.id', $join_type);
		foreach (Categorias::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Categorias'));
	}

	/**
	 * @return Productos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Categorias::getTableName();
		$q->join($to_table, $this_table . '.categoria_id = ' . $to_table . '.id', $join_type);
		foreach (Categorias::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Categorias';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting producto_consumido Objects(rows) from the producto_consumido table
	 * with a producto_id that matches $this->id.
	 * @return Query
	 */
	function getProductoConsumidosRelatedByProductoIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_consumido', 'producto_id', 'id', $q);
	}

	/**
	 * Returns the count of ProductoConsumido Objects(rows) from the producto_consumido table
	 * with a producto_id that matches $this->id.
	 * @return int
	 */
	function countProductoConsumidosRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ProductoConsumido::doCount($this->getProductoConsumidosRelatedByProductoIdQuery($q));
	}

	/**
	 * Deletes the producto_consumido Objects(rows) from the producto_consumido table
	 * with a producto_id that matches $this->id.
	 * @return int
	 */
	function deleteProductoConsumidosRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ProductoConsumidosRelatedByProductoId_c = array();
		return ProductoConsumido::doDelete($this->getProductoConsumidosRelatedByProductoIdQuery($q));
	}

	protected $ProductoConsumidosRelatedByProductoId_c = array();

	/**
	 * Returns an array of ProductoConsumido objects with a producto_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ProductoConsumido[]
	 */
	function getProductoConsumidosRelatedByProductoId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ProductoConsumidosRelatedByProductoId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ProductoConsumidosRelatedByProductoId_c;
		}

		$result = ProductoConsumido::doSelect($this->getProductoConsumidosRelatedByProductoIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ProductoConsumidosRelatedByProductoId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Productos::getProductoConsumidosRelatedByproducto_id
	 * @return ProductoConsumido[]
	 * @see Productos::getProductoConsumidosRelatedByProductoId
	 */
	function getProductoConsumidos($extra = null) {
		return $this->getProductoConsumidosRelatedByProductoId($extra);
	}

	/**
	  * Convenience function for Productos::getProductoConsumidosRelatedByproducto_idQuery
	  * @return Query
	  * @see Productos::getProductoConsumidosRelatedByproducto_idQuery
	  */
	function getProductoConsumidosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_consumido', 'producto_id','id', $q);
	}

	/**
	  * Convenience function for Productos::deleteProductoConsumidosRelatedByproducto_id
	  * @return int
	  * @see Productos::deleteProductoConsumidosRelatedByproducto_id
	  */
	function deleteProductoConsumidos(Query $q = null) {
		return $this->deleteProductoConsumidosRelatedByProductoId($q);
	}

	/**
	  * Convenience function for Productos::countProductoConsumidosRelatedByproducto_id
	  * @return int
	  * @see Productos::countProductoConsumidosRelatedByProductoId
	  */
	function countProductoConsumidos(Query $q = null) {
		return $this->countProductoConsumidosRelatedByProductoId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		if (null === $this->getprecio()) {
			$this->_validationErrors[] = 'precio must not be null';
		}
		if (null === $this->getimagen()) {
			$this->_validationErrors[] = 'imagen must not be null';
		}
		if (null === $this->getstock()) {
			$this->_validationErrors[] = 'stock must not be null';
		}
		if (null === $this->getcategoria_id()) {
			$this->_validationErrors[] = 'categoria_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}