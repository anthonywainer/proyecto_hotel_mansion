<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseCategorias extends ApplicationModel {

	const ID = 'categorias.id';
	const DESCRIPCION = 'categorias.descripcion';
	const CREATED_AT = 'categorias.created_at';
	const UPDATED_AT = 'categorias.updated_at';
	const DELETED_AT = 'categorias.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'categorias';

	/**
	 * Cache of objects retrieved from the database
	 * @var Categorias[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Categorias
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Categorias
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Categorias
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Categorias::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Categorias::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Categorias::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Categorias::setCreatedAt
	 * @return Categorias
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Categorias
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Categorias::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Categorias::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Categorias::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Categorias::setUpdatedAt
	 * @return Categorias
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Categorias
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Categorias::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Categorias::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Categorias::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Categorias::setDeletedAt
	 * @return Categorias
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Categorias
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Categorias
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Categorias
	 */
	static function retrieveById($value) {
		return Categorias::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Categorias
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Categorias
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Categorias
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Categorias
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Categorias
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Categorias[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting productos Objects(rows) from the productos table
	 * with a categoria_id that matches $this->id.
	 * @return Query
	 */
	function getProductossRelatedByCategoriaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('productos', 'categoria_id', 'id', $q);
	}

	/**
	 * Returns the count of Productos Objects(rows) from the productos table
	 * with a categoria_id that matches $this->id.
	 * @return int
	 */
	function countProductossRelatedByCategoriaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Productos::doCount($this->getProductossRelatedByCategoriaIdQuery($q));
	}

	/**
	 * Deletes the productos Objects(rows) from the productos table
	 * with a categoria_id that matches $this->id.
	 * @return int
	 */
	function deleteProductossRelatedByCategoriaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ProductossRelatedByCategoriaId_c = array();
		return Productos::doDelete($this->getProductossRelatedByCategoriaIdQuery($q));
	}

	protected $ProductossRelatedByCategoriaId_c = array();

	/**
	 * Returns an array of Productos objects with a categoria_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Productos[]
	 */
	function getProductossRelatedByCategoriaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ProductossRelatedByCategoriaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ProductossRelatedByCategoriaId_c;
		}

		$result = Productos::doSelect($this->getProductossRelatedByCategoriaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ProductossRelatedByCategoriaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Categorias::getProductossRelatedBycategoria_id
	 * @return Productos[]
	 * @see Categorias::getProductossRelatedByCategoriaId
	 */
	function getProductoss($extra = null) {
		return $this->getProductossRelatedByCategoriaId($extra);
	}

	/**
	  * Convenience function for Categorias::getProductossRelatedBycategoria_idQuery
	  * @return Query
	  * @see Categorias::getProductossRelatedBycategoria_idQuery
	  */
	function getProductossQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('productos', 'categoria_id','id', $q);
	}

	/**
	  * Convenience function for Categorias::deleteProductossRelatedBycategoria_id
	  * @return int
	  * @see Categorias::deleteProductossRelatedBycategoria_id
	  */
	function deleteProductoss(Query $q = null) {
		return $this->deleteProductossRelatedByCategoriaId($q);
	}

	/**
	  * Convenience function for Categorias::countProductossRelatedBycategoria_id
	  * @return int
	  * @see Categorias::countProductossRelatedByCategoriaId
	  */
	function countProductoss(Query $q = null) {
		return $this->countProductossRelatedByCategoriaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}