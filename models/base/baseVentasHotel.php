<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseVentasHotel extends ApplicationModel {

	const ID = 'ventas_hotel.id';
	const MOVIMIENTO_ID = 'ventas_hotel.movimiento_id';
	const MONTO = 'ventas_hotel.monto';
	const EFECTIVO = 'ventas_hotel.efectivo';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ventas_hotel';

	/**
	 * Cache of objects retrieved from the database
	 * @var VentasHotel[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'movimiento_id' => Model::COLUMN_TYPE_INTEGER,
		'monto' => Model::COLUMN_TYPE_DECIMAL,
		'efectivo' => Model::COLUMN_TYPE_DECIMAL,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `movimiento_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $movimiento_id;

	/**
	 * `monto` DECIMAL DEFAULT '0.00'
	 * @var string
	 */
	protected $monto = 0.00;

	/**
	 * `efectivo` DECIMAL DEFAULT '0.00'
	 * @var string
	 */
	protected $efectivo = 0.00;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return VentasHotel
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the movimiento_id field
	 */
	function getMovimientoId() {
		return $this->movimiento_id;
	}

	/**
	 * Sets the value of the movimiento_id field
	 * @return VentasHotel
	 */
	function setMovimientoId($value) {
		return $this->setColumnValue('movimiento_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for VentasHotel::getMovimientoId
	 * final because getMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasHotel::getMovimientoId
	 */
	final function getMovimiento_id() {
		return $this->getMovimientoId();
	}

	/**
	 * Convenience function for VentasHotel::setMovimientoId
	 * final because setMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see VentasHotel::setMovimientoId
	 * @return VentasHotel
	 */
	final function setMovimiento_id($value) {
		return $this->setMovimientoId($value);
	}

	/**
	 * Gets the value of the monto field
	 */
	function getMonto() {
		return $this->monto;
	}

	/**
	 * Sets the value of the monto field
	 * @return VentasHotel
	 */
	function setMonto($value) {
		return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the efectivo field
	 */
	function getEfectivo() {
		return $this->efectivo;
	}

	/**
	 * Sets the value of the efectivo field
	 * @return VentasHotel
	 */
	function setEfectivo($value) {
		return $this->setColumnValue('efectivo', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return VentasHotel
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return VentasHotel
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return VentasHotel
	 */
	static function retrieveById($value) {
		return VentasHotel::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a movimiento_id
	 * value that matches the one provided
	 * @return VentasHotel
	 */
	static function retrieveByMovimientoId($value) {
		return static::retrieveByColumn('movimiento_id', $value);
	}

	/**
	 * Searches the database for a row with a monto
	 * value that matches the one provided
	 * @return VentasHotel
	 */
	static function retrieveByMonto($value) {
		return static::retrieveByColumn('monto', $value);
	}

	/**
	 * Searches the database for a row with a efectivo
	 * value that matches the one provided
	 * @return VentasHotel
	 */
	static function retrieveByEfectivo($value) {
		return static::retrieveByColumn('efectivo', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return VentasHotel
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->movimiento_id = (null === $this->movimiento_id) ? null : (int) $this->movimiento_id;
		return $this;
	}

	/**
	 * @return VentasHotel
	 */
	function setMovimiento(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return VentasHotel
	 */
	function setMovimientosDeDineroRelatedByMovimientoId(MovimientosDeDinero $movimientosdedinero = null) {
		if (null === $movimientosdedinero) {
			$this->setmovimiento_id(null);
		} else {
			if (!$movimientosdedinero->getid()) {
				throw new Exception('Cannot connect a MovimientosDeDinero without a id');
			}
			$this->setmovimiento_id($movimientosdedinero->getid());
		}
		return $this;
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimiento() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDineroRelatedByMovimientoId() {
		$fk_value = $this->getmovimiento_id();
		if (null === $fk_value) {
			return null;
		}
		return MovimientosDeDinero::retrieveByPK($fk_value);
	}

	static function doSelectJoinMovimiento(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMovimientosDeDineroRelatedByMovimientoId($q, $join_type);
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDinero() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * @return VentasHotel
	 */
	function setMovimientosDeDinero(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return VentasHotel[]
	 */
	static function doSelectJoinMovimientosDeDineroRelatedByMovimientoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('MovimientosDeDinero'));
	}

	/**
	 * @return VentasHotel[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'MovimientosDeDinero';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting producto_consumido_hotel Objects(rows) from the producto_consumido_hotel table
	 * with a hotel that matches $this->id.
	 * @return Query
	 */
	function getProductoConsumidoHotelsRelatedByHotelQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_consumido_hotel', 'hotel', 'id', $q);
	}

	/**
	 * Returns the count of ProductoConsumidoHotel Objects(rows) from the producto_consumido_hotel table
	 * with a hotel that matches $this->id.
	 * @return int
	 */
	function countProductoConsumidoHotelsRelatedByHotel(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ProductoConsumidoHotel::doCount($this->getProductoConsumidoHotelsRelatedByHotelQuery($q));
	}

	/**
	 * Deletes the producto_consumido_hotel Objects(rows) from the producto_consumido_hotel table
	 * with a hotel that matches $this->id.
	 * @return int
	 */
	function deleteProductoConsumidoHotelsRelatedByHotel(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ProductoConsumidoHotelsRelatedByHotel_c = array();
		return ProductoConsumidoHotel::doDelete($this->getProductoConsumidoHotelsRelatedByHotelQuery($q));
	}

	protected $ProductoConsumidoHotelsRelatedByHotel_c = array();

	/**
	 * Returns an array of ProductoConsumidoHotel objects with a hotel
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ProductoConsumidoHotel[]
	 */
	function getProductoConsumidoHotelsRelatedByHotel(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ProductoConsumidoHotelsRelatedByHotel_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ProductoConsumidoHotelsRelatedByHotel_c;
		}

		$result = ProductoConsumidoHotel::doSelect($this->getProductoConsumidoHotelsRelatedByHotelQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ProductoConsumidoHotelsRelatedByHotel_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for VentasHotel::getProductoConsumidoHotelsRelatedByhotel
	 * @return ProductoConsumidoHotel[]
	 * @see VentasHotel::getProductoConsumidoHotelsRelatedByHotel
	 */
	function getProductoConsumidoHotels($extra = null) {
		return $this->getProductoConsumidoHotelsRelatedByHotel($extra);
	}

	/**
	  * Convenience function for VentasHotel::getProductoConsumidoHotelsRelatedByhotelQuery
	  * @return Query
	  * @see VentasHotel::getProductoConsumidoHotelsRelatedByhotelQuery
	  */
	function getProductoConsumidoHotelsQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_consumido_hotel', 'hotel','id', $q);
	}

	/**
	  * Convenience function for VentasHotel::deleteProductoConsumidoHotelsRelatedByhotel
	  * @return int
	  * @see VentasHotel::deleteProductoConsumidoHotelsRelatedByhotel
	  */
	function deleteProductoConsumidoHotels(Query $q = null) {
		return $this->deleteProductoConsumidoHotelsRelatedByHotel($q);
	}

	/**
	  * Convenience function for VentasHotel::countProductoConsumidoHotelsRelatedByhotel
	  * @return int
	  * @see VentasHotel::countProductoConsumidoHotelsRelatedByHotel
	  */
	function countProductoConsumidoHotels(Query $q = null) {
		return $this->countProductoConsumidoHotelsRelatedByHotel($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getmovimiento_id()) {
			$this->_validationErrors[] = 'movimiento_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}