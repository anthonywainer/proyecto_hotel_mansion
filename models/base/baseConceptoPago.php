<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseConceptoPago extends ApplicationModel {

	const ID = 'concepto_pago.id';
	const DESCRIPCION = 'concepto_pago.descripcion';
	const MOVIMIENTO_ID = 'concepto_pago.movimiento_id';
	const CREATED_AT = 'concepto_pago.created_at';
	const UPDATED_AT = 'concepto_pago.updated_at';
	const DELETED_AT = 'concepto_pago.deleted_at';
	const MONTO = 'concepto_pago.monto';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'concepto_pago';

	/**
	 * Cache of objects retrieved from the database
	 * @var ConceptoPago[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'movimiento_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'monto' => Model::COLUMN_TYPE_DECIMAL,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `movimiento_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $movimiento_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `monto` DECIMAL DEFAULT '0.00'
	 * @var string
	 */
	protected $monto = 0.00;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return ConceptoPago
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return ConceptoPago
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the movimiento_id field
	 */
	function getMovimientoId() {
		return $this->movimiento_id;
	}

	/**
	 * Sets the value of the movimiento_id field
	 * @return ConceptoPago
	 */
	function setMovimientoId($value) {
		return $this->setColumnValue('movimiento_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for ConceptoPago::getMovimientoId
	 * final because getMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::getMovimientoId
	 */
	final function getMovimiento_id() {
		return $this->getMovimientoId();
	}

	/**
	 * Convenience function for ConceptoPago::setMovimientoId
	 * final because setMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::setMovimientoId
	 * @return ConceptoPago
	 */
	final function setMovimiento_id($value) {
		return $this->setMovimientoId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return ConceptoPago
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ConceptoPago::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for ConceptoPago::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::setCreatedAt
	 * @return ConceptoPago
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return ConceptoPago
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ConceptoPago::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for ConceptoPago::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::setUpdatedAt
	 * @return ConceptoPago
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return ConceptoPago
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for ConceptoPago::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for ConceptoPago::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see ConceptoPago::setDeletedAt
	 * @return ConceptoPago
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the monto field
	 */
	function getMonto() {
		return $this->monto;
	}

	/**
	 * Sets the value of the monto field
	 * @return ConceptoPago
	 */
	function setMonto($value) {
		return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return ConceptoPago
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return ConceptoPago
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveById($value) {
		return ConceptoPago::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a movimiento_id
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveByMovimientoId($value) {
		return static::retrieveByColumn('movimiento_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a monto
	 * value that matches the one provided
	 * @return ConceptoPago
	 */
	static function retrieveByMonto($value) {
		return static::retrieveByColumn('monto', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return ConceptoPago
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->movimiento_id = (null === $this->movimiento_id) ? null : (int) $this->movimiento_id;
		return $this;
	}

	/**
	 * @return ConceptoPago
	 */
	function setMovimiento(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return ConceptoPago
	 */
	function setMovimientosDeDineroRelatedByMovimientoId(MovimientosDeDinero $movimientosdedinero = null) {
		if (null === $movimientosdedinero) {
			$this->setmovimiento_id(null);
		} else {
			if (!$movimientosdedinero->getid()) {
				throw new Exception('Cannot connect a MovimientosDeDinero without a id');
			}
			$this->setmovimiento_id($movimientosdedinero->getid());
		}
		return $this;
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimiento() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDineroRelatedByMovimientoId() {
		$fk_value = $this->getmovimiento_id();
		if (null === $fk_value) {
			return null;
		}
		return MovimientosDeDinero::retrieveByPK($fk_value);
	}

	static function doSelectJoinMovimiento(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMovimientosDeDineroRelatedByMovimientoId($q, $join_type);
	}

	/**
	 * Returns a movimientos_de_dinero object with a id
	 * that matches $this->movimiento_id.
	 * @return MovimientosDeDinero
	 */
	function getMovimientosDeDinero() {
		return $this->getMovimientosDeDineroRelatedByMovimientoId();
	}

	/**
	 * @return ConceptoPago
	 */
	function setMovimientosDeDinero(MovimientosDeDinero $movimientosdedinero = null) {
		return $this->setMovimientosDeDineroRelatedByMovimientoId($movimientosdedinero);
	}

	/**
	 * @return ConceptoPago[]
	 */
	static function doSelectJoinMovimientosDeDineroRelatedByMovimientoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('MovimientosDeDinero'));
	}

	/**
	 * @return ConceptoPago[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MovimientosDeDinero::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (MovimientosDeDinero::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'MovimientosDeDinero';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		if (null === $this->getmovimiento_id()) {
			$this->_validationErrors[] = 'movimiento_id must not be null';
		}
		if (null === $this->getcreated_at()) {
			$this->_validationErrors[] = 'created_at must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}