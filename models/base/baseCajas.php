<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseCajas extends ApplicationModel {

	const ID = 'cajas.id';
	const SALDO_INICIAL = 'cajas.saldo_inicial';
	const SALDO_FINAL = 'cajas.saldo_final';
	const FECHA_APERTURA = 'cajas.fecha_apertura';
	const FECHA_CIERRE = 'cajas.fecha_cierre';
	const ESTADO = 'cajas.estado';
	const USUARIO_ID = 'cajas.usuario_id';
	const CREATED_AT = 'cajas.created_at';
	const UPDATED_AT = 'cajas.updated_at';
	const DELETED_AT = 'cajas.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'cajas';

	/**
	 * Cache of objects retrieved from the database
	 * @var Cajas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'saldo_inicial' => Model::COLUMN_TYPE_DECIMAL,
		'saldo_final' => Model::COLUMN_TYPE_DECIMAL,
		'fecha_apertura' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_cierre' => Model::COLUMN_TYPE_TIMESTAMP,
		'estado' => Model::COLUMN_TYPE_VARCHAR,
		'usuario_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `saldo_inicial` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $saldo_inicial;

	/**
	 * `saldo_final` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $saldo_final;

	/**
	 * `fecha_apertura` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_apertura;

	/**
	 * `fecha_cierre` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_cierre;

	/**
	 * `estado` VARCHAR NOT NULL
	 * @var string
	 */
	protected $estado;

	/**
	 * `usuario_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $usuario_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Cajas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the saldo_inicial field
	 */
	function getSaldoInicial() {
		return $this->saldo_inicial;
	}

	/**
	 * Sets the value of the saldo_inicial field
	 * @return Cajas
	 */
	function setSaldoInicial($value) {
		return $this->setColumnValue('saldo_inicial', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Convenience function for Cajas::getSaldoInicial
	 * final because getSaldoInicial should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getSaldoInicial
	 */
	final function getSaldo_inicial() {
		return $this->getSaldoInicial();
	}

	/**
	 * Convenience function for Cajas::setSaldoInicial
	 * final because setSaldoInicial should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setSaldoInicial
	 * @return Cajas
	 */
	final function setSaldo_inicial($value) {
		return $this->setSaldoInicial($value);
	}

	/**
	 * Gets the value of the saldo_final field
	 */
	function getSaldoFinal() {
		return $this->saldo_final;
	}

	/**
	 * Sets the value of the saldo_final field
	 * @return Cajas
	 */
	function setSaldoFinal($value) {
		return $this->setColumnValue('saldo_final', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Convenience function for Cajas::getSaldoFinal
	 * final because getSaldoFinal should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getSaldoFinal
	 */
	final function getSaldo_final() {
		return $this->getSaldoFinal();
	}

	/**
	 * Convenience function for Cajas::setSaldoFinal
	 * final because setSaldoFinal should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setSaldoFinal
	 * @return Cajas
	 */
	final function setSaldo_final($value) {
		return $this->setSaldoFinal($value);
	}

	/**
	 * Gets the value of the fecha_apertura field
	 */
	function getFechaApertura($format = null) {
		if (null === $this->fecha_apertura || null === $format) {
			return $this->fecha_apertura;
		}
		if (0 === strpos($this->fecha_apertura, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_apertura));
	}

	/**
	 * Sets the value of the fecha_apertura field
	 * @return Cajas
	 */
	function setFechaApertura($value) {
		return $this->setColumnValue('fecha_apertura', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getFechaApertura
	 * final because getFechaApertura should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getFechaApertura
	 */
	final function getFecha_apertura($format = null) {
		return $this->getFechaApertura($format);
	}

	/**
	 * Convenience function for Cajas::setFechaApertura
	 * final because setFechaApertura should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setFechaApertura
	 * @return Cajas
	 */
	final function setFecha_apertura($value) {
		return $this->setFechaApertura($value);
	}

	/**
	 * Gets the value of the fecha_cierre field
	 */
	function getFechaCierre($format = null) {
		if (null === $this->fecha_cierre || null === $format) {
			return $this->fecha_cierre;
		}
		if (0 === strpos($this->fecha_cierre, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_cierre));
	}

	/**
	 * Sets the value of the fecha_cierre field
	 * @return Cajas
	 */
	function setFechaCierre($value) {
		return $this->setColumnValue('fecha_cierre', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getFechaCierre
	 * final because getFechaCierre should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getFechaCierre
	 */
	final function getFecha_cierre($format = null) {
		return $this->getFechaCierre($format);
	}

	/**
	 * Convenience function for Cajas::setFechaCierre
	 * final because setFechaCierre should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setFechaCierre
	 * @return Cajas
	 */
	final function setFecha_cierre($value) {
		return $this->setFechaCierre($value);
	}

	/**
	 * Gets the value of the estado field
	 */
	function getEstado() {
		return $this->estado;
	}

	/**
	 * Sets the value of the estado field
	 * @return Cajas
	 */
	function setEstado($value) {
		return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the usuario_id field
	 */
	function getUsuarioId() {
		return $this->usuario_id;
	}

	/**
	 * Sets the value of the usuario_id field
	 * @return Cajas
	 */
	function setUsuarioId($value) {
		return $this->setColumnValue('usuario_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Cajas::getUsuarioId
	 * final because getUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getUsuarioId
	 */
	final function getUsuario_id() {
		return $this->getUsuarioId();
	}

	/**
	 * Convenience function for Cajas::setUsuarioId
	 * final because setUsuarioId should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setUsuarioId
	 * @return Cajas
	 */
	final function setUsuario_id($value) {
		return $this->setUsuarioId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Cajas
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Cajas::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setCreatedAt
	 * @return Cajas
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Cajas
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Cajas::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setUpdatedAt
	 * @return Cajas
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Cajas
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cajas::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Cajas::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cajas::setDeletedAt
	 * @return Cajas
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Cajas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Cajas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveById($value) {
		return Cajas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a saldo_inicial
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveBySaldoInicial($value) {
		return static::retrieveByColumn('saldo_inicial', $value);
	}

	/**
	 * Searches the database for a row with a saldo_final
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveBySaldoFinal($value) {
		return static::retrieveByColumn('saldo_final', $value);
	}

	/**
	 * Searches the database for a row with a fecha_apertura
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByFechaApertura($value) {
		return static::retrieveByColumn('fecha_apertura', $value);
	}

	/**
	 * Searches the database for a row with a fecha_cierre
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByFechaCierre($value) {
		return static::retrieveByColumn('fecha_cierre', $value);
	}

	/**
	 * Searches the database for a row with a estado
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByEstado($value) {
		return static::retrieveByColumn('estado', $value);
	}

	/**
	 * Searches the database for a row with a usuario_id
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByUsuarioId($value) {
		return static::retrieveByColumn('usuario_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Cajas
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Cajas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->usuario_id = (null === $this->usuario_id) ? null : (int) $this->usuario_id;
		return $this;
	}

	/**
	 * @return Cajas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting movimientos Objects(rows) from the movimientos table
	 * with a caja_id that matches $this->id.
	 * @return Query
	 */
	function getMovimientossRelatedByCajaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos', 'caja_id', 'id', $q);
	}

	/**
	 * Returns the count of Movimientos Objects(rows) from the movimientos table
	 * with a caja_id that matches $this->id.
	 * @return int
	 */
	function countMovimientossRelatedByCajaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Movimientos::doCount($this->getMovimientossRelatedByCajaIdQuery($q));
	}

	/**
	 * Deletes the movimientos Objects(rows) from the movimientos table
	 * with a caja_id that matches $this->id.
	 * @return int
	 */
	function deleteMovimientossRelatedByCajaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MovimientossRelatedByCajaId_c = array();
		return Movimientos::doDelete($this->getMovimientossRelatedByCajaIdQuery($q));
	}

	protected $MovimientossRelatedByCajaId_c = array();

	/**
	 * Returns an array of Movimientos objects with a caja_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Movimientos[]
	 */
	function getMovimientossRelatedByCajaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MovimientossRelatedByCajaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MovimientossRelatedByCajaId_c;
		}

		$result = Movimientos::doSelect($this->getMovimientossRelatedByCajaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MovimientossRelatedByCajaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Cajas::getMovimientossRelatedBycaja_id
	 * @return Movimientos[]
	 * @see Cajas::getMovimientossRelatedByCajaId
	 */
	function getMovimientoss($extra = null) {
		return $this->getMovimientossRelatedByCajaId($extra);
	}

	/**
	  * Convenience function for Cajas::getMovimientossRelatedBycaja_idQuery
	  * @return Query
	  * @see Cajas::getMovimientossRelatedBycaja_idQuery
	  */
	function getMovimientossQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('movimientos', 'caja_id','id', $q);
	}

	/**
	  * Convenience function for Cajas::deleteMovimientossRelatedBycaja_id
	  * @return int
	  * @see Cajas::deleteMovimientossRelatedBycaja_id
	  */
	function deleteMovimientoss(Query $q = null) {
		return $this->deleteMovimientossRelatedByCajaId($q);
	}

	/**
	  * Convenience function for Cajas::countMovimientossRelatedBycaja_id
	  * @return int
	  * @see Cajas::countMovimientossRelatedByCajaId
	  */
	function countMovimientoss(Query $q = null) {
		return $this->countMovimientossRelatedByCajaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();

		if (null === $this->getusuario_id()) {
			$this->_validationErrors[] = 'usuario_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}