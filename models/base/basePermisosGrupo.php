<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class basePermisosGrupo extends ApplicationModel {

	const ID = 'permisos_grupo.id';
	const IDPERMISO = 'permisos_grupo.idpermiso';
	const IDGRUPO = 'permisos_grupo.idgrupo';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'permisos_grupo';

	/**
	 * Cache of objects retrieved from the database
	 * @var PermisosGrupo[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'idpermiso' => Model::COLUMN_TYPE_INTEGER,
		'idgrupo' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `idpermiso` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $idpermiso;

	/**
	 * `idgrupo` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $idgrupo;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return PermisosGrupo
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idpermiso field
	 */
	function getIdpermiso() {
		return $this->idpermiso;
	}

	/**
	 * Sets the value of the idpermiso field
	 * @return PermisosGrupo
	 */
	function setIdpermiso($value) {
		return $this->setColumnValue('idpermiso', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idgrupo field
	 */
	function getIdgrupo() {
		return $this->idgrupo;
	}

	/**
	 * Sets the value of the idgrupo field
	 * @return PermisosGrupo
	 */
	function setIdgrupo($value) {
		return $this->setColumnValue('idgrupo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return PermisosGrupo
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return PermisosGrupo
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return PermisosGrupo
	 */
	static function retrieveById($value) {
		return PermisosGrupo::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a idpermiso
	 * value that matches the one provided
	 * @return PermisosGrupo
	 */
	static function retrieveByIdpermiso($value) {
		return static::retrieveByColumn('idpermiso', $value);
	}

	/**
	 * Searches the database for a row with a idgrupo
	 * value that matches the one provided
	 * @return PermisosGrupo
	 */
	static function retrieveByIdgrupo($value) {
		return static::retrieveByColumn('idgrupo', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return PermisosGrupo
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->idpermiso = (null === $this->idpermiso) ? null : (int) $this->idpermiso;
		$this->idgrupo = (null === $this->idgrupo) ? null : (int) $this->idgrupo;
		return $this;
	}

	/**
	 * @return PermisosGrupo
	 */
	function setPermisosRelatedByIdpermiso(Permisos $permisos = null) {
		if (null === $permisos) {
			$this->setidpermiso(null);
		} else {
			if (!$permisos->getid()) {
				throw new Exception('Cannot connect a Permisos without a id');
			}
			$this->setidpermiso($permisos->getid());
		}
		return $this;
	}

	/**
	 * Returns a permisos object with a id
	 * that matches $this->idpermiso.
	 * @return Permisos
	 */
	function getPermisosRelatedByIdpermiso() {
		$fk_value = $this->getidpermiso();
		if (null === $fk_value) {
			return null;
		}
		return Permisos::retrieveByPK($fk_value);
	}

	/**
	 * Returns a permisos object with a id
	 * that matches $this->idpermiso.
	 * @return Permisos
	 */
	function getPermisos() {
		return $this->getPermisosRelatedByIdpermiso();
	}

	/**
	 * @return PermisosGrupo
	 */
	function setPermisos(Permisos $permisos = null) {
		return $this->setPermisosRelatedByIdpermiso($permisos);
	}

	/**
	 * @return PermisosGrupo[]
	 */
	static function doSelectJoinPermisosRelatedByIdpermiso(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Permisos::getTableName();
		$q->join($to_table, $this_table . '.idpermiso = ' . $to_table . '.id', $join_type);
		foreach (Permisos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Permisos'));
	}

	/**
	 * @return PermisosGrupo
	 */
	function setGruposRelatedByIdgrupo(Grupos $grupos = null) {
		if (null === $grupos) {
			$this->setidgrupo(null);
		} else {
			if (!$grupos->getid()) {
				throw new Exception('Cannot connect a Grupos without a id');
			}
			$this->setidgrupo($grupos->getid());
		}
		return $this;
	}

	/**
	 * Returns a grupos object with a id
	 * that matches $this->idgrupo.
	 * @return Grupos
	 */
	function getGruposRelatedByIdgrupo() {
		$fk_value = $this->getidgrupo();
		if (null === $fk_value) {
			return null;
		}
		return Grupos::retrieveByPK($fk_value);
	}

	/**
	 * Returns a grupos object with a id
	 * that matches $this->idgrupo.
	 * @return Grupos
	 */
	function getGrupos() {
		return $this->getGruposRelatedByIdgrupo();
	}

	/**
	 * @return PermisosGrupo
	 */
	function setGrupos(Grupos $grupos = null) {
		return $this->setGruposRelatedByIdgrupo($grupos);
	}

	/**
	 * @return PermisosGrupo[]
	 */
	static function doSelectJoinGruposRelatedByIdgrupo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Grupos::getTableName();
		$q->join($to_table, $this_table . '.idgrupo = ' . $to_table . '.id', $join_type);
		foreach (Grupos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Grupos'));
	}

	/**
	 * @return PermisosGrupo[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Permisos::getTableName();
		$q->join($to_table, $this_table . '.idpermiso = ' . $to_table . '.id', $join_type);
		foreach (Permisos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Permisos';
	
		$to_table = Grupos::getTableName();
		$q->join($to_table, $this_table . '.idgrupo = ' . $to_table . '.id', $join_type);
		foreach (Grupos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Grupos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getidpermiso()) {
			$this->_validationErrors[] = 'idpermiso must not be null';
		}
		if (null === $this->getidgrupo()) {
			$this->_validationErrors[] = 'idgrupo must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}