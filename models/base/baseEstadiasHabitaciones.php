<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseEstadiasHabitaciones extends ApplicationModel {

	const ID = 'estadias_habitaciones.id';
	const DESCUENTO = 'estadias_habitaciones.descuento';
	const ASISTENTE = 'estadias_habitaciones.asistente';
	const ESTADIA_ID = 'estadias_habitaciones.estadia_id';
	const HABITACION_ID = 'estadias_habitaciones.habitacion_id';
	const CREATED_AT = 'estadias_habitaciones.created_at';
	const UPDATED_AT = 'estadias_habitaciones.updated_at';
	const DELETED_AT = 'estadias_habitaciones.deleted_at';
	const FECHA_RESERVA = 'estadias_habitaciones.fecha_reserva';
	const FECHA_INGRESO = 'estadias_habitaciones.fecha_ingreso';
	const FECHA_SALIDA = 'estadias_habitaciones.fecha_salida';
	const OBSERVACION = 'estadias_habitaciones.observacion';
	const PRECIO_TOTAL = 'estadias_habitaciones.precio_total';
	const DIAS = 'estadias_habitaciones.dias';
	const TIPO_HABITACION = 'estadias_habitaciones.tipo_habitacion';
	const ESTADO_ES_HA = 'estadias_habitaciones.estado_es_ha';
	const AUMENTO = 'estadias_habitaciones.aumento';
	const TIPO_PRECIO = 'estadias_habitaciones.tipo_precio';
	const PRECIO = 'estadias_habitaciones.precio';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'estadias_habitaciones';

	/**
	 * Cache of objects retrieved from the database
	 * @var EstadiasHabitaciones[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descuento' => Model::COLUMN_TYPE_DECIMAL,
		'asistente' => Model::COLUMN_TYPE_VARCHAR,
		'estadia_id' => Model::COLUMN_TYPE_INTEGER,
		'habitacion_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_reserva' => Model::COLUMN_TYPE_DATE,
		'fecha_ingreso' => Model::COLUMN_TYPE_DATE,
		'fecha_salida' => Model::COLUMN_TYPE_DATE,
		'observacion' => Model::COLUMN_TYPE_VARCHAR,
		'precio_total' => Model::COLUMN_TYPE_DECIMAL,
		'dias' => Model::COLUMN_TYPE_INTEGER,
		'tipo_habitacion' => Model::COLUMN_TYPE_INTEGER,
		'estado_es_ha' => Model::COLUMN_TYPE_BOOLEAN,
		'aumento' => Model::COLUMN_TYPE_DECIMAL,
		'tipo_precio' => Model::COLUMN_TYPE_BOOLEAN,
		'precio' => Model::COLUMN_TYPE_DECIMAL,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descuento` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $descuento;

	/**
	 * `asistente` VARCHAR
	 * @var string
	 */
	protected $asistente;

	/**
	 * `estadia_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $estadia_id;

	/**
	 * `habitacion_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $habitacion_id;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `fecha_reserva` DATE
	 * @var string
	 */
	protected $fecha_reserva;

	/**
	 * `fecha_ingreso` DATE
	 * @var string
	 */
	protected $fecha_ingreso;

	/**
	 * `fecha_salida` DATE
	 * @var string
	 */
	protected $fecha_salida;

	/**
	 * `observacion` VARCHAR
	 * @var string
	 */
	protected $observacion;

	/**
	 * `precio_total` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $precio_total;

	/**
	 * `dias` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $dias;

	/**
	 * `tipo_habitacion` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $tipo_habitacion;

	/**
	 * `estado_es_ha` BOOLEAN DEFAULT 'b\'1\''
	 * @var boolean
	 */
	protected $estado_es_ha = 'b\'1\'';

	/**
	 * `aumento` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $aumento;

	/**
	 * `tipo_precio` BOOLEAN DEFAULT 'b\'1\''
	 * @var boolean
	 */
	protected $tipo_precio = 'b\'1\'';

	/**
	 * `precio` DECIMAL DEFAULT ''
	 * @var string
	 */
	protected $precio;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return EstadiasHabitaciones
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descuento field
	 */
	function getDescuento() {
		return $this->descuento;
	}

	/**
	 * Sets the value of the descuento field
	 * @return EstadiasHabitaciones
	 */
	function setDescuento($value) {
		return $this->setColumnValue('descuento', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the asistente field
	 */
	function getAsistente() {
		return $this->asistente;
	}

	/**
	 * Sets the value of the asistente field
	 * @return EstadiasHabitaciones
	 */
	function setAsistente($value) {
		return $this->setColumnValue('asistente', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the estadia_id field
	 */
	function getEstadiaId() {
		return $this->estadia_id;
	}

	/**
	 * Sets the value of the estadia_id field
	 * @return EstadiasHabitaciones
	 */
	function setEstadiaId($value) {
		return $this->setColumnValue('estadia_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getEstadiaId
	 * final because getEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getEstadiaId
	 */
	final function getEstadia_id() {
		return $this->getEstadiaId();
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setEstadiaId
	 * final because setEstadiaId should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setEstadiaId
	 * @return EstadiasHabitaciones
	 */
	final function setEstadia_id($value) {
		return $this->setEstadiaId($value);
	}

	/**
	 * Gets the value of the habitacion_id field
	 */
	function getHabitacionId() {
		return $this->habitacion_id;
	}

	/**
	 * Sets the value of the habitacion_id field
	 * @return EstadiasHabitaciones
	 */
	function setHabitacionId($value) {
		return $this->setColumnValue('habitacion_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getHabitacionId
	 * final because getHabitacionId should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getHabitacionId
	 */
	final function getHabitacion_id() {
		return $this->getHabitacionId();
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setHabitacionId
	 * final because setHabitacionId should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setHabitacionId
	 * @return EstadiasHabitaciones
	 */
	final function setHabitacion_id($value) {
		return $this->setHabitacionId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return EstadiasHabitaciones
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setCreatedAt
	 * @return EstadiasHabitaciones
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return EstadiasHabitaciones
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setUpdatedAt
	 * @return EstadiasHabitaciones
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return EstadiasHabitaciones
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setDeletedAt
	 * @return EstadiasHabitaciones
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the fecha_reserva field
	 */
	function getFechaReserva($format = null) {
		if (null === $this->fecha_reserva || null === $format) {
			return $this->fecha_reserva;
		}
		if (0 === strpos($this->fecha_reserva, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_reserva));
	}

	/**
	 * Sets the value of the fecha_reserva field
	 * @return EstadiasHabitaciones
	 */
	function setFechaReserva($value) {
		return $this->setColumnValue('fecha_reserva', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getFechaReserva
	 * final because getFechaReserva should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getFechaReserva
	 */
	final function getFecha_reserva($format = null) {
		return $this->getFechaReserva($format);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setFechaReserva
	 * final because setFechaReserva should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setFechaReserva
	 * @return EstadiasHabitaciones
	 */
	final function setFecha_reserva($value) {
		return $this->setFechaReserva($value);
	}

	/**
	 * Gets the value of the fecha_ingreso field
	 */
	function getFechaIngreso($format = null) {
		if (null === $this->fecha_ingreso || null === $format) {
			return $this->fecha_ingreso;
		}
		if (0 === strpos($this->fecha_ingreso, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_ingreso));
	}

	/**
	 * Sets the value of the fecha_ingreso field
	 * @return EstadiasHabitaciones
	 */
	function setFechaIngreso($value) {
		return $this->setColumnValue('fecha_ingreso', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getFechaIngreso
	 * final because getFechaIngreso should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getFechaIngreso
	 */
	final function getFecha_ingreso($format = null) {
		return $this->getFechaIngreso($format);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setFechaIngreso
	 * final because setFechaIngreso should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setFechaIngreso
	 * @return EstadiasHabitaciones
	 */
	final function setFecha_ingreso($value) {
		return $this->setFechaIngreso($value);
	}

	/**
	 * Gets the value of the fecha_salida field
	 */
	function getFechaSalida($format = null) {
		if (null === $this->fecha_salida || null === $format) {
			return $this->fecha_salida;
		}
		if (0 === strpos($this->fecha_salida, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_salida));
	}

	/**
	 * Sets the value of the fecha_salida field
	 * @return EstadiasHabitaciones
	 */
	function setFechaSalida($value) {
		return $this->setColumnValue('fecha_salida', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getFechaSalida
	 * final because getFechaSalida should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getFechaSalida
	 */
	final function getFecha_salida($format = null) {
		return $this->getFechaSalida($format);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setFechaSalida
	 * final because setFechaSalida should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setFechaSalida
	 * @return EstadiasHabitaciones
	 */
	final function setFecha_salida($value) {
		return $this->setFechaSalida($value);
	}

	/**
	 * Gets the value of the observacion field
	 */
	function getObservacion() {
		return $this->observacion;
	}

	/**
	 * Sets the value of the observacion field
	 * @return EstadiasHabitaciones
	 */
	function setObservacion($value) {
		return $this->setColumnValue('observacion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the precio_total field
	 */
	function getPrecioTotal() {
		return $this->precio_total;
	}

	/**
	 * Sets the value of the precio_total field
	 * @return EstadiasHabitaciones
	 */
	function setPrecioTotal($value) {
		return $this->setColumnValue('precio_total', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getPrecioTotal
	 * final because getPrecioTotal should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getPrecioTotal
	 */
	final function getPrecio_total() {
		return $this->getPrecioTotal();
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setPrecioTotal
	 * final because setPrecioTotal should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setPrecioTotal
	 * @return EstadiasHabitaciones
	 */
	final function setPrecio_total($value) {
		return $this->setPrecioTotal($value);
	}

	/**
	 * Gets the value of the dias field
	 */
	function getDias() {
		return $this->dias;
	}

	/**
	 * Sets the value of the dias field
	 * @return EstadiasHabitaciones
	 */
	function setDias($value) {
		return $this->setColumnValue('dias', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the tipo_habitacion field
	 */
	function getTipoHabitacion() {
		return $this->tipo_habitacion;
	}

	/**
	 * Sets the value of the tipo_habitacion field
	 * @return EstadiasHabitaciones
	 */
	function setTipoHabitacion($value) {
		return $this->setColumnValue('tipo_habitacion', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getTipoHabitacion
	 * final because getTipoHabitacion should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getTipoHabitacion
	 */
	final function getTipo_habitacion() {
		return $this->getTipoHabitacion();
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setTipoHabitacion
	 * final because setTipoHabitacion should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setTipoHabitacion
	 * @return EstadiasHabitaciones
	 */
	final function setTipo_habitacion($value) {
		return $this->setTipoHabitacion($value);
	}

	/**
	 * Gets the value of the estado_es_ha field
	 */
	function getEstadoEsHa() {
		return $this->estado_es_ha;
	}

	/**
	 * Sets the value of the estado_es_ha field
	 * @return EstadiasHabitaciones
	 */
	function setEstadoEsHa($value) {
		return $this->setColumnValue('estado_es_ha', $value, Model::COLUMN_TYPE_BOOLEAN);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getEstadoEsHa
	 * final because getEstadoEsHa should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getEstadoEsHa
	 */
	final function getEstado_es_ha() {
		return $this->getEstadoEsHa();
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setEstadoEsHa
	 * final because setEstadoEsHa should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setEstadoEsHa
	 * @return EstadiasHabitaciones
	 */
	final function setEstado_es_ha($value) {
		return $this->setEstadoEsHa($value);
	}

	/**
	 * Gets the value of the aumento field
	 */
	function getAumento() {
		return $this->aumento;
	}

	/**
	 * Sets the value of the aumento field
	 * @return EstadiasHabitaciones
	 */
	function setAumento($value) {
		return $this->setColumnValue('aumento', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the tipo_precio field
	 */
	function getTipoPrecio() {
		return $this->tipo_precio;
	}

	/**
	 * Sets the value of the tipo_precio field
	 * @return EstadiasHabitaciones
	 */
	function setTipoPrecio($value) {
		return $this->setColumnValue('tipo_precio', $value, Model::COLUMN_TYPE_BOOLEAN);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getTipoPrecio
	 * final because getTipoPrecio should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::getTipoPrecio
	 */
	final function getTipo_precio() {
		return $this->getTipoPrecio();
	}

	/**
	 * Convenience function for EstadiasHabitaciones::setTipoPrecio
	 * final because setTipoPrecio should be extended instead
	 * to ensure consistent behavior
	 * @see EstadiasHabitaciones::setTipoPrecio
	 * @return EstadiasHabitaciones
	 */
	final function setTipo_precio($value) {
		return $this->setTipoPrecio($value);
	}

	/**
	 * Gets the value of the precio field
	 */
	function getPrecio() {
		return $this->precio;
	}

	/**
	 * Sets the value of the precio field
	 * @return EstadiasHabitaciones
	 */
	function setPrecio($value) {
		return $this->setColumnValue('precio', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveById($value) {
		return EstadiasHabitaciones::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descuento
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByDescuento($value) {
		return static::retrieveByColumn('descuento', $value);
	}

	/**
	 * Searches the database for a row with a asistente
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByAsistente($value) {
		return static::retrieveByColumn('asistente', $value);
	}

	/**
	 * Searches the database for a row with a estadia_id
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByEstadiaId($value) {
		return static::retrieveByColumn('estadia_id', $value);
	}

	/**
	 * Searches the database for a row with a habitacion_id
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByHabitacionId($value) {
		return static::retrieveByColumn('habitacion_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a fecha_reserva
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByFechaReserva($value) {
		return static::retrieveByColumn('fecha_reserva', $value);
	}

	/**
	 * Searches the database for a row with a fecha_ingreso
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByFechaIngreso($value) {
		return static::retrieveByColumn('fecha_ingreso', $value);
	}

	/**
	 * Searches the database for a row with a fecha_salida
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByFechaSalida($value) {
		return static::retrieveByColumn('fecha_salida', $value);
	}

	/**
	 * Searches the database for a row with a observacion
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByObservacion($value) {
		return static::retrieveByColumn('observacion', $value);
	}

	/**
	 * Searches the database for a row with a precio_total
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByPrecioTotal($value) {
		return static::retrieveByColumn('precio_total', $value);
	}

	/**
	 * Searches the database for a row with a dias
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByDias($value) {
		return static::retrieveByColumn('dias', $value);
	}

	/**
	 * Searches the database for a row with a tipo_habitacion
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByTipoHabitacion($value) {
		return static::retrieveByColumn('tipo_habitacion', $value);
	}

	/**
	 * Searches the database for a row with a estado_es_ha
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByEstadoEsHa($value) {
		return static::retrieveByColumn('estado_es_ha', $value);
	}

	/**
	 * Searches the database for a row with a aumento
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByAumento($value) {
		return static::retrieveByColumn('aumento', $value);
	}

	/**
	 * Searches the database for a row with a tipo_precio
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByTipoPrecio($value) {
		return static::retrieveByColumn('tipo_precio', $value);
	}

	/**
	 * Searches the database for a row with a precio
	 * value that matches the one provided
	 * @return EstadiasHabitaciones
	 */
	static function retrieveByPrecio($value) {
		return static::retrieveByColumn('precio', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return EstadiasHabitaciones
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->estadia_id = (null === $this->estadia_id) ? null : (int) $this->estadia_id;
		$this->habitacion_id = (null === $this->habitacion_id) ? null : (int) $this->habitacion_id;
		$this->dias = (null === $this->dias) ? null : (int) $this->dias;
		$this->tipo_habitacion = (null === $this->tipo_habitacion) ? null : (int) $this->tipo_habitacion;
		$this->estado_es_ha = (null === $this->estado_es_ha) ? null : (int) $this->estado_es_ha;
		$this->tipo_precio = (null === $this->tipo_precio) ? null : (int) $this->tipo_precio;
		return $this;
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setEstadia(Estadias $estadias = null) {
		return $this->setEstadiasRelatedByEstadiaId($estadias);
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setEstadiasRelatedByEstadiaId(Estadias $estadias = null) {
		if (null === $estadias) {
			$this->setestadia_id(null);
		} else {
			if (!$estadias->getid()) {
				throw new Exception('Cannot connect a Estadias without a id');
			}
			$this->setestadia_id($estadias->getid());
		}
		return $this;
	}

	/**
	 * Returns a estadias object with a id
	 * that matches $this->estadia_id.
	 * @return Estadias
	 */
	function getEstadia() {
		return $this->getEstadiasRelatedByEstadiaId();
	}

	/**
	 * Returns a estadias object with a id
	 * that matches $this->estadia_id.
	 * @return Estadias
	 */
	function getEstadiasRelatedByEstadiaId() {
		$fk_value = $this->getestadia_id();
		if (null === $fk_value) {
			return null;
		}
		return Estadias::retrieveByPK($fk_value);
	}

	static function doSelectJoinEstadia(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinEstadiasRelatedByEstadiaId($q, $join_type);
	}

	/**
	 * Returns a estadias object with a id
	 * that matches $this->estadia_id.
	 * @return Estadias
	 */
	function getEstadias() {
		return $this->getEstadiasRelatedByEstadiaId();
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setEstadias(Estadias $estadias = null) {
		return $this->setEstadiasRelatedByEstadiaId($estadias);
	}

	/**
	 * @return EstadiasHabitaciones[]
	 */
	static function doSelectJoinEstadiasRelatedByEstadiaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Estadias::getTableName();
		$q->join($to_table, $this_table . '.estadia_id = ' . $to_table . '.id', $join_type);
		foreach (Estadias::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Estadias'));
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setHabitacion(Habitaciones $habitaciones = null) {
		return $this->setHabitacionesRelatedByHabitacionId($habitaciones);
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setHabitacionesRelatedByHabitacionId(Habitaciones $habitaciones = null) {
		if (null === $habitaciones) {
			$this->sethabitacion_id(null);
		} else {
			if (!$habitaciones->getid()) {
				throw new Exception('Cannot connect a Habitaciones without a id');
			}
			$this->sethabitacion_id($habitaciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a habitaciones object with a id
	 * that matches $this->habitacion_id.
	 * @return Habitaciones
	 */
	function getHabitacion() {
		return $this->getHabitacionesRelatedByHabitacionId();
	}

	/**
	 * Returns a habitaciones object with a id
	 * that matches $this->habitacion_id.
	 * @return Habitaciones
	 */
	function getHabitacionesRelatedByHabitacionId() {
		$fk_value = $this->gethabitacion_id();
		if (null === $fk_value) {
			return null;
		}
		return Habitaciones::retrieveByPK($fk_value);
	}

	static function doSelectJoinHabitacion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinHabitacionesRelatedByHabitacionId($q, $join_type);
	}

	/**
	 * Returns a habitaciones object with a id
	 * that matches $this->habitacion_id.
	 * @return Habitaciones
	 */
	function getHabitaciones() {
		return $this->getHabitacionesRelatedByHabitacionId();
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setHabitaciones(Habitaciones $habitaciones = null) {
		return $this->setHabitacionesRelatedByHabitacionId($habitaciones);
	}

	/**
	 * @return EstadiasHabitaciones[]
	 */
	static function doSelectJoinHabitacionesRelatedByHabitacionId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Habitaciones::getTableName();
		$q->join($to_table, $this_table . '.habitacion_id = ' . $to_table . '.id', $join_type);
		foreach (Habitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Habitaciones'));
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setTiposHabitacionesRelatedByTipoHabitacion(TiposHabitaciones $tiposhabitaciones = null) {
		if (null === $tiposhabitaciones) {
			$this->settipo_habitacion(null);
		} else {
			if (!$tiposhabitaciones->getid()) {
				throw new Exception('Cannot connect a TiposHabitaciones without a id');
			}
			$this->settipo_habitacion($tiposhabitaciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipos_habitaciones object with a id
	 * that matches $this->tipo_habitacion.
	 * @return TiposHabitaciones
	 */
	function getTiposHabitacionesRelatedByTipoHabitacion() {
		$fk_value = $this->gettipo_habitacion();
		if (null === $fk_value) {
			return null;
		}
		return TiposHabitaciones::retrieveByPK($fk_value);
	}

	/**
	 * Returns a tipos_habitaciones object with a id
	 * that matches $this->tipo_habitacion.
	 * @return TiposHabitaciones
	 */
	function getTiposHabitaciones() {
		return $this->getTiposHabitacionesRelatedByTipoHabitacion();
	}

	/**
	 * @return EstadiasHabitaciones
	 */
	function setTiposHabitaciones(TiposHabitaciones $tiposhabitaciones = null) {
		return $this->setTiposHabitacionesRelatedByTipoHabitacion($tiposhabitaciones);
	}

	/**
	 * @return EstadiasHabitaciones[]
	 */
	static function doSelectJoinTiposHabitacionesRelatedByTipoHabitacion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TiposHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.tipo_habitacion = ' . $to_table . '.id', $join_type);
		foreach (TiposHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('TiposHabitaciones'));
	}

	/**
	 * @return EstadiasHabitaciones[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Estadias::getTableName();
		$q->join($to_table, $this_table . '.estadia_id = ' . $to_table . '.id', $join_type);
		foreach (Estadias::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Estadias';
	
		$to_table = Habitaciones::getTableName();
		$q->join($to_table, $this_table . '.habitacion_id = ' . $to_table . '.id', $join_type);
		foreach (Habitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Habitaciones';
	
		$to_table = TiposHabitaciones::getTableName();
		$q->join($to_table, $this_table . '.tipo_habitacion = ' . $to_table . '.id', $join_type);
		foreach (TiposHabitaciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'TiposHabitaciones';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting clientes_estadias Objects(rows) from the clientes_estadias table
	 * with a habitacion_estadia_id that matches $this->id.
	 * @return Query
	 */
	function getClientesEstadiassRelatedByHabitacionEstadiaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_estadias', 'habitacion_estadia_id', 'id', $q);
	}

	/**
	 * Returns the count of ClientesEstadias Objects(rows) from the clientes_estadias table
	 * with a habitacion_estadia_id that matches $this->id.
	 * @return int
	 */
	function countClientesEstadiassRelatedByHabitacionEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ClientesEstadias::doCount($this->getClientesEstadiassRelatedByHabitacionEstadiaIdQuery($q));
	}

	/**
	 * Deletes the clientes_estadias Objects(rows) from the clientes_estadias table
	 * with a habitacion_estadia_id that matches $this->id.
	 * @return int
	 */
	function deleteClientesEstadiassRelatedByHabitacionEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ClientesEstadiassRelatedByHabitacionEstadiaId_c = array();
		return ClientesEstadias::doDelete($this->getClientesEstadiassRelatedByHabitacionEstadiaIdQuery($q));
	}

	protected $ClientesEstadiassRelatedByHabitacionEstadiaId_c = array();

	/**
	 * Returns an array of ClientesEstadias objects with a habitacion_estadia_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ClientesEstadias[]
	 */
	function getClientesEstadiassRelatedByHabitacionEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ClientesEstadiassRelatedByHabitacionEstadiaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ClientesEstadiassRelatedByHabitacionEstadiaId_c;
		}

		$result = ClientesEstadias::doSelect($this->getClientesEstadiassRelatedByHabitacionEstadiaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ClientesEstadiassRelatedByHabitacionEstadiaId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting producto_consumido Objects(rows) from the producto_consumido table
	 * with a habita_estadia_id that matches $this->id.
	 * @return Query
	 */
	function getProductoConsumidosRelatedByHabitaEstadiaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_consumido', 'habita_estadia_id', 'id', $q);
	}

	/**
	 * Returns the count of ProductoConsumido Objects(rows) from the producto_consumido table
	 * with a habita_estadia_id that matches $this->id.
	 * @return int
	 */
	function countProductoConsumidosRelatedByHabitaEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return ProductoConsumido::doCount($this->getProductoConsumidosRelatedByHabitaEstadiaIdQuery($q));
	}

	/**
	 * Deletes the producto_consumido Objects(rows) from the producto_consumido table
	 * with a habita_estadia_id that matches $this->id.
	 * @return int
	 */
	function deleteProductoConsumidosRelatedByHabitaEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ProductoConsumidosRelatedByHabitaEstadiaId_c = array();
		return ProductoConsumido::doDelete($this->getProductoConsumidosRelatedByHabitaEstadiaIdQuery($q));
	}

	protected $ProductoConsumidosRelatedByHabitaEstadiaId_c = array();

	/**
	 * Returns an array of ProductoConsumido objects with a habita_estadia_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return ProductoConsumido[]
	 */
	function getProductoConsumidosRelatedByHabitaEstadiaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ProductoConsumidosRelatedByHabitaEstadiaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ProductoConsumidosRelatedByHabitaEstadiaId_c;
		}

		$result = ProductoConsumido::doSelect($this->getProductoConsumidosRelatedByHabitaEstadiaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ProductoConsumidosRelatedByHabitaEstadiaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getClientesEstadiassRelatedByhabitacion_estadia_id
	 * @return ClientesEstadias[]
	 * @see EstadiasHabitaciones::getClientesEstadiassRelatedByHabitacionEstadiaId
	 */
	function getClientesEstadiass($extra = null) {
		return $this->getClientesEstadiassRelatedByHabitacionEstadiaId($extra);
	}

	/**
	  * Convenience function for EstadiasHabitaciones::getClientesEstadiassRelatedByhabitacion_estadia_idQuery
	  * @return Query
	  * @see EstadiasHabitaciones::getClientesEstadiassRelatedByhabitacion_estadia_idQuery
	  */
	function getClientesEstadiassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('clientes_estadias', 'habitacion_estadia_id','id', $q);
	}

	/**
	  * Convenience function for EstadiasHabitaciones::deleteClientesEstadiassRelatedByhabitacion_estadia_id
	  * @return int
	  * @see EstadiasHabitaciones::deleteClientesEstadiassRelatedByhabitacion_estadia_id
	  */
	function deleteClientesEstadiass(Query $q = null) {
		return $this->deleteClientesEstadiassRelatedByHabitacionEstadiaId($q);
	}

	/**
	  * Convenience function for EstadiasHabitaciones::countClientesEstadiassRelatedByhabitacion_estadia_id
	  * @return int
	  * @see EstadiasHabitaciones::countClientesEstadiassRelatedByHabitacionEstadiaId
	  */
	function countClientesEstadiass(Query $q = null) {
		return $this->countClientesEstadiassRelatedByHabitacionEstadiaId($q);
	}

	/**
	 * Convenience function for EstadiasHabitaciones::getProductoConsumidosRelatedByhabita_estadia_id
	 * @return ProductoConsumido[]
	 * @see EstadiasHabitaciones::getProductoConsumidosRelatedByHabitaEstadiaId
	 */
	function getProductoConsumidos($extra = null) {
		return $this->getProductoConsumidosRelatedByHabitaEstadiaId($extra);
	}

	/**
	  * Convenience function for EstadiasHabitaciones::getProductoConsumidosRelatedByhabita_estadia_idQuery
	  * @return Query
	  * @see EstadiasHabitaciones::getProductoConsumidosRelatedByhabita_estadia_idQuery
	  */
	function getProductoConsumidosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('producto_consumido', 'habita_estadia_id','id', $q);
	}

	/**
	  * Convenience function for EstadiasHabitaciones::deleteProductoConsumidosRelatedByhabita_estadia_id
	  * @return int
	  * @see EstadiasHabitaciones::deleteProductoConsumidosRelatedByhabita_estadia_id
	  */
	function deleteProductoConsumidos(Query $q = null) {
		return $this->deleteProductoConsumidosRelatedByHabitaEstadiaId($q);
	}

	/**
	  * Convenience function for EstadiasHabitaciones::countProductoConsumidosRelatedByhabita_estadia_id
	  * @return int
	  * @see EstadiasHabitaciones::countProductoConsumidosRelatedByHabitaEstadiaId
	  */
	function countProductoConsumidos(Query $q = null) {
		return $this->countProductoConsumidosRelatedByHabitaEstadiaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getestadia_id()) {
			$this->_validationErrors[] = 'estadia_id must not be null';
		}
		if (null === $this->gethabitacion_id()) {
			$this->_validationErrors[] = 'habitacion_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}