<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseMediosDeEnvio extends ApplicationModel {

	const ID = 'medios_de_envio.id';
	const DESCRIPCION = 'medios_de_envio.descripcion';
	const CREATED_AT = 'medios_de_envio.created_at';
	const UPDATED_AT = 'medios_de_envio.updated_at';
	const DELETED_AT = 'medios_de_envio.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'medios_de_envio';

	/**
	 * Cache of objects retrieved from the database
	 * @var MediosDeEnvio[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `descripcion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `created_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return MediosDeEnvio
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return MediosDeEnvio
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return MediosDeEnvio
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MediosDeEnvio::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MediosDeEnvio::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for MediosDeEnvio::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MediosDeEnvio::setCreatedAt
	 * @return MediosDeEnvio
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return MediosDeEnvio
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MediosDeEnvio::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MediosDeEnvio::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for MediosDeEnvio::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MediosDeEnvio::setUpdatedAt
	 * @return MediosDeEnvio
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return MediosDeEnvio
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MediosDeEnvio::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MediosDeEnvio::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for MediosDeEnvio::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MediosDeEnvio::setDeletedAt
	 * @return MediosDeEnvio
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return MediosDeEnvio
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return MediosDeEnvio
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return MediosDeEnvio
	 */
	static function retrieveById($value) {
		return MediosDeEnvio::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return MediosDeEnvio
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return MediosDeEnvio
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return MediosDeEnvio
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return MediosDeEnvio
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return MediosDeEnvio
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return MediosDeEnvio[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting ventas Objects(rows) from the ventas table
	 * with a medio_de_envio_id that matches $this->id.
	 * @return Query
	 */
	function getVentassRelatedByMedioDeEnvioIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'medio_de_envio_id', 'id', $q);
	}

	/**
	 * Returns the count of Ventas Objects(rows) from the ventas table
	 * with a medio_de_envio_id that matches $this->id.
	 * @return int
	 */
	function countVentassRelatedByMedioDeEnvioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Ventas::doCount($this->getVentassRelatedByMedioDeEnvioIdQuery($q));
	}

	/**
	 * Deletes the ventas Objects(rows) from the ventas table
	 * with a medio_de_envio_id that matches $this->id.
	 * @return int
	 */
	function deleteVentassRelatedByMedioDeEnvioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->VentassRelatedByMedioDeEnvioId_c = array();
		return Ventas::doDelete($this->getVentassRelatedByMedioDeEnvioIdQuery($q));
	}

	protected $VentassRelatedByMedioDeEnvioId_c = array();

	/**
	 * Returns an array of Ventas objects with a medio_de_envio_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Ventas[]
	 */
	function getVentassRelatedByMedioDeEnvioId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->VentassRelatedByMedioDeEnvioId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->VentassRelatedByMedioDeEnvioId_c;
		}

		$result = Ventas::doSelect($this->getVentassRelatedByMedioDeEnvioIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->VentassRelatedByMedioDeEnvioId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for MediosDeEnvio::getVentassRelatedBymedio_de_envio_id
	 * @return Ventas[]
	 * @see MediosDeEnvio::getVentassRelatedByMedioDeEnvioId
	 */
	function getVentass($extra = null) {
		return $this->getVentassRelatedByMedioDeEnvioId($extra);
	}

	/**
	  * Convenience function for MediosDeEnvio::getVentassRelatedBymedio_de_envio_idQuery
	  * @return Query
	  * @see MediosDeEnvio::getVentassRelatedBymedio_de_envio_idQuery
	  */
	function getVentassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('ventas', 'medio_de_envio_id','id', $q);
	}

	/**
	  * Convenience function for MediosDeEnvio::deleteVentassRelatedBymedio_de_envio_id
	  * @return int
	  * @see MediosDeEnvio::deleteVentassRelatedBymedio_de_envio_id
	  */
	function deleteVentass(Query $q = null) {
		return $this->deleteVentassRelatedByMedioDeEnvioId($q);
	}

	/**
	  * Convenience function for MediosDeEnvio::countVentassRelatedBymedio_de_envio_id
	  * @return int
	  * @see MediosDeEnvio::countVentassRelatedByMedioDeEnvioId
	  */
	function countVentass(Query $q = null) {
		return $this->countVentassRelatedByMedioDeEnvioId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getdescripcion()) {
			$this->_validationErrors[] = 'descripcion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}