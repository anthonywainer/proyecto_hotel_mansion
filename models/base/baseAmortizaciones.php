<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseAmortizaciones extends ApplicationModel {

	const ID = 'amortizaciones.id';
	const MOVIMIENTO_ID = 'amortizaciones.movimiento_id';
	const CRONOGRAMA_PAGO_ID = 'amortizaciones.cronograma_pago_id';
	const MONTO = 'amortizaciones.monto';
	const GLOSA = 'amortizaciones.glosa';
	const CREATED_AT = 'amortizaciones.created_at';
	const UPDATED_AT = 'amortizaciones.updated_at';
	const DELETED_AT = 'amortizaciones.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'amortizaciones';

	/**
	 * Cache of objects retrieved from the database
	 * @var Amortizaciones[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'movimiento_id' => Model::COLUMN_TYPE_INTEGER,
		'cronograma_pago_id' => Model::COLUMN_TYPE_INTEGER,
		'monto' => Model::COLUMN_TYPE_DECIMAL,
		'glosa' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `movimiento_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $movimiento_id;

	/**
	 * `cronograma_pago_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cronograma_pago_id;

	/**
	 * `monto` DECIMAL NOT NULL DEFAULT ''
	 * @var string
	 */
	protected $monto;

	/**
	 * `glosa` VARCHAR NOT NULL
	 * @var string
	 */
	protected $glosa;

	/**
	 * `created_at` TIMESTAMP
	 * @var string
	 */
	protected $created_at;

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Amortizaciones
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the movimiento_id field
	 */
	function getMovimientoId() {
		return $this->movimiento_id;
	}

	/**
	 * Sets the value of the movimiento_id field
	 * @return Amortizaciones
	 */
	function setMovimientoId($value) {
		return $this->setColumnValue('movimiento_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Amortizaciones::getMovimientoId
	 * final because getMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::getMovimientoId
	 */
	final function getMovimiento_id() {
		return $this->getMovimientoId();
	}

	/**
	 * Convenience function for Amortizaciones::setMovimientoId
	 * final because setMovimientoId should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::setMovimientoId
	 * @return Amortizaciones
	 */
	final function setMovimiento_id($value) {
		return $this->setMovimientoId($value);
	}

	/**
	 * Gets the value of the cronograma_pago_id field
	 */
	function getCronogramaPagoId() {
		return $this->cronograma_pago_id;
	}

	/**
	 * Sets the value of the cronograma_pago_id field
	 * @return Amortizaciones
	 */
	function setCronogramaPagoId($value) {
		return $this->setColumnValue('cronograma_pago_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Amortizaciones::getCronogramaPagoId
	 * final because getCronogramaPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::getCronogramaPagoId
	 */
	final function getCronograma_pago_id() {
		return $this->getCronogramaPagoId();
	}

	/**
	 * Convenience function for Amortizaciones::setCronogramaPagoId
	 * final because setCronogramaPagoId should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::setCronogramaPagoId
	 * @return Amortizaciones
	 */
	final function setCronograma_pago_id($value) {
		return $this->setCronogramaPagoId($value);
	}

	/**
	 * Gets the value of the monto field
	 */
	function getMonto() {
		return $this->monto;
	}

	/**
	 * Sets the value of the monto field
	 * @return Amortizaciones
	 */
	function setMonto($value) {
		return $this->setColumnValue('monto', $value, Model::COLUMN_TYPE_DECIMAL);
	}

	/**
	 * Gets the value of the glosa field
	 */
	function getGlosa() {
		return $this->glosa;
	}

	/**
	 * Sets the value of the glosa field
	 * @return Amortizaciones
	 */
	function setGlosa($value) {
		return $this->setColumnValue('glosa', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Amortizaciones
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Amortizaciones::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Amortizaciones::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::setCreatedAt
	 * @return Amortizaciones
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Amortizaciones
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Amortizaciones::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Amortizaciones::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::setUpdatedAt
	 * @return Amortizaciones
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Amortizaciones
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Amortizaciones::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Amortizaciones::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Amortizaciones::setDeletedAt
	 * @return Amortizaciones
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Amortizaciones
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Amortizaciones
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveById($value) {
		return Amortizaciones::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a movimiento_id
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByMovimientoId($value) {
		return static::retrieveByColumn('movimiento_id', $value);
	}

	/**
	 * Searches the database for a row with a cronograma_pago_id
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByCronogramaPagoId($value) {
		return static::retrieveByColumn('cronograma_pago_id', $value);
	}

	/**
	 * Searches the database for a row with a monto
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByMonto($value) {
		return static::retrieveByColumn('monto', $value);
	}

	/**
	 * Searches the database for a row with a glosa
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByGlosa($value) {
		return static::retrieveByColumn('glosa', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Amortizaciones
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Amortizaciones
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->movimiento_id = (null === $this->movimiento_id) ? null : (int) $this->movimiento_id;
		$this->cronograma_pago_id = (null === $this->cronograma_pago_id) ? null : (int) $this->cronograma_pago_id;
		return $this;
	}

	/**
	 * @return Amortizaciones
	 */
	function setCronogramaPago(CronogramasPagos $cronogramaspagos = null) {
		return $this->setCronogramasPagosRelatedByCronogramaPagoId($cronogramaspagos);
	}

	/**
	 * @return Amortizaciones
	 */
	function setCronogramasPagosRelatedByCronogramaPagoId(CronogramasPagos $cronogramaspagos = null) {
		if (null === $cronogramaspagos) {
			$this->setcronograma_pago_id(null);
		} else {
			if (!$cronogramaspagos->getid()) {
				throw new Exception('Cannot connect a CronogramasPagos without a id');
			}
			$this->setcronograma_pago_id($cronogramaspagos->getid());
		}
		return $this;
	}

	/**
	 * Returns a cronogramas_pagos object with a id
	 * that matches $this->cronograma_pago_id.
	 * @return CronogramasPagos
	 */
	function getCronogramaPago() {
		return $this->getCronogramasPagosRelatedByCronogramaPagoId();
	}

	/**
	 * Returns a cronogramas_pagos object with a id
	 * that matches $this->cronograma_pago_id.
	 * @return CronogramasPagos
	 */
	function getCronogramasPagosRelatedByCronogramaPagoId() {
		$fk_value = $this->getcronograma_pago_id();
		if (null === $fk_value) {
			return null;
		}
		return CronogramasPagos::retrieveByPK($fk_value);
	}

	static function doSelectJoinCronogramaPago(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinCronogramasPagosRelatedByCronogramaPagoId($q, $join_type);
	}

	/**
	 * Returns a cronogramas_pagos object with a id
	 * that matches $this->cronograma_pago_id.
	 * @return CronogramasPagos
	 */
	function getCronogramasPagos() {
		return $this->getCronogramasPagosRelatedByCronogramaPagoId();
	}

	/**
	 * @return Amortizaciones
	 */
	function setCronogramasPagos(CronogramasPagos $cronogramaspagos = null) {
		return $this->setCronogramasPagosRelatedByCronogramaPagoId($cronogramaspagos);
	}

	/**
	 * @return Amortizaciones[]
	 */
	static function doSelectJoinCronogramasPagosRelatedByCronogramaPagoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = CronogramasPagos::getTableName();
		$q->join($to_table, $this_table . '.cronograma_pago_id = ' . $to_table . '.id', $join_type);
		foreach (CronogramasPagos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('CronogramasPagos'));
	}

	/**
	 * @return Amortizaciones
	 */
	function setMovimiento(Movimientos $movimientos = null) {
		return $this->setMovimientosRelatedByMovimientoId($movimientos);
	}

	/**
	 * @return Amortizaciones
	 */
	function setMovimientosRelatedByMovimientoId(Movimientos $movimientos = null) {
		if (null === $movimientos) {
			$this->setmovimiento_id(null);
		} else {
			if (!$movimientos->getid()) {
				throw new Exception('Cannot connect a Movimientos without a id');
			}
			$this->setmovimiento_id($movimientos->getid());
		}
		return $this;
	}

	/**
	 * Returns a movimientos object with a id
	 * that matches $this->movimiento_id.
	 * @return Movimientos
	 */
	function getMovimiento() {
		return $this->getMovimientosRelatedByMovimientoId();
	}

	/**
	 * Returns a movimientos object with a id
	 * that matches $this->movimiento_id.
	 * @return Movimientos
	 */
	function getMovimientosRelatedByMovimientoId() {
		$fk_value = $this->getmovimiento_id();
		if (null === $fk_value) {
			return null;
		}
		return Movimientos::retrieveByPK($fk_value);
	}

	static function doSelectJoinMovimiento(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMovimientosRelatedByMovimientoId($q, $join_type);
	}

	/**
	 * Returns a movimientos object with a id
	 * that matches $this->movimiento_id.
	 * @return Movimientos
	 */
	function getMovimientos() {
		return $this->getMovimientosRelatedByMovimientoId();
	}

	/**
	 * @return Amortizaciones
	 */
	function setMovimientos(Movimientos $movimientos = null) {
		return $this->setMovimientosRelatedByMovimientoId($movimientos);
	}

	/**
	 * @return Amortizaciones[]
	 */
	static function doSelectJoinMovimientosRelatedByMovimientoId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Movimientos::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (Movimientos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Movimientos'));
	}

	/**
	 * @return Amortizaciones[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = CronogramasPagos::getTableName();
		$q->join($to_table, $this_table . '.cronograma_pago_id = ' . $to_table . '.id', $join_type);
		foreach (CronogramasPagos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'CronogramasPagos';
	
		$to_table = Movimientos::getTableName();
		$q->join($to_table, $this_table . '.movimiento_id = ' . $to_table . '.id', $join_type);
		foreach (Movimientos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Movimientos';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getmovimiento_id()) {
			$this->_validationErrors[] = 'movimiento_id must not be null';
		}
		if (null === $this->getcronograma_pago_id()) {
			$this->_validationErrors[] = 'cronograma_pago_id must not be null';
		}
		if (null === $this->getmonto()) {
			$this->_validationErrors[] = 'monto must not be null';
		}
		if (null === $this->getglosa()) {
			$this->_validationErrors[] = 'glosa must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}