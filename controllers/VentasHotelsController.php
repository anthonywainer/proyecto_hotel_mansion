<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class VentasHotelsController extends ApplicationController {

	/**
	 * Returns all VentasHotel records matching the query. Examples:
	 * GET /ventas-hotels?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/ventas-hotels.json&limit=5
	 *
	 * @return VentasHotel[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = VentasHotel::getQuery(@$_GET)
            ->andLike('ventas_hotel.efectivo','%'.$_GET['search'].'%')
            ->orLike('ventas_hotel.monto','%'.$_GET['search'].'%')
        ;
		$q = VentasHotel::getQuery(@$_GET);
		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'VentasHotel';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		 $this['ventas_hotels'] = $this['pager']->fetchPage();
        $this['u'] = 'ventas-hotels/index';
        return $this->loadView("admin/index",$this);
	}


	/**
	 * Form to create or edit a VentasHotel. Example:
	 * GET /ventas-hotels/edit/1
	 *
	 * @return VentasHotel
	 */
	function editar($id = null) {
        $ca = Cajas::getAll('where estado= true');
        if (count($ca)>0) {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
            $_REQUEST['caja_id']    = $ca[0]->id;
            $_REQUEST['usuario_id'] = $_SESSION['user']->id;
            $_REQUEST['concepto_pago_id'] = 1;
            $_REQUEST['forma_pago_id'] = 1;
            $_REQUEST['tipo_comprobante_id'] = 1;
            if (!$id){
                $m = new MovimientosDeDinero;
                $m->fromArray($_REQUEST);
                $m->save();

                $_REQUEST['movimiento_id'] = $m->getId();
                $v = new VentasHotel;
                $v->fromArray($_REQUEST);
                $v->save();
                return redirect('ventas-hotels/editar/'.$v->getId());
            }
            $this->getVentasHotel($id)->fromArray(@$_GET);
            $this['u'] = 'ventas-hotels/edit';
            return $this->loadView("admin/index",$this);
        }else{
            $this->flash['errors'][] = "Falta Aperturar Caja";
            return redirect('cajas/mostrar');
        }

	}

	/**
	 * Saves a VentasHotel. Examples:
	 * POST /ventas-hotels/save/1
	 * POST /rest/ventas-hotels/.json
	 * PUT /rest/ventas-hotels/1.json
	 */

	function guardar($id = null) {
	    if (isset($_REQUEST['sa'])){
            $_REQUEST['hotel'] = $id;
	        $vh = new ProductoConsumidoHotel;
	        $vh->fromArray($_REQUEST);
	        $vh->save();
	        $this['id'] = $id;
	        $this['ventas']=VentasHotel::getAll('where id='.$id)[0];
	        return $this->loadView('ventas-hotels/list_productos',$this);
        }
		$ventas_hotel = $this->getVentasHotel($id);

		try {
			$ventas_hotel->fromArray($_REQUEST);
			if ($ventas_hotel->validate()) {
				$ventas_hotel->save();
                $_REQUEST['id'] = $_REQUEST['movimiento_id'];
                $mo = MovimientosDeDinero::retrieveByPK($_REQUEST['movimiento_id']);
                $mo->fromArray($_REQUEST);
                $mo->save();

				$this->flash['messages'][] = 'Venta guardada';

				$this['ventas'] = VentasHotel::getAll('where id='.$ventas_hotel->id)[0];
				return $this->redirect('ventas-hotels/mostrar/'.$ventas_hotel->getId());
			}
			$this->flash['errors'] = $ventas_hotel->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('ventas-hotels/edit/' . $ventas_hotel->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the VentasHotel with the id. Examples:
	 * GET /ventas-hotels/show/1
	 * GET /rest/ventas-hotels/1.json
	 *
	 * @return VentasHotel
	 */
	function mostrar($id = null) {
	    $this['id'] = $id;
        return $this->loadView('ventas-hotels/comprobante',$this);
	}

	/**
	 * Deletes the VentasHotel with the id. Examples:
	 * GET /ventas-hotels/delete/1
	 * DELETE /rest/ventas-hotels/1.json
	 */
	function eliminar($id = null) {

		if (isset($_REQUEST['elip'])){
		    $idh = ProductoConsumidoHotel::getAll('where id='.$id)[0]->hotel;
		    ProductoConsumidoHotel::retrieveByPK($id)->delete();

            return $this->redirect('ventas-hotels/editar/'.$idh);
        }
        $ventas_hotel = $this->getVentasHotel($id);
		try {
			if (null !== $ventas_hotel && $ventas_hotel->delete()) {
				$this['messages'][] = 'Ventas Hotel deleted';
			} else {
				$this['errors'][] = 'Ventas Hotel could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('ventas-hotels');
		}
	}

	/**
	 * @return VentasHotel
	 */
	private function getVentasHotel($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[VentasHotel::getPrimaryKey()])) {
			$id = $_REQUEST[VentasHotel::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new VentasHotel
			$this['ventas_hotel'] = new VentasHotel;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['ventas_hotel'] = VentasHotel::retrieveByPK($id);
		}
		return $this['ventas_hotel'];
	}

}