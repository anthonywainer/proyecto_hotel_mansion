<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class GruposController extends ApplicationController {

	/**
	 * Returns all Grupos records matching the query. Examples:
	 * GET /grupos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/grupos.json&limit=5
	 *
	 * @return Grupos[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Grupos::getQuery(@$_GET)->andLike('grupos.descripcion','%'.$_GET['search'].'%');


		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Grupos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['grupos'] = $this['pager']->fetchPage();
        $this['u'] = 'grupos/index';

        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Grupos. Example:
	 * GET /grupos/edit/1
	 *
	 * @return Grupos
	 */
	function editar($id = null) {
		$this->getGrupos($id)->fromArray(@$_GET);
        $this['u'] = 'grupos/edit';
        $this['modulos'] = Modulos::getAll();
        if($id){
            $p = PermisosGrupo::getAll("WHERE idgrupo=".$id);
            $na=[];
            foreach ($p as $pe){
                array_push($na,$pe->idpermiso);
            }
            $this['permisos'] = $na;
        }
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Grupos. Examples:
	 * POST /grupos/save/1
	 * POST /rest/grupos/.json
	 * PUT /rest/grupos/1.json
	 */
	function guardar($id = null) {
		$grupos = $this->getGrupos($id);

        if ($_REQUEST['id']){
            $idg = $_REQUEST['id'];
            $sql ="DELETE FROM permisos_grupo WHERE permisos_grupo.idgrupo = $idg";
            $result = PermisosGrupo::getConnection()->query($sql);
            $result->execute();
        }
		try {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
            $grupos->fromArray($_REQUEST);

			if ($grupos->validate()) {
				$grupos->save();
                $uid = $grupos->getId();

				foreach ($_REQUEST['permisos'] as $per) {
                    $arr = [];
                    $arr['idpermiso'] = $per;
                    $arr['idgrupo'] = $uid; #ultimo id ingresado
                    $permisos_grupo = new PermisosGrupo();
                    $permisos_grupo->fromArray($arr);
                    $permisos_grupo->save();
                }

				$this->flash['messages'][] = 'Grupos ';
				$this->redirect('grupos/index');
			}
			$this->flash['errors'] = $grupos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('grupos/edit/' . $grupos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Grupos with the id. Examples:
	 * GET /grupos/show/1
	 * GET /rest/grupos/1.json
	 *
	 * @return Grupos
	 */
	function show($id = null) {
		return $this->getGrupos($id);
	}

	/**
	 * Deletes the Grupos with the id. Examples:
	 * GET /grupos/delete/1
	 * DELETE /rest/grupos/1.json
	 */
	function eliminar($id = null) {
		$grupos = $this->getGrupos($id);

		try {
			if (null !== $grupos && $grupos->delete()) {
				$this['errors'][] = 'Grupo ';
			} else {
				$this['errors'][] = 'Grupos no debe ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('grupos');
		}
	}

	/**
	 * @return Grupos
	 */
	private function getGrupos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Grupos::getPrimaryKey()])) {
			$id = $_REQUEST[Grupos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Grupos
			$this['grupos'] = new Grupos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['grupos'] = Grupos::retrieveByPK($id);
		}
		return $this['grupos'];
	}

}