<?php

use Dabl\Controller\Controller;
use Dabl\Controller\ControllerRoute;
use Dabl\StringFormat\StringFormat;

abstract class ApplicationController extends Controller {

	function __construct(ControllerRoute $route = null) {
		parent::__construct($route);
        if(isset($_SESSION['user'])) {

            $ca = Cajas::getAll('where estado= true');
            if ($ca) {
                $fecha_f = date('Y-m-d', strtotime($ca[0]->fecha_apertura . ' +1 days'));
                if (date('Y-m-d') >= $fecha_f) {
                    $ccc = new Cajas;
                    $in = $ccc->getIngresos($ca);
                    $eg = $ccc->getEgresos($ca);
                    $ni = $ccc->getIngresosN($ca);
                    $nih = $ccc->getIngresosH($ca);
                    $in[0]+=$ni[0]+$nih[0];

                    $moc = ($ca[0]->saldo_inicial+$in[0]-$eg[0]);

                    $_REQUEST['estado'] = 0;
                    $_REQUEST['saldo_final'] = $moc;
                    $_REQUEST['fecha_cierre'] = date("Y-m-d h:i:s");

                    $cc = Cajas::retrieveByPK($ca[0]->getId());
                    $cc->fromArray($_REQUEST);
                    $cc->save();
                }
            }

            $idu = $_SESSION['user']->id;
            $idg = UsuariosGrupo::getAll('WHERE usuario_id = ' . $idu)[0]->grupo_id;
            $_SESSION['id_grupo'] = $idg;
            $sql = "SELECT modulos.nombre, modulos.url, modulos.icon, modulos.id FROM permisos_grupo INNER JOIN permisos ON permisos_grupo.idpermiso = permisos.id INNER JOIN modulos ON permisos.idmodulo = modulos.id WHERE permisos_grupo.idgrupo = " . $idg;
            $sqlu = "SELECT modulos.nombre, modulos.url, modulos.icon, modulos.id FROM permisos INNER JOIN modulos ON permisos.idmodulo = modulos.id INNER JOIN permisos_usuario ON permisos_usuario.idpermiso = permisos.id WHERE permisos_usuario.idusuario = ".$idu;
            $result = PermisosGrupo::getConnection()->query($sql." UNION ALL ".$sqlu);

            $sql = "SELECT submodulo.id, submodulo.nombre, submodulo.url, submodulo.icon, submodulo.idmodulo FROM permisos_grupo INNER JOIN permisos ON permisos_grupo.idpermiso = permisos.id INNER JOIN submodulo ON permisos.idsubmodulo = submodulo.id WHERE permisos_grupo.idgrupo = " . $idg." group by submodulo.nombre";
            $sqlu = "SELECT submodulo.id, submodulo.nombre, submodulo.url, submodulo.icon, submodulo.idmodulo FROM permisos INNER JOIN submodulo ON permisos.idsubmodulo = submodulo.id INNER JOIN permisos_usuario ON permisos_usuario.idpermiso = permisos.id WHERE permisos_usuario.idusuario = ".$idu." group by submodulo.nombre";
            $result2 = PermisosGrupo::getConnection()->query($sql." UNION ALL ".$sqlu);
            $this['modulosPrincipal'] = $result->fetchAll();
            $this['SubmodulosPrincipal'] = $result2->fetchAll();
        }
		$this['title'] = 'Hotel Mansión';

		$this['actions'] = array(
			'Home' => site_url()
		);

		$current_controller = str_replace('Controller', '', get_class($this));

		if ('Index' == $current_controller) {
			$this['current_page'] = 'Home';
		} else {
			$this['current_page'] = StringFormat::titleCase($current_controller, ' ');
		}
		foreach (glob(CONTROLLERS_DIR . '*.php') as $controller_file) {
			$controller = str_replace('Controller.php', '', basename($controller_file));
			if ($controller == 'Application' || $controller == 'Index') {
				continue;
			}
			$this['actions'][StringFormat::titleCase($controller, ' ')] = site_url(StringFormat::url($controller));
		}
	}
    public function check_permissions($route){
        $idg = $_SESSION['id_grupo'];
        $idu = $_SESSION['user']->id;
        $select = "SELECT submodulo.url, acciones.accion";
        $from = " FROM  permisos_grupo INNER JOIN permisos ON permisos_grupo.idpermiso = permisos.id INNER JOIN submodulo ON permisos.idsubmodulo = submodulo.id INNER JOIN acciones ON permisos.idaccion = acciones.id WHERE permisos_grupo.idgrupo = " . $idg;
        $fromu = " FROM permisos INNER JOIN submodulo ON permisos.idsubmodulo = submodulo.id INNER JOIN acciones ON permisos.idaccion = acciones.id INNER JOIN permisos_usuario ON permisos_usuario.idpermiso = permisos.id  WHERE permisos_usuario.idusuario = ".$idu;
        $sql = $select.$from;
        $sqlu = $select.$fromu;
        $result2 = PermisosGrupo::getConnection()->query($sql.' UNION ALL '.$sqlu);
        $urls = $result2->fetchAll();

        $up= UrlsPermitidas::getAll();
        if (isset($route[0])) {
            $ro = $route[0];
        } else {
            $ro = 'index';
        }
        $nup = [];
        foreach ($up as $u){
            array_push($nup, $u->url);
        }

        if (isset($up)) {
            if (in_array($ro, $nup) == 1) {
                return true;
            } else {
                if (in_array($ro, array_column($urls, 'url')) == 1) {

                    $sql2 = $sql . ' and url = "' . $ro . '"';
                    $result2 = PermisosGrupo::getConnection()->query($sql2);
                    $urls2 = $result2->fetchAll();

                    if (isset($route[1])) {
                        $rr = $route[1];
                    } else {
                        $rr = 'index';
                    }

                    if (in_array($rr, array_column($urls2, 'accion')) == 1) {
                        return true;
                    }
                };
            }
        }
        return false;
    }

	public function doAction($action_name = null, $params = array()) {

		if ($this->outputFormat != 'html') {
			unset($this['title'], $this['current_page'], $this['actions']);

		}

        if((isset($_SESSION['user']))) {

            if ($this->check_permissions($this->route->getSegments())) {

            } else {
                http_response_code(404);
                return $this->loadView("error404");
            }
        }

        if (in_array($this->outputFormat, array('json', 'jsonp', 'xml'), true)) {
            try {
                return parent::doAction($action_name, $params);
            } catch (Exception $e) {
                error_log($e);
                $this['errors'][] = $e->getMessage();
                if (!$this->loadView) {
                    return;
                }
                $this->loadView('');
            }
        } else {
            return parent::doAction($action_name, $params);
        }

	}

}