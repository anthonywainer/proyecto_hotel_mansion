<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ProductosController extends ApplicationController {

	/**
	 * Returns all Productos records matching the query. Examples:
	 * GET /productos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/productos.json&limit=5
	 *
	 * @return Productos[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Productos::getQuery(@$_GET)
            ->andLike('productos.nombre','%'.$_GET['search'].'%')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Productos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['productos'] = $this['pager']->fetchPage();
        $this['u'] = 'productos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Productos. Example:
	 * GET /productos/edit/1
	 *
	 * @return Productos
	 */
	function editar($id = null) {
		$this->getProductos($id)->fromArray(@$_GET);
        $this['u'] = 'productos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Productos. Examples:
	 * POST /productos/save/1
	 * POST /rest/productos/.json
	 * PUT /rest/productos/1.json
	 */
	function guardar($id = null) {
		$productos = $this->getProductos($id);

		try {

            $target_dir = PUBLIC_DIR.'imagenes_productos/';
            $target_file = $target_dir . basename($_FILES["imagen"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

            if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["imagen"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
            $image=basename( $_FILES["imagen"]["name"]); // used to store the filename in a variable

            $_REQUEST['imagen'] = $image;
            $productos->fromArray($_REQUEST);
            if ($productos->validate()) {
				$productos->save();
				$this->flash['messages'][] = 'Producto guardado';
				$this->redirect('productos/index/' . $productos->getId());
			}
			$this->flash['errors'] = $productos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('productos/edit/' . $productos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Productos with the id. Examples:
	 * GET /productos/show/1
	 * GET /rest/productos/1.json
	 *
	 * @return Productos
	 */
	function show($id = null) {
		return $this->getProductos($id);
	}

	/**
	 * Deletes the Productos with the id. Examples:
	 * GET /productos/delete/1
	 * DELETE /rest/productos/1.json
	 */
	function eliminar($id = null) {
		$productos = $this->getProductos($id);

		try {
			if (null !== $productos && $productos->delete()) {
				$this['errors'][] = 'Producto eliminado';
			} else {
				$this['errors'][] = 'Producto no se ha podido eliminar';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('productos');
		}
	}

	/**
	 * @return Productos
	 */
	private function getProductos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Productos::getPrimaryKey()])) {
			$id = $_REQUEST[Productos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Productos
			$this['productos'] = new Productos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['productos'] = Productos::retrieveByPK($id);
		}
		return $this['productos'];
	}

}