<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class TiposHabitacionesController extends ApplicationController {

	/**
	 * Returns all TiposHabitaciones records matching the query. Examples:
	 * GET /tipos-habitaciones?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/tipos-habitaciones.json&limit=5
	 *
	 * @return TiposHabitaciones[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = TiposHabitaciones::getQuery(@$_GET)
            ->andLike('tipos_habitaciones.descripcion','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'TiposHabitaciones';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['tipos_habitaciones'] = $this['pager']->fetchPage();
        $this['u'] = 'tipos-habitaciones/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a TiposHabitaciones. Example:
	 * GET /tipos-habitaciones/edit/1
	 *
	 * @return TiposHabitaciones
	 */
	function editar($id = null) {
		$this->getTiposHabitaciones($id)->fromArray(@$_GET);
        $this['u'] = 'tipos-habitaciones/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a TiposHabitaciones. Examples:
	 * POST /tipos-habitaciones/save/1
	 * POST /rest/tipos-habitaciones/.json
	 * PUT /rest/tipos-habitaciones/1.json
	 */
	function guardar($id = null) {
		$tipos_habitaciones = $this->getTiposHabitaciones($id);

		try {
            $fecha_actual = date("Y-m-d");
            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }
			$tipos_habitaciones->fromArray($_REQUEST);
			if ($tipos_habitaciones->validate()) {
				$tipos_habitaciones->save();
				$this->flash['messages'][] = 'Tipos Habitaciones guardado';
				$this->redirect('tipos-habitaciones/index');
			}
			$this->flash['errors'] = $tipos_habitaciones->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('tipos-habitaciones/edit/' . $tipos_habitaciones->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the TiposHabitaciones with the id. Examples:
	 * GET /tipos-habitaciones/show/1
	 * GET /rest/tipos-habitaciones/1.json
	 *
	 * @return TiposHabitaciones
	 */
	function show($id = null) {
		return $this->getTiposHabitaciones($id);
	}

	/**
	 * Deletes the TiposHabitaciones with the id. Examples:
	 * GET /tipos-habitaciones/delete/1
	 * DELETE /rest/tipos-habitaciones/1.json
	 */
	function eliminar($id = null) {
		$tipos_habitaciones = $this->getTiposHabitaciones($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $tipos_habitaciones->fromArray($_REQUEST);
		try {
			if (null !== $tipos_habitaciones && $tipos_habitaciones->save()) {
				$this['errors'][] = 'Tipos Habitaciones eliminado';
			} else {
				$this['errors'][] = 'Tipos Habitaciones no se ha podido eliminar';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('tipos-habitaciones');
		}
	}

	/**
	 * @return TiposHabitaciones
	 */
	private function getTiposHabitaciones($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[TiposHabitaciones::getPrimaryKey()])) {
			$id = $_REQUEST[TiposHabitaciones::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new TiposHabitaciones
			$this['tipos_habitaciones'] = new TiposHabitaciones;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['tipos_habitaciones'] = TiposHabitaciones::retrieveByPK($id);
		}
		return $this['tipos_habitaciones'];
	}

}