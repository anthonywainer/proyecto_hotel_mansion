<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class TiposComprobantesController extends ApplicationController {

	/**
	 * Returns all TiposComprobantes records matching the query. Examples:
	 * GET /tipos-comprobantes?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/tipos-comprobantes.json&limit=5
	 *
	 * @return TiposComprobantes[]
	 */
    function __construct(ControllerRoute $route = null)
    {
        parent::__construct($route);
        if (!$_SESSION) {
            return redirect('/login');
        }
    }
	function index() {

        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
		$q = TiposComprobantes::getQuery(@$_GET)
            ->andLike('tipos_comprobantes.descripcion','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;



		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'TiposComprobantes';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['tipos_comprobantes'] = $this['pager']->fetchPage();
        $this['u'] = 'tipos-comprobantes/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a TiposComprobantes. Example:
	 * GET /tipos-comprobantes/edit/1
	 *
	 * @return TiposComprobantes
	 */
	function editar($id = null) {
		$this->getTiposComprobantes($id)->fromArray(@$_GET);
        $this['u'] = 'tipos-comprobantes/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a TiposComprobantes. Examples:
	 * POST /tipos-comprobantes/save/1
	 * POST /rest/tipos-comprobantes/.json
	 * PUT /rest/tipos-comprobantes/1.json
	 */
	function guardar($id = null) {
		$tipos_comprobantes = $this->getTiposComprobantes($id);

		try {
            $fecha_actual = date("Y-m-d");
            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }
			$tipos_comprobantes->fromArray($_REQUEST);
			if ($tipos_comprobantes->validate()) {
				$tipos_comprobantes->save();
				$this->flash['messages'][] = 'Tipos Comprobantes guardado';
				$this->redirect('tipos-comprobantes/index');
			}
			$this->flash['errors'] = $tipos_comprobantes->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('tipos-comprobantes/edit/' . $tipos_comprobantes->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the TiposComprobantes with the id. Examples:
	 * GET /tipos-comprobantes/show/1
	 * GET /rest/tipos-comprobantes/1.json
	 *
	 * @return TiposComprobantes
	 */
	function show($id = null) {
		return $this->getTiposComprobantes($id);
	}

	/**
	 * Deletes the TiposComprobantes with the id. Examples:
	 * GET /tipos-comprobantes/delete/1
	 * DELETE /rest/tipos-comprobantes/1.json
	 */
	function eliminar($id = null) {
		$tipos_comprobantes = $this->getTiposComprobantes($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $tipos_comprobantes->fromArray($_REQUEST);
		try {
			if (null !== $tipos_comprobantes && $tipos_comprobantes->save()) {
				$this['errors'][] = 'Tipos Comprobantes deleted';
			} else {
				$this['errors'][] = 'Tipos Comprobantes could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('tipos-comprobantes');
		}
	}

	/**
	 * @return TiposComprobantes
	 */
	private function getTiposComprobantes($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[TiposComprobantes::getPrimaryKey()])) {
			$id = $_REQUEST[TiposComprobantes::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new TiposComprobantes
			$this['tipos_comprobantes'] = new TiposComprobantes;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['tipos_comprobantes'] = TiposComprobantes::retrieveByPK($id);
		}
		return $this['tipos_comprobantes'];
	}

}