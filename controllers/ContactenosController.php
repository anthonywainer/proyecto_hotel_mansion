<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ContactenosController extends ApplicationController {

	/**
	 * Returns all Contactenos records matching the query. Examples:
	 * GET /contactenos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/contactenos.json&limit=5
	 *
	 * @return Contactenos[]
	 */
   /* function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }*/
	function index() {
        if (!$_SESSION) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $contactenos = $this->getContactenos($id=null);
                try {
                    $contactenos->fromArray($_REQUEST);
                    if ($contactenos->validate()) {
                        $contactenos->save();
                        $this->flash['messages'][] = 'Gracias por contactarnos, enviado para su respuesta';
                        $this->redirect('contactenos/');
                    }
                    $this->flash['errors'] = $contactenos->getValidationErrors();
                } catch (Exception $e) {
                    $this->flash['errors'][] = $e->getMessage();
                }
            }
            return $this->loadView("paginaweb/contactenos", $this);
        }
		$q = Contactenos::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Contactenos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['contactenos'] = $this['pager']->fetchPage();
        $this['u'] = 'contactenos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Contactenos. Example:
	 * GET /contactenos/edit/1
	 *
	 * @return Contactenos
	 */
	function edit($id = null) {
		return $this->getContactenos($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a Contactenos. Examples:
	 * POST /contactenos/save/1
	 * POST /rest/contactenos/.json
	 * PUT /rest/contactenos/1.json
	 */
	function guardar($id = null) {
		$contactenos = $this->getContactenos($id);

		try {
		    $_REQUEST['estado'] = 'leido';
			$contactenos->fromArray($_REQUEST);
			if ($contactenos->validate()) {
				$contactenos->save();
				$this->flash['messages'][] = 'Contacto leido';
				$this->redirect('contactenos/index/');
			}
			$this->flash['errors'] = $contactenos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('contactenos/edit/' . $contactenos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Contactenos with the id. Examples:
	 * GET /contactenos/show/1
	 * GET /rest/contactenos/1.json
	 *
	 * @return Contactenos
	 */
	function mostrar($id = null) {
		 $this->getContactenos($id);
        $this['u'] = 'contactenos/show';
        return $this->loadView("admin/index",$this);
	}

	/**
	 * Deletes the Contactenos with the id. Examples:
	 * GET /contactenos/delete/1
	 * DELETE /rest/contactenos/1.json
	 */
	function eliminar($id = null) {
		$contactenos = $this->getContactenos($id);

		try {
			if (null !== $contactenos && $contactenos->delete()) {
				$this['errors'][] = 'Contact eliminado';
			} else {
				$this['errors'][] = 'Contactenos could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('contactenos');
		}
	}

	/**
	 * @return Contactenos
	 */
	private function getContactenos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Contactenos::getPrimaryKey()])) {
			$id = $_REQUEST[Contactenos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Contactenos
			$this['contactenos'] = new Contactenos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['contactenos'] = Contactenos::retrieveByPK($id);
		}
		return $this['contactenos'];
	}

}