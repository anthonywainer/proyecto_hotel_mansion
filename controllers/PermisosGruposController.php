<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class PermisosGruposController extends ApplicationController {

	/**
	 * Returns all PermisosGrupo records matching the query. Examples:
	 * GET /permisos-grupos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/permisos-grupos.json&limit=5
	 *
	 * @return PermisosGrupo[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = PermisosGrupo::getQuery(@$_GET)
            ->andLike('permisos_grupo.idgrupo','%'.$_GET['search'].'%')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'PermisosGrupo';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		 $this['permisos_grupos'] = $this['pager']->fetchPage();
        $this['u'] = 'permisos-grupos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a PermisosGrupo. Example:
	 * GET /permisos-grupos/edit/1
	 *
	 * @return PermisosGrupo
	 */
	function editar($id = null) {
		 $this->getPermisosGrupo($id)->fromArray(@$_GET);
        $this['u'] = 'permisos-grupos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a PermisosGrupo. Examples:
	 * POST /permisos-grupos/save/1
	 * POST /rest/permisos-grupos/.json
	 * PUT /rest/permisos-grupos/1.json
	 */
	function guardar($id = null) {
		$permisos_grupo = $this->getPermisosGrupo($id);

		try {
			$permisos_grupo->fromArray($_REQUEST);
			if ($permisos_grupo->validate()) {
				$permisos_grupo->save();
				$this->flash['messages'][] = 'Permisos Grupo ';
				$this->redirect('permisos_grupos/index/');
			}
			$this->flash['errors'] = $permisos_grupo->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('permisos_grupos/edit/' . $permisos_grupo->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the PermisosGrupo with the id. Examples:
	 * GET /permisos-grupos/show/1
	 * GET /rest/permisos-grupos/1.json
	 *
	 * @return PermisosGrupo
	 */
	function show($id = null) {
		return $this->getPermisosGrupo($id);
	}

	/**
	 * Deletes the PermisosGrupo with the id. Examples:
	 * GET /permisos-grupos/delete/1
	 * DELETE /rest/permisos-grupos/1.json
	 */
	function delete($id = null) {
		$permisos_grupo = $this->getPermisosGrupo($id);

		try {
			if (null !== $permisos_grupo && $permisos_grupo->delete()) {
				$this['messages'][] = 'Permisos Grupo ';
			} else {
				$this['errors'][] = 'Permisos Grupo could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['errors'] = @$this['messages'];
			$this->redirect('permisos_grupos');
		}
	}

	/**
	 * @return PermisosGrupo
	 */
	private function getPermisosGrupo($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[PermisosGrupo::getPrimaryKey()])) {
			$id = $_REQUEST[PermisosGrupo::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new PermisosGrupo
			$this['permisos_grupo'] = new PermisosGrupo;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['permisos_grupo'] = PermisosGrupo::retrieveByPK($id);
		}
		return $this['permisos_grupo'];
	}

}