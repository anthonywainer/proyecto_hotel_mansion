<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class HotelesController extends ApplicationController {

	/**
	 * Returns all Hoteles records matching the query. Examples:
	 * GET /hoteles?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/hoteles.json&limit=5
	 *
	 * @return Hoteles[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Hoteles::getQuery(@$_GET)
            ->andLike('hoteles.nombre','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Hoteles';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['hoteles'] = $this['pager']->fetchPage();
        $this['u'] = 'hoteles/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Hoteles. Example:
	 * GET /hoteles/edit/1
	 *
	 * @return Hoteles
	 */
	function editar($id = null) {
		$this->getHoteles($id)->fromArray(@$_GET);
        $this['u'] = 'hoteles/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Hoteles. Examples:
	 * POST /hoteles/save/1
	 * POST /rest/hoteles/.json
	 * PUT /rest/hoteles/1.json
	 */
	function guardar($id = null) {
		$hoteles = $this->getHoteles($id);
        $fecha_actual = date("Y-m-d");
        if (!$_REQUEST['id']) {
            $_REQUEST["created_at"] = $fecha_actual;
        }else {
            $_REQUEST["updated_at"] = $fecha_actual;
        }

		try {
			$hoteles->fromArray($_REQUEST);
			if ($hoteles->validate()) {
				$hoteles->save();
				$this->flash['messages'][] = 'Datos guardados';
				$this->redirect('hoteles/index/');
			}
			$this->flash['errors'] = $hoteles->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('hoteles/edit/' . $hoteles->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Hoteles with the id. Examples:
	 * GET /hoteles/show/1
	 * GET /rest/hoteles/1.json
	 *
	 * @return Hoteles
	 */
	function show($id = null) {
		return $this->getHoteles($id);
	}

	/**
	 * Deletes the Hoteles with the id. Examples:
	 * GET /hoteles/delete/1
	 * DELETE /rest/hoteles/1.json
	 */
	function eliminar($id = null) {
		$hoteles = $this->getHoteles($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $hoteles->fromArray($_REQUEST);
		try {
			if (null !== $hoteles && $hoteles->save()) {
				$this['errors'][] = 'datos eliminados';
			} else {
				$this['errors'][] = 'los datos no se han podido eliminar';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('hoteles');
		}
	}

	/**
	 * @return Hoteles
	 */
	private function getHoteles($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Hoteles::getPrimaryKey()])) {
			$id = $_REQUEST[Hoteles::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Hoteles
			$this['hoteles'] = new Hoteles;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['hoteles'] = Hoteles::retrieveByPK($id);
		}
		return $this['hoteles'];
	}

}