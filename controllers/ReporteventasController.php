<?php


class ReporteventasController extends ApplicationController {


	function index() {
        $this['u'] = 'reportes/ventas';
        if (isset($_REQUEST['created_at'])) {

            $begin = $_REQUEST['created_at'];

            $this['created_at'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['created_at'])->format("Y-m-d");

        } else {
            $begin = date('Y-m-d');
            $this['created_at'] = date("d/m/Y");
        }

        $sql = "
SELECT
ventas_hotel.id,
movimientos_de_dinero.created_at,
ventas_hotel.monto,
clientes.nombres,
clientes.apellidos,
formas_pagos.descripcion formap,
tipos_comprobantes.descripcion tipoc
FROM
movimientos_de_dinero
INNER JOIN ventas_hotel ON ventas_hotel.movimiento_id = movimientos_de_dinero.id
INNER JOIN clientes ON movimientos_de_dinero.cliente_id = clientes.id
INNER JOIN formas_pagos ON movimientos_de_dinero.forma_pago_id = formas_pagos.id
INNER JOIN tipos_comprobantes ON movimientos_de_dinero.tipo_comprobante_id = tipos_comprobantes.id
WHERE
	(
		movimientos_de_dinero.created_at BETWEEN '2017-12-05'
		AND '2017-12-05'
	)
GROUP BY
	ventas_hotel.movimiento_id

";
        $this['ventas'] = Ventas::getConnection()->query($sql)->fetchAll();

        return $this->loadView("admin/index",$this);

	}


}