<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ClientesController extends ApplicationController {

	/**
	 * Returns all Clientes records matching the query. Examples:
	 * GET /clientes?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/clientes.json&limit=5
	 *
	 * @return Clientes[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        $_REQUEST['order_by'] = 'clientes.id';
        $_REQUEST['dir'] = 'DESC';
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Clientes::getQuery(@$_GET)
            ->andLike('clientes.nombres','%'.$_GET['search'].'%')
            ->orLike('clientes.apellidos','%'.$_GET['search'].'%')
            ->orLike('clientes.dni','%'.$_GET['search'].'%')
            ->orLike('clientes.direccion','%'.$_GET['search'].'%')
            ->orLike('clientes.telefono','%'.$_GET['search'].'%')
            ->orLike('clientes.procedencia','%'.$_GET['search'].'%')
            ->orLike('clientes.sexo','%'.$_GET['search'].'%')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Clientes';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['clientes'] = $this['pager']->fetchPage();
        $this['u'] = 'clientes/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Clientes. Example:
	 * GET /clientes/edit/1
	 *
	 * @return Clientes
	 */
	function editar($id = null) {
		$this->getClientes($id)->fromArray(@$_GET);
		if (isset($_REQUEST['cli'])){
		    if ($_REQUEST['cli']=='dni'){
                return $this->loadView('clientes/dni',$this);
            }
            if($_REQUEST['cli']=='ruc'){
                return $this->loadView('clientes/ruc',$this);

            }
            return $this->loadView('clientes/form-simple',$this);
        }else{
            $this['u'] = 'clientes/edit';
           return $this->loadView("admin/index",$this);
        }
	}

	/**
	 * Saves a Clientes. Examples:
	 * POST /clientes/save/1
	 * POST /rest/clientes/.json
	 * PUT /rest/clientes/1.json
	 */
	function guardar($id = null) {
		$clientes = $this->getClientes($id);
        $sql= "SELECT
            clientes.dni
            FROM
            clientes
            where clientes.dni = ".$_REQUEST['dni'];
        if(!(Clientes::getConnection()->query($sql)->fetchAll())){

            try {
                $fecha_actual = date("Y-m-d");
                if ($id= null) {
                    $_REQUEST["created_at"] = $fecha_actual;
                }else{
                    $_REQUEST["updated_at"] = $fecha_actual;
                }

                $clientes->fromArray($_REQUEST);
                if ($clientes->validate()) {
                    $clientes->save();
                    $uid = $clientes->getId();
                    $this['uid'] = $uid;
                    #$this->flash['messages'][] = 'Clientes saved';
                    return $this->loadView('estadias/msj/', $this);
                }
                $this->flash['errors'] = $clientes->getValidationErrors();

            } catch (Exception $e) {
                $this->flash['errors'][] = $e->getMessage();
            }
        }else{
            $this['uid'] = "existe";
            return $this->loadView('estadias/msj/',$this);
        }


		$this->redirect('clientes/edit/' . $clientes->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Clientes with the id. Examples:
	 * GET /clientes/show/1
	 * GET /rest/clientes/1.json
	 *
	 * @return Clientes
	 */
	function show($id = null) {
		return $this->getClientes($id);
	}

	/**
	 * Deletes the Clientes with the id. Examples:
	 * GET /clientes/delete/1
	 * DELETE /rest/clientes/1.json
	 */
	function eliminar($id = null) {
		$clientes = $this->getClientes($id);
		try {
			if (null !== $clientes && $clientes->delete()) {
				$this['errors'][] = 'Clientes deleted';
			} else {
				$this['errors'][] = 'Clientes could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('clientes');
		}
	}

	/**
	 * @return Clientes
	 */
	private function getClientes($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Clientes::getPrimaryKey()])) {
			$id = $_REQUEST[Clientes::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Clientes
			$this['clientes'] = new Clientes;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['clientes'] = Clientes::retrieveByPK($id);
		}
		return $this['clientes'];
	}

}