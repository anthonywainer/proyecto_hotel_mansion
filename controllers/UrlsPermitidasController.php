<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class UrlsPermitidasController extends ApplicationController {

	/**
	 * Returns all UrlsPermitidas records matching the query. Examples:
	 * GET /urls-permitidas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/urls-permitidas.json&limit=5
	 *
	 * @return UrlsPermitidas[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
		$q = UrlsPermitidas::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'UrlsPermitidas';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['urls_permitidas'] = $this['pager']->fetchPage();
        $this['u'] = 'urls-permitidas/index';
        $this->loadView("admin/index",$this);
    }

	/**
	 * Form to create or edit a UrlsPermitidas. Example:
	 * GET /urls-permitidas/edit/1
	 *
	 * @return UrlsPermitidas
	 */
	function edit($id = null) {
		$this->getUrlsPermitidas($id)->fromArray(@$_GET);
        $this['u'] = 'urls-permitidas/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a UrlsPermitidas. Examples:
	 * POST /urls-permitidas/save/1
	 * POST /rest/urls-permitidas/.json
	 * PUT /rest/urls-permitidas/1.json
	 */
	function save($id = null) {
		$urls_permitidas = $this->getUrlsPermitidas($id);

		try {
			$urls_permitidas->fromArray($_REQUEST);
			if ($urls_permitidas->validate()) {
				$urls_permitidas->save();
				$this->flash['messages'][] = 'Urls Permitidas saved';
				$this->redirect('urls-permitidas/index/');
			}
			$this->flash['errors'] = $urls_permitidas->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('urls-permitidas/edit/' . $urls_permitidas->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the UrlsPermitidas with the id. Examples:
	 * GET /urls-permitidas/show/1
	 * GET /rest/urls-permitidas/1.json
	 *
	 * @return UrlsPermitidas
	 */
	function show($id = null) {
		return $this->getUrlsPermitidas($id);
	}

	/**
	 * Deletes the UrlsPermitidas with the id. Examples:
	 * GET /urls-permitidas/delete/1
	 * DELETE /rest/urls-permitidas/1.json
	 */
	function delete($id = null) {
		$urls_permitidas = $this->getUrlsPermitidas($id);

		try {
			if (null !== $urls_permitidas && $urls_permitidas->delete()) {
				$this['errors'][] = 'Urls Permitidas deleted';
			} else {
				$this['errors'][] = 'Urls Permitidas could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('urls-permitidas');
		}
	}

	/**
	 * @return UrlsPermitidas
	 */
	private function getUrlsPermitidas($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[UrlsPermitidas::getPrimaryKey()])) {
			$id = $_REQUEST[UrlsPermitidas::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new UrlsPermitidas
			$this['urls_permitidas'] = new UrlsPermitidas;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['urls_permitidas'] = UrlsPermitidas::retrieveByPK($id);
		}
		return $this['urls_permitidas'];
	}

}