<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class CategoriasController extends ApplicationController {

	/**
	 * Returns all Categorias records matching the query. Examples:
	 * GET /categorias?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/categorias.json&limit=5
	 *
	 * @return Categorias[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {

        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Categorias::getQuery(@$_GET)
            ->andLike('categorias.descripcion','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Categorias';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['categorias'] = $this['pager']->fetchPage();
        $this['u'] = 'categorias/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Categorias. Example:
	 * GET /categorias/edit/1
	 *
	 * @return Categorias
	 */
	function editar($id = null) {
		$this->getCategorias($id)->fromArray(@$_GET);
        $this['u'] = 'categorias/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Categorias. Examples:
	 * POST /categorias/save/1
	 * POST /rest/categorias/.json
	 * PUT /rest/categorias/1.json
	 */
	function guardar($id = null) {
		$categorias = $this->getCategorias($id);

		try {
            $fecha_actual = date("Y-m-d");
            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }
			$categorias->fromArray($_REQUEST);
			if ($categorias->validate()) {
				$categorias->save();
				$this->flash['messages'][] = 'Categorias Guardada';
				$this->redirect('categorias/index/');
			}
			$this->flash['errors'] = $categorias->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('categorias/edit/' . $categorias->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Categorias with the id. Examples:
	 * GET /categorias/show/1
	 * GET /rest/categorias/1.json
	 *
	 * @return Categorias
	 */
	function show($id = null) {
		return $this->getCategorias($id);
	}

	/**
	 * Deletes the Categorias with the id. Examples:
	 * GET /categorias/delete/1
	 * DELETE /rest/categorias/1.json
	 */
	function eliminar($id = null) {
		$categorias = $this->getCategorias($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $categorias->fromArray($_REQUEST);
		try {
			if (null !== $categorias && $categorias->save()) {
				$this['errors'][] = 'Categorias deleted';
			} else {
				$this['errors'][] = 'Categorias could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('categorias');
		}
	}

	/**
	 * @return Categorias
	 */
	private function getCategorias($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Categorias::getPrimaryKey()])) {
			$id = $_REQUEST[Categorias::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Categorias
			$this['categorias'] = new Categorias;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['categorias'] = Categorias::retrieveByPK($id);
		}
		return $this['categorias'];
	}

}