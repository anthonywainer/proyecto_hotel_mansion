<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class FormasPagosController extends ApplicationController {

	/**
	 * Returns all FormasPagos records matching the query. Examples:
	 * GET /formas-pagos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/formas-pagos.json&limit=5
	 *
	 * @return FormasPagos[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = FormasPagos::getQuery(@$_GET)
            ->andLike('formas_pagos.descripcion','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;
		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'FormasPagos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['formas_pagos'] = $this['pager']->fetchPage();
        $this['u'] = 'formas-pagos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a FormasPagos. Example:
	 * GET /formas-pagos/edit/1
	 *
	 * @return FormasPagos
	 */
	function editar($id = null) {
		$this->getFormasPagos($id)->fromArray(@$_GET);
        $this['u'] = 'formas-pagos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a FormasPagos. Examples:
	 * POST /formas-pagos/save/1
	 * POST /rest/formas-pagos/.json
	 * PUT /rest/formas-pagos/1.json
	 */
	function guardar($id = null) {
		$formas_pagos = $this->getFormasPagos($id);

		try {
            $fecha_actual = date("Y-m-d");
            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }
			$formas_pagos->fromArray($_REQUEST);
			if ($formas_pagos->validate()) {
				$formas_pagos->save();
				$this->flash['messages'][] = 'Formas Pagos saved';
                $this->redirect('formas-pagos/index');

			}
			$this->flash['errors'] = $formas_pagos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('formas-pagos/edit/' . $formas_pagos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the FormasPagos with the id. Examples:
	 * GET /formas-pagos/show/1
	 * GET /rest/formas-pagos/1.json
	 *
	 * @return FormasPagos
	 */
	function show($id = null) {
		return $this->getFormasPagos($id);
	}

	/**
	 * Deletes the FormasPagos with the id. Examples:
	 * GET /formas-pagos/delete/1
	 * DELETE /rest/formas-pagos/1.json
	 */
	function eliminar($id = null) {
		$formas_pagos = $this->getFormasPagos($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $formas_pagos->fromArray($_REQUEST);
		try {
			if (null !== $formas_pagos && $formas_pagos->save()) {
				$this['errors'][] = 'Formas Pagos deleted';
			} else {
				$this['errors'][] = 'Formas Pagos could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('formas-pagos');
		}
	}

	/**
	 * @return FormasPagos
	 */
	private function getFormasPagos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[FormasPagos::getPrimaryKey()])) {
			$id = $_REQUEST[FormasPagos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new FormasPagos
			$this['formas_pagos'] = new FormasPagos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['formas_pagos'] = FormasPagos::retrieveByPK($id);
		}
		return $this['formas_pagos'];

	}

}