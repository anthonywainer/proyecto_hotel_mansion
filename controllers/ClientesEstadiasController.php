<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ClientesEstadiasController extends ApplicationController {

	/**
	 * Returns all ClientesEstadias records matching the query. Examples:
	 * GET /clientes-estadias?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/clientes-estadias.json&limit=5
	 *
	 * @return ClientesEstadias[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
		$q = ClientesEstadias::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'ClientesEstadias';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['clientes_estadias'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a ClientesEstadias. Example:
	 * GET /clientes-estadias/edit/1
	 *
	 * @return ClientesEstadias
	 */
	function editar($id = null) {
		return $this->getClientesEstadias($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a ClientesEstadias. Examples:
	 * POST /clientes-estadias/save/1
	 * POST /rest/clientes-estadias/.json
	 * PUT /rest/clientes-estadias/1.json
	 */
	function guardar($id = null) {
		$clientes_estadias = $this->getClientesEstadias($id);

		try {
			$clientes_estadias->fromArray($_REQUEST);
			if ($clientes_estadias->validate()) {
				$clientes_estadias->save();
				$this->flash['messages'][] = 'Clientes Estadias saved';
				return $this->loadView('estadias/list_clientes',['ideh'=>$_REQUEST['habitacion_estadia_id']]);
			}
			$this->flash['errors'] = $clientes_estadias->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('clientes-estadias/edit/' . $clientes_estadias->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the ClientesEstadias with the id. Examples:
	 * GET /clientes-estadias/show/1
	 * GET /rest/clientes-estadias/1.json
	 *
	 * @return ClientesEstadias
	 */
	function show($id = null) {
		return $this->getClientesEstadias($id);
	}

	/**
	 * Deletes the ClientesEstadias with the id. Examples:
	 * GET /clientes-estadias/delete/1
	 * DELETE /rest/clientes-estadias/1.json
	 */
	function eliminar($id = null) {
		$clientes_estadias = $this->getClientesEstadias($id);

		try {
			if (null !== $clientes_estadias && $clientes_estadias->delete()) {
				$this['messages'][] = 'Clientes Estadias deleted';
                $this->redirect('estadias/editar/'.$_REQUEST['ideh']);
			} else {
				$this['errors'][] = 'Clientes Estadias could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('clientes-estadias');
		}
	}

	/**
	 * @return ClientesEstadias
	 */
	private function getClientesEstadias($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[ClientesEstadias::getPrimaryKey()])) {
			$id = $_REQUEST[ClientesEstadias::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new ClientesEstadias
			$this['clientes_estadias'] = new ClientesEstadias;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['clientes_estadias'] = ClientesEstadias::retrieveByPK($id);
		}
		return $this['clientes_estadias'];
	}

}