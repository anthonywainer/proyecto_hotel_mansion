<?php

use Dabl\Query\QueryPager;

class LicenciasController extends ApplicationController {

	/**
	 * Returns all Licencia records matching the query. Examples:
	 * GET /licencias?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/licencias.json&limit=5
	 *
	 * @return Licencia[]
	 */
	function index() {
		$q = Licencia::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Licencia';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['licencias'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a Licencia. Example:
	 * GET /licencias/edit/1
	 *
	 * @return Licencia
	 */
	function edit($licencia_id = null) {
		return $this->getLicencia($licencia_id)->fromArray(@$_GET);
	}

	/**
	 * Saves a Licencia. Examples:
	 * POST /licencias/save/1
	 * POST /rest/licencias/.json
	 * PUT /rest/licencias/1.json
	 */
	function save($licencia_id = null) {
		$licencia = $this->getLicencia($licencia_id);

		try {
			$licencia->fromArray($_REQUEST);
			if ($licencia->validate()) {
				$licencia->save();
				$this->flash['messages'][] = 'Licencia saved';
				$this->redirect('licencias/show/' . $licencia->getLicenciaId());
			}
			$this->flash['errors'] = $licencia->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('licencias/edit/' . $licencia->getLicenciaId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Licencia with the licencia_id. Examples:
	 * GET /licencias/show/1
	 * GET /rest/licencias/1.json
	 *
	 * @return Licencia
	 */
	function show($licencia_id = null) {
		return $this->getLicencia($licencia_id);
	}

	/**
	 * Deletes the Licencia with the licencia_id. Examples:
	 * GET /licencias/delete/1
	 * DELETE /rest/licencias/1.json
	 */
	function delete($licencia_id = null) {
		$licencia = $this->getLicencia($licencia_id);

		try {
			if (null !== $licencia && $licencia->delete()) {
				$this['messages'][] = 'Licencia deleted';
			} else {
				$this['errors'][] = 'Licencia could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('licencias');
		}
	}

	/**
	 * @return Licencia
	 */
	private function getLicencia($licencia_id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $licencia_id && isset($_REQUEST[Licencia::getPrimaryKey()])) {
			$licencia_id = $_REQUEST[Licencia::getPrimaryKey()];
		}

		if ('' === $licencia_id || null === $licencia_id) {
			// if no primary key provided, create new Licencia
			$this['licencia'] = new Licencia;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['licencia'] = Licencia::retrieveByPK($licencia_id);
		}
		return $this['licencia'];
	}

}