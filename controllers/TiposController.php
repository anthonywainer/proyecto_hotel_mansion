<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class TiposController extends ApplicationController {

	/**
	 * Returns all Tipos records matching the query. Examples:
	 * GET /tipos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/tipos.json&limit=5
	 *
	 * @return Tipos[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        $_REQUEST['order_by'] = 'tipos.id';
        $_REQUEST['dir'] = 'DESC';
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
		$q = Tipos::getQuery(@$_GET)
            ->andLike('tipos.tipo','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Tipos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
        $this['tipos'] = $this['pager']->fetchPage();
        $this['u'] = 'tipos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Tipos. Example:
	 * GET /tipos/edit/1
	 *
	 * @return Tipos
	 */
	function editar($id = null) {
		return $this->getTipos($id)->fromArray(@$_GET);
        $this['u'] = 'tipos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Tipos. Examples:
	 * POST /tipos/save/1
	 * POST /rest/tipos/.json
	 * PUT /rest/tipos/1.json
	 */
	function guardar($id = null) {
		$tipos = $this->getTipos($id);
        $fecha_actual = date("Y-m-d");
        if (!$_REQUEST['id']) {
            $_REQUEST["created_at"] = $fecha_actual;
        }else {
            $_REQUEST["updated_at"] = $fecha_actual;
        }
		try {
			$tipos->fromArray($_REQUEST);
			if ($tipos->validate()) {
				$tipos->save();
				$this->flash['messages'][] = 'Tipos guardado';
				$this->redirect('tipos/index/');
			}
			$this->flash['errors'] = $tipos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('tipos/edit/' . $tipos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Tipos with the id. Examples:
	 * GET /tipos/show/1
	 * GET /rest/tipos/1.json
	 *
	 * @return Tipos
	 */
	function show($id = null) {
		return $this->getTipos($id);
	}

	/**
	 * Deletes the Tipos with the id. Examples:
	 * GET /tipos/delete/1
	 * DELETE /rest/tipos/1.json
	 */
	function eliminar($id = null) {
		$tipos = $this->getTipos($id);

		try {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["deleted_at"] = $fecha_actual;
            $tipos->fromArray($_REQUEST);
			if (null !== $tipos && $tipos->save()) {
				$this['errors'][] = 'Tipos eliminados';
			} else {
				$this['errors'][] = 'Tipos could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('tipos');
		}
	}

	/**
	 * @return Tipos
	 */
	private function getTipos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Tipos::getPrimaryKey()])) {
			$id = $_REQUEST[Tipos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Tipos
			$this['tipos'] = new Tipos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['tipos'] = Tipos::retrieveByPK($id);
		}
		return $this['tipos'];
	}

}