<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class MovimientosDeDinerosController extends ApplicationController {

    /**
     * Returns all MovimientosDeDinero records matching the query. Examples:
     * GET /movimientos-de-dineros?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
     * GET /rest/movimientos-de-dineros.json&limit=5
     *
     * @return MovimientosDeDinero[]
     */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }


    function index($id= null) {
        if ($id){
            $this['ide'] = $id;
            $this['u'] = 'movimientos-de-dineros/ventas';
            return $this->loadView("admin/index",$this);
        }
        $q = MovimientosDeDinero::getQuery(@$_GET);

        // paginate
        $limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
        $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
        $class = 'MovimientosDeDinero';
        $method = 'doSelectIterator';
        $this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

        if (isset($_GET['count_only'])) {
            return $this['pager'];
        }
        $this['movimientos_de_dineros'] = $this['pager']->fetchPage();
        $this['u'] = 'movimientos-de-dineros/index';
        $this->loadView("admin/index",$this);
    }

    /**
     * Form to create or edit a MovimientosDeDinero. Example:
     * GET /movimientos-de-dineros/edit/1
     *
     * @return MovimientosDeDinero
     */
    function editar($id = null) {
        $uvm = Estadias::retrieveByPK($id);
        $uvm->fromArray(['estado' => 'Inactivo']);
        $uvm->save();

        $query = "where estadia_id=".$id;
        $eh = EstadiasHabitaciones::getAll($query);
        if (count($eh)>0){
            foreach ($eh as $e){
                $sqlH= "UPDATE habitaciones SET estado='disponible' where id=".$e->habitacion_id;
                Habitaciones::getConnection()->query($sqlH)->execute();
            }
        }
        return $this->redirect('estadias');
                #$this->getMovimientosDeDinero($id)->fromArray(@$_GET);
        #$this['u'] = 'movimientos-de-dineros/edit';
        #$this->loadView("admin/index",$this);
    }

    /**
     * Saves a MovimientosDeDinero. Examples:
     * POST /movimientos-de-dineros/save/1
     * POST /rest/movimientos-de-dineros/.json
     * PUT /rest/movimientos-de-dineros/1.json
     */
    function guardar($id = null) {

        $movimientos_de_dinero = $this->getMovimientosDeDinero($id);
        $ca = Cajas::getAll('where estado= true');
        if (count($ca)>0) {
            $fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
            $_REQUEST['caja_id']    = $ca[0]->id;
            $_REQUEST['usuario_id'] = $_SESSION['user']->id;
            try {
                $movimientos_de_dinero->fromArray($_REQUEST);
                $movimientos_de_dinero->save();

                $_REQUEST['movimiento_id'] = $movimientos_de_dinero->getId();
                $_REQUEST['estadia_id'] = $_REQUEST['ide'];
                $_REQUEST['fecha'] = $fecha_actual;
                $vd = new Ventas;
                $vd->fromArray($_REQUEST);
                $vd->save();

                /*$uvm = Estadias::retrieveByPK($_REQUEST['ide']);
                $uvm->fromArray(['estado' => 'Inactivo']);
                $uvm->save();

                $query = "where estadia_id=".$_REQUEST['ide'];
                $eh = EstadiasHabitaciones::getAll($query);
                if (count($eh)>0){
                    foreach ($eh as $e){
                        $sqlH= "UPDATE habitaciones SET estado='disponible' where id=".$e->habitacion_id;
                        Habitaciones::getConnection()->query($sqlH)->execute();
                    }
                }*/
                $this->flash['messages'][] = 'Movimiento De Dinero guardado';
                $this['ide'] = $_REQUEST['ide'];
                return $this->loadView('movimientos-de-dineros/amortizaciones/',$this);

            } catch (Exception $e) {
                $this->flash['errors'][] = $e->getMessage();
            }
        }  else{
            $this->flash['errors'][] = "Falta Aperturar Caja";
        }
        $this->redirect('movimientos-de-dineros/edit/' . $movimientos_de_dinero->getId() . '?' . http_build_query($_REQUEST));
    }

    /**
     * Returns the MovimientosDeDinero with the id. Examples:
     * GET /movimientos-de-dineros/show/1
     * GET /rest/movimientos-de-dineros/1.json
     *
     * @return MovimientosDeDinero
     */
    function mostrar($id = null) {
        $this['id'] = $id;
        return $this->loadView('movimientos-de-dineros/comprobante',$this);
    }

    /**
     * Deletes the MovimientosDeDinero with the id. Examples:
     * GET /movimientos-de-dineros/delete/1
     * DELETE /rest/movimientos-de-dineros/1.json
     */
    function eliminar($id = null) {
        $movimientos_de_dinero = $this->getMovimientosDeDinero($id);

        try {
            if (null !== $movimientos_de_dinero && $movimientos_de_dinero->delete()) {
                $this['messages'][] = 'Movimientos de dinero eliminado';
                return $this->redirect('movimientos-de-dineros/index/'.$_REQUEST['idv']);
            } else {
                $this['errors'][] = 'Movimientos De Dinero could not be deleted';
            }
        } catch (Exception $e) {
            $this['errors'][] = $e->getMessage();
        }

        if ($this->outputFormat === 'html') {
            $this->flash['errors'] = @$this['errors'];
            $this->flash['messages'] = @$this['messages'];
            $this->redirect('movimientos-de-dineros');
        }
    }

    /**
     * @return MovimientosDeDinero
     */
    private function getMovimientosDeDinero($id = null) {
        // look for id in param or in $_REQUEST array
        if (null === $id && isset($_REQUEST[MovimientosDeDinero::getPrimaryKey()])) {
            $id = $_REQUEST[MovimientosDeDinero::getPrimaryKey()];
        }

        if ('' === $id || null === $id) {
            // if no primary key provided, create new MovimientosDeDinero
            $this['movimientos_de_dinero'] = new MovimientosDeDinero;
        } else {
            // if primary key provided, retrieve the record from the db
            $this['movimientos_de_dinero'] = MovimientosDeDinero::retrieveByPK($id);
        }
        return $this['movimientos_de_dinero'];
    }

}