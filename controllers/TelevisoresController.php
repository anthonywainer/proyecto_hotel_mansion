<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class TelevisoresController extends ApplicationController {

	/**
	 * Returns all Televisores records matching the query. Examples:
	 * GET /televisores?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/televisores.json&limit=5
	 *
	 * @return Televisores[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Televisores::getQuery(@$_GET)
            ->andLike('televisores.descripcion','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Televisores';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['televisores'] = $this['pager']->fetchPage();
        $this['u'] = 'televisores/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Televisores. Example:
	 * GET /televisores/edit/1
	 *
	 * @return Televisores
	 */
	function editar($id = null) {
		$this->getTelevisores($id)->fromArray(@$_GET);
        $this['u'] = 'televisores/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Televisores. Examples:
	 * POST /televisores/save/1
	 * POST /rest/televisores/.json
	 * PUT /rest/televisores/1.json
	 */
	function guardar($id = null) {
		$televisores = $this->getTelevisores($id);

		try {
            $fecha_actual = date("Y-m-d");
            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }
			$televisores->fromArray($_REQUEST);
			if ($televisores->validate()) {
				$televisores->save();
				$this->flash['messages'][] = 'Televisor Guardado';
				$this->redirect('televisores/index/');
			}
			$this->flash['errors'] = $televisores->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('televisores/edit/' . $televisores->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Televisores with the id. Examples:
	 * GET /televisores/show/1
	 * GET /rest/televisores/1.json
	 *
	 * @return Televisores
	 */
	function show($id = null) {
		return $this->getTelevisores($id);
	}

	/**
	 * Deletes the Televisores with the id. Examples:
	 * GET /televisores/delete/1
	 * DELETE /rest/televisores/1.json
	 */
	function eliminar($id = null) {
		$televisores = $this->getTelevisores($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $televisores->fromArray($_REQUEST);

		try {
			if (null !== $televisores && $televisores->save()) {
				$this['errors'][] = 'Televisor Eliminado';
			} else {
				$this['errors'][] = 'Televisores no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('televisores');
		}
	}

	/**
	 * @return Televisores
	 */
	private function getTelevisores($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Televisores::getPrimaryKey()])) {
			$id = $_REQUEST[Televisores::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Televisores
			$this['televisores'] = new Televisores;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['televisores'] = Televisores::retrieveByPK($id);
		}
		return $this['televisores'];
	}

}