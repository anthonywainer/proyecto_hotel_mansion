<?php

class IndexController extends ApplicationController {
	function index() {
        $this->layout = 'layouts/main-web';
	}
	function admin(){
        if ($_SESSION) {
            return $this['u'] = 'dashboard/index';
        }else{
            $this->redirect('/');
        }
    }
    function logout(){
        session_destroy();
        $this->redirect('/');
    }
    function habitacion(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/habitaciones",$this);
    }
    function nosotros(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/nosotros",$this);
    }
    function servicios(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/servicios",$this);
    }
    function galeria(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/galeria",$this);
    }
    function contacteno(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/contacteno",$this);

    }
}