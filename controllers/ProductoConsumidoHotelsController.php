<?php

use Dabl\Query\QueryPager;

class ProductoConsumidoHotelsController extends ApplicationController {

	/**
	 * Returns all ProductoConsumidoHotel records matching the query. Examples:
	 * GET /producto-consumido-hotels?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/producto-consumido-hotels.json&limit=5
	 *
	 * @return ProductoConsumidoHotel[]
	 */
	function index() {
		$q = ProductoConsumidoHotel::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'ProductoConsumidoHotel';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['producto_consumido_hotels'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a ProductoConsumidoHotel. Example:
	 * GET /producto-consumido-hotels/edit/1
	 *
	 * @return ProductoConsumidoHotel
	 */
	function edit($id = null) {
		return $this->getProductoConsumidoHotel($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a ProductoConsumidoHotel. Examples:
	 * POST /producto-consumido-hotels/save/1
	 * POST /rest/producto-consumido-hotels/.json
	 * PUT /rest/producto-consumido-hotels/1.json
	 */
	function save($id = null) {
		$producto_consumido_hotel = $this->getProductoConsumidoHotel($id);

		try {
			$producto_consumido_hotel->fromArray($_REQUEST);
			if ($producto_consumido_hotel->validate()) {
				$producto_consumido_hotel->save();
				$this->flash['messages'][] = 'Producto Consumido Hotel saved';
				$this->redirect('producto-consumido-hotels/show/' . $producto_consumido_hotel->getId());
			}
			$this->flash['errors'] = $producto_consumido_hotel->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('producto-consumido-hotels/edit/' . $producto_consumido_hotel->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the ProductoConsumidoHotel with the id. Examples:
	 * GET /producto-consumido-hotels/show/1
	 * GET /rest/producto-consumido-hotels/1.json
	 *
	 * @return ProductoConsumidoHotel
	 */
	function show($id = null) {
		return $this->getProductoConsumidoHotel($id);
	}

	/**
	 * Deletes the ProductoConsumidoHotel with the id. Examples:
	 * GET /producto-consumido-hotels/delete/1
	 * DELETE /rest/producto-consumido-hotels/1.json
	 */
	function delete($id = null) {
		$producto_consumido_hotel = $this->getProductoConsumidoHotel($id);

		try {
			if (null !== $producto_consumido_hotel && $producto_consumido_hotel->delete()) {
				$this['messages'][] = 'Producto Consumido Hotel deleted';
			} else {
				$this['errors'][] = 'Producto Consumido Hotel could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('producto-consumido-hotels');
		}
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	private function getProductoConsumidoHotel($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[ProductoConsumidoHotel::getPrimaryKey()])) {
			$id = $_REQUEST[ProductoConsumidoHotel::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new ProductoConsumidoHotel
			$this['producto_consumido_hotel'] = new ProductoConsumidoHotel;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['producto_consumido_hotel'] = ProductoConsumidoHotel::retrieveByPK($id);
		}
		return $this['producto_consumido_hotel'];
	}

}