<?php


class ReporteEstadiaController extends ApplicationController {


	function index() {

        $this['u'] = 'reportes/estadias';
        if (isset($_REQUEST['date_ini'])) {

            $begin = $_REQUEST['date_ini'];
            $end   = $_REQUEST['date_end'];

            $this['fechai'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_ini'])->format('d/m/Y');
            $this['fechae'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_end'])->format('d/m/Y');

        } else {
            $begin = date('Y-m-d');
            $end   = date('Y-m-d');
            $this['fechai'] = date("d/m/Y");
            $this['fechae'] = date("d/m/Y");
        }

        $sql = "
SELECT
estadias.id,
estadias.fecha_reserva,
estadias.fecha_ingreso,
estadias.fecha_salida,
clientes.nombres,
clientes.apellidos,
estadias_habitaciones.dias,
estadias_habitaciones.precio_total,
habitaciones.numero,
tipos_habitaciones.descripcion,
formas_pagos.descripcion AS forma_pago,
tipos_comprobantes.descripcion as comprobante
FROM
ventas
INNER JOIN estadias ON ventas.estadia_id = estadias.id
INNER JOIN clientes ON estadias.cliente_id = clientes.id
INNER JOIN estadias_habitaciones ON estadias_habitaciones.estadia_id = estadias.id
INNER JOIN habitaciones ON estadias_habitaciones.habitacion_id = habitaciones.id
INNER JOIN tipos_habitaciones ON estadias_habitaciones.tipo_habitacion = tipos_habitaciones.id AND habitaciones.tipo_habitacion_id = tipos_habitaciones.id ,
formas_pagos ,
tipos_comprobantes
WHERE (estadias.fecha_ingreso BETWEEN '".$begin."' AND '".$end."') GROUP BY id       
        ";
        $this['estadias'] = Estadias::getConnection()->query($sql)->fetchAll();

        return $this->loadView("admin/index",$this);
	}


}