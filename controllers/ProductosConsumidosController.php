<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ProductosConsumidosController extends ApplicationController {

	/**
	 * Returns all ProductosConsumidos records matching the query. Examples:
	 * GET /productos-consumidos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/productos-consumidos.json&limit=5
	 *
	 * @return ProductosConsumidos[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
		$q = ProductoConsumido::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'ProductosConsumidos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['producto_consumido'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a ProductosConsumidos. Example:
	 * GET /productos-consumidos/edit/1
	 *
	 * @return ProductosConsumidos
	 */
	function editar($id = null) {
		$this->getProductosConsumidos($id)->fromArray(@$_GET);
        $this['u'] = 'productos-consumidos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a ProductosConsumidos. Examples:
	 * POST /productos-consumidos/save/1
	 * POST /rest/productos-consumidos/.json
	 * PUT /rest/productos-consumidos/1.json
	 */
	function guardar($id = null) {
		$producto_consumido = $this->getProductosConsumidos($id);

		try {

		    $p = $_REQUEST['producto_id'];
		    $pp = Productos::getAll('where id='.$p);
		    $_REQUEST['monto'] = $pp[0]->getPrecio()*$_REQUEST['cantidad'];

            $producto_consumido->fromArray($_REQUEST);
            if ($producto_consumido->validate()) {
                $producto_consumido->save();
                $this->flash['messages'][] = 'Productos Consumidos guardado';
                return $this->loadView('estadias/list_productos',['ideh'=>$_REQUEST['habita_estadia_id']]);
            }
            #$query = "INSERT INTO producto_consumido (monto,producto_id,habita_estadia_id,cantidad) VALUES (".$_REQUEST['monto'].",".$_REQUEST['producto_id'].",".$_REQUEST['habita_estadia_id'].",".$_REQUEST['cantidad'].")";
            #ProductosConsumidos::getConnection()->query($query)->execute();

		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('productos-consumidos/editar/' . $producto_consumido->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the ProductosConsumidos with the id. Examples:
	 * GET /productos-consumidos/show/1
	 * GET /rest/productos-consumidos/1.json
	 *
	 * @return ProductosConsumidos
	 */
	function show($id = null) {
		return $this->getProductosConsumidos($id);
	}

	/**
	 * Deletes the ProductosConsumidos with the id. Examples:
	 * GET /productos-consumidos/delete/1
	 * DELETE /rest/productos-consumidos/1.json
	 */
	function eliminar($id = null) {
		$producto_consumido = $this->getProductosConsumidos($id);

		try {
			if (null !== $producto_consumido && $producto_consumido->delete()) {
				$this['errors'][] = 'Productos Consumidos deleted';
                $this->redirect('estadias/editar/'.$_REQUEST['ideh']);
			} else {
				$this['errors'][] = 'Productos Consumidos could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('productos-consumidos');
		}
	}

	/**
	 * @return ProductosConsumidos
	 */
	private function getProductosConsumidos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[ProductoConsumido::getPrimaryKey()])) {
			$id = $_REQUEST[ProductoConsumido::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new ProductosConsumidos
			$this['producto_consumido'] = new ProductoConsumido;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['producto_consumido'] = ProductoConsumido::retrieveByPK($id);
		}
		return $this['producto_consumido'];
	}

}