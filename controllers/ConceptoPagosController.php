<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ConceptoPagosController extends ApplicationController {

	/**
	 * Returns all ConceptoPago records matching the query. Examples:
	 * GET /concepto-pagos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/concepto-pagos.json&limit=5
	 *
	 * @return ConceptoPago[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }else{
            $ca = Cajas::getAll('where estado= true');
            if (!count($ca)>0){
                $this->flash['errors'][] = "Falta Aperturar Caja";
                return $this->redirect('cajas/mostrar');
            }
        }
    }


	function index() {
		$q = ConceptoPago::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'ConceptoPago';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['concepto_pagos'] = $this['pager']->fetchPage();
        $this['u'] = 'concepto-pagos/index';
        $this->loadView("admin/index",$this);

	}

	/**
	 * Form to create or edit a ConceptoPago. Example:
	 * GET /concepto-pagos/edit/1
	 *
	 * @return ConceptoPago
	 */
	function editar($id = null) {
        $ca = Cajas::getAll('where estado= true');
        $this['max'] = $ca[0]->getSaldo_inicial();
         $this->getConceptoPago($id)->fromArray(@$_GET);
        $this['u'] = 'concepto-pagos/edit';
        return $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a ConceptoPago. Examples:
	 * POST /concepto-pagos/save/1
	 * POST /rest/concepto-pagos/.json
	 * PUT /rest/concepto-pagos/1.json
	 */
	function guardar($id = null) {
        $concepto_pago = $this->getConceptoPago($id);
        $fecha_actual = date("Y-m-d");
        if ($id){
            $movimientos_de_dinero = MovimientosDeDinero::retrieveByPK($_REQUEST['movimiento_id']);
            $_REQUEST["updated_at"] = $fecha_actual;
        }else{
            $movimientos_de_dinero = new MovimientosDeDinero;
            $_REQUEST["created_at"] = $fecha_actual;
        }
        $ca = Cajas::getAll('where estado= true');
        $_REQUEST['forma_pago_id']    = 1;
        $_REQUEST['caja_id']    = $ca[0]->id;
        $_REQUEST['usuario_id'] = $_SESSION['user']->id;

        $movimientos_de_dinero->fromArray($_REQUEST);
        $movimientos_de_dinero->save();
        $_REQUEST['movimiento_id'] = $movimientos_de_dinero->getId();

		try {
			$concepto_pago->fromArray($_REQUEST);
			if ($concepto_pago->validate()) {
				$concepto_pago->save();
				$this->flash['messages'][] = 'Concepto Pago guardado';
				$this->redirect('concepto-pagos/');
			}
			$this->flash['errors'] = $concepto_pago->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('concepto-pagos/edit/' . $concepto_pago->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the ConceptoPago with the id. Examples:
	 * GET /concepto-pagos/show/1
	 * GET /rest/concepto-pagos/1.json
	 *
	 * @return ConceptoPago
	 */
	function show($id = null) {
		return $this->getConceptoPago($id);
	}

	/**
	 * Deletes the ConceptoPago with the id. Examples:
	 * GET /concepto-pagos/delete/1
	 * DELETE /rest/concepto-pagos/1.json
	 */
	function eliminar($id = null) {
		$concepto_pago = $this->getConceptoPago($id);

		try {
			if (null !== $concepto_pago && $concepto_pago->delete()) {
				$this['messages'][] = 'Concepto Pago eliminado';
			} else {
				$this['errors'][] = 'Concepto Pago could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('concepto-pagos');
		}
	}

	/**
	 * @return ConceptoPago
	 */
	private function getConceptoPago($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[ConceptoPago::getPrimaryKey()])) {
			$id = $_REQUEST[ConceptoPago::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new ConceptoPago
			$this['concepto_pago'] = new ConceptoPago;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['concepto_pago'] = ConceptoPago::retrieveByPK($id);
		}
		return $this['concepto_pago'];
	}

}