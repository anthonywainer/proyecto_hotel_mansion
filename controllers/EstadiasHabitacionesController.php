<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class EstadiasHabitacionesController extends ApplicationController {

    /**
     * Returns all EstadiasHabitaciones records matching the query. Examples:
     * GET /estadias-habitaciones?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
     * GET /rest/estadias-habitaciones.json&limit=5
     *
     * @return EstadiasHabitaciones[]
     */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
    function index() {
        $q = EstadiasHabitaciones::getQuery(@$_GET);

        $no = true;
        if ($_GET['id']=="") {
            $this['usuarios'] = Clientes::getAll();
        }else{
            $no = false;
            $sql= "SELECT
            estadias_habitaciones.id,
            habitaciones.numero,
            habitaciones.descripcion,
            habitaciones.estado,
            tipos_habitaciones.descripcion as tipo,
            precio_total as  total
            FROM
            estadias_habitaciones
            INNER JOIN habitaciones ON estadias_habitaciones.habitacion_id = habitaciones.id
            INNER JOIN tipos_habitaciones ON habitaciones.tipo_habitacion_id = tipos_habitaciones.id
            where estadias_habitaciones.estadia_id = ".$_GET['id'];

            $this['habitacion_estadia']=EstadiasHabitaciones::getConnection()->query($sql)->fetchAll();

        }

        $this['no'] = $no;
        $this['u'] = 'estadias-habitaciones/index';
        $this->loadView("admin/index",$this);
    }

    /**
     * Form to create or edit a EstadiasHabitaciones. Example:
     * GET /estadias-habitaciones/edit/1
     *
     * @return EstadiasHabitaciones
     */
    function editar($id = null) {
        $this->getEstadiasHabitaciones($id)->fromArray(@$_GET);
        $this['u'] = 'estadias-habitaciones/index';
        $this->loadView("admin/index",$this);
    }

    /**
     * Saves a EstadiasHabitaciones. Examples:
     * POST /estadias-habitaciones/save/1
     * POST /rest/estadias-habitaciones/.json
     * PUT /rest/estadias-habitaciones/1.json
     */
    function guardar($id = null) {
        $estadias_habitaciones = $this->getEstadiasHabitaciones($id);

        try {
            $fecha_actual = date("Y-m-d");
            if ($_REQUEST['ide']) {
                $_REQUEST['estadia_id'] = $_REQUEST['ide'];
            }else{
                $_REQUEST['estado'] = 'Inactivo';
                #$_REQUEST['estado'] = 'Activo';
                $_REQUEST['usuario_id'] = $_SESSION['user']->id;
                $_REQUEST["created_at"] = $fecha_actual;
                $e = new Estadias;
                $e->fromArray($_REQUEST);
                $e->save();
                $_REQUEST['estadia_id'] = $e->getId();
                $this['idestadia']=$e->getId();
            }
            $_REQUEST['habitacion_id'] = $_REQUEST['idh'];
            $_REQUEST["created_at"] = $fecha_actual;
            $estadias_habitaciones->fromArray($_REQUEST);
            if ($estadias_habitaciones->validate()) {
                $estadias_habitaciones->save();
                $this->flash['messages'][] = 'Estadias Habitaciones saved';
                $ideh =$estadias_habitaciones->getId();
                return $this->redirect("estadias/editar/".$ideh,$this);
            }
            $this->flash['errors'] = $estadias_habitaciones->getValidationErrors();
        } catch (Exception $e) {
            $this->flash['errors'][] = $e->getMessage();
        }

        $this->redirect('estadias-habitaciones/edit/' . $estadias_habitaciones->getId() . '?' . http_build_query($_REQUEST));
    }

    /**
     * Returns the EstadiasHabitaciones with the id. Examples:
     * GET /estadias-habitaciones/show/1
     * GET /rest/estadias-habitaciones/1.json
     *
     * @return EstadiasHabitaciones
     */
    function mostrar($id = null) {
        $this->getEstadiasHabitaciones($id);
        $this['u'] = 'estadias-habitaciones/show';
        $this->loadView("admin/index",$this);
    }

    /**
     * Deletes the EstadiasHabitaciones with the id. Examples:
     * GET /estadias-habitaciones/delete/1
     * DELETE /rest/estadias-habitaciones/1.json
     */
    function eliminar($id = null) {
        $estadias_habitaciones = $this->getEstadiasHabitaciones($id);
        $query = "where id=".$id;
        $eh = EstadiasHabitaciones::getAll($query);

        $sqlH= "UPDATE habitaciones SET estado='disponible' where id=".$eh[0]->habitacion_id;
        Habitaciones::getConnection()->query($sqlH)->execute();
        $sqlg= "DELETE FROM estadias where id=".$eh[0]->estadia_id;
        Estadias::getConnection()->query($sqlg)->execute();
        try {

            if (null !== $estadias_habitaciones && $estadias_habitaciones->delete()) {
                $this['messages'][] = 'Estadias Habitaciones eliminado';
                $this->flash['messages'] = @$this['messages'];
                return $this->redirect('estadias-habitaciones?id='.$eh[0]->estadia_id);
            } else {
                $this['errors'][] = 'Estadias Habitaciones could not be deleted';
            }
        } catch (Exception $e) {
            $this['errors'][] = $e->getMessage();
        }

        if ($this->outputFormat === 'html') {
            $this->flash['errors'] = @$this['errors'];
            $this->flash['messages'] = @$this['messages'];
            $this->redirect('habitaciones/?ide=');
        }
    }

    function eliminarhabitacionestadia($id = null) {
        $estadias_habitaciones = $this->getEstadiasHabitaciones($id);
        $query = "where id=".$id;
        $eh = EstadiasHabitaciones::getAll($query);

        $sqlH= "UPDATE habitaciones SET estado='disponible' where id=".$eh[0]->habitacion_id;
        $ha=Habitaciones::getAll("where id=".$eh[0]->habitacion_id);

        try {
            if (null !== $estadias_habitaciones && $estadias_habitaciones->delete() && $ha->delete()) {
                $this['messages'][] = 'Estadias Habitaciones eliminado';
                $this->flash['messages'] = @$this['messages'];
                return $this->redirect('estadias-habitaciones?id='.$eh[0]->estadia_id);
            } else {
                $this['errors'][] = 'Estadias Habitaciones could not be deleted';
            }
        } catch (Exception $e) {
            $this['errors'][] = $e->getMessage();
        }

        if ($this->outputFormat === 'html') {
            $this->flash['errors'] = @$this['errors'];
            $this->flash['messages'] = @$this['messages'];
            $this->redirect('estadias-habitaciones');
        }
    }

    /**
     * @return EstadiasHabitaciones
     */
    private function getEstadiasHabitaciones($id = null) {
        // look for id in param or in $_REQUEST array
        if (null === $id && isset($_REQUEST[EstadiasHabitaciones::getPrimaryKey()])) {
            $id = $_REQUEST[EstadiasHabitaciones::getPrimaryKey()];
        }

        if ('' === $id || null === $id) {
            // if no primary key provided, create new EstadiasHabitaciones
            $this['estadias_habitaciones'] = new EstadiasHabitaciones;
        } else {
            // if primary key provided, retrieve the record from the db
            $this['estadias_habitaciones'] = EstadiasHabitaciones::retrieveByPK($id);
        }
        return $this['estadias_habitaciones'];
    }

}