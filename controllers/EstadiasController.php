<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class EstadiasController extends ApplicationController {

	/**
	 * Returns all Estadias records matching the query. Examples:
	 * GET /estadias?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/estadias.json&limit=5
	 *
	 * @return Estadias[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
    function tipohabitacion(){

    }
    function permisos(){

    }
    function tipoprecio(){

    }
    function descuento(){

    }
    function aumento(){

    }
	function index() {
		$q = Estadias::getQuery(@$_GET)->add('estado','Activo');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Estadias';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}


        $this['estadias'] = $this['pager']->fetchPage();
        $this['u'] = 'estadias/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Estadias. Example:
	 * GET /estadias/edit/1
	 *
	 * @return Estadias
	 */
	function editar($id = null) {
        $sql= "SELECT * FROM clientes WHERE id NOT IN ( SELECT	cliente_id AS id FROM	clientes_estadias	
                WHERE	estado = 1)";

        $sqleh = " SELECT
                tipos_habitaciones.id as idth,
                tipos_habitaciones.descripcion AS tipo,
                tipos_habitaciones.precio as precioh,
                habitaciones.numero,
                habitaciones.id AS idh,
                habitaciones.descripcion,
                estadias.cliente_id,
                estadias_habitaciones.aumento,
                estadias_habitaciones.precio,
                estadias_habitaciones.descuento,
                estadias_habitaciones.fecha_reserva,
                estadias_habitaciones.fecha_ingreso,
                estadias_habitaciones.fecha_salida,
                estadias_habitaciones.observacion,
                estadias_habitaciones.precio_total,
                estadias_habitaciones.dias,
                estadias_habitaciones.tipo_precio,
                estadias_habitaciones.tipo_habitacion
                FROM
                    estadias_habitaciones
                INNER JOIN estadias ON estadias_habitaciones.estadia_id = estadias.id
                INNER JOIN habitaciones ON estadias_habitaciones.habitacion_id = habitaciones.id
                INNER JOIN tipos_habitaciones ON habitaciones.tipo_habitacion_id = tipos_habitaciones.id
                WHERE
                    estadias_habitaciones.id = ".$id;
        $this['ideh'] = $id;
        $this['habitacion'] = Habitaciones::getConnection()->query($sqleh)->fetchAll();
        $this['clientes']   = Clientes::getConnection()->query($sql)->fetchAll();
		 //$this->getEstadias($id)->fromArray(@$_GET);
        $this['u'] = 'estadias/form-estadia';

        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Estadias. Examples:
	 * POST /estadias/save/1
	 * POST /rest/estadias/.json
	 * PUT /rest/estadias/1.json
	 */
	function actualizar(){
        $de= " ";
        if ($_REQUEST['descuento']!='') {
            $de = "descuento=" . $_REQUEST['descuento'] . ",";
        }
        $fr='';
        $estado="ocupado";


        if (isset($_REQUEST['fecha_reserva'])) {
            if ($_REQUEST['fecha_reserva'] != '') {
                $value = $_REQUEST['fecha_reserva'];
                if (strpos($value, '/')){
                    $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                }
                $fr = " fecha_reserva= '" . $value . "',";
                $estado="reservado";
                $fi = 'null';
            }else{
                $fr = " fecha_reserva= null,";
                $estado="ocupado";
                $value = $_REQUEST['fecha_ingreso'];
                if (strpos($value, '/')){
                    $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                }
                $fi = "'".$value."'";
            }
        }else{
            $value = $_REQUEST['fecha_ingreso'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
            }
            $fi = "'".$value."'";
        }
        $value = $_REQUEST['fecha_salida'];
        if (strpos($value, '/')){
            $value =  DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
        $sqlI = "UPDATE estadias_habitaciones SET ".$de.$fr."
        fecha_ingreso=".$fi.", 
        fecha_salida='". $value."',
        observacion='".$_REQUEST['observacion']."',
        tipo_habitacion='".$_REQUEST['tipo_habitacion']."',
        aumento='".$_REQUEST['aumento']."',
        tipo_precio='".$_REQUEST['tipo_precio']."',
        precio=".$_REQUEST['precio'].",
        precio_total=".$_REQUEST['precio_total'].",
        dias=".$_REQUEST['dias']." 
        where id=".$_REQUEST['ideh'];
        EstadiasHabitaciones::getConnection()->query($sqlI)->execute();

        $query = "SELECT estadia_id,habitacion_id from estadias_habitaciones where id=".$_REQUEST['ideh'];
        $eh = EstadiasHabitaciones::getConnection()->query($query)->fetchAll();


        $sqlH= "UPDATE habitaciones SET estado='".$estado."' where id=".$eh[0]['habitacion_id'];
        Habitaciones::getConnection()->query($sqlH)->execute();
        if(isset($_REQUEST['pagar'])){
            return $this->redirect('movimientos-de-dineros/index/'.$_REQUEST["id_estadia"]);
        }
        return $this->redirect('estadias-habitaciones/?id='.$eh[0]['estadia_id']);
    }

	function guardar($id = null) {
        if (isset($_REQUEST['ideh'])){
            $idh = EstadiasHabitaciones::getAll('where id='.$_REQUEST['ideh']);
            $estadias = $this->getEstadias($idh[0]->getEstadia_id());
            #cambio actualizar estado a Activo
            $_REQUEST["estado"]="Activo";
            $estadias->fromArray($_REQUEST);
            $estadias->save();
            $_REQUEST["id_estadia"]=$idh[0]->getEstadia_id();
            return $this->actualizar($_REQUEST);
        }
		$estadias = $this->getEstadias($id);


		try {
		    if ($id) {
                $fecha_actual = date("Y-m-d");
                $_REQUEST['fecha_reserva'] = $fecha_actual;
            }

            $_REQUEST['estado'] = 'Activo';
            $_REQUEST['usuario_id'] = $_SESSION['user']->id;
            $_REQUEST["created_at"] = $fecha_actual;
			$estadias->fromArray($_REQUEST);

			if ($estadias->validate()) {
				$estadias->save();
				#$this->flash['messages'][] = 'Estadias ';
                $this->redirect('habitaciones/?ide='.$estadias->getId());
			}
			$this->flash['errors'] = $estadias->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('estadias/edit/' . $estadias->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Estadias with the id. Examples:
	 * GET /estadias/show/1
	 * GET /rest/estadias/1.json
	 *
	 * @return Estadias
	 */
	function mostrar($id = null) {
        if ($_REQUEST['f']=='i'){
            $sf = "fecha_ingreso";
        }else{
            $sf = "fecha_reserva";
        }
        $query = "SELECT
            estadias_habitaciones.id
        FROM
            estadias_habitaciones
        INNER JOIN estadias ON estadias_habitaciones.estadia_id = estadias.id
        WHERE
        estadias.estado = 'Activo' AND
        estadias_habitaciones.habitacion_id = ".$id." AND estadias_habitaciones.".$sf." is not null ";
        if(EstadiasHabitaciones::getConnection()->query($query)->fetchAll()){
            $date = $_REQUEST['fecha'];
            if (strpos($date, '/')){
                $date =  DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
            }
            $q = $query." and ('".$date."' BETWEEN CURRENT_DATE() AND estadias_habitaciones.".$sf.")
        ORDER BY  estadias_habitaciones.fecha_salida ASC ";
            echo($q);
            $eh = EstadiasHabitaciones::getConnection()->query($q)->fetchAll();
            if($eh){
                $msm = 'yes';
            }else{
                $msm = '';
            }
            print_r($msm); exit();
        }else{
            print_r('haaa'); exit();
        }


	}

	/**
	 * Deletes the Estadias with the id. Examples:
	 * GET /estadias/delete/1
	 * DELETE /rest/estadias/1.json
	 */
	function eliminar($id = null) {
		$estadias = $this->getEstadias($id);

        $query = "where estadia_id=".$id;
        $eh = EstadiasHabitaciones::getAll($query);
        if (count($eh)>0){
            foreach ($eh as $e){
                $sqlH= "UPDATE habitaciones SET estado='disponible' where id=".$e->habitacion_id;
                Habitaciones::getConnection()->query($sqlH)->execute();
            }
        }



		try {
			if (null !== $estadias && $estadias->delete()) {
				$this['errors'][] = 'Estadia eliminada';
			} else {
				$this['errors'][] = 'Estadias could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('estadias');
		}
	}

	/**
	 * @return Estadias
	 */
	private function getEstadias($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Estadias::getPrimaryKey()])) {
			$id = $_REQUEST[Estadias::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Estadias
			$this['estadias'] = new Estadias;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['estadias'] = Estadias::retrieveByPK($id);
		}
		return $this['estadias'];
	}

}