<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class VentasController extends ApplicationController {

	/**
    +	 * Returns all Ventas records matching the query. Examples:
    +	 * GET /ventas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
    +	 * GET /rest/ventas.json&limit=5
    +	 *
    +	 * @return Ventas[]
    +	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }

	function index() {
        		$q = Ventas::getQuery(@$_GET);
        		// paginate
        		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
        		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
        		$class = 'Ventas';
        		$method = 'doSelectIterator';
        		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

        		if (isset($_GET['count_only'])) {
            			return $this['pager'];
		}
		$this['ventas'] = $this['pager']->fetchPage();
        $this['u'] = 'ventas/index';
        return $this->loadView("admin/index",$this);

	}

	/**
    +	 * Form to create or edit a Ventas. Example:
    +	 * GET /ventas/edit/1
    +	 *
    +	 * @return Ventas
    +	 */
	function editar($id = null) {
	    $this->getVentas($id)->fromArray(@$_GET);
        $this['u'] = 'ventas/edit';
        return $this->loadView("admin/index",$this);
	}

	/**
    +	 * Saves a Ventas. Examples:
    +	 * POST /ventas/save/1
    +	 * POST /rest/ventas/.json
    +	 * PUT /rest/ventas/1.json
    +	 */
	function guardar($id = null) {
        		$ventas = $this->getVentas($id);
        		if(floatval($_REQUEST['monto'])<floatval($_REQUEST['cuota']))
                    $_REQUEST['monto']=floatval($_REQUEST['cuota'])+floatval($_REQUEST['resto']);

                $sqlH="UPDATE movimientos_de_dinero SET monto= ".$_REQUEST['monto']." where id=".$ventas->movimiento_id;
                MovimientosDeDinero::getConnection()->query($sqlH)->execute();
        		try {
            			$ventas->fromArray($_REQUEST);
            			if ($ventas->validate()) {
                				$ventas->save();
                				$this->flash['messages'][] = 'Ventas saved';
                            $this['ide'] = $ventas->getEstadia_id();
                            return $this->loadView('movimientos-de-dineros/amortizaciones/',$this);
                			}
			$this->flash['errors'] = $ventas->getValidationErrors();
		} catch (Exception $e) {
            			$this->flash['errors'][] = $e->getMessage();
            		}

		$this->redirect('ventas/edit/' . $ventas->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
    +	 * Returns the Ventas with the id. Examples:
    +	 * GET /ventas/show/1
    +	 * GET /rest/ventas/1.json
    +	 *
    +	 * @return Ventas
    +	 */
	function show($id = null) {
        		return $this->getVentas($id);
	}

	/**
    +	 * Deletes the Ventas with the id. Examples:
    +	 * GET /ventas/delete/1
    +	 * DELETE /rest/ventas/1.json
    +	 */
	function delete($id = null) {
        		$ventas = $this->getVentas($id);

        		try {
            			if (null !== $ventas && $ventas->delete()) {
                				$this['messages'][] = 'Ventas deleted';
                			} else {
                				$this['errors'][] = 'Ventas could not be deleted';
                			}
		} catch (Exception $e) {
            			$this['errors'][] = $e->getMessage();
            		}

		if ($this->outputFormat === 'html') {
            			$this->flash['errors'] = @$this['errors'];
            			$this->flash['messages'] = @$this['messages'];
            			$this->redirect('ventas');
            		}
	}

	/**
    +	 * @return Ventas
    +	 */
	private function getVentas($id = null) {
        		// look for id in param or in $_REQUEST array
        		if (null === $id && isset($_REQUEST[Ventas::getPrimaryKey()])) {
            			$id = $_REQUEST[Ventas::getPrimaryKey()];
            		}

		if ('' === $id || null === $id) {
            			// if no primary key provided, create new Ventas
            			$this['ventas'] = new Ventas;
            		} else {
            			// if primary key provided, retrieve the record from the db

            			$this['ventas'] = Ventas::retrieveByPK($id);
		}
		return $this['ventas'];
	}

}