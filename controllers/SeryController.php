<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class SeryController extends ApplicationController {

	/**
	 * Returns all Series records matching the query. Examples:
	 * GET /sery?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/sery.json&limit=5
	 *
	 * @return Series[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
    function index() {

        $q = Series::getQuery(@$_GET);
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Series::getQuery(@$_GET)
            ->andLike('series.serie','%'.$_GET['search'].'%')
            ->orLike('series.correlativo','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Series';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['sery'] = $this['pager']->fetchPage();
        $this['u'] = 'sery/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Series. Example:
	 * GET /sery/edit/1
	 *
	 * @return Series
	 */
	function editar($id = null) {
		$this->getSeries($id)->fromArray(@$_GET);
        $this['u'] = 'sery/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Series. Examples:
	 * POST /sery/save/1
	 * POST /rest/sery/.json
	 * PUT /rest/sery/1.json
	 */
	function guardar($id = null) {
		$series = $this->getSeries($id);
        $fecha_actual = date("Y-m-d");
        if (!$_REQUEST['id']) {
            $_REQUEST["created_at"] = $fecha_actual;
        }else {
            $_REQUEST["updated_at"] = $fecha_actual;
        }

		try {
			$series->fromArray($_REQUEST);
			if ($series->validate()) {
				$series->save();
				$this->flash['messages'][] = 'Series saved';
				$this->redirect('sery/index/' . $series->getId());
			}
			$this->flash['errors'] = $series->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('sery/edit/' . $series->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Series with the id. Examples:
	 * GET /sery/show/1
	 * GET /rest/sery/1.json
	 *
	 * @return Series
	 */
	function show($id = null) {
		return $this->getSeries($id);
	}

	/**
	 * Deletes the Series with the id. Examples:
	 * GET /sery/delete/1
	 * DELETE /rest/sery/1.json
	 */
	function eliminar($id = null) {
		$series = $this->getSeries($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $series->fromArray($_REQUEST);

		try {
			if (null !== $series && $series->save()) {
				$this['errors'][] = 'Series deleted';
			} else {
				$this['errors'][] = 'Series could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('sery');
		}
	}

	/**
	 * @return Series
	 */
	private function getSeries($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Series::getPrimaryKey()])) {
			$id = $_REQUEST[Series::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Series
			$this['series'] = new Series;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['series'] = Series::retrieveByPK($id);
		}
		return $this['series'];
	}

}