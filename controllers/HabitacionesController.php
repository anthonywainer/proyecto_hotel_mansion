<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class HabitacionesController extends ApplicationController {

	/**
	 * Returns all Habitaciones records matching the query. Examples:
	 * GET /habitaciones?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/habitaciones.json&limit=5
	 *
	 * @return Habitaciones[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return $this->loadView("paginaweb/habitaciones",$this);
        }
    }

	function index()
    {

        if (isset($_GET['ide'])) {
            if(isset($_REQUEST['dia'])){
                if($_REQUEST['dia']=="+1") {
                    $fechadividida = explode("/", $_REQUEST['date_ini']);
                    $fechaunida = $fechadividida[2] . "-" . $fechadividida[1] . "-" . $fechadividida[0];
                    $nuevafecha = strtotime('+1 day', strtotime($fechaunida));
                    $nuevafecha = date('Y-m-d', $nuevafecha);


                    $begin = new DateTime($nuevafecha);
                    $end = new DateTime($nuevafecha);
                    $this['fechai'] = DateTime::createFromFormat('Y-m-d', $nuevafecha)->format('d/m/Y');
                }else{
                    $fechadividida = explode("/", $_REQUEST['date_ini']);
                    $fechaunida = $fechadividida[2] . "-" . $fechadividida[1] . "-" . $fechadividida[0];
                    $nuevafecha = strtotime('-1 day', strtotime($fechaunida));
                    $nuevafecha = date('Y-m-d', $nuevafecha);

                    $begin = new DateTime($nuevafecha);
                    $end = new DateTime($nuevafecha);
                    $this['fechai'] = DateTime::createFromFormat('Y-m-d', $nuevafecha)->format('d/m/Y');
                }
            }else{

            if (isset($_REQUEST['date_ini'])) {
                $fechadividida=explode("/",$_REQUEST['date_ini']);
                $fechaunida=$fechadividida[2]."-".$fechadividida[1]."-".$fechadividida[0];
                $begin = new DateTime($fechaunida);
                $end   = new DateTime($fechaunida);
                $this['fechai']=$_REQUEST['date_ini'];
                //$this['fechai'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_ini'])->format('d/m/Y');
                //$this['fechae'] = DateTime::createFromFormat('Y-m-d', $_REQUEST['date_end'])->format('d/m/Y');

            } else {
                $begin = new DateTime(date('Y-m-d'));
                $end   = new DateTime(date('Y-m-d'));
                $this['fechai'] = date("d/m/Y");
                $this['fechae'] = date("d/m/Y");
            }
            }
            for ($i = $begin; $i <= $end; $i->modify('+1 day')) {

                $query = "
                        SELECT
                        estadias_habitaciones.estadia_id,
                        estadias_habitaciones.fecha_reserva,
                        estadias_habitaciones.fecha_ingreso,
                        estadias_habitaciones.fecha_salida,
                        estadias_habitaciones.id,
                        estadias_habitaciones.habitacion_id
                        FROM
                        estadias_habitaciones 
                        INNER JOIN estadias ON estadias_habitaciones.estadia_id = estadias.id
                        WHERE
                        estadias.estado = 'Activo' 
                        and ('" . $i->format("Y-m-d") . "' BETWEEN estadias_habitaciones.fecha_ingreso AND estadias_habitaciones.fecha_salida)
                        UNION ALL
                        SELECT
                        estadias_habitaciones.estadia_id,
                        estadias_habitaciones.fecha_reserva,
                        estadias_habitaciones.fecha_ingreso,
                        estadias_habitaciones.fecha_salida,
                        estadias_habitaciones.id,
                        estadias_habitaciones.habitacion_id
                        FROM
                        estadias_habitaciones 
                        INNER JOIN estadias ON estadias_habitaciones.estadia_id = estadias.id
                        WHERE
                        estadias.estado = 'Activo' 
                        and '" . $i->format("Y-m-d") . "' BETWEEN estadias_habitaciones.fecha_reserva
                        AND estadias_habitaciones.fecha_salida
                        ";

                $ddd = Habitaciones::getConnection()->query($query)->fetchAll();
                $this['date_i'] = $i->format("d/m/Y");
                if($ddd){

                    break;
                }
            }
            $this['estadia_habitacion'] = $ddd;
            if ($_GET['ide'] == '') {
                $this['u'] = 'habitaciones/habitaciones-u';
                return $this->loadView("admin/index", $this);
            }

            $sqle = Estadias::getAll("WHERE id=" . $_GET['ide']);
            $idu = $sqle[0]->cliente_id;
            if ($idu) {
                $this['resp'] = Clientes::getAll(" where id= " . $idu);
            }else{
                $this['resp'] = '';
            }
            $this['u'] = 'habitaciones/habitaciones';
            return $this->loadView("admin/index", $this);



        }

        if (!isset($_GET['search'])) {
            $_GET['search'] = '';
        }
        $q = Habitaciones::getQuery(@$_GET)
            ->andLike('habitaciones.numero', '%' . $_GET['search'] . '%');
        // paginate
        $limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
        $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
        $class = 'Habitaciones';
        $method = 'doSelectIterator';
        $this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

        if (isset($_GET['count_only'])) {
            return $this['pager'];
        }
        $this['habitaciones'] = $this['pager']->fetchPage();
        $this['u'] = 'habitaciones/index';
        $this->loadView("admin/index", $this);
    }

	/**
	 * Form to create or edit a Habitaciones. Example:
	 * GET /habitaciones/edit/1
	 *
	 * @return Habitaciones
	 */
	function editar($id = null) {
		$this->getHabitaciones($id)->fromArray(@$_GET);
        $this['u'] = 'habitaciones/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Habitaciones. Examples:
	 * POST /habitaciones/save/1
	 * POST /rest/habitaciones/.json
	 * PUT /rest/habitaciones/1.json
	 */
	function guardar($id = null) {
		$habitaciones = $this->getHabitaciones($id);

		try {
			$habitaciones->fromArray($_REQUEST);
			if ($habitaciones->validate()) {
				$habitaciones->save();
				$this->flash['messages'][] = 'Habitacion guardada';
				$this->redirect('habitaciones/index/' . $habitaciones->getId());
			}
			$this->flash['errors'] = $habitaciones->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('habitaciones/edit/' . $habitaciones->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Habitaciones with the id. Examples:
	 * GET /habitaciones/show/1
	 * GET /rest/habitaciones/1.json
	 *
	 * @return Habitaciones
	 */
	function show($id = null) {
		return $this->getHabitaciones($id);
	}

	/**
	 * Deletes the Habitaciones with the id. Examples:
	 * GET /habitaciones/delete/1
	 * DELETE /rest/habitaciones/1.json
	 */
	function eliminar($id = null) {
		$habitaciones = $this->getHabitaciones($id);

		try {
			if (null !== $habitaciones && $habitaciones->delete()) {
				$this['messages'][] = 'Habitacion eliminada';
			} else {
				$this['errors'][] = 'Habitacion no se ha podido eliminar';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('habitaciones');
		}
	}

	/**
	 * @return Habitaciones
	 */
	private function getHabitaciones($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Habitaciones::getPrimaryKey()])) {
			$id = $_REQUEST[Habitaciones::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Habitaciones
			$this['habitaciones'] = new Habitaciones;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['habitaciones'] = Habitaciones::retrieveByPK($id);
		}
		return $this['habitaciones'];
	}

}