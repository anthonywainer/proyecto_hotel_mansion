<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ModulosController extends ApplicationController {

	/**
	 * Returns all Modulos records matching the query. Examples:
	 * GET /modulos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/modulos.json&limit=5
	 *
	 * @return Modulos[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Modulos::getQuery(@$_GET)
            ->andLike('modulos.nombre','%'.$_GET['search'].'%')
        ;

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Modulos';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['modulos'] = $this['pager']->fetchPage();
        $this['u'] = 'modulos/index';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Form to create or edit a Modulos. Example:
	 * GET /modulos/edit/1
	 *
	 * @return Modulos
	 */
	function editar($id = null) {
		$this->getModulos($id)->fromArray(@$_GET);
        $this['u'] = 'modulos/edit';
        $this->loadView("admin/index",$this);
	}

	/**
	 * Saves a Modulos. Examples:
	 * POST /modulos/save/1
	 * POST /rest/modulos/.json
	 * PUT /rest/modulos/1.json
	 */
	function guardar($id = null) {
		$modulos = $this->getModulos($id);

		try {
			$modulos->fromArray($_REQUEST);
			if ($modulos->validate()) {
				$modulos->save();
				$this->flash['messages'][] = 'Modulos saved';
				$this->redirect('modulos/index/');
			}
			$this->flash['errors'] = $modulos->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('modulos/edit/' . $modulos->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Modulos with the id. Examples:
	 * GET /modulos/show/1
	 * GET /rest/modulos/1.json
	 *
	 * @return Modulos
	 */
	function show($id = null) {
		return $this->getModulos($id);
	}

	/**
	 * Deletes the Modulos with the id. Examples:
	 * GET /modulos/delete/1
	 * DELETE /rest/modulos/1.json
	 */
	function delete($id = null) {
		$modulos = $this->getModulos($id);

		try {
			if (null !== $modulos && $modulos->delete()) {
				$this['errors'][] = 'Módulo eliminado';
			} else {
				$this['errors'][] = 'Modulos could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('modulos');
		}
	}

	/**
	 * @return Modulos
	 */
	private function getModulos($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Modulos::getPrimaryKey()])) {
			$id = $_REQUEST[Modulos::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Modulos
			$this['modulos'] = new Modulos;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['modulos'] = Modulos::retrieveByPK($id);
		}
		return $this['modulos'];
	}

}