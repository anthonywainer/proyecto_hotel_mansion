<?php

use Dabl\Query\QueryPager;

class TiposConceptoPagosController extends ApplicationController {

	/**
	 * Returns all TiposConceptoPago records matching the query. Examples:
	 * GET /tipos-concepto-pagos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/tipos-concepto-pagos.json&limit=5
	 *
	 * @return TiposConceptoPago[]
	 */
	function index() {
		$q = TiposConceptoPago::getQuery(@$_GET);

		// paginate
		$limit = empty($_REQUEST['limit']) ? 25 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'TiposConceptoPago';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		return $this['tipos_concepto_pagos'] = $this['pager']->fetchPage();
	}

	/**
	 * Form to create or edit a TiposConceptoPago. Example:
	 * GET /tipos-concepto-pagos/edit/1
	 *
	 * @return TiposConceptoPago
	 */
	function edit($id = null) {
		return $this->getTiposConceptoPago($id)->fromArray(@$_GET);
	}

	/**
	 * Saves a TiposConceptoPago. Examples:
	 * POST /tipos-concepto-pagos/save/1
	 * POST /rest/tipos-concepto-pagos/.json
	 * PUT /rest/tipos-concepto-pagos/1.json
	 */
	function save($id = null) {
		$tipos_concepto_pago = $this->getTiposConceptoPago($id);

		try {
			$tipos_concepto_pago->fromArray($_REQUEST);
			if ($tipos_concepto_pago->validate()) {
				$tipos_concepto_pago->save();
				$this->flash['messages'][] = 'Tipos Concepto Pago saved';
				$this->redirect('tipos-concepto-pagos/show/' . $tipos_concepto_pago->getId());
			}
			$this->flash['errors'] = $tipos_concepto_pago->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('tipos-concepto-pagos/edit/' . $tipos_concepto_pago->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the TiposConceptoPago with the id. Examples:
	 * GET /tipos-concepto-pagos/show/1
	 * GET /rest/tipos-concepto-pagos/1.json
	 *
	 * @return TiposConceptoPago
	 */
	function show($id = null) {
		return $this->getTiposConceptoPago($id);
	}

	/**
	 * Deletes the TiposConceptoPago with the id. Examples:
	 * GET /tipos-concepto-pagos/delete/1
	 * DELETE /rest/tipos-concepto-pagos/1.json
	 */
	function delete($id = null) {
		$tipos_concepto_pago = $this->getTiposConceptoPago($id);

		try {
			if (null !== $tipos_concepto_pago && $tipos_concepto_pago->delete()) {
				$this['messages'][] = 'Tipos Concepto Pago deleted';
			} else {
				$this['errors'][] = 'Tipos Concepto Pago could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('tipos-concepto-pagos');
		}
	}

	/**
	 * @return TiposConceptoPago
	 */
	private function getTiposConceptoPago($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[TiposConceptoPago::getPrimaryKey()])) {
			$id = $_REQUEST[TiposConceptoPago::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new TiposConceptoPago
			$this['tipos_concepto_pago'] = new TiposConceptoPago;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['tipos_concepto_pago'] = TiposConceptoPago::retrieveByPK($id);
		}
		return $this['tipos_concepto_pago'];
	}

}