<?php

use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;


class UsuariosController extends ApplicationController {

	/**
	 * Returns all Usuarios records matching the query. Examples:
	 * GET /usuarios?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/usuarios.json&limit=5
	 *
	 * @return Usuarios[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        $q = Usuarios::getQuery(@$_GET)
            ->andLike('usuarios.nombres','%'.$_GET['search'].'%')
            ->andNull('deleted_at')
        ;
		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Usuarios';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['usuarios'] = $this['pager']->fetchPage();
		$this['u'] = 'usuarios/index';
        $this->loadView("admin/index",$this);

	}

	/**
	 * Form to create or edit a Usuarios. Example:
	 * GET /usuarios/edit/1
	 *
	 * @return Usuarios
	 */
	function editar($id = null) {

        $grupo = Grupos::getAll();
		$this->getUsuarios($id)->fromArray(@$_GET);
        if ($_REQUEST['id']){
            $extra= "WHERE usuario_id = $id";
            $grupo2 = UsuariosGrupo::getAll($extra);
            $this['idgrupo'] =  $grupo2;
        }
        $this['u'] = 'usuarios/edit';
        $this['grupos'] = $grupo;
        $this->loadView("admin/index",$this);
	}


	function permisos($id=null){
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $this['u'] = 'usuarios/permisos';
            $this['modulos'] = Modulos::getAll();

            if ($id) {
                $ugr = UsuariosGrupo::getAll("where usuario_id= " . $id);

                $pg = PermisosGrupo::getAll("WHERE idgrupo= " . $ugr[0]->grupo_id);
                $pu = PermisosUsuario::getAll("WHERE idusuario= " . $id);
                $ng = [];
                foreach ($pg as $pe) {
                    array_push($ng, $pe->idpermiso);
                }
                $nu = [];
                foreach ($pu as $peu) {
                    array_push($nu, $peu->idpermiso);
                }

                $this['permisos_g'] = $ng;
                $this['permisos_u'] = $nu;
            }
            $this['idp']=$id;
            $this->loadView("admin/index", $this);
        }else{
            if ($id){
                $sql ="DELETE FROM permisos_usuario WHERE idusuario = $id";
                $result = PermisosUsuario::getConnection()->query($sql);
                $result->execute();
            }

            if (isset($_REQUEST['permisos'])){
                foreach ($_REQUEST['permisos'] as $p){
                    $usuarios_permiso = new PermisosUsuario();
                    $arr = [];
                    $arr['idpermiso'] = $p;
                    $arr['idusuario'] = $id;
                    $usuarios_permiso->fromArray($arr);
                    $usuarios_permiso->save();
                }
            }
            $this->flash['messages'][] = 'Permisos Guardados';


            $this->redirect('usuarios/index');
        }
    }


	/**
	 * Saves a Usuarios. Examples:
	 * POST /usuarios/save/1
	 * POST /rest/usuarios/.json
	 * PUT /rest/usuarios/1.json
	 */

	function guardar($id = null) {
		$usuarios = $this->getUsuarios($id);
        $usuarios_grupo = new UsuariosGrupo;
        if ($_REQUEST['id']){
            $idg = $_REQUEST['id'];
            $sql ="DELETE FROM usuarios_grupo WHERE usuarios_grupo.usuario_id = $idg";
            $result = UsuariosGrupo::getConnection()->query($sql);
            $result->execute();
        }
		try {
            /*$fecha_actual = date("Y-m-d");
            $_REQUEST["created_at"] = $fecha_actual;
			$usuarios->fromArray($_REQUEST);*/
            $fecha_actual = date("Y-m-d");

            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }
            $usuarios->fromArray($_REQUEST);

            if (!$_REQUEST['id']) {
                $_REQUEST["created_at"] = $fecha_actual;
            }else {
                $_REQUEST["updated_at"] = $fecha_actual;
            }


			if ($usuarios->validate()) {
				$usuarios->save();

                $arr = [];
                $arr['grupo_id'] = $_REQUEST['grupos'];
                $arr['usuario_id'] = $usuarios->getId(); #ultimo id ingresado

                $usuarios_grupo->fromArray($arr);
                $usuarios_grupo->save();


				$this->flash['messages'][] = 'Usuario ';


				$this->redirect('usuarios/index');
			}
			$this->flash['errors'] = $usuarios->getValidationErrors();
		} catch (Exception $e) {
		    	$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('usuarios/edit/' . $usuarios->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Usuarios with the id. Examples:
	 * GET /usuarios/show/1
	 * GET /rest/usuarios/1.json
	 *
	 * @return Usuarios
	 */
	function show($id = null) {
		return $this->getUsuarios($id);
	}

	/**
	 * Deletes the Usuarios with the id. Examples:
	 * GET /usuarios/delete/1
	 * DELETE /rest/usuarios/1.json
	 */
	function eliminar($id = null) {
		$usuarios = $this->getUsuarios($id);
        $fecha_actual = date("Y-m-d");
        $_REQUEST["deleted_at"] = $fecha_actual;
        $usuarios->fromArray($_REQUEST);

		try {
			if (null !== $usuarios && $usuarios->save()) {
				$this['errors'][] = 'Usuario ';
			} else {
				$this['errors'][] = 'Usuarios could not be deleted';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('usuarios');
		}
	}

	/**
	 * @return Usuarios
	 */
	private function getUsuarios($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Usuarios::getPrimaryKey()])) {
			$id = $_REQUEST[Usuarios::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Usuarios
			$this['usuarios'] = new Usuarios;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['usuarios'] = Usuarios::retrieveByPK($id);
		}
		return $this['usuarios'];
	}



}